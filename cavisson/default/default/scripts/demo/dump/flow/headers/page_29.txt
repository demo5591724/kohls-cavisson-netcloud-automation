--Request 
POST https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C16
Host: api-bd.kohls.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Accept: application/json
Content-Type: application/json
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Origin: https://www.kohls.com
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
ede-bundle_params: {\"channelId\":\"WebStore\",\"pageId\":\"Home\",\"executedInfoList\":[{\"placementId\":\"RedesignHP1\",\"departmentName\":\"\",\"category\":\"\",\"subCategory\":\"\",\"bundleId\":\"1353\",\"placementRevision\":\"1688147959039\",\"bundleRevision\":\"1697829319058\",\"kiraRevision\":\"1673978256750\",\"uuid\":\"69260bc8-b5c8-4635-9f3f-b90db5da94d5\",\"infoMeta\":{\"type\":\"FlatRec\"},\"infoPayload\":{\"executedAlgorithmsInfo\":{\"executedAlgorithmInfoList\":[{\"algorithmId\":\"314\",\"ccpParametersUsed\":[],\"isPersonalized\":false}],\"contextBasedOptimized\":false,\"autoOptimized\":false,\"optimizedMetric\":\"None\"},\"executedRulesInfo\":{\"executedRuleInfoList\":[],\"backfillDisabledFromRule\":false},\"isAb\":false,\"abTestId\":\"\",\"currentAbVariationId\":\"\",\"recGenerationCycleStatus\":{\"recGenerationCycleMask\":71468255805568,\"requestId\":\"e3568fc8-88e0-420e-8dc7-8093a6627328\"}}}]}
access-control-allow-origin: *
access-control-expose-headers: EDE-AB_VAR_PARAMS, EDE-BUNDLE_PARAMS, Content-Type
content-type: application/json
vary: Accept-Encoding
content-encoding: gzip
date: Tue, 21 Nov 2023 07:08:43 GMT
content-length: 2390
set-cookie: 019846f7bdaacd7a765789e7946e59ec=fc7d6f83d7ff0348a0c892bd8b321a74; path=/; HttpOnly; Secure; SameSite=None
set-cookie: akacd_EDE_GCP=2177452799~rv=56~id=c6dbbd69ef83efe31201e383d9008a62; path=/; Expires=Mon, 31 Dec 2038 23:59:59 GMT; Secure; SameSite=None
set-cookie: ak_bmsc=1733F3B387FEF71C136DA864ECB0EF01~000000000000000000000000000000~YAAQxikhF1y5/NOLAQAAZbu18BUEapEFxCKT28OqpTT2ZZmqN9ndT+VFROKble6H9DSuef1FLZyZJGgWbTNqHUNHapDOC8N1j77+7dHrnbqpfgZaDIJz8LzZks9dAREuu1aujRpOVaMY6ujX2Fq5jdPZHnK3kKKBufrKBbwbnmVDE08lTaeaB2zYvCfpPxkaEnVFLn6QDMCiymWeJl3fhA8uWc9dCfAXQRMNgkPGG2t25U5BBgInoFlNVKey1kXIOu/CJH1AxsXUxeS+z0zEWfSYxGzjDbP6Fj/JdK/Ei2nQaIDUbAWeo5RcivsHS4JLJcAUSwSPHoGiujGMOBs1d5mHULDAtrVqHCju5wrL5Qo8PxOiuQup3ggrEFOlsrSemOK8BG/yNw==; Domain=.kohls.com; Path=/; Expires=Tue, 21 Nov 2023 09:08:43 GMT; Max-Age=7200; Secure
fwdhst: mkt-ede-prd.apps-ext.gcpusc1.prd.xpaas.kohls.com
akamai-request-id2: 23.33.41.198:483ced8f
strict-transport-security: max-age=15768000 ; includeSubDomains ; preload
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=970509&uid=-2796735126378358917&sec=8776374&t=ri&e=1127582&p=1&ve=10281141&va=%5B26223332%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873482958924880&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523672&rri=3974421
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: dMBWew08Bl85ckjAnhuF9agkSQ82w2v80-irMrqHgjsh_xZFqDYjVw==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=259644&uid=-2796735126378358917&sec=8776374&t=ri&e=1179251&p=1&ve=11202288&va=%5B26647761%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873483984187660&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523676&rri=3203149
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: B78HnLtYhg8Sv19ZeEL-3iY6KQxlQ_ZC2bmHes3FPQItY-aBk4nY9Q==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=298375&uid=-2796735126378358917&sec=8776374&t=ri&e=1289933&p=1&ve=11374557&va=%5B27236876%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873482114156325&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523678&rri=1450082
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: mdUSlwrEYe88_6IUWSJCFEmnzf_gl_ibVdKskX8GpQ2JgJYEvHgP6A==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=431730&uid=-2796735126378358917&sec=8776374&t=ri&e=1309946&p=1&ve=11951163&va=%5B27308034%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873483464965333&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523680&rri=3636839
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: 26Rix_TFZjoealjme_yqiRfhvnibUSE9ZR05mEPLIvyWdZNmZPkHKw==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=901335&uid=-2796735126378358917&sec=8776374&t=ri&e=1343143&p=1&ve=11836075&va=%5B27429945%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873484275145109&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523681&rri=5028584
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: iccTG4cumvKd6LpOzXVWOqxAKDfBPJnA5k5GH8tKBWjcoAgQw5QYbQ==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=16426&uid=-2796735126378358917&sec=8776374&t=ri&e=1364209&p=1&ve=11678654&va=%5B27506569%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873483916093229&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523684&rri=3384916
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: jGyKiiT6FXYCtnZStyhBlp27RQB-UdZ0Hro5J8YBl9lGbai4BrRPfg==
----
--Request 
GET https://cdn.dynamicyield.com/api/8776374/images/2cd0d42621000__chevron-left.png
Host: cdn.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/png
content-length: 709
date: Thu, 16 Nov 2023 08:19:34 GMT
last-modified: Wed, 14 Sep 2022 16:09:23 GMT
etag: \"090018169663446a612e50a83c111ef7\"
cache-control: max-age=604800
accept-ranges: bytes
server: DYCDN
via: 1.1 ea4a33625617615e13496b292edda6d6.cloudfront.net (CloudFront)
age: 427750
content-security-policy: default-src 'none'; img-src 'self'; script-src 'none'; style-src 'self'; media-src 'self'; object-src 'none'; sandbox;
link: <//st.dynamicyield.com>; rel=\"dns-prefetch\", <//st.dynamicyield.com>; rel=\"preconnect\", <//rcom.dynamicyield.com>; rel=\"dns-prefetch\", <//rcom.dynamicyield.com>; rel=\"preconnect\", <//async-px.dynamicyield.com>; rel=\"dns-prefetch\", <//async-px.dynamicyield.com>; rel=\"preconnect\"
x-cache: Hit from cloudfront
x-amz-cf-pop: CMH68-P1
x-amz-cf-id: FIdbXWLNKzzs-zMDhuYcwqEIuJ1qPH_WxEiPWxAg_AmsclD1B4qn5g==
----
--Request 
GET https://cdn.dynamicyield.com/api/8776374/images/77232a5ffd1__chevron-right.png
Host: cdn.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/png
content-length: 730
date: Thu, 16 Nov 2023 08:12:50 GMT
last-modified: Wed, 14 Sep 2022 16:09:27 GMT
etag: \"425ba69852ade7366960e4544b2931ac\"
cache-control: max-age=604800
accept-ranges: bytes
server: DYCDN
via: 1.1 ea4a33625617615e13496b292edda6d6.cloudfront.net (CloudFront)
age: 428154
content-security-policy: default-src 'none'; img-src 'self'; script-src 'none'; style-src 'self'; media-src 'self'; object-src 'none'; sandbox;
link: <//st.dynamicyield.com>; rel=\"dns-prefetch\", <//st.dynamicyield.com>; rel=\"preconnect\", <//rcom.dynamicyield.com>; rel=\"dns-prefetch\", <//rcom.dynamicyield.com>; rel=\"preconnect\", <//async-px.dynamicyield.com>; rel=\"dns-prefetch\", <//async-px.dynamicyield.com>; rel=\"preconnect\"
x-cache: Hit from cloudfront
x-amz-cf-pop: CMH68-P1
x-amz-cf-id: mzCi-vXcDyLY-MC14wkq3rh13dj3x-fqr7EUmqB1peti_iHZpCCt1A==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=406906&uid=-2796735126378358917&sec=8776374&t=ri&e=1494574&p=1&ve=12255008&va=%5B27939630%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873481999335558&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523730&rri=7010250
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: YYWV72jTRuiZLWsuqcHn3vBHW6bMuF30t_so_HPDJyRvWHHGXj4SFw==
----
--Request 
GET https://async-px.dynamicyield.com/var?cnst=1&_=659291&uid=-2796735126378358917&sec=8776374&t=ri&e=1499924&p=1&ve=12255037&va=%5B27954911%5D&ses=5d0e8cd2dde4c6e754f17773f6a628da&expSes=84609&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&expVisitId=3724873482250470029&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1700550523732&rri=6721451
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: wPoiTxJqh_i4iZAgx0qPX5Jx_FzZWMe4RWnHvclHQRGjk_rem-9Y9w==
----
--Request 
GET https://async-px.dynamicyield.com/dpx?cnst=1&_=304418&name=User%20Session&props=undefined&uid=-2796735126378358917&sec=8776374&cl=dk.l.c.ms.&ses=5d0e8cd2dde4c6e754f17773f6a628da&l=def&p=1&sd=&rf=&trf=0&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&url=https%3A%2F%2Fwww.kohls.com%2F&exps=%5B%5B%221067036%22%2C%229863190%22%2C%2225792075%22%2C0%2Cnull%2Cnull%2C%223724873480774415658%22%2C%222%22%2C%223%22%5D%2C%5B%221096558%22%2C%2210092862%22%2C%2226047526%22%2C0%2Cnull%2Cnull%2C%223724873483239044703%22%2C%221%22%2Cnull%5D%2C%5B%221127582%22%2C%2210281141%22%2C%2226223332%22%2C0%2Cnull%2Cnull%2C%223724873482958924880%22%2C%221%22%2Cnull%5D%2C%5B%221179251%22%2C%2211202288%22%2C%2226647761%22%2C0%2Cnull%2Cnull%2C%223724873483984187660%22%2C%221%22%2Cnull%5D%2C%5B%221289933%22%2C%2211374557%22%2C%2227236876%22%2C0%2Cnull%2Cnull%2C%223724873482114156325%22%2C%221%22%2Cnull%5D%2C%5B%221309946%22%2C%2211951163%22%2C%2227308034%22%2C0%2Cnull%2Cnull%2C%223724873483464965333%22%2C%221%22%2Cnull%5D%2C%5B%221343143%22%2C%2211836075%22%2C%2227429945%22%2C0%2Cnull%2Cnull%2C%223724873484275145109%22%2C%221%22%2Cnull%5D%2C%5B%221364209%22%2C%2211678654%22%2C%2227506569%22%2C0%2Cnull%2Cnull%2C%223724873483916093229%22%2C%221%22%2Cnull%5D%2C%5B%221494574%22%2C%2212255008%22%2C%2227939630%22%2C0%2Cnull%2Cnull%2C%223724873481999335558%22%2C%221%22%2Cnull%5D%2C%5B%221499924%22%2C%2212255037%22%2C%2227954911%22%2C0%2Cnull%2Cnull%2C%223724873482250470029%22%2C%221%22%2Cnull%5D%2C%5B%221127311%22%2C%2210280236%22%2C%2226221758%22%2C0%2Cnull%2Cnull%2C%223724873484361296749%22%2C%221%22%2Cnull%5D%2C%5B%221185440%22%2C%2211002655%22%2C%2227022616%22%2C0%2Cnull%2Cnull%2C%223724873484789745617%22%2C%221%22%2Cnull%5D%2C%5B%221597920%22%2C%2212623934%22%2C%2228281649%22%2C0%2Cnull%2Cnull%2C%223724873482246920778%22%2C%221%22%2Cnull%5D%2C%5B%221578901%22%2C%2212550437%22%2C%2228217081%22%2C0%2Cnull%2Cnull%2C%223724873483909636835%22%2C%221%22%2Cnull%5D%5D&expSes=84609&tsrc=Direct&reqts=1700550523743&rri=9535805&geoData=US_MI_Detroit
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: TMcs54kBA0oS2M80PPwhglCODZLkExO-JjTMBNMp039pwduzgt4KbQ==
----
--Request 
GET https://async-px.dynamicyield.com/dpx?cnst=1&_=110685&name=New_Holiday_2023_User&props=%7B%7D&uid=-2796735126378358917&sec=8776374&cl=dk.l.c.ms.&ses=5d0e8cd2dde4c6e754f17773f6a628da&l=def&p=1&sd=&rf=&trf=0&aud=1408117.1438654.1476022.1899869.1910576.2028982.2099082.1362540.1362542.1468187.1951645.2013139&url=https%3A%2F%2Fwww.kohls.com%2F&exps=%5B%5B%221067036%22%2C%229863190%22%2C%2225792075%22%2C0%2Cnull%2Cnull%2C%223724873480774415658%22%2C%222%22%2C%223%22%5D%2C%5B%221096558%22%2C%2210092862%22%2C%2226047526%22%2C0%2Cnull%2Cnull%2C%223724873483239044703%22%2C%221%22%2Cnull%5D%2C%5B%221127582%22%2C%2210281141%22%2C%2226223332%22%2C0%2Cnull%2Cnull%2C%223724873482958924880%22%2C%221%22%2Cnull%5D%2C%5B%221179251%22%2C%2211202288%22%2C%2226647761%22%2C0%2Cnull%2Cnull%2C%223724873483984187660%22%2C%221%22%2Cnull%5D%2C%5B%221289933%22%2C%2211374557%22%2C%2227236876%22%2C0%2Cnull%2Cnull%2C%223724873482114156325%22%2C%221%22%2Cnull%5D%2C%5B%221309946%22%2C%2211951163%22%2C%2227308034%22%2C0%2Cnull%2Cnull%2C%223724873483464965333%22%2C%221%22%2Cnull%5D%2C%5B%221343143%22%2C%2211836075%22%2C%2227429945%22%2C0%2Cnull%2Cnull%2C%223724873484275145109%22%2C%221%22%2Cnull%5D%2C%5B%221364209%22%2C%2211678654%22%2C%2227506569%22%2C0%2Cnull%2Cnull%2C%223724873483916093229%22%2C%221%22%2Cnull%5D%2C%5B%221494574%22%2C%2212255008%22%2C%2227939630%22%2C0%2Cnull%2Cnull%2C%223724873481999335558%22%2C%221%22%2Cnull%5D%2C%5B%221499924%22%2C%2212255037%22%2C%2227954911%22%2C0%2Cnull%2Cnull%2C%223724873482250470029%22%2C%221%22%2Cnull%5D%2C%5B%221127311%22%2C%2210280236%22%2C%2226221758%22%2C0%2Cnull%2Cnull%2C%223724873484361296749%22%2C%221%22%2Cnull%5D%2C%5B%221185440%22%2C%2211002655%22%2C%2227022616%22%2C0%2Cnull%2Cnull%2C%223724873484789745617%22%2C%221%22%2Cnull%5D%2C%5B%221597920%22%2C%2212623934%22%2C%2228281649%22%2C0%2Cnull%2Cnull%2C%223724873482246920778%22%2C%221%22%2Cnull%5D%2C%5B%221578901%22%2C%2212550437%22%2C%2228217081%22%2C0%2Cnull%2Cnull%2C%223724873483909636835%22%2C%221%22%2Cnull%5D%5D&expSes=84609&tsrc=Direct&reqts=1700550523747&rri=1899579&geoData=US_MI_Detroit
Host: async-px.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-length: 0
date: Tue, 21 Nov 2023 07:08:43 GMT
access-control-allow-origin: *
access-control-allow-methods: POST, GET, OPTIONS
access-control-allow-headers: Content-Type, Authorization, Content-Length, X-Requested-With
cache-control: no-cache, no-store, must-revalidate
pragma: no-cache
expires: 0
x-cache: Miss from cloudfront
via: 1.1 03093c003b20d410ed3ec3e4bb2d569c.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: HG3C5A9XeddkUPfjm5sYgRbA_YvRFJbLZ49PvsDON_LibrfofIAP2Q==
----
--Request 
GET https://ib.adnxs.com/getuid?https://c.bing.com/c.gif?cid=$UID&vid=ce3cb6c0883c11ee9c5d21bc71586f02&Red3=RMT_AN_4024145&rn=384311
Host: ib.adnxs.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 307
server: nginx/1.21.3
date: Tue, 21 Nov 2023 07:08:43 GMT
content-type: text/html; charset=utf-8
content-length: 0
cache-control: no-store, no-cache, private
pragma: no-cache
expires: Sat, 15 Nov 2008 16:00:00 GMT
p3p: policyref=\"http://cdn.adnxs-simple.com/w3c/policy/p3p.xml\", CP=\"NOI DSP COR ADM PSAo PSDo OURo SAMo UNRo OTRo BUS COM NAV DEM STA PRE\"
x-xss-protection: 0
access-control-allow-credentials: true
access-control-allow-origin: *
accept-ch: Sec-CH-UA-Full-Version-List,Sec-CH-UA-Arch,Sec-CH-UA-Model,Sec-CH-UA-Platform-Version,Sec-CH-UA-Bitness
location: https://ib.adnxs.com/bounce?%2Fgetuid%3Fhttps%3A%2F%2Fc.bing.com%2Fc.gif%3Fcid%3D%24UID%26vid%3Dce3cb6c0883c11ee9c5d21bc71586f02%26Red3%3DRMT_AN_4024145%26rn%3D384311
an-x-request-uuid: bb24e79a-9ca7-4c42-98e4-f15e1ff50b1e
set-cookie: uuid2=6153098989108340742; SameSite=None; Path=/; Max-Age=7776000; Expires=Mon, 19-Feb-2024 07:08:43 GMT; Domain=.adnxs.com; Secure; HttpOnly
x-proxy-origin: 192.3.3.250; 192.3.3.250; 582.bm-nginx-loadbalancer.mgmt.nym2.adnexus.net; adnxs.com
----
--Request 
GET https://c.tagdelivery.com/c.gif?from=uet&rn=381251
Host: c.tagdelivery.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 302
cache-control: private, no-cache, proxy-revalidate, no-store
pragma: no-cache
location: https://c.bing.com/c.gif?from=uet&rn=381251&ctsa=mr&CtsSyncId=7AA3F89E5C3C4F078357BAFD0A64A2D0&RedC=c.tagdelivery.com&MXFR=23C6DC66B6D969FC3C59CFB6B2D97928
server: Microsoft-IIS/10.0
x-powered-by: ASP.NET
p3p: CP=\"BUS CUR CONo FIN IVDo ONL OUR PHY SAMo TELo\"
set-cookie: SM=T; domain=c.tagdelivery.com; path=/; SameSite=None; Secure;
set-cookie: MUID=23C6DC66B6D969FC3C59CFB6B2D97928; domain=.tagdelivery.com; expires=Sun, 15-Dec-2024 07:08:43 GMT; path=/; SameSite=None; Secure; Priority=High;
date: Tue, 21 Nov 2023 07:08:43 GMT
content-length: 0
----

