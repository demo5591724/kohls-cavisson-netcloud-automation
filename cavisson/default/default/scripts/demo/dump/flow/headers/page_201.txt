--Request 
POST https://jsa-khls.domdog.io/report-uri/a9a6fb14-365a-4648-b17b-2e47930f8b49/1/pp/2.0
Host: jsa-khls.domdog.io
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204
date: Tue, 21 Nov 2023 07:09:28 GMT
cf-cache-status: DYNAMIC
report-to: {\"endpoints\":[{\"url\":\"https:\\/\\/a.nel.cloudflare.com\\/report\\/v3?s=CoMduTkkYfVdYlCOC0Z9v6zLfm7aTuaZ3PNbCNEOO6p0zMWNruAI731GLZh65vzJ1J12tdZliGHb0DzjNaOebriYc47xRNAWNz8oG2CaBR5S1VE3mfY32LtAfdg8E7Y7JHz965Q%3D\"}],\"group\":\"cf-nel\",\"max_age\":604800}
nel: {\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}
server: cloudflare
cf-ray: 82971b7c9c2ba1fe-YYZ
alt-svc: h3=\":443\"; ma=86400
----
--Request 
GET https://js-cdn.dynatrace.com/jstag/1740040f04f/bf27853irn/cf6a6b14e494e621_complete.js
Host: js-cdn.dynatrace.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Origin: https://www.kohls.com
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript;charset=utf-8
date: Tue, 21 Nov 2023 06:35:33 GMT
timing-allow-origin: *
x-oneagent-js-injection: true
traffic-source: UNKNOWN
dynatrace-response-source: Cluster
dynatrace-response-id: A28ZNWE8OXNU
expires: Tue, 21 Nov 2023 07:35:33 GMT
cache-control: public, max-age=3600
access-control-allow-origin: *
content-encoding: gzip
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 71fd64ca8017d30cdbfc030bfad84ca8.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P3
x-amz-cf-id: lDDKCFSckSfCg2xZE3xgL-3Lwga3U-Hl3zoqO9wlbp6g-itDr-V_vw==
age: 1988
----
--Request 
GET https://cdn.cookielaw.org/scripttemplates/202306.1.0/otBannerSdk.js
Host: cdn.cookielaw.org
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:08:41 GMT
content-type: application/javascript
content-length: 99599
content-encoding: gzip
content-md5: XJk1ZZTljtwHFT3qcIJg+w==
last-modified: Wed, 12 Jul 2023 06:29:36 GMT
etag: 0x8DB82A15D413626
x-ms-request-id: 7a75efb1-601e-0081-6c94-b47ab1000000
x-ms-version: 2009-09-19
x-ms-lease-status: unlocked
x-ms-blob-type: BlockBlob
access-control-expose-headers: x-ms-request-id,Server,x-ms-version,Content-Type,Content-Encoding,Last-Modified,ETag,Content-MD5,x-ms-lease-status,x-ms-blob-type,Content-Length,Date,Transfer-Encoding
access-control-allow-origin: *
cache-control: max-age=86400
cf-cache-status: HIT
age: 33685
accept-ranges: bytes
vary: Accept-Encoding
x-content-type-options: nosniff
server: cloudflare
cf-ray: 82971a595c4937d0-YYZ
----
--Request 
GET https://designsystem.kohls.com/web-core/3.25.0/p-678e26a0.js
Host: designsystem.kohls.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Origin: https://www.kohls.com
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: script
Referer: https://designsystem.kohls.com/web-core/3.25.0/kds-web-core.esm.js
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/x-javascript
accept-ranges: bytes
content-encoding: br
etag: \"7ebbc56bdb4393e51b9387b4c966b4ff:1657747669.277465\"
last-modified: Fri, 10 Nov 2023 00:21:48 GMT
vary: Accept-Encoding
content-length: 5569
cache-control: max-age=174258
date: Tue, 21 Nov 2023 07:09:28 GMT
access-control-allow-methods: GET, OPTIONS
access-control-allow-headers: *
access-control-allow-origin: *
----
--Request 
GET https://cdn.evgnet.com/beacon/kohlsinc2/prod/scripts/evergage.min.js
Host: cdn.evgnet.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:28 GMT
via: 1.1 varnish
cache-control: max-age=120
age: 9
x-served-by: cache-ewr18125-EWR
x-cache: HIT
x-cache-hits: 1
x-timer: S1700550569.556472,VS0,VE1
vary: Accept-Encoding
timing-allow-origin: *
x-amz-id-2: oMtYjc5hVwQYphL+J5F6Twp4rpbkfroT5/2mhb90l56XgJ/NZYl5O9I/lKzpnjniQ7gxRa2VgQgFFyMqQ97SF+n64gVPs2NG05GSaAwLjFg=
x-amz-request-id: XVK4VSJP9AG7WV0J
x-amz-replication-status: COMPLETED
last-modified: Wed, 01 Nov 2023 22:19:28 GMT
etag: \"e3ff8c68eefdf92479ca9188a58eb13b\"
x-amz-server-side-encryption: AES256
content-encoding: gzip
x-amz-meta-evergage-beacon-ver: 16
x-amz-meta-evergage-sum: dbb5e52c8bf18e3110430860b1da7ea07de42196
x-amz-version-id: tbo2hxwgE8TV7KbNGilW8sczDuHk_qNq
content-type: application/javascript; charset=utf-8
server: AmazonS3
accept-ranges: bytes
content-length: 46203
----

