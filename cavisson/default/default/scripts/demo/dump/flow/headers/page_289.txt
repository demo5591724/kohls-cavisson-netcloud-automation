--Request 
POST https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=Horizontal1FilteredViewViewTest%7C15
Host: api-bd.kohls.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Accept: application/json, text/javascript, */*; q=0.01
Content-Type: application/json
X-APP-API_KEY: NQeOQ7owHHPkdkMkKuH5tPpGu0AvIIOu
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Origin: https://www.kohls.com
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
ede-bundle_params: {\"channelId\":\"WebStore\",\"pageId\":\"PDP\",\"executedInfoList\":[{\"placementId\":\"Horizontal1FilteredViewViewTest\",\"departmentName\":\"\",\"category\":\"\",\"subCategory\":\"\",\"bundleId\":\"1482\",\"placementRevision\":\"1692112054775\",\"bundleRevision\":\"1693250158562\",\"kiraRevision\":\"1692050793065\",\"uuid\":\"aba97459-121d-4712-8303-f4ed4cafb09e\",\"infoMeta\":{\"type\":\"FlatRec\"},\"infoPayload\":{\"executedAlgorithmsInfo\":{\"executedAlgorithmInfoList\":[{\"algorithmId\":\"401\",\"ccpParametersUsed\":[\"productNumbers\"],\"isPersonalized\":false}],\"contextBasedOptimized\":false,\"autoOptimized\":false,\"optimizedMetric\":\"None\"},\"executedRulesInfo\":{\"executedRuleInfoList\":[{\"ruleId\":\"1\",\"ruleType\":\"BURY\"}],\"backfillDisabledFromRule\":false},\"isAb\":false,\"abTestId\":\"\",\"currentAbVariationId\":\"\",\"recGenerationCycleStatus\":{\"recGenerationCycleMask\":71468390023232,\"requestId\":\"e220deb5-3eb8-4db5-95ce-436eaea95873\"}}}]}
access-control-allow-origin: *
access-control-expose-headers: EDE-AB_VAR_PARAMS, EDE-BUNDLE_PARAMS, Content-Type
content-type: application/json
vary: Accept-Encoding
content-encoding: gzip
date: Tue, 21 Nov 2023 07:09:40 GMT
content-length: 2037
set-cookie: 019846f7bdaacd7a765789e7946e59ec=96ce8954f66f12076f2b8e7249dbfdaf; path=/; HttpOnly; Secure; SameSite=None
set-cookie: akacd_EDE_GCP=2177452799~rv=15~id=80f3bf0915d719de106ed0522dd38882; path=/; Expires=Mon, 31 Dec 2038 23:59:59 GMT; Secure; SameSite=None
set-cookie: ak_bmsc=D8C16D79BC77330A803033C3C8D1927F~000000000000000000000000000000~YAAQxikhFwPB/NOLAQAA2pe28BVXo/rKOOihH3rTMcxHPD3TUsToqzjKMUaKgzmHXTCIlXbXB3tM7+Yd1+EVxLMvxhIONBDodT2/d8/mzjNsG9LT/2B001YK9wCpFF3r0edeBA/dImnAEs6z46pgFSLyAdTsi1eM78/Qw6M6dglDqb/FCoRzhAc6yFf8jlMcS3BngSnorkfT/f/fpUlnK6W7TjShgA8qBuiiCv8cvTMv8azCPcHza/N1sTe+xeALaBD/imog3x0tcsJ3M9x4dgLNsS5LBhlMc4V9mu2l5Y1pLV0Dc71cpbuzdS+GRs6yq9jmBB5e0UesUsPhtTlDjyrkGorOMrwvhUOah1fVLMReIl/C7NwY+eBVUN90iIqm1J/WptLpLg==; Domain=.kohls.com; Path=/; Expires=Tue, 21 Nov 2023 09:09:40 GMT; Max-Age=7200; Secure
fwdhst: mkt-ede-prd.apps-ext.gcpusc1.prd.xpaas.kohls.com
akamai-request-id2: 23.33.41.198:483d5d9e
strict-transport-security: max-age=15768000 ; includeSubDomains ; preload
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/6229706_Black?wid=300&hei=300&op_sharpen=1
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
access-control-allow-origin: *
last-modified: Wed, 27 Sep 2023 04:43:54 GMT
etag: \"fb3aa6338319ee002e59d7ef9d3124d1\"
-x-adobe-smart-imaging: 4554
timing-allow-origin: *
-x-adobe-assetlist: [kohls/6229706_Black]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
content-type: image/webp
content-length: 7486
expires: Sat, 25 Nov 2023 07:07:01 GMT
date: Tue, 21 Nov 2023 07:09:40 GMT
akamai-grn: 0.2e10de17.1700550580.4080915
----

