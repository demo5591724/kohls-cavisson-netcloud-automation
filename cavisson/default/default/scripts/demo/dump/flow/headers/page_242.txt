--Request 
POST https://datastream.stylitics.com/api/engagements
Host: datastream.stylitics.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/json
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:33 GMT
content-type: text/html
access-control-allow-origin: *
access-control-allow-methods: GET, PUT, PATCH, POST, DELETE, OPTIONS
access-control-allow-headers: Origin, User-Agent, Authorization, Content-Type, Accept, Accept-Encoding, Accept-Language
x-cloud-trace-context: c89deb13fde46c9af2e0f9c0e37247f6
cf-cache-status: DYNAMIC
server: cloudflare
cf-ray: 82971b9bfccf36a5-YYZ
content-encoding: gzip
----

