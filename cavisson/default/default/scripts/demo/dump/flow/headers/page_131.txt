--Request 
POST https://pixel.fohr.co/track
Host: pixel.fohr.co
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:15 GMT
access-control-allow-origin: https://www.kohls.com
vary: Origin
content-security-policy: frame-ancestors https://*.fohr.co;
x-frame-options: deny
x-xss-protection: 1; mode=block
x-content-type-options: nosniff
referrer-policy: same-origin
permissions-policy: accelerometer=(), autoplay=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), fullscreen=(), geolocation=(self), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), xr-spatial-tracking=()
strict-transport-security: max-age=31536000; includeSubDomains
cf-cache-status: DYNAMIC
report-to: {\"endpoints\":[{\"url\":\"https:\\/\\/a.nel.cloudflare.com\\/report\\/v3?s=WtlZPt9llKD0%2Bc8%2B2WlEIJf6Q6zImUu9Xsk7Go%2B4UnX1%2FW7iDRCjnk0DwJMxKG0%2FCZomG9VeN73yytKdzAB4neoxQ52o7HYZFWDBoGYzv54j208b4fEBrvODIzQAxi8%3D\"}],\"group\":\"cf-nel\",\"max_age\":604800}
nel: {\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}
server: cloudflare
cf-ray: 82971b2e7f8836fe-YYZ
alt-svc: h3=\":443\"; ma=86400
----
--Request 
GET https://tjxbfc1n.micpn.com/p/js/1.js
Host: tjxbfc1n.micpn.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/javascript
x-uuid: 48a65c01-2457-4413-a37f-40a7f73ae834
cache-control: no-cache max-age=0
expires: Thu, 01 Dec 1994 16:00:00 GMT
pragma: no-cache
p3p: policyref=\"https://movableink.com/w3c/p3p.xml\", CP=\"DEVa PSAa PSDa IVAa IVDa OUR IND DSP NON COR NAV UNI\"
timing-allow-origin: https://www.kohls.com
date: Tue, 21 Nov 2023 07:05:35 GMT
content-encoding: gzip
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 8558d1ba2a2dab6b2b795204a93d7f80.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P5
x-amz-cf-id: dNKaa1FRW5zMfJxXfTBCTciMrlt6kKReUSwraBncZky7LB8-6aEx_Q==
age: 220
----
--Request 
GET https://cdnssl.clicktale.net/www47/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js
Host: cdnssl.clicktale.net
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Origin: https://www.kohls.com
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:15 GMT
x-amz-server-side-encryption: AES256
cache-control: max-age=900
x-amz-version-id: FgyQSlbxoN1Raj4lpTrKrSYGkzyBuz32
server: AmazonS3
x-cache: Hit from cloudfront
via: 1.1 192ddb149ecb1751bd671d09f051050a.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P5
x-amz-cf-id: c4_cf9isevD9BInIYWHcFJSN6r3cR7PuvkhtOQ_i3BwbxRy_4Iqy5A==
age: 0
timing-allow-origin: *
access-control-allow-origin: *
content-type: application/javascript;charset=utf-8
content-length: 77316
last-modified: Sat, 18 Nov 2023 13:38:16 GMT
etag: \"575b4b74d564ec06f0d19ac52f6311dd\"
content-encoding: br
accept-ranges: bytes
----
--Request 
GET https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?value=0&guid=ON&script=0
Host: googleads.g.doubleclick.net
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 302
p3p: policyref=\"https://googleads.g.doubleclick.net/pagead/gcn_p3p_.xml\", CP=\"CURa ADMa DEVa TAIo PSAo PSDo OUR IND UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR\"
timing-allow-origin: *
cross-origin-resource-policy: cross-origin
date: Tue, 21 Nov 2023 07:09:15 GMT
pragma: no-cache
expires: Fri, 01 Jan 1990 00:00:00 GMT
cache-control: no-cache, must-revalidate
location: https://www.google.com/pagead/1p-user-list/1071871169/?value=0&guid=ON&script=0&is_vtc=1&cid=CAQSGwDICaaNjxl7s-47Po2uPux6LEqG02Ij-qci0w&random=3251572556
content-type: image/gif
x-content-type-options: nosniff
server: cafe
content-length: 42
x-xss-protection: 0
set-cookie: test_cookie=CheckForPermission; expires=Tue, 21-Nov-2023 07:24:15 GMT; path=/; domain=.doubleclick.net; Secure; SameSite=none
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
----
--Request 
GET https://bat.bing.com/p/action/4024145.js
Host: bat.bing.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
cache-control: private,max-age=1800
content-type: application/javascript; charset=utf-8
content-encoding: br
vary: Accept-Encoding
x-cache: CONFIG_NOCACHE
accept-ch: Sec-CH-UA-Arch, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version, Sec-CH-UA-Full-Version-List, Sec-CH-UA-Mobile, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version
x-msedge-ref: Ref A: 476DB94DF9AC4E9A8C991067CA036E4D Ref B: EWR311000103027 Ref C: 2023-11-21T07:08:43Z
date: Tue, 21 Nov 2023 07:08:43 GMT
----
--Request 
GET https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=ff744f42-e2ce-4683-9cb5-b45d234591dc&sid=ce3c5ea0883c11ee858a29be7baeeb0f&vid=ce3cb6c0883c11ee9c5d21bc71586f02&vids=0&msclkid=N&pi=0&lg=en-US&sw=1313&sh=575&sc=24&tl=Womens%20Sweaters%20-%20Tops,%20Clothing%20%7C%20Kohl%27s&p=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-sweaters-tops-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3ASweaters%2BCategory%3ATops%2BDepartment%3AClothing%2BAssortment%3AKohls%2520Black%2520Friday%26BST%3D6262123%3A6237310%3A6149518%3A6125449%3A6149518%3A6176056%3A6119381%3A6438581%3A6143395%3A6125326%3A4922640%3A6169493%26icid%3Dhpmf-BF-offer-womenssweaters-vlhd26%26kls_sbp%3D79613869088917606480426504271561296302&r=https%3A%2F%2Fwww.kohls.com%2F&lt=2411&pt=1700550550599,1002,1002,,,38,38,38,38,38,,45,981,1210,1005,2314,2314,2408,2410,2410,2411&pn=0,0&evt=pageLoad&sv=1&rn=617904
Host: bat.bing.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204
cache-control: no-cache, must-revalidate
pragma: no-cache
expires: Fri, 01 Jan 1990 00:00:00 GMT
set-cookie: MUID=382D4CA4A8846C5D0D475F74A90E6D05; domain=.bing.com; expires=Sun, 15-Dec-2024 07:09:15 GMT; path=/; SameSite=None; Secure; Priority=High;
set-cookie: MR=0; domain=bat.bing.com; expires=Tue, 28-Nov-2023 07:09:15 GMT; path=/; SameSite=None; Secure;
strict-transport-security: max-age=31536000; includeSubDomains; preload
access-control-allow-origin: *
x-cache: CONFIG_NOCACHE
accept-ch: Sec-CH-UA-Arch, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version, Sec-CH-UA-Full-Version-List, Sec-CH-UA-Mobile, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version
x-msedge-ref: Ref A: D58E99D0D329485681E5A239B5A46892 Ref B: EWR311000103027 Ref C: 2023-11-21T07:09:15Z
date: Tue, 21 Nov 2023 07:09:15 GMT
----

