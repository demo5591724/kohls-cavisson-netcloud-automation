--Request 
POST https://endpoint.dlp.retail.adeptmind.ai/interlinks
Host: endpoint.dlp.retail.adeptmind.ai
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Accept: application/json
Content-Type: application/json
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:15 GMT
server: istio-envoy
x-is-fallback: false
content-length: 1241
content-type: application/json
access-control-allow-origin: *
access-control-allow-credentials: true
x-envoy-upstream-service-time: 4
x-envoy-decorator-operation: dlpws-endpoint.dlp-webservices-prod.svc.cluster.local:80/*
via: 1.1 google
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
----

