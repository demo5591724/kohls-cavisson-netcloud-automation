--Request 
POST https://analytics.tiktok.com/api/v2/pixel
Host: analytics.tiktok.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
server: nginx
content-length: 0
access-control-allow-headers: Authorization,*
access-control-allow-methods: GET,POST,PUT,PATCH,DELETE,HEAD,OPTIONS,UPDATE
access-control-allow-origin: *
x-tt-logid: 202311210710173B2D688D3356EBCA744C
server-timing: inner; dur=33
server-timing: cdn-cache; desc=MISS, edge; dur=4, origin; dur=41
x-tt-trace-host: 0108b4541b3bf73b144eb940f80f5a56569c560cd2e3f9c77b9bf350f765667569b198ce2873b96d8033773c4641fe2f1245ecc60ae2a5b40cf428a24d0a39ca18c373a1334124c233606e51db0f15e54d0dbc46be6912fe7da60afd38ed03f354
expires: Tue, 21 Nov 2023 07:10:17 GMT
cache-control: max-age=0, no-cache, no-store
pragma: no-cache
date: Tue, 21 Nov 2023 07:10:17 GMT
x-cache: TCP_MISS from a23-54-64-75.deploy.akamaitechnologies.com (AkamaiGHost/11.3.2.1-52518411) (-)
x-tt-trace-tag: id=16;cdn-cache=miss;type=dyn
x-origin-response-time: 41,23.54.64.75
x-akamai-request-id: 7f918b2
----
--Request 
GET https://sc-static.net/scevent.min.js
Host: sc-static.net
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript;charset=utf-8
content-length: 17231
server: CloudFront
date: Tue, 21 Nov 2023 07:08:45 GMT
access-control-allow-headers: Content-Type
access-control-allow-origin: *
content-encoding: gzip
cache-control: private, s-maxage=0, max-age=600
x-cache: Miss from cloudfront
via: 1.1 949219e108de746f94237ff81555dda2.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P4
x-amz-cf-id: whFQ31gxhlLzhruBKSZsDcxExq0JxShwOi4KkhiSVckRZqH2ywBE1w==
----
--Request 
GET https://www.googletagmanager.com/gtag/js?id=DC-8632166
Host: www.googletagmanager.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript; charset=UTF-8
access-control-allow-origin: *
access-control-allow-credentials: true
access-control-allow-headers: Cache-Control
content-encoding: br
vary: Accept-Encoding
date: Tue, 21 Nov 2023 07:08:45 GMT
expires: Tue, 21 Nov 2023 07:08:45 GMT
cache-control: private, max-age=900
last-modified: Tue, 21 Nov 2023 06:00:00 GMT
cross-origin-resource-policy: cross-origin
server: Google Tag Manager
content-length: 68999
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
----
--Request 
GET https://tr.snapchat.com/config/com/99ed79db-3aec-4b85-9a61-ed80a2993b62.js?v=3.6.0-2311172034
Host: tr.snapchat.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Origin: https://www.kohls.com
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:10:17 GMT
access-control-allow-origin: https://www.kohls.com
content-type: application/javascript
strict-transport-security: max-age=31536000; includeSubDomains; preload
strict-transport-security: max-age=31536000; includeSubDomains
content-length: 171
x-envoy-upstream-service-time: 0
server: API Gateway
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
access-control-allow-credentials: true
via: 1.1 google, 1.1 google
----

