--Request 
POST https://mboxedge35.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=cabcdd4927cd4756a91cdbcb38abdfc6&version=2.10.0
Host: mboxedge35.tt.omtrdc.net
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:08:53 GMT
content-type: application/json;charset=UTF-8
vary: origin,access-control-request-method,access-control-request-headers,accept-encoding
access-control-allow-origin: https://www.kohls.com
access-control-allow-credentials: true
x-request-id: 2e885515d06d307f414c2b0397bc6e0d
timing-allow-origin: *
accept-ch: Sec-CH-UA, Sec-CH-UA-Arch, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version, Sec-CH-UA-Mobile, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version-List
content-encoding: gzip
strict-transport-security: max-age=31536000; includeSubDomains
referrer-policy: strict-origin-when-cross-origin
x-xss-protection: 1; mode=block
x-content-type-options: nosniff
server: adobe
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231121-black-friday-deals-19
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 84239
access-control-allow-origin: *
last-modified: Wed, 15 Nov 2023 08:28:10 GMT
etag: \"bf1d82402dda774a60a74973608bf340\"
-x-adobe-smart-imaging: -74085
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231121-black-friday-deals-19]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Tue, 28 Nov 2023 06:00:12 GMT
date: Tue, 21 Nov 2023 07:08:54 GMT
akamai-grn: 0.2e10de17.1700550534.406e4e2
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231121-black-friday-deals-8
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 46218
access-control-allow-origin: *
last-modified: Wed, 15 Nov 2023 08:28:10 GMT
etag: \"f726f3997fb4d1a418a47ab027ae3d11\"
-x-adobe-smart-imaging: -25832
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231121-black-friday-deals-8]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Tue, 28 Nov 2023 06:00:12 GMT
date: Tue, 21 Nov 2023 07:08:54 GMT
akamai-grn: 0.2e10de17.1700550534.406e4e3
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231121-black-friday-deals-20
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 44891
access-control-allow-origin: *
last-modified: Wed, 15 Nov 2023 08:28:10 GMT
etag: \"87a9f75e7b62efef365644c324545163\"
-x-adobe-smart-imaging: -26835
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231121-black-friday-deals-20]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Tue, 28 Nov 2023 06:00:12 GMT
date: Tue, 21 Nov 2023 07:08:54 GMT
akamai-grn: 0.2e10de17.1700550534.406e4ed
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231121-black-friday-deals-21
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 46482
access-control-allow-origin: *
last-modified: Wed, 15 Nov 2023 08:28:10 GMT
etag: \"10e784a6080297cf76e6067b1baf0d6c\"
-x-adobe-smart-imaging: -27290
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231121-black-friday-deals-21]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Tue, 28 Nov 2023 06:00:12 GMT
date: Tue, 21 Nov 2023 07:08:54 GMT
akamai-grn: 0.2e10de17.1700550534.406e4ee
----

