--Request 
POST https://jsa-khls.domdog.io/report-uri/a9a6fb14-365a-4648-b17b-2e47930f8b49/1/pp/2.0
Host: jsa-khls.domdog.io
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204
date: Tue, 21 Nov 2023 07:10:14 GMT
cf-cache-status: DYNAMIC
report-to: {\"endpoints\":[{\"url\":\"https:\\/\\/a.nel.cloudflare.com\\/report\\/v3?s=ZiqQv00Kel8xbw0s%2BJwXUdzIIFGXKISyFTouVLzO0yK%2BLCGPar39smSuK34fvb%2Fkx1uBaY9yTMJGNT5mTLYMwjrNy1yyWEpNYK2oRQG99dNLSfK8uSMrehZh4x0yUvBUNA56c40%3D\"}],\"group\":\"cf-nel\",\"max_age\":604800}
nel: {\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}
server: cloudflare
cf-ray: 82971c9cbf0339c5-YYZ
alt-svc: h3=\":443\"; ma=86400
----
--Request 
GET https://cdn.evgnet.com/beacon/kohlsinc2/prod/scripts/evergage.min.js
Host: cdn.evgnet.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:28 GMT
via: 1.1 varnish
cache-control: max-age=120
age: 9
x-served-by: cache-ewr18125-EWR
x-cache: HIT
x-cache-hits: 1
x-timer: S1700550569.556472,VS0,VE1
vary: Accept-Encoding
timing-allow-origin: *
x-amz-id-2: oMtYjc5hVwQYphL+J5F6Twp4rpbkfroT5/2mhb90l56XgJ/NZYl5O9I/lKzpnjniQ7gxRa2VgQgFFyMqQ97SF+n64gVPs2NG05GSaAwLjFg=
x-amz-request-id: XVK4VSJP9AG7WV0J
x-amz-replication-status: COMPLETED
last-modified: Wed, 01 Nov 2023 22:19:28 GMT
etag: \"e3ff8c68eefdf92479ca9188a58eb13b\"
x-amz-server-side-encryption: AES256
content-encoding: gzip
x-amz-meta-evergage-beacon-ver: 16
x-amz-meta-evergage-sum: dbb5e52c8bf18e3110430860b1da7ea07de42196
x-amz-version-id: tbo2hxwgE8TV7KbNGilW8sczDuHk_qNq
content-type: application/javascript; charset=utf-8
server: AmazonS3
accept-ranges: bytes
content-length: 46203
----
--Request 
GET https://cdn.cookielaw.org/scripttemplates/202306.1.0/otBannerSdk.js
Host: cdn.cookielaw.org
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:08:41 GMT
content-type: application/javascript
content-length: 99599
content-encoding: gzip
content-md5: XJk1ZZTljtwHFT3qcIJg+w==
last-modified: Wed, 12 Jul 2023 06:29:36 GMT
etag: 0x8DB82A15D413626
x-ms-request-id: 7a75efb1-601e-0081-6c94-b47ab1000000
x-ms-version: 2009-09-19
x-ms-lease-status: unlocked
x-ms-blob-type: BlockBlob
access-control-expose-headers: x-ms-request-id,Server,x-ms-version,Content-Type,Content-Encoding,Last-Modified,ETag,Content-MD5,x-ms-lease-status,x-ms-blob-type,Content-Length,Date,Transfer-Encoding
access-control-allow-origin: *
cache-control: max-age=86400
cf-cache-status: HIT
age: 33685
accept-ranges: bytes
vary: Accept-Encoding
x-content-type-options: nosniff
server: cloudflare
cf-ray: 82971a595c4937d0-YYZ
----

