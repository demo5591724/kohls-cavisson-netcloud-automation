--Request 
GET https://token.rubiconproject.com/khaos.json?
Host: token.rubiconproject.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Origin: https://eus.rubiconproject.com
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://eus.rubiconproject.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
P3P: CP=\"NOI CURa ADMa DEVa TAIa OUR BUS IND UNI COM NAV INT\"
Pragma: no-cache
Cache-Control: no-cache,no-store,must-revalidate
Expires: 0
X-RPHost: b08401febecfa1b1a0c0270265f29df4
access-control-allow-credentials: true
access-control-allow-origin: https://eus.rubiconproject.com
content-type: application/json; charset=UTF-8
content-length: 7
set-cookie: khaos=LP7ZVJYX-1Y-7QIM; Max-Age=31536000; Expires=Wed, 20 Nov 2024 07:08:46 GMT; Path=/; Domain=.rubiconproject.com; Secure; SameSite=None
set-cookie: audit=1|vhxjpc70zhEKnGY0GrFnF29UAgZiacWrPaFmpt5RsaHPDufCv4UQzPYQkmRokKi1Vpg7d5N1mHbqFTrNE4+z9qDrxqInxg513OlDu/ORdD8=; Max-Age=31536000; Expires=Wed, 20 Nov 2024 07:08:46 GMT; Path=/; Domain=.rubiconproject.com; Secure; SameSite=None
----
--Request 
GET https://ib.adnxs.com/async_usersync?cbfn=queuePixels
Host: ib.adnxs.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://acdn.adnxs.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 307
server: nginx/1.21.3
date: Tue, 21 Nov 2023 07:08:46 GMT
content-type: text/html; charset=utf-8
content-length: 0
cache-control: no-store, no-cache, private
pragma: no-cache
expires: Sat, 15 Nov 2008 16:00:00 GMT
p3p: policyref=\"http://cdn.adnxs-simple.com/w3c/policy/p3p.xml\", CP=\"NOI DSP COR ADM PSAo PSDo OURo SAMo UNRo OTRo BUS COM NAV DEM STA PRE\"
x-xss-protection: 0
accept-ch: Sec-CH-UA-Full-Version-List,Sec-CH-UA-Arch,Sec-CH-UA-Model,Sec-CH-UA-Platform-Version,Sec-CH-UA-Bitness
location: https://ib.adnxs.com/bounce?%2Fasync_usersync%3Fcbfn%3DqueuePixels
an-x-request-uuid: 23a7c301-ab72-42f1-a37d-0b51991174d1
set-cookie: uuid2=577032700501687999; SameSite=None; Path=/; Max-Age=7776000; Expires=Mon, 19-Feb-2024 07:08:46 GMT; Domain=.adnxs.com; Secure; HttpOnly
x-proxy-origin: 192.3.3.250; 192.3.3.250; 582.bm-nginx-loadbalancer.mgmt.nym2.adnexus.net; adnxs.com
----

