--Request 
POST https://pixel.fohr.co/track
Host: pixel.fohr.co
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: text/plain;charset=UTF-8
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:10:16 GMT
access-control-allow-origin: https://www.kohls.com
vary: Origin
content-security-policy: frame-ancestors https://*.fohr.co;
x-frame-options: deny
x-xss-protection: 1; mode=block
x-content-type-options: nosniff
referrer-policy: same-origin
permissions-policy: accelerometer=(), autoplay=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), fullscreen=(), geolocation=(self), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), xr-spatial-tracking=()
strict-transport-security: max-age=31536000; includeSubDomains
cf-cache-status: DYNAMIC
report-to: {\"endpoints\":[{\"url\":\"https:\\/\\/a.nel.cloudflare.com\\/report\\/v3?s=%2FswxDG7Mobg2N6h%2FNC%2BHKnv2nFsqtcrIba5EXWIvEPfD%2BG5fFuUK%2FfebHTKboSCv5z%2F7KipD%2BIJ92UkK8elJpZIa9WCWO1UMU8eM6eUtQBeKy3UzD%2BrC%2BOpVw8yHVRA%3D\"}],\"group\":\"cf-nel\",\"max_age\":604800}
nel: {\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}
server: cloudflare
cf-ray: 82971ca6ce33a1da-YYZ
alt-svc: h3=\":443\"; ma=86400
----
--Request 
GET https://tjxbfc1n.micpn.com/p/js/1.js
Host: tjxbfc1n.micpn.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: text/javascript
x-uuid: 48a65c01-2457-4413-a37f-40a7f73ae834
cache-control: no-cache max-age=0
expires: Thu, 01 Dec 1994 16:00:00 GMT
pragma: no-cache
p3p: policyref=\"https://movableink.com/w3c/p3p.xml\", CP=\"DEVa PSAa PSDa IVAa IVDa OUR IND DSP NON COR NAV UNI\"
timing-allow-origin: https://www.kohls.com
date: Tue, 21 Nov 2023 07:05:35 GMT
content-encoding: gzip
vary: Accept-Encoding
x-cache: Hit from cloudfront
via: 1.1 9b0436675c860f7dd8f83017e2edc338.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P5
x-amz-cf-id: I-72z9UlWc1rG_-RqxTSbjT702YlLk8z6OOMexpwQmHERbShEwVmiQ==
age: 281
----
--Request 
GET https://cdnssl.clicktale.net/www47/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js
Host: cdnssl.clicktale.net
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Origin: https://www.kohls.com
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:09:15 GMT
x-amz-server-side-encryption: AES256
cache-control: max-age=900
x-amz-version-id: FgyQSlbxoN1Raj4lpTrKrSYGkzyBuz32
server: AmazonS3
x-cache: Hit from cloudfront
via: 1.1 192ddb149ecb1751bd671d09f051050a.cloudfront.net (CloudFront)
x-amz-cf-pop: CMH68-P5
x-amz-cf-id: c4_cf9isevD9BInIYWHcFJSN6r3cR7PuvkhtOQ_i3BwbxRy_4Iqy5A==
age: 0
timing-allow-origin: *
access-control-allow-origin: *
content-type: application/javascript;charset=utf-8
content-length: 77316
last-modified: Sat, 18 Nov 2023 13:38:16 GMT
etag: \"575b4b74d564ec06f0d19ac52f6311dd\"
content-encoding: br
accept-ranges: bytes
----
--Request 
GET https://bat.bing.com/p/action/4024145.js
Host: bat.bing.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
cache-control: private,max-age=1800
content-type: application/javascript; charset=utf-8
content-encoding: br
vary: Accept-Encoding
x-cache: CONFIG_NOCACHE
accept-ch: Sec-CH-UA-Arch, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version, Sec-CH-UA-Full-Version-List, Sec-CH-UA-Mobile, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version
x-msedge-ref: Ref A: 476DB94DF9AC4E9A8C991067CA036E4D Ref B: EWR311000103027 Ref C: 2023-11-21T07:08:43Z
date: Tue, 21 Nov 2023 07:08:43 GMT
----
--Request 
GET https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=4faeabff-b389-44e1-9848-3ea9fd7e4262&sid=ce3c5ea0883c11ee858a29be7baeeb0f&vid=ce3cb6c0883c11ee9c5d21bc71586f02&vids=0&msclkid=N&pi=0&lg=en-US&sw=1313&sh=575&sc=24&tl=Checkout%20-%20Kohls.com&p=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fv2%2Fcheckout.jsp&r=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&lt=1032&pt=1700550613272,550,573,,,13,13,13,13,13,,20,414,426,619,1032,1032,1032,,,&pn=0,0&evt=pageLoad&sv=1&rn=456246
Host: bat.bing.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204
cache-control: no-cache, must-revalidate
pragma: no-cache
expires: Fri, 01 Jan 1990 00:00:00 GMT
set-cookie: MUID=1F7C42E7171A698F339D5137169568E1; domain=.bing.com; expires=Sun, 15-Dec-2024 07:10:16 GMT; path=/; SameSite=None; Secure; Priority=High;
set-cookie: MR=0; domain=bat.bing.com; expires=Tue, 28-Nov-2023 07:10:16 GMT; path=/; SameSite=None; Secure;
strict-transport-security: max-age=31536000; includeSubDomains; preload
access-control-allow-origin: *
x-cache: CONFIG_NOCACHE
accept-ch: Sec-CH-UA-Arch, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version, Sec-CH-UA-Full-Version-List, Sec-CH-UA-Mobile, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version
x-msedge-ref: Ref A: 13F9362DCE6C4C73AF47EDE28992D595 Ref B: NYCEDGE1419 Ref C: 2023-11-21T07:10:16Z
date: Tue, 21 Nov 2023 07:10:15 GMT
----
--Request 
GET https://cdn.dynamicyield.com/scripts/1.213.0/dy-coll-nojq-min.js
Host: cdn.dynamicyield.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: application/javascript
date: Mon, 16 Oct 2023 06:55:04 GMT
last-modified: Sun, 15 Oct 2023 07:23:38 GMT
etag: W/\"31949d58649dc92ad662ada9f4e45f97\"
x-amz-server-side-encryption: AES256
cache-control: max-age=31536000
server: DYCDN
content-encoding: gzip
vary: Accept-Encoding
via: 1.1 ea4a33625617615e13496b292edda6d6.cloudfront.net (CloudFront)
age: 3111220
link: <//st.dynamicyield.com>; rel=\"dns-prefetch\", <//st.dynamicyield.com>; rel=\"preconnect\", <//rcom.dynamicyield.com>; rel=\"dns-prefetch\", <//rcom.dynamicyield.com>; rel=\"preconnect\", <//async-px.dynamicyield.com>; rel=\"dns-prefetch\", <//async-px.dynamicyield.com>; rel=\"preconnect\"
x-cache: Hit from cloudfront
x-amz-cf-pop: CMH68-P1
x-amz-cf-id: fNsr7eu8PVkRjsxtTz3DNSbbG2h3TAW9L1tKxld8ex3PqqmM2pNuCQ==
----
--Request 
GET https://static.ads-twitter.com/uwt.js
Host: static.ads-twitter.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:10:16 GMT
cache-control: no-cache
x-served-by: cache-iad-kcgs7200157-IAD
x-cache: HIT
vary: Accept-Encoding,Host
p3p: CP=\"CAO DSP LAW CURa ADMa DEVa TAIa PSAa PSDa IVAa IVDa OUR BUS IND UNI COM NAV INT\"
x-tw-cdn: FT
last-modified: Thu, 27 Oct 2022 18:55:37 GMT
x-amz-server-side-encryption: AES256
content-type: application/javascript; charset=utf-8
etag: \"32ad004436155ec972bc50e6238b5b67+gzip\"
content-encoding: gzip
accept-ranges: bytes
content-length: 15375
----
--Request 
GET https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/7a8648cd60a8/RC52a3a89d2834475a96ee97f4c4ee7e9a-source.min.js
Host: assets.adobedtm.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
content-type: application/x-javascript
etag: \"c3f8eea087eda271fe3a803f1d4cfa06:1700078393.11621\"
last-modified: Wed, 15 Nov 2023 19:59:53 GMT
server: AkamaiNetStorage
vary: Accept-Encoding
content-encoding: gzip
cache-control: max-age=3600
expires: Tue, 21 Nov 2023 08:08:44 GMT
date: Tue, 21 Nov 2023 07:08:44 GMT
content-length: 859
access-control-allow-origin: https://www.kohls.com
timing-allow-origin: *
----
--Request 
GET https://c.tagdelivery.com/c.gif?from=uet&rn=299195
Host: c.tagdelivery.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 302
cache-control: private, no-cache, proxy-revalidate, no-store
pragma: no-cache
location: https://c.bing.com/c.gif?from=uet&rn=299195&ctsa=mr&CtsSyncId=CEFD325B2F474C9F85CC304A8144C7F8&RedC=c.tagdelivery.com&MXFR=22C02C2D7C7B624D27593FFD787B723B
server: Microsoft-IIS/10.0
x-powered-by: ASP.NET
p3p: CP=\"BUS CUR CONo FIN IVDo ONL OUR PHY SAMo TELo\"
set-cookie: SM=T; domain=c.tagdelivery.com; path=/; SameSite=None; Secure;
set-cookie: MUID=22C02C2D7C7B624D27593FFD787B723B; domain=.tagdelivery.com; expires=Sun, 15-Dec-2024 07:10:16 GMT; path=/; SameSite=None; Secure; Priority=High;
date: Tue, 21 Nov 2023 07:10:16 GMT
content-length: 0
----

