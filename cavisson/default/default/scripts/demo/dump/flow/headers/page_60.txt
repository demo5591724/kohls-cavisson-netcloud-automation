--Request 
GET https://kohlsinc2.us-7.evergage.com/api2/event/prod?event=eyJzb3VyY2UiOnsicGFnZVR5cGUiOiJob21lcGFnZSIsInVybCI6Imh0dHBzOi8vd3d3LmtvaGxzLmNvbS8iLCJ1cmxSZWZlcnJlciI6IiIsImNoYW5uZWwiOiJXZWIiLCJiZWFjb25WZXJzaW9uIjoxNiwiY29uZmlnVmVyc2lvbiI6IjIzIiwiY29udGVudFpvbmVzIjpbXX0sInVzZXIiOnsiYW5vbnltb3VzSWQiOiIwZTQ5NzFiZGRiMjQ2NDYxIiwiaWRlbnRpdGllcyI6e319LCJpbnRlcmFjdGlvbiI6eyJuYW1lIjoiSG9tZXBhZ2UifSwicGFnZVZpZXciOnRydWUsImNvbnNlbnRzIjpbXSwiYWNjb3VudCI6e30sIl90b29sc0V2ZW50TGlua0lkIjoiOTMxOTk3MjAwMzI0MjAyNyIsImV4cGxhaW4iOnRydWV9
Host: kohlsinc2.us-7.evergage.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
Accept: application/json, text/javascript, */*; q=0.01
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
date: Tue, 21 Nov 2023 07:08:46 GMT
content-type: application/json;charset=UTF-8
set-cookie: AWSALBTG=rVExpqfD/4ru54KhI5lt/2JJpbs0h3TQY73L5VJQAdtu13bj7Usd0PgP8/R4vSM7ariDUtdTPhy6jGKfWxSMGhEmFhTOiwaL3V6aJvQVqi6OYn7ssc1Lf59xRJWqWwXktPhqcXJz4FX9AmjiU+XmvmRXR3XnnnMVVgzVahSJfUhXb7K93RQ=; Expires=Tue, 28 Nov 2023 07:08:46 GMT; Path=/
set-cookie: AWSALBTGCORS=rVExpqfD/4ru54KhI5lt/2JJpbs0h3TQY73L5VJQAdtu13bj7Usd0PgP8/R4vSM7ariDUtdTPhy6jGKfWxSMGhEmFhTOiwaL3V6aJvQVqi6OYn7ssc1Lf59xRJWqWwXktPhqcXJz4FX9AmjiU+XmvmRXR3XnnnMVVgzVahSJfUhXb7K93RQ=; Expires=Tue, 28 Nov 2023 07:08:46 GMT; Path=/; SameSite=None; Secure
set-cookie: AWSALB=kKdqDUhlot96phNFuVlzoWEyl0d85BpB8LQcV7oKsxkIwo31mn9+ZC3GTeW7itMQshXRsQLI5mWlBvWuNVTnFdgCH3ZcyCmZm6VK54xW9lJ1Y9EGsJydMPiQjiPt; Expires=Tue, 28 Nov 2023 07:08:46 GMT; Path=/
set-cookie: AWSALBCORS=kKdqDUhlot96phNFuVlzoWEyl0d85BpB8LQcV7oKsxkIwo31mn9+ZC3GTeW7itMQshXRsQLI5mWlBvWuNVTnFdgCH3ZcyCmZm6VK54xW9lJ1Y9EGsJydMPiQjiPt; Expires=Tue, 28 Nov 2023 07:08:46 GMT; Path=/; SameSite=None; Secure
access-control-allow-origin: https://www.kohls.com
timing-allow-origin: *
cache-control: no-cache, no-store
access-control-allow-credentials: true
x-content-type-options: nosniff
vary: accept-encoding
content-encoding: gzip
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231121-black-friday-deals-9
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 68397
access-control-allow-origin: *
last-modified: Wed, 15 Nov 2023 08:28:10 GMT
etag: \"2705305677040404fbde1f87e4cbe023\"
-x-adobe-smart-imaging: -52481
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231121-black-friday-deals-9]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Tue, 28 Nov 2023 06:00:11 GMT
date: Tue, 21 Nov 2023 07:08:45 GMT
akamai-grn: 0.2e10de17.1700550525.406abd0
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231119-black-friday-deals-14
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 79543
access-control-allow-origin: *
last-modified: Tue, 14 Nov 2023 06:49:53 GMT
etag: \"92d067fe7d1c9d0c356d16a98ba804b5\"
-x-adobe-smart-imaging: -64787
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231119-black-friday-deals-14]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Sun, 26 Nov 2023 06:00:09 GMT
date: Tue, 21 Nov 2023 07:08:45 GMT
akamai-grn: 0.2e10de17.1700550525.406abd1
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231119-black-friday-deals-4
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 43483
access-control-allow-origin: *
last-modified: Tue, 14 Nov 2023 06:49:53 GMT
etag: \"9ab83b6d1b25ff8db72218b486f24801\"
-x-adobe-smart-imaging: -18355
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231119-black-friday-deals-4]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Sun, 26 Nov 2023 06:00:09 GMT
date: Tue, 21 Nov 2023 07:08:45 GMT
akamai-grn: 0.2e10de17.1700550525.406abd2
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/hp-231121-black-friday-deals-10
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
content-type: image/jpeg
content-length: 107205
access-control-allow-origin: *
last-modified: Wed, 15 Nov 2023 08:28:10 GMT
etag: \"d65ac385ba09510a4fe51d5550249e5e\"
-x-adobe-smart-imaging: -96809
timing-allow-origin: *
-x-adobe-assetlist: [kohls/hp-231121-black-friday-deals-10]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
expires: Tue, 28 Nov 2023 06:00:11 GMT
date: Tue, 21 Nov 2023 07:08:45 GMT
akamai-grn: 0.2e10de17.1700550525.406abd3
----

