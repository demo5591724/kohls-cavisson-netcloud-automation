--Request 
POST https://d9.flashtalking.com/lgc
Host: d9.flashtalking.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://servedby.flashtalking.com
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://servedby.flashtalking.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: GET,POST,SERVER
Access-Control-Allow-Origin: https://servedby.flashtalking.com
Content-Type: application/json;charset=ISO-8859-1
Date: Tue, 21 Nov 2023 07:09:22 GMT
P3P: policyref=\"localhost/w3c/D9_p3p_.xml\", CP=\"NON DSP ADM DEV PSD IVDo OTPi OUR IND STP PHY PRE NAV UNI\"
Server: Apache/2.4.52 () OpenSSL/1.0.2k-fips
set-cookie: _D9J=9a89e2bf96464efdba1642792e40bdfb; path=/; Max-Age=31536000; Expires=Wed, 20-Nov-2024 07:09:23 UTC; SameSite=None;Secure;Domain=flashtalking.com
Content-Length: 103
Connection: keep-alive
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/6169493_Black?wid=240&hei=240&op_sharpen=1
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
access-control-allow-origin: *
last-modified: Wed, 27 Sep 2023 04:18:53 GMT
etag: \"65a093a6c42be949ea4ab9074019e38a\"
-x-adobe-smart-imaging: 2493
timing-allow-origin: *
-x-adobe-assetlist: [kohls/6169493_Black]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
content-type: image/webp
content-length: 3170
expires: Fri, 24 Nov 2023 09:01:42 GMT
date: Tue, 21 Nov 2023 07:09:23 GMT
akamai-grn: 0.2e10de17.1700550563.407a35f
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/4922627_Berry?wid=240&hei=240&op_sharpen=1
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
access-control-allow-origin: *
last-modified: Wed, 27 Sep 2023 04:13:56 GMT
etag: \"d04470d5cdcf37c5fb1255d43c49c9da\"
-x-adobe-smart-imaging: 2552
timing-allow-origin: *
-x-adobe-assetlist: [kohls/4922627_Berry]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
content-type: image/webp
content-length: 5180
expires: Sun, 26 Nov 2023 03:36:02 GMT
date: Tue, 21 Nov 2023 07:09:23 GMT
akamai-grn: 0.2e10de17.1700550563.407a360
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/6110701_Cream?wid=240&hei=240&op_sharpen=1
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
access-control-allow-origin: *
last-modified: Wed, 27 Sep 2023 05:02:37 GMT
etag: \"438375c89eaaca019525dd313158fc31\"
-x-adobe-smart-imaging: 1335
timing-allow-origin: *
-x-adobe-assetlist: [kohls/6110701_Cream]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
content-type: image/webp
content-length: 6278
expires: Mon, 27 Nov 2023 05:01:47 GMT
date: Tue, 21 Nov 2023 07:09:23 GMT
akamai-grn: 0.2e10de17.1700550563.407a361
----
--Request 
GET https://media.kohlsimg.com/is/image/kohls/6237310_Unique_Green?wid=240&hei=240&op_sharpen=1
Host: media.kohlsimg.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
access-control-allow-origin: *
last-modified: Wed, 27 Sep 2023 04:49:23 GMT
etag: \"e68a7367a5e77a1ecc04edd2dfb09195\"
-x-adobe-smart-imaging: 2005
timing-allow-origin: *
-x-adobe-assetlist: [kohls/6237310_Unique_Green]
server: Unknown
strict-transport-security: max-age=31536000; includeSubDomains
content-type: image/webp
content-length: 7884
expires: Sun, 26 Nov 2023 22:49:23 GMT
date: Tue, 21 Nov 2023 07:09:23 GMT
akamai-grn: 0.2e10de17.1700550563.407a38b
----
--Request 
GET https://servedby.flashtalking.com/track/101965;1478;403;17005505-6312-2772-FB25-0210C86A1706/?ft_data=d9:53f4b842bef745acae01973a7831ddaa;d9s:53f4b842bef745acae01973a7831ddaa&cachebuster=354249.45920451
Host: servedby.flashtalking.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://servedby.flashtalking.com/container/1638;101965;1478;iframe/?spotName=Category_Page&U1=6262123,6237310,6149518,6125449,6176056,6119381,6438581,6125326,4922640,6169493,4922627,6110701,6121308,6123856,6131877,6176065,6177932,6229706,6250998,6264246,6287802,6302496,6310961,6315769,6381694,4959669,6137076,5588439,5768492,6122638,6226866,6123395,5588506,6206243,6197072,6230463,6101575,6206211,6183757,6004313,6124369,5587888,5940204,6251731,6270310,6262011,6286393,6123808&U2=1234&U3=79613869088917606480426504271561296302&U7=d38e961d-eee7-4879-8340-ea94827e289a&U10=14201&cachebuster=398298.5894678621
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Type: image/gif
Content-Length: 42
Server: prod-xre-app30.ash11
Expires: Tue, 21 Nov 2023 07:09:23 GMT
Cache-Control: max-age=0, no-cache, no-store
Pragma: no-cache
Date: Tue, 21 Nov 2023 07:09:23 GMT
Connection: keep-alive
Strict-Transport-Security: max-age=86400
----
--Request 
GET https://servedby.flashtalking.com/segment/modify/xa2-xa211;;pixel/?valuePairs=klsct0;klsurct1&setTime=0;0&granularity=day;day
Host: servedby.flashtalking.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://servedby.flashtalking.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Content-Type: image/gif
Content-Length: 42
Server: prod-xre-app7.ash11
Expires: Tue, 21 Nov 2023 07:09:23 GMT
Cache-Control: max-age=0, no-cache, no-store
Pragma: no-cache
Date: Tue, 21 Nov 2023 07:09:23 GMT
Connection: keep-alive
Set-Cookie: flashtalkingad1=\"GUID=580356D102BC2E|segment=(xa2-t:5803-m:klsct0,xa211-t:5803-m:klsurct1)\";Comment=\"Flashtalking Cookie\";Path=/;Domain=.flashtalking.com;Expires=Fri, 20-Dec-2024 17:09:23 GMT;SameSite=None;Secure
Strict-Transport-Security: max-age=86400
----
--Request 
GET https://fdz.flashtalking.com/services/kohls/FBI-3059/segment/?uid=53f4b842bef745acae01973a7831ddaa&id=6262123
Host: fdz.flashtalking.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://servedby.flashtalking.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 302
date: Tue, 21 Nov 2023 07:09:23 GMT
content-type: text/html; charset=UTF-8
location: https://servedby.flashtalking.com/segment/modify/xa2-xa211;;pixel/?valuePairs=klsct0;klsurct1&setTime=0;0&granularity=day;day
server: nginx
----
--Request 
GET https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=225&t=2&li=gpt_m202311140101&jk=1722480090237966&bg=!kZKlkt3NAAZxrfrxUa07ADQBe5WfOKRB9m_p8hpNGr1VgsDlA7VaKkDDdfE2t3YIak1EnN9H4JQIU0Fsq8eO9ZrgS3w_AgAAANVSAAAAB2gBB5kDAEPsAG1IiMvih1NkTXcxQ5_ztEvXjgSAPRS4mz09Ey-LZTt3qLh2VpDGW0je2_Tm5LRD7E7SPMWXexRAlHJpgIr0sEJ0xSjGEIgIT1dL1ytKzNS5gADLGVhvt60mM6PQFwzS3zIcIS2ExNb98AaZj_Mc-jL6rlyOP2CvH1gwHQRncvlSoxQTGexc4RjIvKQju4HvXXGb_yHgZmVHCpH2ATPuuZJPBa74WDl1yO2YHkD1CxGG-cMJFofCa83U5DgljmzLdUh0I7QnCRY579JzKZGMfE-37ajRxegP9iSH93Mxr29TU8lQbLyi3EyOXvTgnz2uqkTFTSTicE34DzT5Oof7O74aDIqJMze6rogpQKZ0IFZI6HTV3OyBJwQlVXGrDuwyvlmDDzX-LR1eKqdbNTH0MHjpYnEHqrAHzev4fCA38wGAQ1KkacLwSi1EUJUDNSw0c1TKU9U7LKasfirvH-zsfjWXlQFlPxiIm6gAhdKu2jrds0rmBV05uwHvPbUwoT8y1BdZVNxpKb2zTVQ7DtE0IiOY3jM7st7Hi9v90-9Hq2Qdq-8w2hjDO5IUc9OpXWxUTltF3GG5pUD44ns9vaPzK-RJrd9IatJmC_yeafBpae7iWgveyNNLyVKqMbsUcphykHEpwFoOIXj9nk0SEUfHQnOH9xvYfytgIuyYJEjb6j22irZVJzCrR8nu8eAS3j_TygUwovPCFIvn5t6c-KpnNoBb2CKIfp4d5B-9PKnEdCjbCg0a0RxuMOnSXGShL98p2tPZLHwO4QZkDh-YuOk22qEsxhu6PWKF5v7riZxtR14hKNhzvKVlPy55fPCkiSuo3TsrN_6WRj19Pul2zTM20E0t7rqsq0APpfeVsy2rfnUwvoEYP2Xk8MXGfjnG5IBrdZf66IB6daA7ZJDLEZSAXokdTgVw03LFH2vzsOWYB8xD3Le6lYBBbDgIOgjIrtVI66yT_rV4QU2GmQhwKl0dA1pikMvpUDkK4h6Z2bOUL-Y3j5UJP34IB7m5E8OpNg
Host: pagead2.googlesyndication.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204
p3p: policyref=\"https://www.googleadservices.com/pagead/p3p.xml\", CP=\"NOI DEV PSA PSD IVA IVD OTP OUR OTR IND OTC\"
timing-allow-origin: *
cross-origin-resource-policy: cross-origin
content-type: text/html; charset=UTF-8
x-content-type-options: nosniff
date: Tue, 21 Nov 2023 07:09:23 GMT
server: cafe
content-length: 0
x-xss-protection: 0
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000
----

--Response 
HTTP/1.1 200 OK
Content-Type: image/gif
Content-Length: 42
Server: prod-xre-app23.ash11
Expires: Tue, 21 Nov 2023 07:09:23 GMT
Cache-Control: max-age=0, no-cache, no-store
Pragma: no-cache
Date: Tue, 21 Nov 2023 07:09:23 GMT
Connection: keep-alive
Set-Cookie: flashtalkingad1=\"GUID=5803F25D880082|segment=(xa2-t:5803-m:klsct0,xa211-t:5803-m:klsurct1)\";Comment=\"Flashtalking Cookie\";Path=/;Domain=.flashtalking.com;Expires=Fri, 20-Dec-2024 17:09:23 GMT;SameSite=None;Secure
Strict-Transport-Security: max-age=86400
----

