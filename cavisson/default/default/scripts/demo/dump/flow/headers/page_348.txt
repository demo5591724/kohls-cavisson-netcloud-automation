--Request 
POST https://17de4c12.akstat.io/
Host: 17de4c12.akstat.io
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-platform: \"Linux\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: */*
Origin: https://www.kohls.com
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: empty
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204 No Content
Access-Control-Allow-Origin: https://www.kohls.com
X-XSS-Protection: 0
Access-Control-Allow-Credentials: true
Timing-Allow-Origin: *
Content-Type: image/gif
Expires: Tue, 21 Nov 2023 07:10:01 GMT
Cache-Control: max-age=0, no-cache, no-store
Pragma: no-cache
Date: Tue, 21 Nov 2023 07:10:01 GMT
Connection: keep-alive
----
--Request 
GET https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/7a8648cd60a8/RCefe32536577a4558abe3c8dcd8da7ca6-source.min.js
Host: assets.adobedtm.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
accept-ranges: bytes
content-type: application/x-javascript
etag: \"c3f8eea087eda271fe3a803f1d4cfa06:1700078393.11621\"
last-modified: Wed, 15 Nov 2023 19:59:53 GMT
server: AkamaiNetStorage
vary: Accept-Encoding
content-encoding: gzip
cache-control: max-age=3600
expires: Tue, 21 Nov 2023 08:08:51 GMT
date: Tue, 21 Nov 2023 07:08:51 GMT
content-length: 801
access-control-allow-origin: https://www.kohls.com
timing-allow-origin: *
----
--Request 
GET https://bat.bing.com/p/action/4024145.js
Host: bat.bing.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: script
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200
cache-control: private,max-age=1800
content-type: application/javascript; charset=utf-8
content-encoding: br
vary: Accept-Encoding
x-cache: CONFIG_NOCACHE
accept-ch: Sec-CH-UA-Arch, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version, Sec-CH-UA-Full-Version-List, Sec-CH-UA-Mobile, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version
x-msedge-ref: Ref A: 476DB94DF9AC4E9A8C991067CA036E4D Ref B: EWR311000103027 Ref C: 2023-11-21T07:08:43Z
date: Tue, 21 Nov 2023 07:08:43 GMT
----
--Request 
GET https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=a9f88fe7-f7fb-456e-bd4f-3ebc4d5b31ee&sid=ce3c5ea0883c11ee858a29be7baeeb0f&vid=ce3cb6c0883c11ee9c5d21bc71586f02&vids=0&msclkid=N&pi=0&lg=en-US&sw=1313&sh=575&sc=24&tl=Shopping%20Bag%20-%20Kohls.com&p=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&r=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-6262123%2Fjuniors-so-crewneck-sweater.jsp%3Fcolor%3DBlack%26prdPV%3D1%26isClearance%3Dfalse&lt=1121&pt=1700550596574,326,331,,,2,2,2,2,2,,8,234,265,393,972,972,991,1119,1119,1121&pn=0,0&evt=pageLoad&sv=1&rn=441442
Host: bat.bing.com
sec-ch-ua: \"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\"
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36
sec-ch-ua-platform: \"Linux\"
Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Sec-Fetch-Dest: image
Referer: https://www.kohls.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 204
cache-control: no-cache, must-revalidate
pragma: no-cache
expires: Fri, 01 Jan 1990 00:00:00 GMT
set-cookie: MUID=2271B7F17296667123BDA421731C6727; domain=.bing.com; expires=Sun, 15-Dec-2024 07:10:01 GMT; path=/; SameSite=None; Secure; Priority=High;
set-cookie: MR=0; domain=bat.bing.com; expires=Tue, 28-Nov-2023 07:10:01 GMT; path=/; SameSite=None; Secure;
strict-transport-security: max-age=31536000; includeSubDomains; preload
access-control-allow-origin: *
x-cache: CONFIG_NOCACHE
accept-ch: Sec-CH-UA-Arch, Sec-CH-UA-Bitness, Sec-CH-UA-Full-Version, Sec-CH-UA-Full-Version-List, Sec-CH-UA-Mobile, Sec-CH-UA-Model, Sec-CH-UA-Platform, Sec-CH-UA-Platform-Version
x-msedge-ref: Ref A: D6BE5466C45B496E8DB06CBB34CBE5E8 Ref B: EWR311000103027 Ref C: 2023-11-21T07:10:01Z
date: Tue, 21 Nov 2023 07:10:01 GMT
----

