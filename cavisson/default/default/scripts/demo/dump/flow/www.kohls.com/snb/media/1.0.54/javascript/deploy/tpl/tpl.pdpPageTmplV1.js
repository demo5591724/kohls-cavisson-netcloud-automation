<script id="tpl-pdpPanel" type="text/x-jsrender">
	{{for  tmpl='tpl-pdpPanel-desktop' /}}
</script>

<script id="tpl-pdpPanel-desktop" type="text/x-jsrender">
	
<div id="content" class="pdp_container {{if productV2JsonData.productType.isVGC}}productType__gift-card{{/if}}">
	<div id="{{id:'root'}}" class="snb-container >
		{{panel:'pdpMetaInfoPanel'}}
		<div class="pdp-content">
	        {{panel:'pdpContentPanel'}}
	    </div>
	    <div class="external">
			<div id="stylitics">
				{{panel:'pdpStyliticsPanel'}}
			</div>
	        <div id="trendage">
	            {{panel:'pdpTrendagePanel'}}
	        </div>
	        <div id="PDP_recommendations">
	            {{panel:'pdpBDRecsPanel'}}
	        </div>
			<div id="PDP_monetization_HL">
				<div id="kmncarousel-page-anchor"></div>
	            {{panel:'pdpFeaturedProductsPanel'}}
	        </div>
			{{if productV2JsonData.richCopyBlock}}
				<link rel="preload" href="{{v_$resourceRoot}}css/creativeSlot.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		        <noscript>
			        <link rel="stylesheet" href="{{v_$resourceRoot}}css/creativeSlot.css">
		        </noscript>
	            <div class="creative-slot seo-tag-cloud">
	                {{:productV2JsonData.richCopyBlock}}
	            </div>
	        {{/if}}
	        <div id="PDP_web_collage">
	            {{panel:'pdpWebCollageInlinePanel'}}
	        </div>
			{{if $env.curalateSiteName}}
			<div data-crl8-container-id="custom-product" data-crl8-filter="productId:'{{>productV2JsonData.webID}}'"></div>
			{{/if}}
	        <div id="PDP_ratings_details">
	            {{panel:'pdpBVDetailsPanel'}}
	        </div>
			<div class="dfpHorizontalAd">
	            {{if $env.enable_googleDFP_product_details_page && $env.enable_googleDFP_product_details_page_super_leaderboard}}
	                <div id="dfp_super_leaderboard" class="dfp_super_leaderboard" style="text-align: center;"></div>
	            {{/if}}
	        </div>
			<div id="PDP_recommendations_bottom">
				<div id="bd_rec_{{:$env.bdRenderingPlacementH3}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
			</div>
	        <div id="PDP_monetization_AS_links">
	            {{panel:'pdpAdSensePanel'}}
	        </div>
	        <div id="PDP_monetization_DFP">
	            {{panel:'pdpDFPAdsPanel'}}
	        </div>
	    </div>
	    {{if productV2JsonData.textCopyBlock}}
			<link rel="preload" href="{{v_$resourceRoot}}css/creativeSlot.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		        <noscript>
			        <link rel="stylesheet" href="{{v_$resourceRoot}}css/creativeSlot.css">
		        </noscript>
			<div class="creative-slot seo-tag-cloud">
				{{:productV2JsonData.textCopyBlock}}
			</div>
	    {{/if}}
	    <a href="javascript:void(0);" class="scrollToNext pdpScroll" alt="next product"><span class="hidden-accessiblity-link">Next Product</sapn></a>
	    <a href="javascript:void(0);" class="scrollToNextDiv pdpScroll" alt="next product"><span class="hidden-accessiblity-link">Next Product</sapn></a>
	</div>
</div>
</script><script id="tpl-pdpMetaInfo" type="text/x-jsrender">
<div id="{{id:'root'}}">
 	<div itemscope itemtype="http://schema.org/Product">
		<meta itemprop="name" content="{{:productV2JsonData.productTitle}}" />
		<meta itemprop="description" content="{{:~removeSpecialCharacters(productV2JsonData.metaInfo && productV2JsonData.metaInfo[0].metaDescription)}}" />
        <meta itemprop="url" content="{{:~getUrlForMeta(productV2JsonData.seoURL)}}" />
        <meta itemprop="image" content="{{:~getImagesForMeta(productV2JsonData)}}" />
		<meta itemprop="brand" content="{{:productV2JsonData.brand}}" />
        <meta itemprop="seller" content="Kohls" />
		{{if ~getReviewCount(productV2JsonData.ratingCount)}}<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<meta itemprop="ratingValue" content="{{:productV2JsonData.avgRating}}" />
			<meta itemprop="ratingCount" content="{{:productV2JsonData.ratingCount}}" />
		</div>{{/if}}
		{{for ~getSkuMetaData(productV2JsonData.SKUS, productV2JsonData.skuId) ~productV2JsonData=productV2JsonData ~env=$env}}
			{{if (#index < 50) && ~checkCounterForSchema(#data, ~env.itemScopeLimit)>0 }}
				<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="{{:skuCode}}" />
                <meta itemprop="url" content="{{:~getUrlForMeta(~productV2JsonData.seoURL)}}" />
				<meta itemprop="price" content="{{:~getPriceForMeta(price, ~env)}}" />
				<meta itemprop="priceCurrency" content="USD" />
                {{if availability == "In Stock"}}
				    <meta itemprop="availability" content="http://schema.org/InStock" />
                {{else}}
                    <meta itemprop="availability" content="https://schema.org/OutOfStock" />
                {{/if}}
				<meta itemprop="itemCondition" content="http://schema.org/NewCondition" />
				{{if ~env.enableLiaSchemaPdp && ~productV2JsonData.storeIds && ~productV2JsonData.skuId }}
					<link itemprop="potentialAction" itemscope itemtype="http://schema.org/BuyAction"/>
					<div itemprop="deliveryLeadTime" itemscope itemtype="http://schema.org/QuantitativeValue">
						<span itemprop="value" content="0"/>
					</div>
					{{if skuCode == ~productV2JsonData.skuId }}
						<link itemprop="availableDeliveryMethod" href="./../../../../../../../schema.org/OnSitePickup"/>
						<div itemprop="availableAtOrFrom" itemscope itemtype="http://schema.org/Place">
							<span itemprop="branchCode" content="{{:~productV2JsonData.storeIds}}"/>
						</div>
					{{/if}}
				{{/if}}
				</div> {{if availability == "In Stock" }} {{for ~getSelectedSkuData(#data, ~productV2JsonData) /}} {{/if}}
			{{/if}}
		{{/for}}
		{{for productV2JsonData.SKUS ~productV2JsonData=productV2JsonData ~env=$env}}
			{{for ~updateSizeColor(~productV2JsonData, #data, skuId) /}}
		{{/for}}
	</div>
	{{if $env.brightTagEnabled}}
		{{for ~selectedSku=productV2JsonData.selectedSku tmpl="pdp_bright_tag_head" /}}
	{{/if}}
	</div>
</script>

<script id="pdp_bright_tag_head" type="text/x-jsrender">
	{{if $env.isSignalMarketingDataEnabled}}
		<script type="text/javascript">
			var pageData=
			{
			"pageDetails":{
				"pageURL":"{{:fullURL}}",
				"pageName": "{{:productV2JsonData.productTitle}}".replace(/&#34;/ig, "\""),
				"pageSite":"kohls.com",
				"pageChannel":"desktop",
				"pageType":"{{:$env.brightTagPageTypeDetail}}",
				"defaultLanguage":"{{:$env.brightTagDefaultLanguage}}",
				"browserPostalCode": "N/A",
				"timeStamp":""
				},
				"productDetails":
				{
					"departmentName":"{{:productV2JsonData.monetizationData.department}}",
					"categoryName":"{{:productV2JsonData.monetizationData.category}}",
					"subcategoryName":"{{:productV2JsonData.monetizationData.subCategory}}",
					"pageView":"Other",
					"pageFindingMethod":"{{:$env.brightTagPageFindingMethod}}",
					"pageKeywords":"N/A",
					"totalItems":"{{:$env.brightTagtotalItems}}",
					"collectionID":"{{:$env.brightTagCollectionID}}",
					{{if (~selectedSku) }}
						"pageItems": [	{
											"productName":"{{:productV2JsonData.productTitle}}".replace(/&#34;/ig, "\""),
											"productID":"{{:productV2JsonData.webID}}",
											"productSKU":"{{:~selectedSku.skuId}}",
											"originalPrice":"{{:~selectedSku.regularPrice}}",
											"isOnSale":"{{:~selectedSku.isOnSale}}",
											"salePrice":"{{:~selectedSku.salePrice}}"==''?'N/A':"{{:~selectedSku.salePrice}}",
											"isInStock":"{{:~selectedSku.inventoryStatus}}"
										}
									]
					{{/if}}
				}
			};
		{{:"<"}}/script>
		{{/if}}

		{{if !$env.isSignalMarketingDataEnabled}}
		<script type="text/javascript">
			var VisitorId = (Kjs.cookie.get("DYN_USER_ID")!="") ? Kjs.cookie.get("DYN_USER_ID") :"";
			var pageData=
			{
			"page_details":{
					"pageSite":"kohls.com",
					"pageExperience":"kohls.com",
					"pageType":"{{:$env.brightTagPageTypeDetail}}",
					"pageView":"Other",
					"pageFindingMethod":"{{:$env.brightTagPageFindingMethod}}",
					"pageKeyword":"",
					"pageTotalItems":"",
					"pageitemIDs":"{{:productV2JsonData.webID}}",
					"pageSKUIDs":"",
					"pageTypeHL":"product"
				},
				"Customer_Details":	{
					"customerID":VisitorId,
					"customerIsLoggedIn":"{{:$env.brightTagCustomerIsLoggedIn}}"
				},
				{{if (~selectedSku) }}
				"product_Details": {
									"itemName":"{{:productV2JsonData.productTitle}}".replace(/&#34;/ig, "\""),
									"itemProductID":"{{:productV2JsonData.webID}}",
									"itemSkuID":"{{:~selectedSku.skuId}}",
									"itemOriginalPrice":"{{:~selectedSku.regularPrice}}",
									"itemOnSale":"{{:~selectedSku.isOnSale}}",
									"itemSalePrice":"{{:~selectedSku.salePrice}}"==''?'N/A':"{{:~selectedSku.salePrice}}",
									"itemIsInStock":"{{:~selectedSku.inventoryStatus}}"
								}
				{{/if}}
			};
		{{:"<"}}/script>
		{{/if}}

		<script type="text/javascript">
			var ishookLogicEnabled = {{:hooklogicEnabled}};
			if(window.enableHookLogic==false){
				ishookLogicEnabled ="false";
			}
			var placement = null;
			var placementA = document.getElementById('hl_1_999');
			var placementB = document.getElementById('hl_2_999');
			if(placementA!=null){
					placement = "hl_1_999";
			}
			if(placementB!=null){
					placement = "hl_2_999";
			}
			if(placementA!=null && placementB!=null){
					placement = "hl_1_999,hl_2_999";
			}
			var pagetype ="{{:$env.brightTagPagetype}}";
		{{:"<"}}/script>
		
</script>
<script id="tpl-pdpBreadcrumb" type="text/x-jsrender">
<div id="{{id:'root'}}" class="PDP_breadcrumb">
    <link rel="preload" href="{{v_$resourceRoot}}css/pdpBreadcrumb.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpBreadcrumb.css">
    </noscript>
    <ul class="bread-crupms">
        <li id='backArrow'><span class="back-arrow" onclick="javascript:window.history.back(-1);return false;"> </span></li>
        <li id='backButton'><a onclick="javascript:window.history.back(-1);return false;" href="javascript:void(0);">Back</a> <span class="divider-vertical"></span> </li>
        {{if productV2JsonData.breadcrumbs}}
            {{for productV2JsonData.breadcrumbs ~len=productV2JsonData.breadcrumbs.length}}
                <li class="pdp-breadcrumb-name">
                    <a href="{{:seoURL}}" {{if #index == (~len-1)}}class="last-li"{{/if}}>{{:name}}</a>
                    {{if #index < (~len-1)}}<span class="next-arrow"></span>{{/if}}
                </li>
            {{/for}}
        {{/if}}
    </ul>
    <div id="pdp_breadcrumb_nextproduct" style="float:right;"></div>
</div>
</script><script id="tpl-pdpTitle" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-left-alignment" >
    <div class="pdp-error disabled" id="error-display">
		<p>
			<img src="{{_$env.resourceRoot}}images/error-icon.png" class="errorImg" alt="Info Error">
			<span>Some information is missing or invalid below.</span>
		</p>
		<p id="skuerror"></p>
	</div>
	{{if productV2JsonData.productStatus && productV2JsonData.productStatus.toLowerCase()=='out of stock' && productV2JsonData.environment != 'staging' && !productV2JsonData.isBopusEligible && !productV2JsonData.isBossEligible}}
	<div class="outofstock-message">
		<p class="warning-message">
			<img src="./../../../../../../../designsystem.kohls.com/assets/2.37.0/icons/input-warning-icon.svg" alt="warning icon" />
			We're sorry, this item is out of stock. Visit us again soon to see if it has returned.
		</p>
	</div>
	{{/if}}
</div>
</script><script id="tpl-productImageZoom-main" type="text/x-jsrender">
	<div class="pdpHeroImageZoom" id="{{id:'root'}}">
		{{if $env.enableInlineCSS}}
			{{:$cssFile('pdpZoomImage.css')}}
		{{else}}
            <link type="text/css" rel="stylesheet" href="{{v_$resourceRoot}}css/pdpZoomImage.css" />
		{{/if}}
		<div class="heroImageContainer">
			<button id="{{id:'exitZoom'}}" class="exitZoom"></button>
			<div id="heroImgWrapper">
				<div id="{{id:'slider'}}" class="slider">
					<div class="pdpCarouselZoom" id="{{id:'carousel'}}">
						<ul class="slides">
							{{if images && images.length > 0}}
								<li class="imageSlider"><img src="{{_~productImageParam(images[0].url__1200_)}}" class='pdp_heroimage'/></li>
							{{/if}}
							{{if altImages && altImages.length > 0}}
								{{for altImages}}
									<li class="imageSlider"><img src="{{_~productImageParam(_data.url__1200_)}}" class='pdp_altimage'/></li>
								{{/for}}
							{{/if}}
							{{if videos && videos.length && videos[0].url}}
								{{for videos}}
									{{if #data.url.toLowerCase().indexOf('.mp4') > -1 }}
										<li class="imageSlider"><video controls id="{{id:'video'}}" src={{__data.url}} style="width:100%"></video></li>
									{{/if}}
								{{/for}}
							{{/if}}
						</ul>
					</div>
				</div>
				<div class="fullImage-flex-btn">
					<a href="javascript:void(0)" class="flex-prev-large" id="{{id:'prevLarge'}}"></a>
				</div>
				<div class="fullImage-flex-btn">
					<a href="javascript:void(0)" class="flex-next-large" id="{{id:'nextLarge'}}"></a>
				</div>

				<div id="{{id:'customNav'}}" class="custom-navigation">

					<a href="javascript:void(0)" class="flex-prev" id="{{id:'prev'}}"></a>
				{{if ((videos && videos.length && videos[0].url.toLowerCase().indexOf('.mp4') > -1) + (altImages && altImages.length) + (images && images.length)) > 6}}
					<div id="{{id:'pageNumCarousel'}}" class="pageNumberCarousel">1 <span class="ofText"> of</span> {{:((altImages && altImages.length) + (images && images.length) + (videos && videos.length && videos[0].url.toLowerCase().indexOf('.mp4') > -1 ))}} </div>
					{{if videos && videos.length && videos[0].url && videos[0].url.toLowerCase() && videos[0].url.toLowerCase().indexOf('.mp4') > -1 }}
						{{for videos}}
							{{if #data.url.toLowerCase().indexOf('.mp4') > -1 }}
								<a href="#!" class="flex-videoicon" id="{{id:'videoIcon'}}"></a>
							{{/if}}
						{{/for}}
					{{/if}}
				{{else}}
					<div id="{{id:'customControls'}}" class="custom-controls-container"></div>
				{{/if}}
				<a href="#" class="flex-next" id="{{id:'next'}}"></a>
				</div>
			</div>
			<div id="{{id:'swatches'}}" class="swatches"></div>
		</div>
	</div>
</script>

<script id="tpl-productImageZoom-swatches" type="text/x-jsrender">
	<div class="zoom-swatches" id="{{id:'root'}}">
		<button id="{{id:'prevSwatch'}}" class="zoom-swatch-prev" {{if  variants.colorList && variants.colorList.length < 8 }}style="display:none"{{/if}}>
			<div class="zoom-swatch-glyph"></div>
		</button>
		<div id="{{id:'colorSwatches'}}" class="zoom-color-swatches">
		{{if ~getImageData(#data) != '' && variants.swatchImage != undefined && variants.swatchImage != ''}}
			<div class="zoom-swatch-carousel-container">
				<ul id="{{id:'swatchList'}}" class="zoom-swatch-carousel"{{if  variants.colorList && variants.colorList.length < 8 }} style="display:flex"{{/if}}>
				{{for swatchList ~environment=environment}}
					<li>
					{{if ~environment=="staging" && colorFlag}}
						<div id="{{id:'outerSwatch'}}" class="zoom-swatch-color-container {{:~root.activeColor == color ? 'selected':''}}">
							<div class="zoom-swatch-color {{:~root.activeColor == color ? 'active':''}}">
								<a id="{{id:'colorSwatch'}}" data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="swatch-color no-inventory" href="javascript:void(0);" style="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}"><img alt="out of stock" src='{{_~root.$env.resourceRoot}}images/swatch_unavail.gif'/>
									<span style="opacity:0"> out of stock</span>
								</a>
							</div>
						</div>
					{{else colorFlag===false}}
						<div id="{{id:'outerSwatch'}}" class="zoom-swatch-color-container {{:~root.activeColor == color ? 'selected':''}}">
							<div class="zoom-swatch-color {{:~root.activeColor == color ? 'active':''}}">
								<a id="{{id:'colorSwatch'}}" data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="swatch-color" href="javascript:void(0);" style="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}">{{if oosFlag}}<img alt="out of stock" src='{{_~root.$env.resourceRoot}}images/swatch_unavail.gif'/>{{/if}}
								</a>
							</div>
						</div>
					{{else colorFlag===true}}
						<div id="{{id:'outerSwatch'}}" class="zoom-swatch-color-container {{:~root.activeColor == color ? 'selected':''}}">
							<div class="zoom-swatch-color color-unavailable" title="out of stock">
								<a id="{{id:'colorSwatch'}}" data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="swatch-color" href="javascript:void(0);" style="background:url({{:backgroundURL}}) no-repeat;pointer-events: none;" rel="{{:largeImgURL}}"><img alt="out of stock" src='{{_~root.$env.resourceRoot}}images/swatch_unavail.gif'/>
									<span style="opacity:0"> out of stock</span>
								</a>
							</div>
						</div>
					{{/if}}
					</li>
				{{/for}}
				</ul>
			</div>
		{{/if}}
		</div>
		<button id="{{id:'nextSwatch'}}" class="zoom-swatch-next" {{if variants.colorList && variants.colorList.length < 8}}style="display:none"{{/if}}>
			<div class="zoom-swatch-glyph"></div>
		</button>
	</div>
</script>
<script id="tpl-pdpHeroImage" type="text/x-jsrender">
	<div class="pdpCarousal-Wrapper" id="{{id:'root'}}">
		{{if !$env.isMobile }}
			{{panel:'pdpLargeImagesPanel'}}
		{{else}}
			{{if ~imageList(productV2JsonData).length == 1}}
			<div class="pdp-image-view">
				<div class="pdp-image-single" id="{{id:'largeImageContainer'}}"></div>
			</div>
			{{else}}
			<div class="pdp-image-view">
				<div class="close-button" id="{{id:'modalClose'}}">x</div>
				<div class="pdp-image hidden" id="{{id:'largeImageContainer'}}"></div>
				<div class="pdp-slider-cnt">
					<div id="{{id:'prevSlide'}}" class="slider-control slider-prev disabled">
						<a href="javascript:void(0)" title="Previous set of images" class="slider-button"><span class="slider-img"></span></a>
					</div>
					<div class="pdp-slider hidden" id="{{id:'slider'}}">
						<ul class="slides" id="{{id:'slides'}}">
							{{for ~imageList(productV2JsonData)}}
							<li class="slider-image {{:type}}" id="{{id:'sliderImage'}}">
								<div class="{{if (type !== 'video' || !~root.productV2JsonData.isFlexslider)}}slider-image-box{{/if}}">
							{{if type == 'hero'}}
								<img
									srcset="{{:~productImageParam(url,'96')}} 96w,
											{{:~productImageParam(url,'400')}} 400w"
									sizes="(max-width: 728px) 400px, 96px"
									src="{{:~productImageParam(url,'96')}}"
									alt="{{>altText}}"/>
							{{/if}}
							{{if type == 'alt'}}
								<img
									srcset="{{if socialThumbnailImgUrl}}
											{{:~productImageParam(socialThumbnailImgUrl,'96')}}
										{{else}}
											{{:~productImageParam(url,'96')}}
										{{/if}} 96w,
											{{:~productImageParam(url,'400')}} 400w"
									sizes="(max-width: 728px) 400px, 96px"
									{{if socialThumbnailImgUrl}}
										src="{{:~productImageParam(socialThumbnailImgUrl,'96')}}"
									{{else}}
										src="{{:~productImageParam(url,'96')}}"
									{{/if}}
									alt="{{>altText}}"
									{{if username}}data-username="{{>username}}"{{/if}}
									{{if displayName}}data-display-name="{{>displayName}}"{{/if}}
									{{if labels}}data-labels="{{>labels}}"{{/if}}
								"/>
							{{/if}}
							{{if type == 'video'}}
								{{if ~root.productV2JsonData.isFlexslider}}
									<div class="sliderVideoMobile">
										<video class="videoPreview" src="" width="400px" height="380px" preload="metadata"></video>
										<img class="video-play-button" src="{{v_~root.$resourceRoot}}images/pdp/ic-play-circle.svg" alt="{{>altText}}"/>
									</div>
								{{else}}
									<div class="sliderVideoNotMobile">
										<video class="videoPreview" src="{{_url}}_t=2" width="100px" height="100px" preload="metadata"></video>
										<img class="video-play-button" src="{{v_~root.$resourceRoot}}images/pdp/ic-play-circle.svg" alt="{{>altText}}"/>
									</div> 
								{{/if}}
							{{/if}}
							</div>
							</li>
							{{/for}}
						</ul>
					</div>
					<div id="{{id:'nextSlide'}}" class="slider-control slider-next disabled">
						<a href="javascript:void(0)" title="Next set of images" class="slider-button"><span class="slider-img"></span></a>
					</div>
				</div>
			</div>
			{{/if}}
		{{/if}}
	<div style="clear:both"></div>

	<div class="pdp-additional-links">
		<div class="pdp-additional-links-marker" style="display:flex;">
			{{if $env.enableStylitics}}
				<div class="additional-link tce-link-container" style="display:none">
					<img src="{{v_$resourceRoot('snb')}}images/9-dots.png" class="tce-link-icon"/><a id="{{id:'styliticsLink'}}" href="#tce-anchor" class="tce-jump-link">Ways to Style It</a>
				</div>
			{{/if}}
			{{if $env.enableWebcollageMosaic && !productV2JsonData.isSephora}}
				<div id="wc-mosaic" class='additional-link'></div>
			{{/if}}
		</div>
	</div>
</div>

</script>

<script id="tpl-largeImage-image" type="text/x-jsrender">
	<div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails is-ready"><a href="{{:largeImgUrl}}"><img class="pdp-image-main-img" src="{{_url}}" alt="{{>altText}}"/></a></div>
</script>

<script id="tpl-largeImage-video" type="text/x-jsrender">
	<video controls id="video" src="{{_url}}" width="600px" height="600px"></video>
</script>
<script id="tpl-pdpLargeImages" type="text/x-jsrender">
    <div id="{{id:'root'}}" class="pdp-large-images-root-panel">
        <div class="PDP_Large_Images" id="{{id:'pdp_large_images'}}">
            {{for productV2JsonData.images}}
                <div class="pdp-large-hero-image" id="{{id:'pdp-slide-image'}}">
                    <span class="hover-zoom-icon"></span>
                    <img
                       srcset="{{:~productImageParam(#data.url,'805')}} 1200w,
                        {{:~productImageParam(#data.url,'400')}} 400w"
                        sizes="(max-width: 728px) 400px, 805px"
                        loading="lazy"
                        alt="{{>altText}}"/>                    
                </div>
            {{/for}}
            <div class="pdp-large-alt-images">
                {{for ~imageList(productV2JsonData)}}
                    {{if type == 'alt'}}
                        <div class="large-alt-image" id="{{id:'pdp-slide-image'}}">
                            <span class="hover-zoom-icon"></span>
                            <img
                            src="{{_~productImageParam(_data.url__390_)}}"
                            loading="lazy"
                            alt="{{>altText}}"/>
                        </div>
                    {{/if}}
                    {{if type == 'video'}}
                        <div class="large-alt-image" id="{{id:'pdp-slide-image'}}">
                            <video class="videoPreview" src="{{_url}}_t=2" width="100%" height="100%" preload="metadata"></video>
                            <img class="video-play-button" src="{{v_~root.$resourceRoot}}images/pdp/ic-play-circle.svg" alt="{{>altText}}"/>
                        </div> 
                    {{/if}}
                {{/for}}
            </div> 
        </div>
        {{if ~imageList(productV2JsonData).length > 5}}
            <div id="{{id:'largeImagesShowMore'}}" class="show-more-less">
                <button class="show-more-large-images">Show More</button>
            </div>
        {{/if}}
    </div>
</script>

<script id="tpl-pdp-large-image-slider" type="text/x-jsrender">
    <div class="pdp-large-image-slider">
        <button class="close-button" id="{{id:'modalClose'}}"></button>
        <div id="{{id:'pdp-large-image-slider'}}">
            <div id="{{id:'prevSlide'}}" class="slider-control slider-prev disabled">
                <a href="javascript:void(0)" title="Previous set of images" class="slider-button"><span class="slider-img"></span></a>
            </div>
            <div class="pdp-large-slider spinner" id="{{id:'slider'}}">
                <ul class="slides" id="{{id:'slides'}}">
                    {{for ~imageList(productV2JsonData)}}
                        <li class="pdp-large-slider-image {{if socialLargeImgUrl}}pdp-social-slider-image{{/if}}">
                            {{if type == 'hero'}}
                                <img
                                    class="pdp-large-slider-image-zoom"
                                   srcset="{{:~productImageParam(url,'1500')}}&qlt=60 1500w,
                                            {{:~productImageParam(url,'400')}} 400w"
                                    sizes="(max-width: 728px) 400px, 790px"
                                    src="{{_~productImageParam(url__1500_)}}_qlt=60"
                                    alt="{{>altText}}"/>
                            {{/if}}
                            {{if type == 'alt'}}
                                <img
                                    class="pdp-large-slider-image-zoom"
                                   srcset="{{:~productImageParam(url,'1500')}}&qlt=60 1500w,
                                    {{:~productImageParam(url,'400')}} 400w"
                                    sizes="(max-width: 728px) 400px, 790px"
                                    src="{{_~productImageParam(url__1500_)}}_qlt=60"
                                    alt="{{>altText}}"/>
                                {{if socialLargeImgUrl}}
                                    {{for tmpl='tpl-social-alt-images-info' /}}
                                {{/if}}
                            {{/if}}
                            {{if type == 'video'}}
                                <div class="pdp-large-video">
                                    <video class="videoPreview" src="{{_url}}_t=2" width="750px" height="750px" preload="metadata"></video>
                                    <img class="video-play-button" src="{{v_~root.$resourceRoot}}images/pdp/ic-play-circle.svg" alt="{{>altText}}"/>
                                </div>
                            {{/if}}
                        </li>
                    {{/for}}
                </ul>
            </div>
            <div id="{{id:'nextSlide'}}" class="slider-control slider-next disabled">
                <a href="javascript:void(0)" title="Next set of images" class="slider-button"><span class="slider-img"></span></a>
            </div>
        </div>
    </div>
</script>

<script id="tpl-social-alt-images-info" type="text/x-jsrender">
    <div class="social-image-caption">
        <p class="social-image-heading">Shared by {{if displayName}}{{:displayName}}{{else}}{{:username}}{{/if}}</p>
        {{for ~getCaptionsData(#data)}}
                <div class="social-labels">
                    <div class="social-button-container">
                        <span tabindex="0" class="social-label-button" aria-describedby="social-label-kohls-partner">
                            {{:#data.title}}
                        </span>
                        <div class="social-label-description hidden" id="social-label-kohls-partner" role="tooltip">
                            {{:#data.description}}
                        </div>
                    </div>
                </div>
        {{/for}}
    </div>
</script>

<script id="tpl-pdpGroupPricing" type="text/x-jsrender">
    <div id="{{id:'root'}}">
        {{for productV2JsonData.SKUSGroupPricing.priceKeyOrder}}
            {{include ~opt=~root.productV2JsonData.SKUSGroupPricing.prices[#data]}}
                <div id="group-pdp-Pricing">
                    <div id="group-pdpprice-price-container">
                        {{for ~productV2JsonData=~root.productV2JsonData ~opt.price tmpl=~getPriceTemplate(~opt) /}}
                        {{for ~Offers=~opt.price.yourPriceInfo.appliedOffers  ~price=~opt.price tmpl='tpl-pdp-pricing-ypappliedoffers' /}}
                        {{for ~Offers=~opt.price.yourPriceInfo.saveMoreOffers ~price=~opt.price tmpl='tpl-pdp-pricing-ypsavemoreoffer' /}}
                        {{for ~opt ~productID=~root.productV2JsonData.webID tmpl="tpl-pdp-color-swatches" /}}
                    </div>
                </div>
            {{/include}}
        {{/for}} 
    </div>
</script><script id="tpl-pdpHeroImage-zoom" type="text/x-jsrender">
	<div class="pdpCarousal-Wrapper" id="{{id:'root'}}">
		<link rel="preload" href="{{v_$resourceRoot}}css/pdpHeroImage.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		<noscript>
			<link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpHeroImage.css">
		</noscript>
		{{if !productV2JsonData.altImages && ~calculateProductImagesLength(productV2JsonData)}}
		<section class="slider singleImage" id="sliderSection">
			<div class="pdpCarousal"  id="{{id:'pdpCarousal'}}">
				<ul class="slides">
					<li class="imageSlider flex-active-slide"><img src="{{_~productImageParam(productV2JsonData.images[0].url__600_)}}" class='pdp_heroimage singleSlide' id="{{id:'heroimage'}}"/></li>
				</ul>
			</div>
		</section>
		{{else}}
		<section class="slider multiImage" id="sliderSection">
			<div class="pdpCarousal flexslider" id="{{id:'pdpCarousal'}}">
				<ul class="slides">
					{{if productV2JsonData.images && productV2JsonData.images.length > 0}}
						<li class="imageSlider"><img src="{{_~productImageParam(productV2JsonData.images[0].url__600_)}}" class='pdp_heroimage' id="{{id:'heroimage'}}"/></li>
					{{/if}}
					{{if productV2JsonData.altImages && productV2JsonData.altImages.length > 0}}
						{{for productV2JsonData.altImages}}
							<li class="imageSlider"><img id="{{id:'altimg'}}" {{:~productImageLazy(#data.url,'600', #index)}} class='pdp_altimage'/></li>
						{{/for}}
					{{/if}}
					{{if productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url }}
						{{for productV2JsonData.videos}}
							{{if  #data.url.toLowerCase().indexOf('.mp4') > -1 }}
								<li class="imageSlider altVideoLink" id="slide2"><video controls id="video" src={{__data.url}} width="600px" height="600px"></video></li>
							{{/if}}
						{{/for}}
					{{/if}}
				</ul>
			</div>
			</section>
		{{/if}}
	<div style="clear:both"></div>

	<div class="custom-navigation" id="custom-navigation">
		<a href="#" class="flex-prev"></a>
		{{if ((productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url.toLowerCase().indexOf('.mp4') > -1) + (productV2JsonData.altImages && productV2JsonData.altImages.length) + (productV2JsonData.images && productV2JsonData.images.length)) > 6}}
			<div class="pageNumberCarousel">1 <span class="ofText">of</span> {{:((productV2JsonData.altImages && productV2JsonData.altImages.length) + (productV2JsonData.images && productV2JsonData.images.length) + (productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url.toLowerCase().indexOf('.mp4') > -1 ))}} </div>
			{{if productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url && productV2JsonData.videos[0].url.toLowerCase() && productV2JsonData.videos[0].url.toLowerCase().indexOf('.mp4') > -1 }}
				{{for productV2JsonData.videos}}
					<a href="#!" class="flex-videoicon"></a>
				{{/for}}
			{{/if}}
		{{else}}
			<div class="custom-controls-container"></div>
		{{/if}}
		<a href="#" class="flex-next" onclick=""></a>
	</div>
	<div class="pdp-additional-links">
		{{if $env.enableStylitics}}
			<div class="additional-link tce-link-container" style="display:none">
				<img src="{{v_$resourceRoot('snb')}}images/9-dots.png" class="tce-link-icon"/><a id="{{id:'styliticsLink'}}" href="#tce-anchor" class="tce-jump-link">Ways to Style It</a>
			</div>
		{{/if}}
		{{if $env.enableWebcollageMosaic && !productV2JsonData.isSephora}}
			<div id="wc-mosaic" class='additional-link'></div>
		{{/if}}
		</ul>
	</div>
</div>
</script>
<script id="tpl-pdpContent" type="text/x-jsrender">
    {{for  tmpl='tpl-pdpContent-desktop' /}}
</script>
<script id="tpl-pdpContent-desktop" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="PDP_breadcrumb">
		{{panel:'pdpBreadcrumbPanel'}}
    </div>
    <div id="PDP_product_name">
		{{panel:'pdpTitlePanel'}}
    </div>
	{{if $env.enableHeroAlign}}
	<div id="PDP_colGrid">
        <div id="PDP_block1">
            {{!-- Top Rated, Best Seller Badges --}}
            {{if productV2JsonData.badges && !productV2JsonData.isSephora}}
            {{for ~creativePDPBadges(productV2JsonData.badges)}}
                {{if badgeName === 'best seller' || badgeName === 'top rated'}}
                <div class="creative_badge_block">
                    <span {{if badgeName == 'best seller'}} class="creativeBest" {{else}} class="creativeTop"{{/if}}>
                    <span {{if badgeName == 'best seller'}} class="creativeBadgeBest" {{else}}class="creativeBadgeTop"{{/if}}> {{:badgeName}}</span>
                    </span>
                </div>
                {{/if}}
            {{/for}}
            {{/if}}

                {{!-- Sephora Badges --}}
            {{if productV2JsonData.productTitle}}
                <div class="pdp-title-container" id="{{id:'pdpTitle'}}">
                    {{if productV2JsonData.marketplaceitem}}
                        <kds-badge text="marketplace" badge-type="marketplace" class="marketplaceitem-item"></kds-badge>
                    {{/if}}
                    {{if productV2JsonData.isEarlyAccessProduct}}
                        <div class="early-access-badge">Kohl's Rewards Early Access</div>
                    {{/if}}
                    {{if productV2JsonData.isSephora}}
                      <div class="sephora-logo">
                        <img src="{{v_$env.resourceRoot}}kds-web-core/assets/sephora/tags/large-k.svg" alt="Sephora logo" />
                        {{if productV2JsonData.badges}}
                        {{for ~creativePDPBadges(productV2JsonData.badges)}}
                            {{if badgeName}}
                            {{if badgeName === 'new badge'}}
                                <img class="new-badge" alt="New Logo" src="{{_~root.$env.resourceRoot}}/images/pdp/new-logo.png" />
                            {{else}}
                                <div class="creative_badge_block">
                                <span {{if badgeName === 'best seller'}} class="creativeBest" {{else}} class="creativeTop"{{/if}}>
                                    <span {{if badgeName === 'best seller'}} class="creativeBadgeBest" {{else}}class="creativeBadgeTop"{{/if}}> {{:badgeName}}</span>
                                </span>
                                </div>
                            {{/if}}
                            {{/if}}
                        {{/for}}
                        {{/if}}
                      </div>
                    {{/if}}
                    <h1 class="product-title">
                        {{:productV2JsonData.productTitle}}
                    </h1>
                    {{if productV2JsonData.brand}}
                        <div class="sub-product-title"> by <a href="{{:productV2JsonData.brandSeoUrl}}">{{:productV2JsonData.brand}} </a></div>
                    {{/if}}
                </div>
            {{/if}}
            <div class="pdp-main-bazarvoice-ratings">
                {{if $env.bazaarvoiceEnabled}}
                    {{if $env.bvConversion}}
                        <div data-bv-show="rating_summary" data-bv-seo="false" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
            </div>
            {{!-- The following is a panel tag, not a converter, so note subtle difference in syntax --}}
           
            <link rel="preload" href="{{v_$resourceRoot}}css/pdpHeroImage.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
            <link rel="preload" href="{{v_$resourceRoot}}css/pdpLargeImages.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
                <noscript>
                    <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpHeroImage.css">
                    <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpLargeImages.css">
                </noscript>
            {{panel 'pdpHeroImagePanel' class="stickyPanel" /}}

        </div>
        <div id="PDP_block2">
            {{if productV2JsonData.isSephora}}
                <div id="bi-main-container"></div>
            {{/if}}
            <div class="stickyPanel">
              <div id="product-specifications">
                  <link rel="preload" href="{{v_$resourceRoot}}css/pdpPrdConfig.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
                  <link rel="preload" href="{{v_$resourceRoot}}css/pdpGroupPricing.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
                  <noscript>
                      <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpPrdConfig.css">
                      <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpGroupPricing.css">
                  </noscript>
                  <div id="PDP_product_configuration">
                      {{panel:'pdpPrdConfigPanel'}}
                  </div>
              </div>
              <div id="kmncarousel-add_buy_carousel-anchor"></div>
          </div>
        </div>
    </div>
	<div id="PDP_product_details">
		{{panel:'pdpDetailsTabPanel'}}
	</div>
  <div id="kmncarousel-add_buy_carousel_mobile-anchor"></div>
	{{else}}
	<div id="PDP_colGrid">
        <div id="PDP_block1">
            {{if productV2JsonData.productTitle}}
                <div class="pdp-title-container" id="{{id:'pdpTitle'}}">
                    {{if productV2JsonData.isSephora}}
                        <div class="sephora-logo">
                            <img src="{{v_$env.resourceRoot}}kds-web-core/assets/sephora/tags/large-k.svg" alt="Sephora logo" />
                        </div>
                    {{/if}}
                    <h1 class="product-title">
                        {{:productV2JsonData.productTitle}}
                    </h1>
                    {{if productV2JsonData.brand}}
                        <div class="sub-product-title"> by <a href="{{:productV2JsonData.brandSeoUrl}}">{{:productV2JsonData.brand}} </a></div>
                    {{/if}}
                </div>
            {{/if}}
            <div class="pdp-main-bazarvoice-ratings">
                {{if $env.bazaarvoiceEnabled}}
                    {{if $env.bvConversion}}
                        <div data-bv-show="rating_summary" data-bv-seo="false" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
            </div>
			{{panel 'pdpHeroImagePanel' class="sticky-panel" /}}
			<div id="PDP_product_details" style="width:100%;">
				{{panel:'pdpDetailsTabPanel'}}
			</div>
        </div>
        <div id="PDP_block2">
            <div id="product-specifications">
                <div id="PDP_product_configuration">
					{{panel:'pdpPrdConfigPanel'}}
                </div>
            </div>
        </div>
    </div>
	{{/if}}
</div>
</script>

<script id="tpl-pdpContent-mobile" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="PDP_breadcrumb">
		{{panel:'pdpBreadcrumbPanel'}}
    </div>
    <div id="PDP_product_name">
		{{panel:'pdpTitlePanel'}}
    </div>
    <div id="PDP_colGrid_mobile">
        <div id="PDP_block1_mobile">
            {{if productV2JsonData.productTitle}}
                <div class="pdp-title-container" id="{{id:'pdpTitle'}}">
                    <h1 class="product-title">
                        {{:productV2JsonData.productTitle}}
                    </h1>
                    {{if productV2JsonData.brand}}
                        <div class="sub-product-title"> by <a href="{{:productV2JsonData.brandSeoUrl}}">{{:productV2JsonData.brand}} </a></div>
                    {{/if}}
                </div>
            {{/if}}
            <div class="pdp-main-bazarvoice-ratings">
                {{if $env.bazaarvoiceEnabled}}
                    {{if $env.bvConversion}}
                        <div data-bv-show="inline_rating" data-bv-seo="false" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
            </div>
            {{!-- The following is a panel tag, not a converter, so note subtle difference in syntax --}}
			{{panel 'pdpHeroImagePanel' class="pdp_mobile_heroimage"/}}
        </div>
        <div id="PDP_block2_mobile">
            <div id="product-specifications">
                <div id="PDP_product_configuration">
					{{panel:'pdpPrdConfigPanel'}}
                </div>
            </div>
        </div>
    </div>
	<div id="PDP_product_details" style="width:96%;">
		{{panel:'pdpDetailsTabPanel'}}
	</div>
</div>
</script>
<script id="tpl-pdpPrdConfig" type="text/x-jsrender">
{{if productV2JsonData.productStatus && productV2JsonData.productStatus.toLowerCase()=='out of stock' && productV2JsonData.environment != 'staging' && !productV2JsonData.isBopusEligible && !productV2JsonData.isBossEligible}}
<div id="{{id:'root'}}" class="product_boss_tmpl"> 
	<input type="button" title= "Out of stock" alt="Out of stock" id="outOfStock" value="OUT OF STOCK"/>
</div>
{{else}}
<div id="{{id:'root'}}" class="product_boss_tmpl">
	<div id="PDP_z1Social_badge"></div>
	{{for productV2JsonData.teaserData ~gwpLink=productV2JsonData.staticContents.gwppwpDetailsLink }}
		<div class="gwppwpTeaserMsg">
			<div class="get-teaser"></div>
			<div class="gwp-pwp-teaser">
				{{:configuredOfferMessage}}
				{{if isBuyProduct}}
					{{if isPwpOffer}}
						<span><a href="javascript:void(0);" class="learn-more-link" id="pdp-gwp-pwp" onClick="Kjs.pdpDetailsTabPanel.Evt_openTab('#tabpwp', 'Special Savings Item');" title="{{:~gwpLink}}">{{:~gwpLink}}</a></span>
					{{else}}
						<span><a href="javascript:void(0);" class="learn-more-link" id="pdp-gwp-pwp" onClick="Kjs.pdpDetailsTabPanel.Evt_openTab('#tabgwp', 'giftwithpurchase');" title="{{:~gwpLink}}">{{:~gwpLink}}</a></span>
					{{/if}}
				{{/if}}
			</div>
		</div>
	{{/for}}

	{{if productV2JsonData.rebate && productV2JsonData.rebate.shortDescription}}
	<div class="pdp-rebates">
		<p class="pdp-rebatetxt">
			{{:productV2JsonData.rebate.shortDescription}}
			<span><a href="javascript:void(0)" class="learn-more-link" onClick="Kjs.pdpDetailsTabPanel.Evt_openTab('#tabRebate', 'rebates');" id="pdp-rebatelnk" title="Learn More">Learn More</a></span>
		</p>
	</div>
	{{/if}}
	<div id="pdp-Pricing">
		<ul class="pricingList">
			<li>
				{{if productV2JsonData.SKUSGroupPricing}}
					{{panel:'pdpGroupPricingPanel'}}
				{{else}}
					<div id="pdpprice-price-container">
						{{for ~productV2JsonData=productV2JsonData productV2JsonData.price tmpl=~getPriceTemplate(productV2JsonData) /}}
						{{for ~Offers=productV2JsonData.price.yourPriceInfo.appliedOffers  ~price=productV2JsonData.price tmpl='tpl-pdp-pricing-ypappliedoffers' /}}
						{{for ~Offers=productV2JsonData.price.yourPriceInfo.saveMoreOffers ~price=productV2JsonData.price tmpl='tpl-pdp-pricing-ypsavemoreoffer' /}}
					</div>
				{{/if}}
				<ul class="subpricinglist tag-nostyle-fatc-excl-price">
					{{for ~badges= ~getBogoBadges(productV2JsonData.badges)  productV2JsonData.price tmpl='tpl-pdp-pricing-promotion' /}}
					{{for tmpl='tpl-pdp-pricing-kc-section' /}}
				</ul>
				{{if productV2JsonData.price.percentageOff && productV2JsonData.price.percentageOff.length}}
					<div class="perOff">
						{{:~perOffPrice(productV2JsonData.price.percentageOff)}}
					</div>
				{{/if}}
			</li>
			{{if productV2JsonData.surcharges != null && productV2JsonData.surcharges != "" && productV2JsonData.surcharges.length>0}}
				<li>
					<div class="surcharge-msg">
					{{if productV2JsonData.surcharges != null && productV2JsonData.surcharges != "" && productV2JsonData.surcharges.length>0}}
						<div class="surcharge-fee">
							{{if productV2JsonData.surcharges[0].shippingService == null}}
								Shipping Surcharge <span>${{:productV2JsonData.surcharges[0].value}}</span>
							{{else}}
								Surcharge with <span class='surcharge_value'><a id='shipping_retuns' href='javascript:void(0);' title='{{:productV2JsonData.surcharges[0].shippingService}}'>{{:productV2JsonData.surcharges[0].shippingService}}</a></span> ${{:productV2JsonData.surcharges[0].value}}
							{{/if}}
						</div>
					{{/if}}
					</div>
				</li>
			{{/if}}
			{{if productV2JsonData.exclusions && productV2JsonData.exclusions.shortDescription}}
				<li>
					<div class="pdp-exclusions">
						<p class="pdp-exclusion-msg">{{:productV2JsonData.exclusions.shortDescription}}</p>
					</div>
				</li>
			{{/if}}
			{{if productV2JsonData.price.promotion && productV2JsonData.price.promotion.tieredPrice != null && productV2JsonData.price.promotion.tieredPrice != ""}}
				<li class="tag-nostyle-fatc-excl-price">
					<div class="tiered-price red-font">
						INSTANT SAVINGS
						{{for productV2JsonData.price.promotion.tieredPrice}}
							<span>{{>#data}}</span>
						{{/for}}
					</div>
				</li>
			{{/if}}
			{{if productV2JsonData.ShowGwpPwpElements}}
				{{if productV2JsonData.gwp && productV2JsonData.gwp.isMultipleGiftsAllowed == false}}
					<li class="gwp-terms-label tag-nostyle-fatc-excl-price">
						<div class="gwp-terms-label">{{:productV2JsonData.staticContents.gwpTermsLabel}}</div>
					</li>
				{{/if}}
				{{if productV2JsonData.pwp && productV2JsonData.pwp.isMultipleGiftsAllowed == false}}
					<li class="gwp-terms-label tag-nostyle-fatc-excl-price">
						<div class="gwp-terms-label">{{:productV2JsonData.staticContents.pwpTermsLabel}}</div>
					</li>
				{{/if}}
			{{/if}}
		</ul>
	</div>
	<div class="pdp-sephora-badges">
	{{for ~sephoraBadges(productV2JsonData.badges)}}
		<span class="sephora-pdp-badges">{{:title}}</span>
	{{/for}}
	</div>
	{{if productV2JsonData.swatchImages.length > 0}}
    <div class="pdp-product-color clearfix{{if productV2JsonData.SKUSGroupPricing}} pdp-colors-group-pricing{{/if}}">
		{{for productV2JsonData  ~productID=productV2JsonData.webID tmpl= "tpl-pdp-color-swatches" /}}
    </div>
	{{/if}}
	{{if productV2JsonData.variants.sizeList }}
	    <div class="pdp-product-size">
		    {{for productV2JsonData  ~env=$env  ~productID=productV2JsonData.webID tmpl= "tpl-pdp-size" /}}
		</div>
	{{/if}}
	{{if $env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC }}
		<div class="vgc-panel">
			{{for tmpl= "tpl_PDP_VGC_tmpl" /}}
		</div>
	{{/if}}
	{{if !productV2JsonData.productType.isVGC && (productV2JsonData.variants.displaySizeGuide || $env.fitPredictor) }}
        <div class="fit-element">
            {{if productV2JsonData.variants.displaySizeGuide }}
				<div class="sizeguidewrapper">
                	<a href="javascript:void(0);" id="{{id:true 'sizeguide'}}" class="sizeguide" title="{{:productV2JsonData.staticContents.pdpSizeGuide}}">{{:productV2JsonData.staticContents.pdpSizeGuide}}</a>
				</div>
            {{/if}}
            {{if $env.fitPredictor }}
                <div class="fp-root" data-product-id="{{:productV2JsonData.webID}}"></div>
            {{/if}}
        </div>
	{{/if}}
	{{if productV2JsonData.isEarlyAccessProduct && $env.isGuestUser !== false}}
		{{for $env=$env tmpl= "tpl-early-access" /}}
	{{else}}
		<div id="pdp-scarcity-badge"></div>
		{{if !($env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC) }}
			<div class="pdp-product-qty-wrapper">
				<div class="pdp-product-quantity-info">
					<div class="quantity" id="{{id: 'pdpQuantity'}}">
						<label for="productQuantity" id="quantity-label" class="qty-label">Quantity</label><br/>
						<div class="pdp-quantity clearfix">
							<ul class="qty-wrap clearfix {{getItem!=undefined?(getItem.isMultipleGiftsAllowed?'':(getItem.isGiftProdPDP)?'isget':''):''}}">
								<li>
									<div class="pdp-qtty pdp-qtty-boss-decrease" tabindex="0" role="button" data-qttyoptn="decrease">
									<p class="pdp-product-decrease  pdp-qtty-boss-disable"></p>
									</div>
								</li>
								<li class="product-quantity-value-wrapper">
									<input  name="productQuantity" id="{{id: 'productQuantity'}}" alt="Product quantity" title="Product quantity" type="text" value="{{:productV2JsonData.cart != undefined ? productV2JsonData.cart.quantity:productV2JsonData.Qty}}" class="pdp-product-quantity" maxlength="3" size="2" data-qttyoptn="change" onpaste="return false" onkeyup="" autocomplete="off" {{:(getItem)?(getItem.isMultipleGiftsAllowed?'':(getItem.isGiftProdPDP)?'disabled':''):''}}/>
								</li>
								<li>
								<div class="pdp-product-increase pdp-qtty pdp-qtty-boss-increase" data-qttyoptn="increase"  tabindex="0" role="button">
									<p>+</p>
								</div>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="inventory-message">
		<img src="./../../../../../../../designsystem.kohls.com/assets/2.34.0/icons/input-warning-icon.svg" class="quantity-warning-icon" alt="quantity-warning-icon" />
		<img src="./../../../../../../../designsystem.kohls.com/assets/2.34.0/icons/alert/cross-icon.svg" class="close-icon" alt="close-icon" />
		<div class="pdp-qty-inventory-msg">
			<div class="pdp-qty-inventory-msg-txt"></div>
		</div>
		</div>
		{{/if}}

	{{if productV2JsonData.valueAddedIcons && productV2JsonData.valueAddedIcons.length > 0 && productV2JsonData.badges}}
			<div class="prod_badgesblock">
				{{for ~getBadgesForPDP(productV2JsonData.badges,$env.resourceRoot,$env.enableDiePhaseTwo)}}
					{{if ~isExcludeBadges(#data) }}
						<img title="{{:title}}" alt="{{:title}}" src="{{_imageURL}}" />
					{{/if}}
				{{/for}}
			</div>
		{{/if}}
		{{if $env.showAvailLoader }}
			<div class="fulfillment-loader hide">
				<img src="{{_$env.resourceRoot}}images/ajax_loader_30x30.gif" alt="ajax_loader">
			</div>
		{{/if}}

	<div class="pdp-shipment-info-boss clearfix{{if productV2JsonData.productType.isVGC}} vgc-addtobag{{/if}}" {{if productV2JsonData.isGiftCard}}style="display:none"{{/if}}>
		{{for productV2JsonData ~env=$env ~productType=productV2JsonData.productType tmpl= "pdp_shipment_info_boss" /}}
	</div>
		{{if $env.enableNotifyMe }}
			<div class="pdp-notify-`info clearfix">
			</div>
		{{/if}}

		<div class="product_boss_tmpl{{if productV2JsonData.enableFavoriting}} favorite-enabled{{/if}}" id="{{id: 'addToBagPDP'}}">
			<div class="pdp-product-addtobag addtoBagContainerBOSS">
				<div class="addtobag-container">
					{{if productV2JsonData.showOOSbutton}}
						<input type="button" title= "Out of stock" alt="Out of stock" id="outOfStock" value="OUT OF STOCK"/>
					{{else}}
						<div class="addtobag-action {{if $env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC}} addtoBagContainerVGC {{/if}}">
							<div class="add-to-bag-pdp">
								<input type="button" title= "Add To Cart" alt="Add To Cart" class="{{:(productV2JsonData.cart != undefined) ?'pdp-updatebag':'pdp-addtobag'}} {{if !productV2JsonData.isSkuAvailable}}muted{{/if}}" id="addtobagID" name="{{:(productV2JsonData.gwp != undefined || productV2JsonData.pwp != undefined)?'get-':''}}pdp-addtobag" data-prodId="{{:productV2JsonData.webID}}" value="{{:(productV2JsonData.cart != undefined)?'Update Cart':'Add to Cart'}}"/>
								<img alt="kohls_img404" class="submit-loader" id="submit_loaderID_631486" src="{{_$env.resourceRoot}}images/loading.svg"/>
							</div>
						</div>
					{{/if}}
				</div>	
			</div> 
		</div>
		
		
		{{if productV2JsonData.enableFavoriting}}
			<div id="{{id: 'pdpFavorite'}}" class="pdp-favorite-wrap"> 			
				<svg class="pdp-favorite{{if productV2JsonData.saveForLaterAdded}} active{{/if}}" viewBox="0 0 31.9 28.2"><path fill="none" d="M22.8 1h-.2a7.3 7.3 0 0 0-6.4 3.6l-.1.1H16l-.1-.1A7.5 7.5 0 0 0 9.4 1h-.1C8.1 1 7 1.3 6 1.8S4.1 2.9 3.2 3.6 1.9 5.4 1.6 6.4 1 8.6 1 9.7c0 3.8 1.5 6.9 9.9 14.1l3.3 2.8c.5.5 1.1.7 1.8.7s1.3-.2 1.8-.7l3.3-2.8c8.4-7.1 9.8-10.2 9.8-14.1 0-1.1-.2-2.2-.6-3.3-.4-1-.9-2.1-1.8-2.8A8.2 8.2 0 0 0 22.8 1z"/><path fill="#FFF" d="m21.8 11.1-6.7 6.8c-.3.3-.7.3-1 .1l-.1-.1-3.9-4a.8.8 0 0 1 0-1.1c.3-.3.7-.3 1-.1l.1.1 3.4 3.4 6.1-6.2v-.1a1 1 0 0 1 1 0c.4.4.4.9.1 1.2z"/></svg>
			</div>
		{{/if}}

		<div class="DY_social_proof"></div>
	{{/if}}
	  {{!--
        {{if productV2JsonData.valueAddedIcons && productV2JsonData.valueAddedIcons.length > 0 && productV2JsonData.badges}}
          <div class="prod_badgesblock">
            {{for ~getBadgesForPDP(productV2JsonData.badges,$env.resourceRoot,$env.enableDiePhaseTwo)}}
               {{if ~isExcludeBadges(#data) }}
               <img title="{{:title}}" alt="{{:title}}" src="{{_imageURL}}" />
               {{/if}}
            {{/for}}
           </div>
         {{/if}}
       --}}
</div>
{{/if}}

</script>
<script id="tpl-early-access" type="text/x-jsrender">
	<div class="kohls-rewards-early-access">
		<div class="kohls-rewards-available">This item is currently available only to Kohl's Rewards Members from Sept 29 to Oct 1.</div>
		<div class="kohls-rewards-early-access-bottom">
			<div class="kohls-rewards-logo"><img src="{{_$env.resourceRoot}}/images/pdp/Kohls-Rewards-Logo-Horizontal.png" alt="Kohls Rewards"/></div>
			<div>Sign in or join now to get early access to the entire Crayola x Kohl's collection and much more.</div>
			<div>
				<a href='/myaccount/signin.jsp?icid=earlyaccessjoin'>
					<input type="button" title= "Join Kohls Rewards" alt="Join Kohls Rewards" id="joinKohlsRewards" value="Join Kohls Rewards" />
				</a>
			</div>
			<div>Already a Member? <span><a href='/myaccount/signin.jsp?icid=earlyaccesslogin'>Sign in</a></span></div>
		</div>
	</div>
</script>

<script id="tpl-pdp-pricing-loyaltyV2-Earn" type="text/x-jsrender">
	{{if showPlus}}
		<p class="pdpprice-kc-row-earn-plus tag-nostyle-fatc-excl-price">
		<img src="{{_~env.resourceRoot}}images/lpf/power-of-the-plus.png" class="pdpprice-kc-row-earn-icon" alt="Plus">
		{{:kohlsRewards}}
		</p><img src="{{_~env.resourceRoot}}images/pdp/pdp-kohls-rewards.png" class="rewardsIcon" alt="rewards">
	{{else}}
		<p class="pdpprice-kc-row-earn tag-nostyle-fatc-excl-price">
		{{:kohlsRewards}}
		</p><img src="{{_~env.resourceRoot}}images/pdp/pdp-kohls-rewards.png" class="rewardsIcon" alt="rewards">
	{{/if}}
</script>

<script id="tpl-pdp-pricing-kohlsCash" type="text/x-jsrender">
	<p class="pdpprice-kc-row tag-nostyle-fatc-excl-price">
		<a href="{{:kohlsCash.kcExclusions}}" title="details" class="pdpprice-kc-row-link" target="_new" id="" skuId="">
			<img src="{{_~env.resourceRoot}}images/lpf/02-kohl-s-cash-3-d.png" class="pdpprice-kc-row-icon" width="29" height="22" alt="Wallet icon.">
			get {{:~kcEarned(kohlsCash)}} Kohl&#39;s Cash {{:~kcMessage(kohlsCash)}}
			<span class="pdpprice-kc-row-details" >details</span>
		</a>
	</p>
</script>

<script id="tpl-pdp-pricing-rewards-anonymous" type="text/x-jsrender">
	<p class="pdpprice-kc-row-earn-nonloggedin">
		{{if productV2JsonData.isKohlsCashAvail}}
			<img src="{{_~env.resourceRoot}}images/lpf/power-of-the-plus.png" class="pdpprice-kc-row-earn-icon" alt="Plus">
		{{/if}}
		{{if $env.hasKcc}}
			{{:productV2JsonData.staticContents.KohlsRewardsNMCardHolderaLabel}}
		{{else}}
			{{:productV2JsonData.staticContents.kohlsRewardsNonMemberLabel}}
		{{/if}}
	</p><img src="{{_$env.resourceRoot}}images/pdp/pdp-kohls-rewards.png" class="rewardsIcon" alt="rewards">
</script>

<script id="tpl-pdp-pricing-kc-section" type="text/x-jsrender">
		{{if $env.rewardsPilot}}
			{{if productV2JsonData.isKohlsCashAvail || productV2JsonData.isLoyaltyKohlsCashAvail}}
				<li>
					{{if productV2JsonData.isKohlsCashAvail}}
						{{include productV2JsonData.price ~env=$env tmpl='tpl-pdp-pricing-kohlsCash' /}}
					{{/if}}
					{{if productV2JsonData.isLoyaltyKohlsCashAvail}}
						<div class="kohlsCash loyaltyV2KohlsCashContainer tag-nostyle-fatc-excl-price">
							{{include productV2JsonData.loyaltyKohlsCash ~env=$env tmpl='tpl-pdp-pricing-loyaltyV2-Earn' /}}
						</div>
					{{/if}}
				</li>
			{{/if}}
		{{else}}
			{{if productV2JsonData.isKohlsCashAvail || productV2JsonData.nonLoggedinRewardsMessage}}
				<li>
					{{if productV2JsonData.isKohlsCashAvail}}
						{{include productV2JsonData.price ~env=$env tmpl='tpl-pdp-pricing-kohlsCash' /}}
					{{/if}}
					{{if productV2JsonData.nonLoggedinRewardsMessage}}
						{{include ~env=$env tmpl='tpl-pdp-pricing-rewards-anonymous' /}}
					{{/if}}
				</li>
			{{/if}}
		{{/if}}

</script>

<script id="tpl-pdp-pricing-promotion" type="text/x-jsrender">
	{{if ~badges && ~badges.length>0}}
	<li>
		<div id="pdpprice-price-promotion-container">
			<p class="pdpprice-row5">
				{{for ~badges}}
					{{if altText === "BUY_1_GET_1_50_PERCENTAGE"}}
						<span class="pdpprice-fifty-percent-off-icon"></span>
						Buy one, get one 50% off 
						{{if ~root.productV2JsonData.SKUSGroupPricing && ~root.productV2JsonData.SKUSGroupPricing.isAtLeastOneClearanceSku}}
							<i>(excludes Clearance)</i>
						{{/if}}
					{{else}}
					<span class="pdpprice-row5-text">
						{{:title}}
					</span>
					{{/if}}
				{{/for}}
			</p>
		</div>
	</li>
	{{/if}}
	{{if promotion && promotion.group !=null }}
	<li>
		<div id="pdpprice-price-promotion-container">
			<p class="pdpprice-row5">
				<span class="pdpprice-row5-text"> {{> promotion.group[0]}}</span>
			</p>
		</div>
	</li>
	{{/if}}

</script>

<script id="tpl-pdp-pricing-ypsavemoreoffer" type="text/x-jsrender">
	{{if ~Offers && ~Offers.length>0}}
		<div id="{{id:'offerCodeContainer'}}">
			{{for ~Offers ~len=~Offers.length}}
				{{if ~len==1 && #data.code && #data.code.length==13}}
					<span class="supc-offer-message">save more {{:~root.productV2JsonData.staticContents.supcOfferMessage}}</span>
					<a href='/myaccount/dashboard.jsp#offers'>
						<img src="./../../../../../../../www.kohls.com/snb/media/images/pdp/pdp-yp-icon.png" class="pdpprice-details-icon" width="12" height="12" alt="Promo details."/>
					</a>
				{{else}}
				<a href="javascript:void(0)" title="details" class="YP_details" data-idx="{{:#getIndex()}}" id="{{:#getIndex()}}" skuId="{{if (~price.skuCode)}}{{:~price.skuCode}}{{/if}}" tag="{{:~yourPriceDate(#data.offerEffectiveDate)}}" data-desc="" data-promo="">
					<p class="pdpprice-row4">
						<span class="pdpprice-row4-text-promocode-savemore your-price-save-more">save more with</br>{{>#data.code}}</span>
						<span class="pdpprice-row4-text-stack">
							{{if #data.discountType == 'dollarOff'}}
								(${{>#data.discountAmount}} OFF)
							{{else}}
								({{>#data.discountAmount}}% OFF)
							{{/if}}
								<img src="./../../../../../../../media.kohlsimg.com/is/image/kohls/20200515-dt-price-story-info-icon_scl=1_fmt=png8_n" class="pdpprice-details-icon" width="14" height="14" alt="Promo details."/>
							{{if #data.paymentMessage}}{{>#data.paymentMessage}}{{else}}at checkout{{/if}}
							{{if #getIndex() < (~len-1)}} and {{/if}}
						</span>
					</p>
				</a>
				{{/if}}
			{{/for}}
		</div>
	{{/if}}
</script>

<script id="tpl-pdp-pricing-ypappliedoffers" type="text/x-jsrender">
	{{if ~Offers && ~Offers.length>0}}
		<span id="{{id:'offerCodeContainer'}}">
			{{for ~Offers ~len=~Offers.length}}
				{{if ~len==1 && #data.code && #data.code.length==13}}
					<span class="supc-offer-message">{{:~root.productV2JsonData.staticContents.supcOfferMessage}}</span>
					<a href='/myaccount/dashboard.jsp#offers'>
						<img src="./../../../../../../../www.kohls.com/snb/media/images/pdp/pdp-yp-icon.png" class="pdpprice-details-icon" width="12" height="12" alt="Promo details."/>
					</a>
				{{else}}
				<a href="javascript:void(0)" title="details" class="YP_details" data-idx="{{:#getIndex()}}" id="{{:#getIndex()}}" skuId="{{if (~price.skuCode)}}{{:~price.skuCode}}{{/if}}" tag="{{:~yourPriceDate(#data.offerEffectiveDate)}}" data-desc="" data-promo="">
					<span class="pdpprice-row4">
						<span class="pdpprice-row4-text-promocode"> with code <span class="pdpprice-row4-text-promocode-bold">{{>#data.code}}</span> at checkout</span>
						<img src="./../../../../../../../www.kohls.com/snb/media/images/pdp/pdp-yp-icon.png" class="pdpprice-details-icon" width="12" height="12" alt="Promo details."/>
					</span>
				</a>
				{{/if}}
			{{/for}}
		</span>
	{{/if}}
</script>
<script id="tpl-pdp-pricing-suppressed" type="text/x-jsrender">
	<p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
		<span class="pdpprice-row1-reg-price">
			{{:~formatRangePriceNew(regularPrice)}} {{:~getRegularPriceLabel(regularPriceType)}}
		</span>
	</p>
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text">
			{{:mainPriceStr}}
		</span>
		<span class="pdpprice-row2-main-text-label-use">
			<a href='javascript:void(0)' class='learnMoreLink' onClick='Kjs.pdpDetailsTabPanel.Evt_openTab("#tabSpecialPricing","specialPricing");'>Learn More</a>
		</span>
	</p>
</script>
<script id="tpl-pdp-pricing-yourprice" type="text/x-jsrender">
	<p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
		<span class="pdpprice-row1-sale-price pdpprice-row1-sale-price-striked">
			{{:~formatRangePriceNew(salePrice)}} {{:~getSalePriceStatusLabel(salePriceStatus)}}
		</span>
		<span class="pdpprice-row1-reg-price pdpprice-row1-reg-price-striked">
			{{:~formatRangePriceNew(regularPrice)}} {{:~getRegularPriceLabel(regularPriceType)}}
		</span>
	</p>
	<span class="pdpprice-row2">
		<span class="pdpprice-row2-main-text pdpprice-row2-main-text-purple">{{:mainPriceStr}}</span>
	</span>
</script>

<script id="tpl-pdp-pricing-SalePrice" type="text/x-jsrender">
	<p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
		<span class="pdpprice-row1-reg-price pdpprice-row1-reg-price-striked">
			{{:~formatRangePriceNew(regularPrice)}} {{:~getRegularPriceLabel(regularPriceType)}}
		</span>
	</p>
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text {{if salePriceStatus && salePriceStatus.toLowerCase() !== 'mixed'}}pdpprice-row2-main-text-red{{/if}}">
			{{:mainPriceStr}}
		</span>
		<span class="pdpprice-row2-main-text-label-sale">{{:~getSalePriceStatusLabel(salePriceStatus, #data.yourPriceInfo)}}</span>
	</p>
</script>
<script id="tpl-pdp-pricing-regularprice" type="text/x-jsrender">
	{{!-- <p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
	</p> --}}
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text">
			{{:mainPriceStr}}
		</span>
	</p>
</script>
<script id="tpl-pdp-color-swatches" type="text/x-jsrender">
		<div class="pdp-product-size_lbls m-top10">
			{{if variants.preSelectedColor && variants.preSelectedColor != 'NO COLOR'}}
				<p class="swatch-label"> Color:
					<span class="sel-color-swatch">{{:variants.preSelectedColor}}</span>
				</p>
			{{/if}}
		</div>
		{{if variants.preSelectedColor && variants.preSelectedColor != 'NO COLOR' && variants.colorList && variants.colorList.length > 1 && ~root.productV2JsonData.showColorSwatchesDropdown}}
			<div class="pdp-colorSwatches-dropdown">
				<select id="{{id:true 'colorswatches_select'}}" aria-expanded="false" alt="selectColor">
					{{for ~colorsObj(variants, ~root.productV2JsonData.webID) ~environment=~root.productV2JsonData.environment}}
						<option value="{{:color}}"
							dataIcon={{:backgroundURL}}
							dataOutOfStock="{{:oosFlag}}"
							{{if oosFlag}} aria-label="{{:color}} - out of stock"{{/if}}
							dataDesc="{{if skuColorDescription}}{{:skuColorDescription}}{{/if}}" {{if ~root.productV2JsonData.preSelectedColor == color}}selected=selected{{/if}} 
							{{if colorFlag===true}}disabled=disabled{{/if}}>
								{{:color}}
						</option>
					{{/for}}
				</select>
			</div>
		{{/if}}
		<div class="pdp-color-swatches-info" id="{{id:'pdpColorSwatch'}}">
			{{for ~colorsObj(variants, ~root.productV2JsonData.webID) ~environment=~root.productV2JsonData.environment}}
			{{if ~environment=="staging" && colorFlag}}
				<div class="pdp-product-swatch-outer {{:~root.productV2JsonData.preSelectedColor == color ? 'color-container':''}}">
					<div class="pdp-product-swatch {{:~root.productV2JsonData.preSelectedColor == color ? 'active':''}}">
						<a data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="color-swatch no-inventory out-of-stock-color" href="javascript:void(0);" style="" data-bg="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}">
							<span style="opacity:0"> out of stock</span>
						</a>
					</div>
				</div>
			{{else colorFlag===false}}
				<div class="pdp-product-swatch-outer {{:~root.productV2JsonData.preSelectedColor == color ? 'color-container':''}}">
					<div class="pdp-product-swatch {{:~root.productV2JsonData.preSelectedColor == color ? 'active':''}}">
						<a data-skuColor="{{:color}}"
							title="{{:color}}"
							class="color-swatch{{if oosFlag}} out-of-stock-color{{/if}}"
							{{if oosFlag}} aria-label="{{:color}} - out of stock" {{/if}}
							href="javascript:void(0);" 
							style="" 
							data-bg="background:url({{:backgroundURL}}) no-repeat" 
							rel="{{:largeImgURL}}">
						</a>
					</div>
				</div>
			{{else colorFlag===true}}
				<div class="pdp-product-swatch-outer {{:~root.productV2JsonData.preSelectedColor == color ? 'color-container':''}}">
					<div class="pdp-product-swatch color-unavailable" title="out of stock">
						<a data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="color-swatch out-of-stock-color" href="javascript:void(0);" style="" data-bg="background:url({{:backgroundURL}}) no-repeat;" rel="{{:largeImgURL}}">
							<span style="opacity:0"> out of stock</span>
						</a>
					</div>
				</div>
			{{/if}}
			{{/for}}
		</div>
</script>
<script id="tpl-pdp-sizeOnlyDropDown" type="text/x-jsrender">
	<select id="{{id:true 'size-dropdown'}}"  aria-expanded="false">
		<option data-skusize="false" class="select-size" selected="selected" disabled = "disabled" hidden="hidden">{{:~root.productV2JsonData.isGiftCard?'Please Choose an Amount':'Please Choose a Size'}}</option>
		{{for ~sizeObj(variants) }}
			{{if ~root.$env.environment=="staging" && sizeFlag}}
				<option class="no-inventory" value='{{>size}}' data-skusize='{{>size}}' disabled="disabled">{{>size}}</option>
			{{else sizeFlag===false}}
				<option {{if oosFlag}}class="size-dropdown-oos" {{/if}}
					{{if (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize && size == ~root.productV2JsonData.variants.preSelectedSize)}}selected='selected'{{/if}} 
					value='{{>size}}' 
					data-skusize='{{>size}}' 
					dataOutOfStock="{{:oosFlag}}">
						{{>size}}
				</option>
			{{else sizeFlag===true}}
				<option value='{{>size}}' 
					data-skusize='{{>size}}' 
					disabled="disabled" 
					dataOutOfStock="{{:oosFlag}}">
						{{>size}}
				</option></a>
			{{/if}}
		{{/for}}
	</select>
</script>
<script id="tpl-pdp-size" type="text/x-jsrender">
	{{if variants.sizeList  ~sizeLength = ~sizLength(variants)}}
		<label {{if ~sizeLength > 12}} for="size-dropdown" {{/if}} class="swatch-label m-top10 swatch-label-display">
				<img alt="swatch-label-warning img" class="swatch-label-warning" src="{{_~root.$env.resourceRoot}}images/warning-glyph-black.svg"/>
			{{:~root.$env.enableVgcRedesign && productType && productType.isVGC?'Amount':~root.productV2JsonData.staticContents.browseStaticSize}}{{:~sizeLength > 12?'':': '}}
			<span class="size-swatch">{{:~sizeLength > 12?'':variants.preSelectedSize && variants.preSelectedSize != "NO SIZE"?variants.preSelectedSize:~root.productV2JsonData.isGiftCard?'Please Choose an Amount':'Please Choose a Size'}}</span>
			{{if variants.displaySizeGuide != undefined && variants.displaySizeGuide && ~root.$env.Fit != true && ~root.$env.fitPredictor != true && ~root.$env.trueFit != true}}
				<div class="sizeguidewrapper">
					<a href="javascript:void(0);" id="{{id:true 'sizeguide'}}" class="sizeguide" title="{{:~root.productV2JsonData.staticContents.pdpSizeGuide}}">{{:~root.productV2JsonData.staticContents.pdpSizeGuide}}</a>
				</div>
			{{/if}}
		</label>
		{{if ~sizeLength <=12}}
			<div class="pdp-waist-size_info clearfix" id="{{id:'pdpSizeSwatch'}}">
				{{for ~sizeObj(variants)}}
					{{if ~root.$env.environment=="staging" && sizeFlag}}
						<a class="pdp-size-swatch size-unavailable no-inventory" title="{{>size}}" data-size='{{>size}}'  href="javascript:void(0);">
							{{>size}}
						</a>
					{{else sizeFlag===false}}
						<a class="pdp-size-swatch {{if oosFlag}}size-oos {{/if}}{{if (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize && size == ~root.productV2JsonData.variants.preSelectedSize)}}active{{/if}}"
							{{if oosFlag}} aria-label="{{>size}} - out of stock" {{/if}}
							title="{{>size}}" 
							data-size='{{>size}}' 
							href="javascript:void(0);">
								{{>size}}
						</a>
					{{else sizeFlag===true}}
						<a class="pdp-size-swatch size-unavailable" title="{{>size}}" data-size='{{>size}}' href="javascript:void(0);">{{>size}}</a>
					{{/if}}
				{{/for}}
			</div>
		{{else}}
			{{if ~root.productV2JsonData.isMobile}}
				{{for ~root.productV2JsonData  ~env=$env  ~productID=~root.productV2JsonData.webID tmpl= "tpl-pdp-sizeOnlyDropDown" /}}
			{{else}}
				<select id="{{id:true 'size-dropdown'}}"  aria-expanded="false">
					<option data-skusize="false" class="select-size" selected="selected" disabled = "disabled" hidden="hidden">
						{{if ~root.productV2JsonData.activeSize == "NO SIZE"}}
							{{:~root.productV2JsonData.isGiftCard?'Please Choose an Amount':'Please Choose a Size'}}
						{{else (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize)}}
							{{:~root.productV2JsonData.preSelectedSize}}
						{{else}}
							{{:~root.productV2JsonData.activeSize}}
						{{/if}}	
					</option>
					{{for ~sizeObj(variants) }}
						{{if ~root.$env.environment=="staging" && sizeFlag}}
							<option class="no-inventory" value='{{>size}}' data-skusize='{{>size}}' disabled="disabled">{{>size}}</option>
						{{else sizeFlag===false}}
							<option 
							{{if oosFlag}}
								class="size-dropdown-oos" 
								data-class="size-dropdown-oos" 
							{{/if}}
							{{if (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize && size == ~root.productV2JsonData.variants.preSelectedSize)}}
								selected='selected'
							{{/if}} 
								value='{{>size}}' 
								data-skusize='{{>size}}'
								dataOutOfStock="{{:oosFlag}}">
									{{>size}}
							</option>
						{{else sizeFlag===true}}
							<option class="size-dropdown-test" 
								value='{{>size}}' 
								data-skusize='{{>size}}' 
								dataOutOfStock="{{:oosFlag}}">
									{{>size}}
							</option></a>
						{{/if}}
					{{/for}}
				</select>
			{{/if}}
		{{/if}}
	{{/if}}
</script>
<script id="tpl_PDP_VGC_tmpl" type="text/x-jsrender">
	<form name="vgc create form" class="vgc-create-form" method="post" id="{{id:'vgcForm'}}">
		<p>{{:productV2JsonData.staticContents.vgcDisclaimerText}}</p>
		<label for="recipient-name" class="vgc-label firstnameField">{{:productV2JsonData.staticContents.vgcRecipientNameLabel}}</label>
		<div class="vgc-error hide"><a title="Please enter a message">Name must be {{:$env.nameFieldMaxVGC}} characters or fewer.</a></div>
		<div class="vgc-error hide"><a title="Please enter a recipient name">Please enter a recipient name.</a></div>
    	<input name="recipient-name" title="Recipient name" alt="Recipient name" value="" class="vgc-input input-firstnameField" maxlength={{:$env.nameFieldMaxVGC+1}} type="text" id="muteCheck"/>
		<label for="recipient-email" class="vgc-label label-emailField">{{:productV2JsonData.staticContents.vgcRecipientEmailLabel}}</label>
		<div class="vgc-error vgc-invalidEmail hide"><a title="Please enter a recipient email">Please enter a valid recipient email address.</a></div>
		<div class="vgc-error hide"><a title="Please enter a recipient email">Please enter a recipient email.</a></div>
    	<input name="recipient-email" title="name@mail.com" alt="Recipient Name" value="" class="vgc-input recipient-emailField" type="text" id="muteCheck" />
		<label for="sender-name" class="vgc-label firstnameField">{{:productV2JsonData.staticContents.vgcSenderNameLabel}}</label>
		<div class="vgc-error hide"><a title="Please enter a message">Name must be {{:$env.nameFieldMaxVGC}} characters or fewer.</a></div>
		<div class="vgc-error hide"><a title="Please enter a name">Please enter a name.</a></div>
		<input name="sender-name" value="" title="Sender name" alt="Sender name" class="vgc-input input-firstnameField" maxlength={{:$env.nameFieldMaxVGC+1}} type="text" id="muteCheck" />
		<label for="sender-email" class="vgc-label label-emailField">{{:productV2JsonData.staticContents.vgcSenderEmailLabel}}</label>
		<div class="vgc-error vgc-invalidEmail hide"><a title="Please enter an email address">Please enter a valid email address.</a></div>
		<div class="vgc-error hide"><a title="Please enter an email address">Please enter an email address.</a></div>
		<input name="sender-email" value=""  title="Sender email" alt="Sender email" class="vgc-input from-emailField" type="text" id="muteCheck"/><br>
		<label for="gift-message" class="vgc-label message">{{:productV2JsonData.staticContents.vgcGiftMessageLabel}}<span class="vgc-maxmessage">&nbsp;({{:$env.characterMaxVGC}} characters max)</span></label>
		<div class="vgc-error hide"><a title="Please enter a message">Message must be {{:$env.characterMaxVGC}} characters or fewer.</a></div>
		<div class="vgc-error hide"><a title="Please enter a message">Please enter a message.</a></div>
		<textarea name="gift-message" maxlength={{:$env.characterMaxVGC+1}} class="vgc-input vgc-message" id="muteCheck">{{:productV2JsonData.staticContents.vgcGiftMessageText}}</textarea>
		<span class="egiftCard-deliverytime-message">{{:productV2JsonData.staticContents.newvgcFulfillmentTypeText}}</span>
	</form>
</script>

<script type="text/x-jsrender" id="pdp_shipment_info_boss">
<!--Checking shipment section -->
<div class="pdp-product-addtobag shipping-fulfillment" id="{{id:'fullfillmentContainer'}}">
	{{if boss && boss != 'false'}}
		<div class="preferred-store">
		{{if boss.storeName}}<span class="my-store-text">Store</span>{{/if}}
		{{if boss.showStar}}
			<span class="star-icon"> <svg role="img" alt="star" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 34 32" height="16" width="17"><g transform="translate(1 1)"><path d="M16.499 25.937l8.196 3.949a1.15 1.15 0 0 0 1.635-1.199l-1.291-8.8a1.145 1.145 0 0 1 .32-.973l6.308-6.36a1.146 1.146 0 0 0-.623-1.937L22.088 9.1a1.148 1.148 0 0 1-.819-.588L17.011.604a1.147 1.147 0 0 0-2.022 0L10.73 8.51c-.168.312-.47.529-.82.588L.957 10.617a1.145 1.145 0 0 0-.624 1.937l6.309 6.36c.254.256.372.617.32.973l-1.291 8.8a1.149 1.149 0 0 0 1.635 1.199l8.196-3.95a1.15 1.15 0 0 1 .998 0z" vector-effect="non-scaling-stroke" fill="#000" fill-rule="nonzero"></path></g></svg> </span>
		{{/if}}
		<span class="{{if boss.isdisabled}}option-disabled {{/if}}my-store-name">
			{{:boss.storeName}}
		</div>
	{{else bopus && bopus != 'false'}}
		<div class="preferred-store">
		{{if bopus.storeName}}<span class="my-store-text">Store</span>{{/if}}
		{{if bopus.showStar}}
			<span class="star-icon"> <svg role="img" alt="star" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 34 32" height="16" width="17"><g transform="translate(1 1)"><path d="M16.499 25.937l8.196 3.949a1.15 1.15 0 0 0 1.635-1.199l-1.291-8.8a1.145 1.145 0 0 1 .32-.973l6.308-6.36a1.146 1.146 0 0 0-.623-1.937L22.088 9.1a1.148 1.148 0 0 1-.819-.588L17.011.604a1.147 1.147 0 0 0-2.022 0L10.73 8.51c-.168.312-.47.529-.82.588L.957 10.617a1.145 1.145 0 0 0-.624 1.937l6.309 6.36c.254.256.372.617.32.973l-1.291 8.8a1.149 1.149 0 0 0 1.635 1.199l8.196-3.95a1.15 1.15 0 0 1 .998 0z" vector-effect="non-scaling-stroke" fill="#000" fill-rule="nonzero"></path></g></svg> </span>
		{{/if}}
		<span class="{{if bopus.isdisabled}}option-disabled {{/if}}my-store-name">
			{{:bopus.storeName}}
		</div>
	{{/if}}
	<div class="fulfillment-container">
	<form id="shipmentSelection">
		<div role="radiogroup">
		{{if bopus && bopus != 'false'}}
			<div role="radio" tabindex="0" id="bopusContainer" class="bossAlign fulfillment-box {{if bopus.isPickupNotAvailable}}not-available{{/if}}" 
				{{if bopus.radioButtonChecked}} aria-checked="true" {{/if}} {{if bopus.isdisabled}} aria-disabled="true" {{/if}} aria-labelledby="bopusTab">
				<div class="ship-to-store-img {{if bopus.isdisabled}} option-disabled {{/if}}">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path class="ship-to-store-svg" fill="#444" fill-rule="nonzero"
						d="M16.875 3.125c1.036 0 1.875.84 1.875 1.875v10.625h.63c.311 0 .569.23.613.532l.007.093a.626.626 0 0 1-.62.625H.62a.626.626 0 0 1 0-1.25h.63V5c0-1.036.84-1.875 1.875-1.875h13.75zm0 1.25H3.125A.625.625 0 0 0 2.5 5v10.625h1.25v-5.313c0-.48.362-.877.828-.93l.11-.007h10.625c.48 0 .877.362.93.828l.007.11v5.312h1.25V5a.625.625 0 0 0-.625-.625zM7.5 10.625H5v5h2.5v-5zm3.75 0h-2.5v5h2.5v-5zm3.75 0h-2.5v5H15v-5zm-.626-4.375c.315 0 .575.23.62.532l.006.093a.63.63 0 0 1-.626.625H5.626A.624.624 0 0 1 5 6.875a.63.63 0 0 1 .626-.625h8.748z" />
				</svg>
				</div>
				<input type="radio" id="bopusTab" name="radio-group" value="bopus" title="FREE Store Pickup Today"
					data-storeId="{{:bopus.storeId}}" data-postalCode="{{:bopus.postalCode}}"
					{{if bopus.isdisabled}} disabled {{/if}} {{if bopus.radioButtonChecked}} checked {{/if}} aria-hidden="true" 
				/>                
					<label for="bopusTab" class="title bopusTab"> 
					{{:bopus.label}}
					</label>
					{{if bopus.notAvailable}}
					<div class="availability">{{:bopus.notAvailable}}</div>
					{{if bopus.isChangeStoreLink}}
						<a href="javascript:void(0);" class="ce-fisCOS s-pickupstore"
						data-postalCode=" {{:bopus.postalCode}}"
						title="Select a store">Change Store</a>
					{{/if}} 
					{{/if}}
					{{if !bopus.isdisabled}}
					<div class="availability">Available</div>
					{{if bopus.storePickupSLA}}<div class="readyTime">{{:bopus.storePickupSLA}}</div>{{/if}}
					{{/if}}                
			</div>
		{{/if}}
		<!-- Ship to store addition -->
		{{if boss && boss != 'false'}}
			<div role="radio" tabindex="0" id="bossContainer" class="bossAlign fulfillment-box" 
				{{if boss.radioButtonChecked}} aria-checked="true" {{/if}} {{if boss.isdisabled}} aria-disabled="true" {{/if}} aria-labelledby="bossTab">
				<div class="ship-to-store-img {{if boss.isdisabled}} option-disabled {{/if}}">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path class="ship-to-store-svg" fill="#444" fill-rule="nonzero"
							d="M16.875 3.125c1.036 0 1.875.84 1.875 1.875v10.625h.63c.311 0 .569.23.613.532l.007.093a.626.626 0 0 1-.62.625H.62a.626.626 0 0 1 0-1.25h.63V5c0-1.036.84-1.875 1.875-1.875h13.75zm0 1.25H3.125A.625.625 0 0 0 2.5 5v10.625h1.25v-5.313c0-.48.362-.877.828-.93l.11-.007h10.625c.48 0 .877.362.93.828l.007.11v5.312h1.25V5a.625.625 0 0 0-.625-.625zM7.5 10.625H5v5h2.5v-5zm3.75 0h-2.5v5h2.5v-5zm3.75 0h-2.5v5H15v-5zm-.626-4.375c.315 0 .575.23.62.532l.006.093a.63.63 0 0 1-.626.625H5.626A.624.624 0 0 1 5 6.875a.63.63 0 0 1 .626-.625h8.748z" />
					</svg>
				</div>
				<input type="radio" id="bossTab" name="radio-group" value="boss" title="FREE Ship to Store"
					data-storeId="{{:boss.storeId}}" data-postalCode="{{:boss.postalCode}}" 
					{{if boss.isdisabled}} disabled {{/if}} 
					{{if boss.radioButtonChecked}} checked {{/if}} aria-hidden="true"
				/>
				<label for="bossTab" class="title bopusTab{{if boss.isdisabled}} option-disabled{{/if}}">
					{{:boss.label}}
				</label>
				{{if !boss.isdisabled}}
					<div class="availability">Available</div>
					{{if boss.storePickupSLA}}<div class="readyTime">{{:boss.storePickupSLA}}</div>{{/if}}
					{{/if}} 
				</div>
		{{/if}}
		{{if ship && ship != 'false'}}
			<div role="radio" tabindex="0" id="shipContainer" class="bossAlign fulfillment-box{{if ship.shipNotAvailable}} not-available{{/if}}" 
				{{if ship.radioButtonChecked}} aria-checked="true" {{/if}} 
				{{if ship.isdisabled}} aria-disabled="true" {{/if}} 
				aria-labelledby="shipTab">
				<div class="ship-to-me-img {{if ship.isdisabled}} option-disabled {{/if}}">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path class="ship-to-me-svg" fill="#666" fill-rule="evenodd"
							d="M3.401 16.5a2.999 2.999 0 0 1 5.198 0H15V5.47c0-.43-.024-.552-.091-.676a.478.478 0 0 0-.205-.201c-.13-.069-.257-.093-.698-.093H3A.75.75 0 0 1 3 3h11.006c.665 0 1.024.068 1.398.266.353.186.634.464.824.813.202.373.272.731.272 1.392V7.5h4.077c.723 0 1.108.074 1.51.289.374.2.673.5.874.875.215.401.289.786.289 1.509V16.5h.107c.355 0 .643.336.643.75s-.288.75-.643.75H21a3 3 0 0 1-6 0H9a3 3 0 0 1-6 0H1.5a.837.837 0 0 1-.4-.092.626.626 0 0 1-.258-.258.837.837 0 0 1-.092-.4c0-.154.032-.289.092-.4a.626.626 0 0 1 .258-.258.837.837 0 0 1 .4-.092v-2.25c0-.154.032-.289.092-.4a.626.626 0 0 1 .258-.258.837.837 0 0 1 .4-.092c.154 0 .289.032.4.092.111.06.199.147.258.258.06.111.092.246.092.4v2.25h.401zM16.5 9v6.401a2.999 2.999 0 0 1 4.099 1.099h1.151v-6.327c0-.496-.03-.648-.112-.802a.613.613 0 0 0-.26-.26c-.153-.082-.305-.111-.8-.111H16.5zM.75 6H6a.75.75 0 0 1 0 1.5H.75a.75.75 0 0 1 0-1.5zm1.5 2.25H6a.75.75 0 0 1 0 1.5H2.25a.75.75 0 0 1 0-1.5zm1.5 2.25H6A.75.75 0 1 1 6 12H3.75a.75.75 0 1 1 0-1.5zm14.25.75a.75.75 0 1 1 0-1.5h2.175c.456 0 .825.37.825.825v2.175a.75.75 0 1 1-1.5 0v-1.5H18zM6 19.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm12 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z" />
					</svg>
				</div>
				<input type="radio" id="shipTab" name="radio-group" value="ship" title="Ship to me"
					{{if ship.isdisabled}} disabled {{/if}} {{if ship.radioButtonChecked}} checked {{/if}} aria-hidden="true" 
				/>
				{{if ship.shipNotAvailable}}
					<div>
						<label for="shipTab" class="title">{{:ship.label}}</label>
						<div class="availability">{{:ship.unavailableLabel}}</div>
					</div>
				{{else}}
					<label for="shipTab" class="title shipTab{{if ship.isdisabled}} option-disabled{{/if}}">            
						{{:ship.label}}    
					</label>
					<div class="availability">Available</div>
					<div class="freeText">{{:ship.text}}</div>
					{{if ship.shipSLA}}<div class="readyTime">{{:ship.shipSLA}}</div>{{/if}}
				{{/if}}          
			</div>
		{{/if}}
		</div>
		{{if oos}}
		<div class="bossAlign">
		<div class="oos-sku"> {{:oos.label}}
		</div>
		</div>
		{{/if}}
	</form>

	{{if checkOtherStoresLink}}
		<div class="addtobag-storepickup-selectstore showFindStorelocatoroverlay bossStore" style="display:block">
				{{:checkOtherStoresLink}}
				<a href="javascript:void(0);" class="ce-fisCOS s-pickupstore"
				data-postalCode="{{if boss && boss != 'false'}}
									{{:boss.postalCode}}
									{{else bopus && bopus != 'false'}}
									{{:bopus.postalCode}}
									{{/if}}"
				title="Select a store">other stores</a>.
		</div>
	{{/if}}
	</div>
</div>
</script>
<script type="text/x-jsrender" id="mosaic_smartcard_popup_template">
	<div class="smartcart-modal">
		<div class="sc-message"><p class="sc-added">{{:staticContents.savingLauncherTitle }}</p></div>
		<!-- Start: sc-container -->
		<div class="sc-container">
			<!-- Start: sc-rpanel -->
			<div class="sc-lpanel">
				<div class="sc-image"><img src="{{_modeldata.cartItems[0].itemProperties.image.url.replace(_wid=180____wid=250_).replace(_hei=180____hei=250_)_}}" alt="{{~convertHTML(modeldata.cartItems[0].itemProperties.productTitle) }}" /></div>
				<h1 class="sc-title">{{:modeldata.cartItems[0].itemProperties.productTitle }}</h1>
				<!-- Pricing -->
				<div class="mainprice-container">
					{{if price.salePriceStatus != null && price.salePriceStatus.toLowerCase() == 'sale' }}
						<div class="main-pricelabel">
							<div class="main-pricelabel red-font">{{:price.salePriceStatus }}</div>
							<div class="main-price red-font">
								{{if price.salePrice && price.salePrice.minPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.salePrice.minPrice) }}
								{{/if}}
								{{if price.salePrice && price.salePrice.maxPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.salePrice.maxPrice) }}
								{{/if}}
							</div>
						</div>
						<div class="regorg-small">{{:price.regularPriceType}}&nbsp;
							{{if price.regularPrice && price.regularPrice.minPrice}}
								${{:Kjs.pdpUtils.formatPrice(price.regularPrice.minPrice) }}
							{{/if}}
							{{if price.regularPrice && price.regularPrice.maxPrice}}
								${{:Kjs.pdpUtils.formatPrice(price.regularPrice.maxPrice) }}
							{{/if}}
						</div>
					{{else}}
						<div class="main-pricelabel">
							<div class="main-pricelabel">{{:price.regularPriceType}}</div>
							<div class="main-price">
								{{if price.regularPrice && price.regularPrice.minPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.regularPrice.minPrice) }}
								{{/if}}
								{{if price.regularPrice && price.regularPrice.maxPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.regularPrice.maxPrice) }}
								{{/if}}
							</div>
						</div>
					{{/if}}
				</div>
				<!-- Your Pricing -->
				{{if $env.YP_enable && price.yourPriceInfo && price.yourPriceInfo.appliedOffers && price.yourPriceInfo.appliedOffers.length }}
					<div class="your-Price"></div>
				{{/if}}
			</div>
			<!-- End: sc-lpanel -->
			<!-- Start: sc-rpanel -->
			<div class="sc-rpanel">
				<div class="sc-option-icon"></div>
				<div class="sc-option-msg">{{:staticContents.savingLauncherSubTitle }}</div>
				<div class="sc-options">
					<div id="change-to-ship" class="sc-optcont selected">
						<div class="sc-optcont-left">
							{{:staticContents.savingLauncherShipPromoMessage.replace('{0}',modeldata.incentiveDetails.incentiveThreshold) }}
						</div>
						<div class="sc-optcont-right"></div>
					</div>
					<div id="change-to-bopus" class="sc-optcont">
						<div class="sc-optcont-left">
							{{:staticContents.savingLauncherPickUpPromoMessage.replace('{0}',modeldata.incentiveDetails.storeDetails.storeName) }}
						</div>
						<div class="sc-optcont-right"></div>
					</div>
				</div>
			</div>
			<!-- End: sc-rpanel -->
		</div>
		<!-- End: sc-container -->
	</div>
 </script>
<!-- Notify Me Changes -->
<script id="pdp_notify_me" type="text/x-jsrender">
	{{if notifyMeData.showNotifyButton || notifyMeData.status=="NOT_FOUND"}}
		<div class="clear notify-line">
			<p class="notify-text-bold">{{:staticContents.notifyMeItemOutOfStock}}</p>
			<p class="notify-msg">{{:~convertHTML(staticContents.notifyMeWewillSendYouTextmessage)}}</p>
			<span><input type="text" id="{{id:true 'inputnotifyme'}}" class="input-field add_ph_mask_filed" maxlength="13" title="Area code and number" alt="Area code and number" placeholder="Area code and number" name="notify_phone"></span>
			<span><input type="button" maxlength="13" class="notify-btn" id="notify-me" name="pdp-notify" title="Notify me" alt="Notify me" value="NOTIFY ME"></span>
			<p class="notify-msg-phone">{{:staticContents.notifyMeYourPhoneNumberUseThisItemOnly}}</p>
		</div>
	{{else}}
		{{if (notifyMeData.status=="USER_ALREADY_SUBSCRIBED") && notifyMeData.enabled && !notifyMeData.verified}}
			<div class="clear notify-line">
				<p class="notify-text-bold">{{:staticContents.notifyMeItemOutOfStock}}</p>
				<p class="notify-msg"><img src="./../../../../../../../www.kohls.com/snb/media/images/ic-warning.png" alt="notifyMe-icon" class="notifyme-icon"/>{{:staticContents.notifyMeVerifyYourMobileDeviceInstock}}</p>
				{{if notifyMeData.showRemoveButton}}<p class="remove-me"><a class="cancel-subscription" href="#">{{:staticContents.notifyMeCancelNotification}}</a></p>{{/if}}
			</div>
		{{else notifyMeData.status=="SUCCESS" && notifyMeData.enabled}}
			<div class="clear notify-line">
				<p class="notify-text-bold">{{:staticContents.notifyMeThanksYouAllset}}</p>
				<p class="notify-msg">{{:staticContents.notifyMeWillsendTextNotification}}</p>
				{{if notifyMeData.showRemoveButton}}<p class="remove-me"><a class="cancel-subscription" href="#">{{:staticContents.notifyMeCancelNotification}}</a></p>{{/if}}
			</div>
		{{else notifyMeData.status=="USER_ALREADY_SUBSCRIBED" && notifyMeData.enabled && notifyMeData.verified}}
			<div class="clear notify-line">
				<p class="notify-text-bold">{{:staticContents.notifyMeAlreadySignup}}</p>
				<p class="notify-msg">{{:staticContents.notifyMeWillsendTextNotification}}</p>
				{{if notifyMeData.showRemoveButton}}<p class="remove-me"><a class="cancel-subscription" href="#">{{:staticContents.notifyMeCancelNotification}}</a></p>{{/if}}
			</div>
		{{/if}}
	{{/if}}
</script>
<!-- Notify Me Changes Ends -->

<script id="favorite_modal" type="text/x-jsrender">
	<div id="{{id: 'pdp-favorite-modal'}}">
				<div class="pdp-favorite-bg"></div>
				<div class="pdp-favorite-window">
					<div class="pdp-favorite-content">
						<header>
							<h1>Saved for later!</h1> 
						</header>
						<section>
							<p>This item has been added to your Cart in your <b>Saved for Later</b> list. <a href="/checkout/shopping_cart.jsp" class="go-to-cart">Go to Cart</a></p>
						</section>
						<footer>
							<p>Did saving this item meet your expectations?</p>
							<svg class="thumbsUp" viewBox="0 0 32 30.5"><path fill="#676460" d="M29.7 15.7c-.2.6-.3 1.2-.1 1.8.1.5.3 1.4-.1 2.3-.3.6-.3 1.2-.1 1.8.1.2.4 1.5-.7 2.5-.4.4-.7.9-.7 1.5 0 .7-.4 1.4-.9 1.9-1 .8-2.6 1.2-4.8 1-4.2-.4-8.3-.5-12.5-.5V15.2c0-1 0-1.9.6-2.7 2.3-3.2 3.1-6.6 3.7-8.8l.5-1.7c1.5.2 3 1.3 3 4.2v4.2c0 .8.6 1.5 1.4 1.5h7.2c2.5 0 2.9.4 3.3.9.6.7.7 1.7.2 2.9zM2.8 28c-.4 0-.8-.4-.8-.9V15.6c0-.5.4-.8.8-.8h5V28h-5zm28.7-11.1v-.5c1-2.5.2-4.1-.6-5-1-1.1-2.2-1.5-4.8-1.5h-6.6V6.2c0-4.3-2.7-6.2-5.3-6.2-1.3 0-1.6 1.4-2.1 3.3-.5 2.1-1.2 5.2-3.3 8.1l-.7 1.4H2.8C1.3 12.7 0 14 0 15.6v11.6C0 28.7 1.3 30 2.8 30h6c.1 0 6.5-.1 13.3.5h1.2c2.2 0 3.9-.5 5.1-1.6a5 5 0 0 0 1.6-3.3c1.3-1.2 1.8-2.9 1.3-4.6v-.3c.7-1.5.4-3 .2-3.8z"/></svg>
							<svg class="thumbsDown" viewBox="0 0 32 30.5"><path fill="#676460" d="M29.7 14.8c-.2-.6-.3-1.2-.1-1.8.1-.5.3-1.4-.1-2.3-.3-.6-.3-1.2-.1-1.8.1-.2.4-1.5-.7-2.5-.5-.4-.7-.9-.8-1.4 0-.7-.4-1.4-.9-1.9-1-.8-2.6-1.2-4.8-1-4.1.3-8.2.4-12.4.4v12.7c0 1 0 1.9.6 2.7 2.3 3.2 3.1 6.6 3.7 8.8.1.6.3 1.4.5 1.7 1.5-.1 3-1.3 3-4.2V20c0-.8.6-1.5 1.4-1.5h7.2c2.5 0 2.9-.4 3.3-.9.6-.6.7-1.6.2-2.8zM2.8 2.5c-.4 0-.8.4-.8.9V15c0 .5.4.8.8.8h5V2.5h-5zm28.7 11.1v.5c1 2.5.2 4.1-.6 5-1 1.1-2.2 1.5-4.8 1.5h-6.6v3.7c0 4.3-2.7 6.2-5.3 6.2-1.3 0-1.6-1.4-2.1-3.3-.5-2.1-1.2-5.2-3.3-8.1l-.7-1.4H2.8A2.8 2.8 0 0 1 0 14.8V3.4C0 1.8 1.3.5 2.8.5h6c.1 0 6.5.1 13.3-.5h1.2c2.2 0 3.9.5 5.1 1.6A5 5 0 0 1 30 4.9V5a4.5 4.5 0 0 1 1.2 4.5v.3c.8 1.5.5 3 .3 3.8z"/></svg>
						</footer>
					</div>
				</div>
			</div>
</script>

<script id="tpl-pdpDetailsTab" type="text/x-jsrender">
    {{for  tmpl='tpl-pdpDetailsTab-desktop' /}}
</script>
<script id="tpl-pdpDetailsTab-desktop" type="text/x-jsrender">
    <div id="{{id:'root'}}" class="PDP_prdDetails_comp">
        <link rel="preload" href="{{v_$resourceRoot}}css/pdpDetailsTab.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		<noscript>
			<link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpDetailsTab.css">
		</noscript>
        <div id="{{id:'pdpTabContent'}}" class="pdpTabContent">
        {{if (productV2JsonData.productDetails && productV2JsonData.productDetails != '') }}
            <button id="tabProductDetails" class="tablinks {{:(productV2JsonData.defaultAccordion=='productDetails')?'active':''}}" tabContentId="productDetailsTabContent">product details</button>
            <div id='productDetailsTabContent' class="tabcontent {{:(productV2JsonData.defaultAccordion=='productDetails')?'active':''}}"><div class="inner">{{:productV2JsonData.productDetails}}
				<p id='skuProductDetails'></p>
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
        </div> 
        {{/if}}
        {{if (productV2JsonData.sizing && productV2JsonData.sizing != '') }}
            <button id='tabSizing' class="tablinks {{:(productV2JsonData.defaultAccordion=='sizing')?'active':''}}" tabContentId="sizingTabContent">sizing</button>
            <div id='sizingTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.sizing}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
            </div>
        {{/if}}
        {{if (productV2JsonData.ingredients && productV2JsonData.ingredients != '') }}
            <button id="tabIngredients" class="tablinks {{:(productV2JsonData.defaultAccordion=='ingredients')?'active':''}}" tabContentId="ingredientsTabContent">ingredients</button>
            <div id='ingredientsTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.ingredients}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
		</div>
        {{/if}}
        {{if (productV2JsonData.howToUse && productV2JsonData.howToUse != '') }}
            <button id="tabHowtoUse" class="tablinks {{:(productV2JsonData.defaultAccordion=='howToUse')?'active':''}}" tabContentId="howToUseTabContent">how to use</button>
            <div id='howToUseTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.howToUse}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
		</div>   
        {{/if}}
        {{if (productV2JsonData.aboutTheBrand && productV2JsonData.aboutTheBrand != '') }}
            <button id="tabAboutTheBrand" class="tablinks {{:(productV2JsonData.defaultAccordion=='aboutTheBrand')?'active':''}}" tabContentId="aboutTheBrandTabContent">about the brand</button>
            <div id='aboutTheBrandTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.aboutTheBrand}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
		</div>    
        {{/if}}
        {{if (productV2JsonData.careInstructions && productV2JsonData.careInstructions != '') }}
            <button id="tabCareInstructions" class="tablinks {{:(productV2JsonData.defaultAccordion=='careInstructions')?'active':''}}" tabContentId="careInstructionsTabContent">care instructions</button>
            <div id='careInstructionsTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.careInstructions}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
		</div>      
        {{/if}}
        {{if (productV2JsonData.shippingAndReturn && productV2JsonData.shippingAndReturn != '') }}
            <button id='tabShippingAndReturns' class="tablinks {{:(productV2JsonData.defaultAccordion=='shippingAndReturn')?'active':''}}" tabContentId="shippingAndReturnTabContent">shipping & returns</button>
            <div id='shippingAndReturnTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.shippingAndReturn}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
		</div> 
        {{/if}}
        {{if (productV2JsonData.relatedInformation && productV2JsonData.relatedInformation != '') }}
             <button id="tabRelatedInformation" class="tablinks {{:(productV2JsonData.defaultAccordion=='relatedInformation')?'active':''}}" tabContentId="relatedInformationTabContent">{{: productV2JsonData.marketplaceitem ? 'Seller Information; Report Suspicious Activity' : 'related information'}}</button>
            <div id='relatedInformationTabContent' class="tabcontent"><div class="inner">{{:productV2JsonData.relatedInformation}}
				<div class="seemoreParentDiv">
					<button class="seemore"></span>
				</div>
			</div>
		</div>    
        {{/if}}
        {{if (productV2JsonData.gwp && productV2JsonData.gwp != '') }}
            <button id="tabgwp" class="tablinks {{:(productV2JsonData.defaultAccordion=='gwp')?'active':''}}" tabContentId="gwpTabContent">gift with purchase</button>
            <div id='gwpTabContent' class="tabcontent">
				<div class="inner">
					{{if (productV2JsonData.teaserData && productV2JsonData.teaserData != '') }}
						{{for tmpl="tpl-pdpDetailsTab-gwp" /}}
					{{/if}}
					<div class="seemoreParentDiv">
						<button class="seemore"></span>
					</div>
            </div>
		</div>
        {{/if}}
        {{if (productV2JsonData.pwp && productV2JsonData.pwp != undefined && productV2JsonData.pwp != '') }}
            <button id="tabpwp" class="tablinks {{:(productV2JsonData.defaultAccordion=='pwp')?'active':''}}" tabContentId="pwpTabContent">Special Savings Item</button>
            <div id='pwpTabContent' class="tabcontent">
				<div class="inner">
					{{if (productV2JsonData.teaserData && productV2JsonData.teaserData != '') }}
						{{for tmpl="tpl-pdpDetailsTab-gwp" /}}
					{{/if}}
					<div class="seemoreParentDiv">
						<button class="seemore"></span>
					</div>
				</div>
            </div>
        {{/if}}
        {{if (productV2JsonData.rebate && productV2JsonData.rebate != undefined && productV2JsonData.rebate.longDescription != null) }}
            <button id='tabRebate' class="tablinks {{:(productV2JsonData.defaultAccordion=='rebate')?'active':''}}" tabContentId="rebateTabContent">rebates</button>
            <div id='rebateTabContent' class="tabcontent">
                <div class="inner">
					{{:productV2JsonData.rebate.longDescription}}
					<div class="seemoreParentDiv">
						<button class="seemore"></span>
					</div>
                </div>
            </div>
        {{/if}}
        {{if (productV2JsonData.exclusions && productV2JsonData.exclusions.longDescription != null) }}
            <button class="tablinks {{:(productV2JsonData.defaultAccordion=='exclusions')?'active':''}}" tabContentId="exclusionsTabContent">exclusions</button>
            <div id='exclusionsTabContent' class="tabcontent">
                <div class="inner">
					{{:productV2JsonData.exclusions.longDescription}}
					<div class="seemoreParentDiv">
						<button class="seemore"></span>
					</div>
                </div>
            </div>
        {{/if}}
        {{if (productV2JsonData.pricingDetails && productV2JsonData.pricingDetails != '') }}
            <button id='tabSpecialPricing' class="tablinks {{:(productV2JsonData.defaultAccordion=='pricingDetails')?'active':''}}" tabContentId="pricingDetailsTabContent">special pricing</button>
            <div id='pricingDetailsTabContent' class="tabcontent">
                <div class="inner">
					{{:productV2JsonData.pricingDetails}}
					<div class="seemoreParentDiv">
						<button class="seemore"></span>
					</div>
                </div>
            </div>
        {{/if}}
        {{if (productV2JsonData.vendorDetails && productV2JsonData.vendorDetails != '') }}
            <button id='tabVendorDetails' class="tablinks {{:(productV2JsonData.defaultAccordion=='vendorDetails')?'active':''}}" tabContentId="vendorDetailsTabContent">vendor details</button>
            <div id='vendorDetailsTabContent' class="tabcontent">
                <div class="inner">
					{{:productV2JsonData.vendorDetails}}
					<div class="seemoreParentDiv">
						<button class="seemore"></span>
					</div>
                </div>
            </div>
        {{/if}}
        </div>
    </div>
</script>
<script type='text/x-jsrender' id='tpl-pdpDetailsTab-gwp'>
    {{if productV2JsonData.teaserData.getProductDetails && productV2JsonData.teaserData.getProductDetails.getProducts}}
		<div class="qualify-message">{{:productV2JsonData.teaserData.initialMsg}}</div>
        <div class="offer-message">{{:productV2JsonData.teaserData.dynamicOfferMessage}}</div>
		<div class="carousel external-control">
			<div class="prev-button"><button class="prev-item disabled"><div class="carousel-button"></div></button></div>
			<div id="giftpro" class="fleft getCarousel">
				<ul>
					<li class="adjustprice">
					{{for productV2JsonData.teaserData.getProductDetails.getProducts}}
							<div class="item get-item">
							<div class="get-image {{:regularPrice==''?'zero-dollar':''}}">
									<a href="{{:seoUrl}}{{:seoUrl=='' ?'javascript:void(0);':''}}" title="{{:displayName}}"><img src="{{_imageURL}}" alt="{{:displayName}}"/>
								<span class="get-prod-title">{{:displayName}}</span></a>
								</div>
								{{if yourPrice != 'undefined' && yourPrice != '' && yourPrice != undefined }}
									<p class="get-prod-yourprice price-hide">Your Price:&nbsp;{{:yourPrice}}</p>
								{{/if}}
								{{if salePrice != 'undefined' && salePrice != '' && salePrice != undefined && ~root.productV2JsonData.pwp }}
									<p class="get-prod-saleprice">Sale:&nbsp;{{:salePrice}}</p>
								{{else}}
									{{if ~root.productV2JsonData.pwp }}
										<p class="get-prodprice">Regular:&nbsp;{{:regularPrice}}</p>
									{{else regularPrice != ''}}
										<p class="get-prodprice">Value:&nbsp;{{:regularPrice}}</p>
									{{/if}}
								{{/if}}
							</div>
						{{if (#index+1) % 4 == 0 && ~root.productV2JsonData.teaserData.getProductDetails.getProductCount > (#index+1)}}
						</li>
						<li class="adjustprice">
						{{/if}}
					{{/for}}
					</li>
				</ul>
			</div>
		</div>
        <div class="clear"></div>
    {{/if}}
</script>

<script type='text/x-jsrender' id='tpl-pdpDetailsTab-tab'>
    <button id="{{:~id}}" class="tablinks {{:~isDefault?'active':''}}" tabContentId="{{:~contentId}}">{{:~tabName}}</button>
    <div class="separator"></div>
    <div id='{{:~contentId}}' class="tabcontent" style="display:{{:~isDefault?'block':'none'}}">{{:~details}}
		<p id='skuProductDetails'></p>
	</div>
    <div class="seemore_cnt_mobile  disabled">
        <span class="seemore_mobile" id="seemore-{{:~contentId}}"><img src="{{v_$env.resourceRoot}}images/pdp/rectangle.png"/> </span>
    </div>
</script>
<script id="tpl-pdpFloatingCart-main" type="text/x-jsrender">
<div id="{{id:'root'}}">
	<link rel="preload" href="{{v_$resourceRoot}}css/pdpFloatingCart.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		<noscript>
			<link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpFloatingCart.css">
		</noscript>
	<div id="{{id:true 'floating-atc-equity'}}">
		<div class="floating-atc-content">
			<div class="floating-atc-left">
				<div class="floating-atc-img"><img src="{{_imageSrc}}"/></div>
				<div class="floating-atc-info">
					<div class="floating-atc-title"><p class="floating-atc-prod-title">{{:productTitle}}</p></div>
					<div class="floating-atc-swatches" id="{{id:'info'}}">
						{{if productColor}}<div class="floating-atc-color-swatch">Color: <span>{{>productColor}}</span></div>{{/if}}
						{{if productSize}}<div class="floating-atc-size-swatch">Size: <span>{{>productSize}}</span></div>{{/if}}
					</div>
				</div>
			</div>
			<div class="floating-atc-right">
				<div class="floating-atc-price" id="{{id:'pricing'}}">
					{{for ~productV2JsonData=~root.productV2JsonData productV2JsonData.price tmpl=~getPriceTemplate(productV2JsonData) /}}
				</div>
				<div class="floating-atc-finalize" id="{{id:'finalize'}}">
					<div class="floating-atc-shipping">
						<p class="floating-atc-boss" id="{{id:'boss'}}">{{:shippingText}}</p>
						<p class="floating-atc-store" id="{{id:'store'}}">{{:shippingStoreText}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</script>
<script id="tpl-pdpAdSense" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-afsh-container">
    <link rel="preload" href="{{v_$resourceRoot}}css/pdpAdSense.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpAdSense.css">
    </noscript>
    {{if $env.enable_afsh}}
        <div class="gglad_block_afshcontainer ggl_sponsered_links">
            <div class="ggl_greybg">Sponsored Links</div>
            <div class="ggl_helplink_container">
                <span class="ggl_helptext_afsh">What&#39;s this?</span>
            </div>
            <div id="afshcontainer"></div>
            <div class="ggl_tooltip_content_afsh">
                <p class="ggl-tooltip-title">
                    Sponsored Links
                <img src="./../../../../../../../www.kohls.com/snb/media/images/ggl_close_btn.png" alt="closebtn" class="afsh_close_help"/>
                </p>
                <p class="ggl_help_content">{{:$env.googleAdSense_whatsthis_helptext}}</p>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8">
        $ready(function _$ready_disableGoogleAdSenseThumbnails($){
            var afshContainer = ".gglad_block_afshcontainer";
            if(window.disableGoogleAdSenseThumbnails == true){
                $(afshContainer).hide();
            }
        });
        {{:"<"}}/script>
    {{/if}}
    {{if $env.enable_googleDFP_product_details_page && $env.enable_googleDFP_product_details_page_728x90}}
		<div id="dfp_leaderboard_728x90" style="text-align: center;"></div>
	{{/if}}
    <div class="pdp-afs-dfpRectangle">
        <div class="pdp-afs-container">
            {{if $env.afsh_show_bottom_rail_ads && $env.enable_afsh}}
                <div class="gglad_block_adcontainer1 ggl_sponsered_links">
                    <div class="ggl_greybg">Sponsored Links</div>
                    <div class="ggl_helplink_container">
                        <span class="ggl_helptext">What&#39;s this?</span>
                    </div>
                    <div id="adcontainer1"></div>
                </div>
            {{/if}}
            {{if $env.enable_admarketplace}}
                <div class="gglad_block_adcontainer5 ggl_sponsered_links" id="admarketplace-container">
                    <div class="ggl_greybg">Sponsored Links</div>
                    <div class="ggl_helplink_container">
                        <span class="ggl_helptext">What&#39;s this?</span>
                    </div>
                    <div id="amp_unit"></div>
                </div>
            {{/if}}
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
        var Dsp_numberofproducts = '',
            show_bottom_rail_ads = true,
            pageName = 'regularProductPage',
            queryVal = "{{:productV2JsonData.productTitle}}",
            channelVal = 'null';

        var pubIdForMVT = 'partner-kohls-pdp';
        var queryForMVT = "{{:productV2JsonData.productTitle}}";
        if(show_bottom_rail_ads){
            function getNumberOfAds(numAdsMap) {
                var adelem, adnameNum;
                for (var name in numAdsMap) {
                    adnameNum = name + ' has ' + numAdsMap[name] + ' ads';
                    if(numAdsMap[name] <= 0) {
                        adelem = ".gglad_block_" + name;
                        $(adelem).hide();
                        if (name == 'adcontainer1' && (enable_admarketplace != 'undefined')) {
                            showAdMarketPlace();
                        }
                    }
                }
            }
            if(queryVal != "" && (window.disableGoogleAdSenseText == undefined || window.disableGoogleAdSenseText == false)){
                    var pageOptions = {
                    'linkTarget': "{{:$env.linkTarget}}",
                    'pubId': "{{:$env.googleAds_pubId_partnerKohls}}",
                    'query': "{{if productV2JsonData.metaInfo!=null}}{{:productV2JsonData.metaInfo[0].metaDescription}}{{else}}0{{/if}}",
                    'hl': "{{:$env.hl}}",
                    'domainLinkAboveDescription': "{{:$env.domainLinkAboveDescription}}",
                    'detailedAttribution': "{{:$env.detailedAttribution}}",
                    'attributionText': '',
                    'adPage': "{{:$env.adPage}}",
                    'channel': channelVal,
                    'adsResponseCallback': getNumberOfAds,
                    'adtest': '{{:$env.adtest}}',
                    'titleBold': "{{:$env.titleBold}}",
                    'plusOnes': "{{:$env.plusOnes}}",
                    'sellerRatings': "{{:$env.sellerRatings}}",
                    'adLayout': "{{:$env.pageOptionsAdLayout}}",
                    'siteLinks': "{{:$env.siteLinks}}"
                };

                var adblock1 = {
                    'container': 'adcontainer1',
                    'width': "{{:$env.adblock_bottomrail_width}}",
                    'number': "{{:$env.adblock_bottomrail_number}}",
                    'lines': "{{:$env.adblock_bottomrail_lines}}",
                    'fontFamily': "{{:$env.adblock_fontFamily}}",
                    'fontSizeTitle': "{{:$env.adblock_fontSizeTitle}}",
                    'fontSizeDescription': "{{:$env.adblock_fontSizeDescription}}",
                    'fontSizeDomainLink': "{{:$env.adblock_fontSizeDomainLink}}",
                    'fontSizePlusOnes': "{{:$env.adblock_bottomrail_fontSizePlusOnes}}",
                    'colorTitleLink': "{{:$env.adblock_colorTitleLink}}",
                    'colorText': "{{:$env.adblock_colorText}}",
                    'colorDomainLink': "{{:$env.adblock_colorDomainLink}}",
                    'colorBackground': "{{:$env.adblock_colorBackground}}",
                    'colorBorder': "{{:$env.adblock_colorBorder}}",
                    'borderSelections': "{{:$env.adblock_borderSelections}}",
                    'colorPlusOnes': "{{:$env.adblock_bottomrail_colorPlusOnes}}",
                    'noTitleUnderline': "{{:$env.adblock_bottomrail_noTitleUnderline}}",
                    'detailedAttribution': "{{:$env.adblock_bottomrail_detailedAttribution}}",
                    'longerHeadlines': "{{:$env.longerHadblock_bottomrail_longerHeadlineseadlines}}"
                };
                _googCsa('ads', pageOptions, adblock1);
            }
        }
    {{:"<"}}/script>

    <script type="text/javascript">
    $ready(function _$ready_google_adsense($) {
        mypage = $(location).attr('href');
        isSearchPage = mypage.match(/search.jsp/gi);
        if(!isSearchPage){
            $(".ggl_helptext_2").on('click',function(){
                var offset = $(this).offset();
                topPosition = offset.top + 15;
                leftPosition = offset.left - 245;
                if($(".kohls-ad-wallpaper").length > 0){
                    var topPositionNew = $(".ggl_tooltip_content").parents("#container").offset().top;
                    var leftPositionNew = $(".ggl_tooltip_content").parents("#container").offset().left;
                    topPosition = topPosition - topPositionNew;
                    leftPosition = leftPosition - leftPositionNew;
                }
                $('.ggl_tooltip_content').toggle();
                $('.ggl_tooltip_content').css({'top':topPosition,'left':leftPosition});
            });

            $('.ggl_helptext').on('click', function(e){
                var offset = $(this).offset();
                var topPosition = offset.top+15;
                var leftPosition = offset.left-1;
                if($(".kohls-ad-wallpaper").length > 0){
                    var topPositionNew = $(".ggl_tooltip_content").parents("#container").offset().top;
                    var leftPositionNew = $(".ggl_tooltip_content").parents("#container").offset().left;
                    topPosition = topPosition - topPositionNew;
                    leftPosition = leftPosition - leftPositionNew;
                }
                $('.ggl_tooltip_content').toggle();
                $('.ggl_tooltip_content').css({'top':topPosition,'left':leftPosition});
            });
            $('.ggl_helptext_afsh').on('click', function(e){
                var topPosition = 32;
                var adsWidth = $(".gglad_block_afshcontainer.ggl_sponsered_links").width();
                adsWidth = adsWidth - 325;
                $('.ggl_tooltip_content_afsh').toggle();
                $('.ggl_tooltip_content_afsh').css({'top':topPosition,'left':adsWidth+'px'});
            });
            $('.close_help_container').on('click', function(){
                $(".ggl_tooltip_content").hide();
            });

            $('.afsh_close_help').on('click', function(){
                $(".ggl_tooltip_content_afsh").hide();
            });
        }
    });
    {{:"<"}}/script>

    {{if $env.enable_google_ads_site && $env.enable_afsh}}
    <script type="text/javascript" charset="utf-8">
        if((window.disableGoogleAdSenseThumbnails == undefined || window.disableGoogleAdSenseThumbnails == false) && (window.enableBottomrailThumbnailAds == undefined || window.enableBottomrailThumbnailAds == true)){
            var container = "afshcontainer";
            var channel = '';
            var pageName = 'regularProductPage';
            var afshParameters = "tops and tees womens dana buchman";
            var pThree = productV2JsonData.monetizationData.subCategory ? productV2JsonData.monetizationData.subCategory : "";
            var queryValue = "";
            function hideContainer(containerName, adsLoaded)
            {
                if (!adsLoaded){
                    var adelem = ".gglad_block_" + container;
                    $(adelem).hide();
                }
            }
            function hideContainer2()
            {
                var adelem2 = ".gglad_block_" + container;
                $(adelem2).hide();
            }
            function enableAfsAll() {
                queryValue = afshParameters;
                if (!isQueryEmpty()) {
                    if(channel != "") {
                        channel+="+";
                    }
                    channel += "test_pdp_query_allsignals";
                }
            }
            function enableSubcategory() {
                queryValue = pThree;
                if (!isQueryEmpty()) {
                    if(channel != "") {
                        channel+="+";
                    }
                    channel += "test_pdp_query_p3";
                }
            }
            function isQueryEmpty() {
                if (queryValue === "") {
                    queryValue = "{{:productV2JsonData.productTitle}}";
                    queryValue = queryValue.replace(/[^\w\s]/gi, '');
                    return true;
                }
                return false;
            }
            if (pageName === "regularProductPage" || pageName === "collectionPDPPage") {
                if (window.enablePdpAfsAll == true && window.enablePdpAfsSubcategory == false) {
                    enableAfsAll();
                } else if (window.enablePdpAfsAll == false && window.enablePdpAfsSubcategory == true){
                    enableSubcategory();
                } else {
                    var queryGroup = "test_pdp_query_p3";
                    if (queryGroup != null && queryGroup != "googleAds_AFSh_PDP_query_value") {
                        queryGroup = queryGroup.toLowerCase();
                        if (queryGroup != "test_pdp_query_p3") {
                            enableAfsAll();
                        } else {
                            enableSubcategory();
                        }
                    } else {
                        enableAfsAll();
                    }
                }
            } else {
                queryValue = "{{:productV2JsonData.productTitle}}";
            }
            if (queryValue != "") {
                var afsh_pageOptions = {
                    'pubId'    : "{{:$env.afsh_pdp_pubId}}",
                    'query'    : queryValue,
                    'channel'  : channel,
                    'adsafe'   : "{{:$env.adsafe}}",
                    'adtest'   : "{{:$env.adtest}}",
                    'hl'       : "{{:$env.hl}}",
                    'theme'    : "{{:$env.afsh_background_theme}}",
                    'priceMin' : "{{:$env.afsh_priceMin}}",
                    'priceMax' : "{{:$env.afsh_priceMax}}",
                    'priceCurrency': "{{:$env.afsh_priceCurrency}}",
                    'linkTarget'   : "{{:$env.linkTarget}}"
                };
                var afsh_adblock = {
                    'container'        : container,
                    'width'            : "{{:$env.afsh_width}}",
                    'height'           : 265,
                    'adLoadedCallback' : hideContainer
                };
                _googCsa('plas', afsh_pageOptions, afsh_adblock);
            } else {
                hideContainer2();
            }
        }
    {{:"<"}}/script>
    {{/if}}
</script>
<script id="tpl-pdpBDRecs" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <link rel="preload" href="{{v_$resourceRoot}}css/pdpBDRecs.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
		    <noscript>
			    <link rel="stylesheet" href="{{v_$resourceRoot}}css/pdpBDRecs.css">
		    </noscript>
{{if $env.bigDataEnabled && $env.bigDataRecs }}
    <div id="bd_rec_{{:$env.bdRenderingPlacement}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
    <div id="bd_rec_{{:$env.bdRenderingPlacementH2}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
{{/if}}
</div>
</script>
<script id="tpl-pdpBVDetails" type="text/x-jsrender">
    {{for  tmpl='tpl-pdpBVDetails-desktop' /}}
</script>

<script id="tpl-pdpBVDetails-desktop" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-BV-tabs clearfix">
    {{if $env.bvConversion}}
        <div data-bv-show="review_highlights" data-bv-product-id="{{:productV2JsonData.webID}}" style="margin:10px 0 40px 0;"></div>
    {{/if}}
    <div class="product-cd">
        <ul class="productcdMenu" role="tablist">
            {{if $env.bazaarvoiceEnabled}}
            <li role="tab"><a id="bv-content-show" class="rating-content active" onclick="sc_bvTabOpen('{{:productV2JsonData.webId}}')" href="#rating-content">Ratings &amp; Reviews</a>
            </li>
            <li role="tab">
                <a href="#qa-content" id="Qusetionstab" onclick="sc_bvQATabOpen('{{:productV2JsonData.webId}}')">
                Q&A</a>
            </li>
            <script type="text/javascript" >
                var numberOfReviews = "{{:$env.numberOfReviews}}";
                var isValidforBV = "{{:$env.isValidforBV}}";
            {{:"<"}}/script>
            {{/if}}
        </ul>
        <div id="qa-content" class="display-none">
            <div class="prod_description1">
                <div class="Bdescription">
                {{if $env.bvConversion}}
                    {{if $env.bvLazyLoading}}
                        <div id="bvQuestions" data-bv-productId="{{:productV2JsonData.webID}}" style="min-height:150px">
                            <div>
                                <img src="./../../../../../../../www.kohls.com/snb/media/images/pmp_imgs/pmp_search_loader.gif" class="bv_review_loader" alt="review loader"/>
                            </div>
                        </div>
                    {{else}}
                        <div data-bv-show="questions" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
                </div>
            </div>
            <div class="hor_separator"></div>
        </div>
        {{if $env.bazaarvoiceEnabled}}
            <div class="hidden_accordion_segments">
                <div id="ratings-segment"></div>
            </div>
            <div id="rating-content" >
                {{:productV2JsonData.bvResponse}}
                {{if $env.bvConversion}}
                    {{if $env.bvLazyLoading}}
                        <div id="bvReviews" data-bv-productId="{{:productV2JsonData.webID}}" style="min-height:150px">
                            <div>
                                <img src="./../../../../../../../www.kohls.com/snb/media/images/pmp_imgs/pmp_search_loader.gif" class="bv_review_loader" alt="review loader"/>
                            </div>
                        </div>
                    {{else}}
                        <div data-bv-show="reviews" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
                <div id="submissionContainerUrlParameters" style="display: none;"></div>
                <div id="submissionContainerUrl" style="display: none;"></div>
                <div class="hor_separator"></div>
            </div>
        {{/if}}
    </div>
</div>

</script>

<script id="tpl-pdpBVDetails-mobile" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-BV-tabs clearfix">
    <div class="product-cd">
        {{if $env.bazaarvoiceEnabled}}
            <div id="rating-content" >
                {{:productV2JsonData.bvResponse}}
                {{if $env.bvConversion}}
                    {{if $env.bvLazyLoading}}
                        <div id="bvReviews" data-bv-productId="{{:productV2JsonData.webID}}" style="min-height:150px">
                            <div>
                                <img src="./../../../../../../../www.kohls.com/snb/media/images/pmp_imgs/pmp_search_loader.gif" class="bv_review_loader" alt="review loader"/>
                            </div>
                        </div>
                    {{else}}
                        <div data-bv-show="reviews" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
                <div id="submissionContainerUrlParameters" style="display: none;"></div>
                <div id="submissionContainerUrl" style="display: none;"></div>
                <div class="hor_separator"></div>
            </div>
        {{/if}}
    </div>
</div>
</script>
<script id="tpl-pdpDFPAds" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div class="dfpThreeAmigosAd">
        {{if $env.enable_googleDFP_product_details_page}}
            <div id="pdp-threeamigos">
                {{if $env.enable_googleDFP_collection_page}}
                    <div class="amigo-container">
                        {{if $env.enable_googleDFP_collection_page_300x250_bottomleft}}
                            <div id="dfp_medium_rectangle_300x250_bottomleft" class="dfp_medium_rectangle_300x250_bottomleft">
                            </div>
                        {{/if}}
                    </div>
                    <div class="amigo-container">
                        {{if $env.enable_googleDFP_collection_page_300x250_bottommiddle}}
                            <div id="dfp_medium_rectangle_300x250_bottommiddle" class="dfp_medium_rectangle_300x250_bottommiddle">
                            </div>
                        {{/if}}
                    </div>
                    <div class="amigo-container">
                        {{if $env.enable_googleDFP_collection_page_300x250_bottomright}}
                            <div id="dfp_medium_rectangle_300x250_bottomright" class="dfp_medium_rectangle_300x250_bottomright">
                            </div>
                        {{/if}}
                    </div>
                {{/if}}
            </div>
        {{/if}}
    </div>
</div>
</script>
<script id="tpl-pdpFeaturedProducts" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="promote-iq-main"></div>
    {{if $env.isNewHookLogicEnabled}}
        <div id="ProductDetailWeb-Carousel"></div>
    {{/if}}
</div>
</script><script id="tpl-pdpTrendage" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="trendage" class="trendage">
    {{if $env.enableTrendageGrid }}
        <div id="trendage_grid" class="trendage_grid"></div>
    {{/if}}
    </div>
    {{if $env.enableTrendageCarousal }}
    <div id="trendage_carousal" class="trendage_carousal"></div>
    {{/if}}
</div>
</script><script id="tpl-pdpWebCollageInline" type="text/x-jsrender">
<div id="{{id:'root'}}">
{{if $env.enableWebcollageInline }}
    <div id="wc-power-page"></div>
{{/if}}
</div>
</script><script id="offer_popup_template" type="text/x-jsrender">
    <div class="popup-container offerPopupModal">
        <div>
            <a href="javascript:void(0)" class="close-popup kas-close-popup close-modal" alt="Close popup" title="Close Popup"></a>
            <p class='logo'><img src="{{_$env.resourceRoot}}images/wallet-images/print-kohls-logo.png" alt="Kohl's logo" /></p>
            <h3>{{:description}}</h3>
            <div class="promo-code-cont">
                {{if code.length > 2}}
                    <span class="promo-code-label kas-promo-code-label">PROMO CODE: </span>
                    <span class="promo-code-value kas-promo-code-value">{{:code}}</span>
                {{/if}}
                <p class="offer-start-end kas-offer-start-end">{{:offerEffectiveDate.endFullDate}}</p>
            </div>
        </div>
        <div class="clear"></div>
        <div class="barcode-section">
                <div class="barcode kas-barcode" id="popupBarcode"></div>
                <p><span class="barcode-number kas-barcode-number">{{:barCode}}</span>
                </p>
            </div>
        <div class="mid-section kas-mid-section">
            <div class="promocode-container-print kas-promocode-container-print">
                {{if code.length > 2}}
                    <p class="promo-code">PROMO CODE: <span class="promo-code-lable">{{:code}}<span></p>
                {{/if}}
            </div>
            <p class="coupon-valid-txt kas-coupon-valid-txt normal-text-transform">{{:termsConditions}}</p>
        </div>
        <div class="bottom-section kas-bottom-section">
            <ul>
                <li class="print-link kas-print-link"><a href="javascript:void(0)" alt="Print" title="print" data-printid="0" id="{{:idx}}" skuId="{{:skuId}}" class="go-to-printPopup ce-focusPP"></a></li>
            </ul>
        </div>
    </div>
</script>
