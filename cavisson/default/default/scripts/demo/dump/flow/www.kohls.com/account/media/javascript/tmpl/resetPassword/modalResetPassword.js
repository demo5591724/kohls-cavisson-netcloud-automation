<script id="kohlsForgotPasswordTmpl" type="text/x-jsrender">
	<div class="forgot_password">
		<div class="modal_recpass modal_non_login modal-content" id="modal_recpass" style="width:626px;"> 
			<div id="modal_recpass_title" class="modal_title kas-modal-title">
			{{:staticContents.kls_sta_forgot_modal_reset_your_pass}}
			</div>
			<div id="modal_recpass_instructions" class="kas-instructions"> 
			<p>{{:staticContents.kls_msg_cmdm_profile_reset_pwd_msg}}</p> 
			</div>
			<div class="modal_error_message kas-error-message" id="modal_recpass_error"><ul></ul></div>
			<form id="modal_recpass_form" name="hint_question_answer" method="post">
				<div id="modal_recpass_form_fields" class="modal_recpass_form_fields clearfix kas-modal_recpass_form_fields">
					<div class="modal_input_label title kas-modal-input-label" id="modal_recpass_email_label">
					{{:staticContents.kls_sta_forgot_modal_emailaddress}}
					</div>	
					<label for="user_email" class="ada-lable-user-email" id="lable-user-email">{{:staticContents.kls_sta_forgot_modal_emailaddress}}</label>
					<input id="user_email" maxlength="40" name="user_email" value="" class="input-box modal_input_field email kas-input-email" type="text" aria-labelledby="lable-user-email error-email-area" /> 
					<div class="rd-CaptchaWrap clear"> 
						<div id="recaptcha-kohlsSoftLogin" class="g-recaptcha" style="margin-bottom:10px;margin-top:5px;"></div>  
					</div>
				</div>
				<div id="nudata_container" class="kas-nudata-container"></div>
				<input id="nudataSessionID" name="nd-sessionId" class="kas-nudataSessionID" type="hidden" value="" />
				<div id="modal_create_form_full_buttons">
					<div class="modal_form_buttons kas-modal-form-buttons" id="modal_create_form_buttons">
						<input name="create_acct" class="modal_continue_button  kas-continue-button" value="{{:staticContents.kls_sta_forgot_modal_send}}" id="modal_send_password_button" type="button" />	
						<img src="{{v_~root.$imageRoot('acm')}}images/ajax-loader.gif"  alt="loading" class="submit_loader" style="display:none;float:left;margin-left:40px;margin-top: 10px;">
						<a class="kas-create create" style="display:none;" data-href="/myaccount/modal/kohls_modal_create.jsp" href="javascript:void(0);"> <input id="modal_login_create_button" value="{{:staticContents.kls_sta_forgot_modal_create_account}}" type="button" /></a>
						<p class="modal_cancel_link kas-cancel-link" style="display: none;">
							<a href="#" data-href="/myaccount/modal/kohls_modal_login.jsp" class="kohls_recpass_cancel" style="display: none;">
							{{:staticContents.kls_sta_forgot_modal_cancel}}
							</a> 
						</p>
						<p class="kas-link-softlogin link_softlogin" style="float:left;line-height: 28px;margin-left:20px;display:none;">
							<a href="#" data-href="/myaccount/checkout/common/session_time_out_login.jsp" id="kohls_recpass_cancel">
							{{:staticContents.kls_sta_forgot_modal_cancel}}
							</a> 
						</p>
						<p class="kas-skava-link-softlogin skava_link_softlogin" style="float:left;line-height: 28px;margin-left:20px;display:none;">
							<a href="#" data-href="/checkout/common/session_timeout_modal_login.jsp">
							{{:staticContents.kls_sta_forgot_modal_cancel}}
							</a> 						
						</p>
						<p class="kas-skava-normal-link skava_normal_link" style="float:left;line-height: 28px;margin-left:20px;display:none;">
							<a href="#" data-href="/upgrade/myaccount/modal_login.jsp">
							{{:staticContents.kls_sta_forgot_modal_cancel}}
							</a>
						</p>
						<p class="kas-landingpage-link landingpage_link" style="float: left; line-height: 28px; margin-left: 20px; display:none">
							<button class="modal-recpass-button-cancel" rel="modal:close">{{:staticContents.kls_sta_forgot_modal_cancel}}</button>
						</p>	
					</div>
				</div>
				<div class="clear"></div>
				<script src='./../../../../../../www.google.com/recaptcha/api.js_onload=onloadCallback_render=explicit'>  {{:"<"}}/script>
				<script type="text/javascript">
					var recaptchaWidget = {};
					var onloadCallback = function() {
						$('#recaptcha-kohlsSoftLogin.g-recaptcha').html('');
						$('#recaptcha-kohlsSoftLogin.g-recaptcha').each(function (i, captcha) {
							recaptchaWidget[captcha.id] = grecaptcha.render(captcha, {
								'sitekey' : $env.reCaptchaSecretKey
							});
						});
					};
					var resetReCaptcha = function(widget) {
						grecaptcha.reset(recaptchaWidget[widget]);
					};
				{{:"<"}}/script>
			</form>
		</div>

		<div class="modal_recpass" id="modal_recpass_confirm" style="display:none">
			<div id="modal_recpass_title" class="modal_title">
			{{:staticContents.kls_static_cmdm_profile_check_email_para}}
			</div>
			<div id="modal_recpass_instructions">         
				<p>
				{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_message}}
					<span class="confirm_user_email"><b></b></span>. 
					{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_message_one}}
				</p> 
			</div>
			<div id="modal_create_form_full_buttons">
				<div class="modal_form_buttons" id="modal_create_form_buttons">
						<input name="create_acct" data-href="/myaccount/modal/kohls_modal_login.jsp" class="modal_continue_button kohls_recpass_cancel" value="{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_button_one}}" id="modal_create_continue_button" style="display: none;" type="button" /> 
						<input name="create_acct" style="display:none" class="softlogin_continue" value="{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_button_one}}" type="button" />
						<input name="create_acct" style="display:none" class="skava_softlogin_continue" value="{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_button_one}}" type="button" />
						<input name="create_acct" style="display:none" class="skava_normal_continue" value="{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_button_one}}" type="button" />
						<input name="create_acct" style="display:none" class="modal_continue_button landingpage_continue" value="{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_button_one}}" type="button" />
				</div>
			</div>        
		</div>


	</div>	
</script>
