<script id="sendLinkOTP" type="text/x-jsrender">
	<div id="OTPLinkSend" class="tr-otp-container kas-tr-otp-container">
		<a id="close-otpModal" class="custom-reset-password-close-btn" title="Close overlay" tabindex="0"></a>
		<div class="otp-model-header">
			<span class="otp-model-header-title kas-otp-model-header-title">{{:staticContents.otpForgotPassword}}</span>
		</div>
		<div class="tr-otp-inner-container kas-tr-otp-inner-container">
			<div class="clear"></div>
			<div class="clear tr-otp-box-caption kas-tr-otp-box-caption">
				{{:staticContents.otpAdditionalActionNeeded}}
			</div>
			<div class="clear"></div>
			<div class="tr-otp-box-message kas-tr-otp-box-message"> 
			{{:staticContents.otpToVerifyYourIdentityText}}
			</div>

			{{if userDetails && userDetails.phoneNumber}}
				<div class="tr-otp-masked-number kas-tr-otp-masked-number">(XXX) XXX-<b>{{:userDetails.phoneNumber.slice(6,10)}}</b></div>
			{{/if}}
			
			{{if $env.ksOTPEnterNewNumberLink !== true }}
			<div class="invalid-mobile-number-error-message display-none">
				<span class="ic-alert-circle-glyph"></span>
				<span class="error-message">{{:staticContents.otpPhoneNumberNotRecognized}}</span>
			</div>
			<div class="payfone-identify-text display-none">
				<span>{{:staticContents.otpMobilePhoneNumber}}</span>
			</div>			
			<div class="tr-otp-ask-new-number kas-tr-otp-ask-new-number {{if !userDetails.phoneNumber}} display-none {{/if}}">
				<span class="tr-otp-ask-new-number-link kas-tr-otp-ask-new-number-link"> 
					<a href="javascript:void(0)">{{:staticContents.otpEnterDifferentNumber}}
					</a>
				</span>
			</div>
			<div class="tr-otp-ask-new-number-input kas-tr-otp-ask-new-number-input {{if userDetails.phoneNumber}} display-none {{/if}}">
				<div class="display-none">
					<label>{{:staticContents.otpMobilePhoneNumber}}</label>
				</div>
				<input type="text" maxlength="14" placeholder="{{:staticContents.otpPlaceholdersPhoneNo}}" title = "{{:staticContents.otpPlaceholdersPhoneNo}}" class="new-number kas-new-number" id= "ask-new-number-input"/>
				<span class="cancel-newMobileNumber kas-cancel-newMobileNumber"><a href="javascript:void(0)"></a></span>
			</div>
			<div class="payfone-text-message display-none">    
    			<div class="tr-error-cross-icon"></div>
				<span style="margin-left: 5px;">{{:staticContents.otpEnterVaildNumber}}</span>
			</div>
			{{/if}}
			<div class="tr-otp-massege-valid-link kas-tr-otp-massege-valid-link">
				<div class="tr-otp-massege-valid-link-msg kas-tr-otp-massege-valid-link-msg">{{:Kjs.resetPasswordOTP.replaceText($env.OTPTempLinkDurationMinutes,staticContents.otpTheLinkWillBeActiveForMinutes)}}</div>
			</div>
			<div class="tr-otp-customer-service-massege kas-tr-otp-customer-service-massege">{{:staticContents.otpOrCallCustomerService}}</div>
			<div class="tr-otp-consent">{{:staticContents.otpConsentDisclaimerMassege}}</div>
			<div class="tr-otp-sent-btn-section kas-tr-otp-sent-btn-section">
				<button class="cancel-btn-text-white kas-cancel-btn-text-white">{{:staticContents.otpCancel}}</button>
				<span class="ma-save-address-loader display-none"><img class="submit_loader" src="{{_$env.resourceRoot}}images/ajax-loader.gif" alt="loading"/></span>
				<button class="send-greenBtn-text-white kas-send-greenBtn-text-white">{{:staticContents.otpSendLink}} </button>
			</div>
		</div>
	</div>
</script>

<script id="temporaryLinkSentTpl" type="text/x-jsrender">  
	<div id="temporaryLinkHasBeenSent" class="kas-tr-otp-container tr-otp-container"> 
		<a id="close-otpModal" class="custom-reset-password-close-btn" title="Close overlay" tabindex="0"></a>
		<div class="otp-model-header kas-otp-model-header">
			<span class="otp-model-header-title kas-otp-model-header-title">{{:staticContents.otpLinkSent}}</span>
		</div>
		<div class="tr-otp-inner-container kas-tr-otp-inner-container">
			<div class="kas-tr-otp-box-message tr-otp-box-message" style="margin-top:40px; width: 620px;">{{:Kjs.resetPasswordOTP.replaceText(userDetails.phoneNumber,staticContents.otpCheckMobilePhoneNumber)}}</div>
			<div class="tr-otp-box-caption-bold kas-tr-otp-box-caption-bold">{{:staticContents.otpKeepThisPageOpen}}</div>
				<div class="tr-otp-ask-new-number kas-tr-otp-ask-new-number">
					<span class="tr-otp-ask-new-number-link kas-tr-otp-ask-new-number-link">
						<a href="javascript:void(0)">{{:staticContents.otpRequestAnotherLink}}</a>
					</span>
				</div>
			<div class="tr-otp-massege-valid-link kas-tr-otp-massege-valid-link">
				<div class="tr-otp-massege-valid-link-msg kas-tr-otp-massege-valid-link-msg">{{:Kjs.resetPasswordOTP.replaceText($env.OTPTempLinkDurationMinutes,staticContents.otpTheLinkWillBeActiveForMinutes)}}</div>
			</div>
		</div>
	</div>
</script>

<script id="linkNoLongerValidTpl" type="text/x-jsrender">
	<div id="linkNoLongerValid" class="tr-otp-container kas-tr-otp-container">
		<a id="close-otpModal" class="custom-reset-password-close-btn" title="Close overlay" tabindex="0"></a>
		<div class="otp-model-header kas-otp-model-header">
			<span class="otp-model-header-title kas-otp-model-header-title">{{:staticContents.otpLinkNoLongerActive}}</span>
		</div>
		<div class="tr-otp-inner-container kas-tr-otp-inner-container">
			<div class="clear tr-otp-box-caption kas-tr-otp-box-caption">
				<div class="tr-otp-box-caption-bold kas-tr-otp-box-caption-bold">{{:staticContents.otpLinkNoLongerActiveInconvenience}}</div>
			</div>
			<div class="linkNoLongerValid kas-linkNoLongerValid">
				<span class="tr-otp-ask-new-number-link kas-tr-otp-ask-new-number-link">
					<a href="javascript:void(0)">{{:staticContents.otpRequestAnotherLink}}</a> 
				</span>
			</div>
			<div class="tr-otp-customer-service-massege kas-tr-otp-customer-service-massege">{{:staticContents.otpCallCustomerServiceToRestPassword}}</div>
			<div class="tr-otp-massege-valid-link kas-tr-otp-massege-valid-link">
				<div class="tr-otp-massege-valid-link-msg kas-tr-otp-massege-valid-link-msg">{{:Kjs.resetPasswordOTP.replaceText($env.OTPTempLinkDurationMinutes,staticContents.otpTheLinkWillBeActiveForMinutes)}}</div>
			</div>
		</div>
	</div> 
</script>

<script id="contactCustomerServiceTpl" type="text/x-jsrender">
	<div id="contactCustomerService" class="tr-otp-container kas-contactCustomerService">
		<a id="close-otpModal" class="custom-reset-password-close-btn" title="Close overlay" tabindex="0"></a>
		<div class="otp-model-header kas-otp-model-header">
			<span class="otp-model-header-title kas-call-customer-service-header-title">{{:staticContents.otpContactCustomerService}}</span>
		</div>
		<div class="tr-otp-inner-container kas-something-went-wrong-inner-container">
			<div class="clear tr-otp-box-caption kas-tr-otp-box-caption">
				<div class="tr-otp-box-caption-bold kas-something-went-wrong-caption">{{:staticContents.otpSorrySomethingWentWrong}}</div>
			</div>
			<div class="tr-otp-customer-service-massege kas-call-customer-service-message">{{:staticContents.otpContactcustomerServiceToRestPassword}}</div>
		</div>
	</div> 
</script>

<script id="checkYourEmailTpl" type="text/x-jsrender">
	<div id="checkYourEmailContainer" class="tr-otp-container kas-checkYourEmailTpl">
		<a id="close-otpModal" class="custom-reset-password-close-btn" title="Close overlay" tabindex="0"></a>
		<div class="otp-model-header kas-otp-model-header">
			<span class="otp-model-header-title kas-otp-model-header-title">{{:staticContents.kls_static_cmdm_profile_check_email_para}}</span>
		</div>
		<div class="tr-otp-inner-container kas-tr-otp-inner-container">
			<div class="tr-otp-customer-service-massege kas-tr-otp-customer-service-massege">
				<p>
				{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_message}}
				<br/><br/>
				{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_message_one}}</p>
			</div>
		</div>
		<div class="tr-otp-sent-btn-section kas-tr-otp-sent-btn-section">
			<button class="backToSign-greenBtn-text-white kas-backToSign-greenBtn-text-white">{{:staticContents.kls_msg_cmdm_profile_pwd_forgot_button_one}}</button>
		</div>
	</div>
</script>
