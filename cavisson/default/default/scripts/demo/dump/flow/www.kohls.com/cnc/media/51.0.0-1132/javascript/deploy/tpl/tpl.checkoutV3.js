<script id="tpl-checkoutV3Panel" type="text/x-jsrender">
    {{!-- /* checkoutv3 page block */ --}}
    <a href="#main-content-checkout" class="skip-to-main-content">Skip to main content</a>
    <div id="{{id: 'actionPanel'}}"></div>
    <div></div>
    <div class="smart-cart-screen no-padding-top" id="main-content-checkout">
        <div id="{{id: 'checkoutOutOfStockPanel'}}"></div>
        <div class="smart-cart-screen-block">   
            <div class="left-column-block">
                <div class="contact-common-block new-popup-design-selector">
                    <form id="signinForm">
                    <div id="{{id: 'signupPanel'}}"></div>
                    </form>
                </div>
                <div id="{{id: 'openOffersPanel'}}" class="open-offers-panel full-screen-width-parent-selector"></div>
                <div id="{{id: 'messageSlotPanel'}}" class="message-slot-panel"></div>
                <div id="{{id: 'pickUpPanel'}}" class="pickup-panel full-screen-width-parent-selector"></div>
                <div id="{{id: 'eGiftPanel'}}" class="egiftcard-panel"></div>
                <div id="addressyPanelForm"></div>
                <div id="{{id: 'addressyPanel'}}"></div>
                <div id="{{id: 'giftCardPanel'}}"></div>
                <div id="addGiftCard"></div>
                <div id="{{id: 'paymentPanel'}}"></div>
                <div id="billingPanelForm"></div>
                <div id="billingAddressPanelForm"></div>
                <div id="{{id: 'billingAddresPanel'}}"></div>
            </div>
            <!-- *********************************  CENTER GUTTER START ********************************* -->
            <div class="screen-gutter"></div>
            <!-- *********************************  CENTER GUTTER END ********************************* -->
            <div class="right-column-block">
                <div class="cart-block"><div id="{{id: 'orderSummary'}}"></div></div>
                <div id="{{id: 'cartRewardPanel'}}" class="cart-earnings-panel"></div>
                <div class="view-order-summary">
                    <div id="{{id: 'digitalReceiptPanel'}}"></div>
                </div>
            </div>
            <div id="{{id: 'orderDisclaimerPanel'}}" class="order-disclaimer-panel"></div>
    </div>

</script>	<script id="tpl-signupPanel-template" type="text/x-jsrender">
{{if isSoftLoggedIn==true}}
    {{for tmpl="tpl-signinPanel-softloggedin-template"/}}
{{else #data.state=='guestUserCheck'}}
    {{for tmpl="tpl-checkGuestUser"/}}
{{else #data.state=='postCallSuccess'}}
    {{for tmpl="tpl-cartHasUserData"/}}
{{else #data.state=='clicked'}}
    {{for tmpl="tpl-signUpForm"/}}
{{/if}}

</script>

<!-- Guest user initial name div start------>
<script id="tpl-checkGuestUser" type="text/x-jsrender">
{{if isGuestUser }}
<div class="guest-name-form">
        <div class="contact-common-header">{{:cncLabels.kls_contactForm_message_for_guest_user}}</div>
        <div class="sign-in-details-block">
                <div class="sign-in-form-group-block">
                        <div class="sign-in-form-group"  id="{{id:'guest-name-form'}}">
                                <div class="form-group-label" >{{:cncLabels.kls_contactForm_firstName_text}}</div>
                                <input type="text" class="sign-in-form-input" id="{{id:'guest-first-name'}}">
                                <div class="error-message-contact">{{:cncLabels.kls_contactForm_error_text}}</div>
                        </div>
                </div>                
                <div class="sign-in-hr"></div>
                <div class="sign-in-form-form-group" id="{{id:'sign-in-form'}}">
                        <label class="signInText">{{:cncLabels.kls_contactForm_signIn_text}}
                        </label>
                </div>                
</div>
</div>
<!-- guest user initial name div end ----->

    {{else}}
                <div class="contact-signed-in-block">
                <div class="signed-in-name ctHidden">{{:cartJsonData.userData.profile.firstName}} {{:cartJsonData.userData.profile.lastName}}</div>
                <div class="signed-in-email ctHidden">{{:cartJsonData.userData.profile.emailId}}</div>
                </div>
{{/if}}
</script>

<script id="tpl-cartHasUserData" type="text/x-jsrender">
    <div class="contact-signed-in-block">
        <div class="edit-signed-in" id="{{id:'edit-signIn-button'}}"><a href="javascript:void(0);"><img class="edit-signed-in-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/contactFormImages/pencil@3x.png" alt="" title=""></a></div>
        <div class="signed-in-name ctHidden">{{:cartJsonData.userData.firstName}} {{:cartJsonData.userData.lastName}}</div>
        <div class="signed-in-email ctHidden">{{:cartJsonData.userData.emailId}}</div>
    </div>
</script>
        <!--/* // filled form section */-->
        <script id="tpl-signUpForm" type="text/x-jsrender">
<div class="guest-signup-form validateInline" id ="guestSignUpForm">
            <div id="{{id:'cancel-signUp-button'}}" class="panel_button_close"></div>
            <div class="contact-common-header">{{:cncLabels.kls_contactForm_message_for_guest_signup}}</div>
            <div class="sign-in-details-block">
                    <div class="sign-in-form-group-block">
                            <div class="sign-in-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="firstName" id="guestFirstName" minlength="2" maxlength="40" autocomplete="off" class="sign-in-form-input sign-in-first-name text-input-name required">
                                <label for="guestFirstName" class="form-group-label">{{:cncLabels.kls_contactForm_firstName_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="sign-in-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="lastName" id="guestLastName" minlength="2" maxlength="40" autocomplete="off" class="sign-in-form-input sign-in-last-name text-input-name required">
                                <label for="guestLastName" class="form-group-label">{{:cncLabels.kls_contactForm_lastName_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="sign-in-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="phoneNumber" id="guestPhoneNumber" placeholder=""  autocomplete="off" maxlength="14" minlength="14" class="sign-in-form-input sign-in-phone-number phone-number-input required">
                                <label for="guestPhoneNumber" class="form-group-label">{{:cncLabels.kls_contactForm_phoneNumber_text}}</label>
                                <div class="error-message-contact"></div>                             
                            </div>
                            <div class="sign-in-form-group">
                            <div class="contact-phone-no-bottom-text-description">{{:cncLabels.kls_contactForm_phoneNumber_warning_text}}</div>
                            </div>
                            <div class="sign-in-form-group sign-in-email-form">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="emailId" id="guestEmail" autocomplete="off" maxlength="40" class="sign-in-form-input sign-in-email email-input required">
                                <label for="guestEmail" class="form-group-label">{{:cncLabels.kls_contactForm_email_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                    </div>   
                    <div class="sign-in-form-form-group">
                            <label class="checkbox-green-container">{{:cncLabels.kls_contactForm_signup_checkbox_text}}
                                <input type="checkbox" id="{{id:'enrollLoyaltyAndSalesAlert'}}" class="enrollLoyaltyAndSalesAlert" name="enrollLoyaltyAndSalesAlert">
                                <span class="checkbox-green-checkbox"></span>
                            </label>
                    </div>
                    <div class="contact-btn-block">                        
                        <kds-button id="{{id:'return-to-signIn-btn'}}" title="{{:cncLabels.kls_contactForm_r_signin_button_text}}" size="sm" button-type="secondary">
                            {{:cncLabels.kls_contactForm_r_signin_button_text}}
                        </kds-button>
                        <kds-button id="{{id:'continue-as-guest-btn'}}" title="{{:continueAsGuestBtnText}}" size="sm">
                            {{:continueAsGuestBtnText}}
                        </kds-button>
                    </div>
            </div>
    </div>
    </script>

<script id="tpl-signinPanel-softloggedin-template" type="text/x-jsrender">
    <div class="contact-common-header">{{:cncLabels.kls_sign_in_welcome_back_label}} {{:cartJsonData.userData.profile.firstName}}</div>
    <div class="password-in-details-block">
        <div class="sign-in-form-group-block">
            <div class="sign-in-form-group" id="{{id:'sign-in-form'}}">
            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                <input type="text" id = "softLoggedinPassword" class="sign-in-form-input sign-in-form-input">
                <div class="form-group-label">{{:cncLabels.kls_sign_in_password_label}}</div>
                <div class="error-message-contact">Error</div>
            </div>
        </div>   
    </div>
    <div class="contact-hr"></div>
    <div class="contact-common-green-text contact-top-margin" id="{{id:'guest-checkout'}}">{{:cncLabels.kls_checkout_guest}}</div>
</script><script id="tpl-paymentMainTemplate" type="text/x-jsrender">
<!-- START redesign code -->
	<div class="payment-saved-msg sr-only" role="status" aria-live="polite" style="display:none;">Payment information updated</div>
	<div {{if isPrimaryCardExpired}} class="payment-common-block generic-panel-error new-payment-common-block new-popup-design-selector" {{else}} class="payment-common-block new-payment-common-block new-popup-design-selector"{{/if}}>
		<div id="payment-panel-error">			
			{{if state == "editingCard"}}
				{{for tmpl='tpl-addPaymentFormRedesign' ~env = $env/}}	
			{{else}}
				{{if isUserLoggedIn}}
					{{for tmpl='tpl-paymentLoggedInUserMainRedesign' ~env = $env /}}
				{{else}}
					{{for tmpl='tpl-paymentGuestUserMainRedesign' ~env = $env /}}
				{{/if}}
			{{/if}}
		</div>
	</div>
<!-- END redesign code -->
</script>


<script id="tpl-paymentGuestUserMainRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->

    {{if state == "addCard" || state == "addCardForm" || state == "default"}}
		{{for tmpl='tpl-addPaymentFormRedesign' ~env = $env/}}

	{{else state == "showingSingleCard" || state == "showingSingleCard_AddCard"}}
		<div id="guest-payment-block">
			<div class="payment-header-title-card">Pay ${{:remainingPayment}} with</div>
			{{if kcEventText}}
				<div class="payment-kcEvent-banner">{{:kcEventText}}</div>
			{{/if}}
			{{for tmpl='tpl-paymentSingleCardGuestRedesign' ~env = ~env/}}
			{{for tmpl="tpl-paymentAddCardRedesign" /}}
		</div>
	{{else state == "showingSingleCard_AddCard_Form"}}
		{{for tmpl='tpl-addPaymentFormRedesign' ~env = ~env/}}
	{{/if}}
<!-- END redesign code -->
</script>



<script id="tpl-paymentGuestUserMain" type="text/x-jsrender">
<!-- BEGIN old code -->

    {{if state == "addCard" || state == "addCardForm" || state == "default"}}
		{{for tmpl='tpl-paymentAddCardContainer' ~env = ~env/}}

	{{else state == "showingSingleCard" || state == "showingSingleCard_AddCard" || state == "showingSingleCard_AddCard_Form"}}
		<div id="guest-payment-block">
			<div class="payment-common-header">{{:cncLabels.kls_paymentForm_pay_with}}</div>
			{{if kcEventText}}
				<div class="payment-kcEvent-banner">{{:kcEventText}}</div>
			{{/if}}
			{{for tmpl='tpl-paymentSingleCardGuest' ~env = ~env/}}
			{{if state == "showingSingleCard_AddCard_Form"}}
				{{for tmpl='tpl-moreWaysToPayFooter' /}}
			{{else}}
				{{for tmpl="tpl-paymentAddCard" /}}
			{{/if}}
		</div>
	{{/if}}
<!-- END old code -->
</script>

<script id="tpl-paymentLoggedInUserMainRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code-->

	{{if state == "addCard" || state == "addCardForm" || state == "default"}}
		{{for tmpl='tpl-addPaymentFormRedesign' ~env = ~env/}}

	{{else state == "showingSingleCard" || state == "showingSingleCard_AddCard" || state == "showingSingleCard_AddCard_Form"}}
		{{if state != "showingSingleCard_AddCard_Form"}}
			<div class="payment-header-title-card">Pay ${{:remainingPayment}} with</div>
		{{/if}}
		
		{{if kcEventText}}
		<!--
			<div class="payment-kcEvent-banner">{{:kcEventText}}</div>
		-->
		{{/if}}
		{{if state != "showingSingleCard_AddCard_Form"}}
			<div class="payment-card-options">
				{{for tmpl='tpl-paymentSingleCardLoggedInRedesign' ~env = ~env/}}
			</div>
		{{/if}}

		{{if state == "showingSingleCard" }}
			{{if hasMorePayment == 0}}
				{{if showAddMoreCardsButton}}
					{{for tmpl="tpl-paymentAddCardRedesign" /}}
				{{else}}
					{{for tmpl='tpl-moreWaysToPayFooterRedesign' /}}
				{{/if}}
			{{else}}
				{{for tmpl="tpl-paymentAddCardRedesign" /}}
			{{/if}}

		{{else state == "showingSingleCard_AddCard"}}
			{{for tmpl="tpl-paymentAddCardRedesign" /}}
			
		{{else state == "showingSingleCard_AddCard_Form"}}
				{{for tmpl='tpl-addPaymentFormRedesign' /}}
		{{/if}}


	{{else state == "showingAllCards"}}			
		<div class="payment-header-title-card">Pay ${{:remainingPayment}} with</div>	
		{{if kcEventText}}
		<!--
			<div class="payment-kcEvent-banner">{{:kcEventText}}</div>
		-->
		{{/if}}
		{{for tmpl='tpl-paymentMultiCardContainerRedesign' ~env = ~env /}}
		{{if showAddMoreCardsButton}}
			{{for tmpl="tpl-paymentAddCardRedesign" /}}
		{{else}}
			{{for tmpl='tpl-moreWaysToPayFooterRedesign' /}}
		{{/if}}
		
	{{else state == "showingAllCards_AddCard" || state == "showingAllCards_AddCard_Form"}}
		<div class="payment-header-title-card">Pay ${{:remainingPayment}} with</div>
		{{for tmpl='tpl-paymentMultiCardContainerRedesign' ~env = ~env /}}
		{{if state == "showingAllCards_AddCard"}}
			{{for tmpl="tpl-paymentAddCardRedesign" /}}
		{{else state == "showingAllCards_AddCard_Form"}}
			{{for tmpl='tpl-moreWaysToPayFooterRedesign' /}}
		{{/if}}
	{{/if}}

<!-- END redesign code -->
</script>

<script id="tpl-paymentLoggedInUserMain" type="text/x-jsrender">
<!-- BEGIN old code -->

	{{if state == "addCard" || state == "addCardForm" || state == "default"}}
		{{for tmpl='tpl-paymentAddCardContainer' ~env = ~env/}}


	{{else state == "showingSingleCard" || state == "showingSingleCard_AddCard" || state == "showingSingleCard_AddCard_Form" }}
		<div class="payment-common-header">{{:cncLabels.kls_paymentForm_pay_with}}</div>
		{{if kcEventText}}
			<div class="payment-kcEvent-banner">{{:kcEventText}}</div>
		{{/if}}
		<div class="payment-card-options saved-card-bottom-pad">
			{{for tmpl='tpl-paymentSingleCardLoggedIn' ~env = ~env/}}
		</div>

		{{if state == "showingSingleCard" }}
			{{if hasMorePayment == 0}}
				{{if showAddMoreCardsButton}}
					{{for tmpl="tpl-paymentAddCard" /}}
				{{else}}
					{{for tmpl='tpl-moreWaysToPayFooter' /}}
				{{/if}}
			{{else}}
				<div class="kohls-charge-block-green-text" id="{{id:'show-more-cards-btn'}}">{{: cncLabels.kls_paymentForm_show_more_card}}</div>
			{{/if}}

		{{else state == "showingSingleCard_AddCard"}}
			{{for tmpl="tpl-paymentAddCard" /}}
			
		{{else state == "showingSingleCard_AddCard_Form"}}
			{{for tmpl='tpl-moreWaysToPayFooter' /}}
		{{/if}}


	{{else state == "showingAllCards"}}			
		<div class="payment-common-header">{{:cncLabels.kls_paymentForm_pay_with}}</div>
		{{if kcEventText}}
			<div class="payment-kcEvent-banner">{{:kcEventText}}</div>
		{{/if}}
		{{for tmpl='tpl-paymentMultiCardContainer' ~env = ~env /}}
		{{if showAddMoreCardsButton}}
			{{for tmpl="tpl-paymentAddCard" /}}
		{{else}}
			{{for tmpl='tpl-moreWaysToPayFooter' /}}
		{{/if}}
		
	{{else state == "showingAllCards_AddCard" || state == "showingAllCards_AddCard_Form"}}
		<div class="payment-common-header">{{:cncLabels.kls_paymentForm_pay_with}}</div>
		{{for tmpl='tpl-paymentMultiCardContainer' ~env = ~env /}}
		{{if state == "showingAllCards_AddCard"}}
			{{for tmpl="tpl-paymentAddCard" /}}
		{{else state == "showingAllCards_AddCard_Form"}}
			{{for tmpl='tpl-moreWaysToPayFooter' /}}
		{{/if}}
	{{/if}}

<!-- END old code -->
</script>

<script id="tpl-paymentAddCardRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
	<p {{if isUserLoggedIn}} class="linePlusTextInMiddleOtherPaymentMethods kds" {{else}} class="linePlusTextInMiddle kds" {{/if}}><span class="body-01">{{:cncLabels.kls_paymentForm_divider_or}}</span></p>
	<div class="payment-card-options payment-card-options-redesign">
		{{if state == "addCard" || state == "addCardForm" || state == "default"}}
			<div class="payment-common-header default-state-header">{{:cncLabels.kls_paymentForm_payment_header}}</div>
		{{else}}
			{{if isUserLoggedIn}}
				<div class="other-payment-methods-menu kds" id="{{id:'toggle-payment-methods-redesign'}}">
					<span class="other-payment-methods-title body-02">{{:cncLabels.kls_other_payment_methods}}</span>
					<img class= {{if state == "showingSingleCard" || state == "showingAllCards"}}"more-payments-chevron"{{else}}"more-payments-chevron flip-img-vertical"{{/if}} src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-down.png" alt="" title=""></img>
				</div>
			{{/if}}
		{{/if}}

		{{if state == "showingSingleCard_AddCard_Form"}}
		<div class="payment-option-buttons">
			{{if !~root.$env.enablePayLaterFeature}}
				<div class="paypal-venmo-container">
					{{if ~root.$env.enablePaypalFeature}}
						<div class="paypal-button KAS-paypal-checkout paypal-btn-redesign">
							<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
							<div id="paypal-button" class="paypal-button-class-redesign"></div>
						</div>	
					{{/if}}
					{{if ~root.$env.enableVenmoFeature}}
						<button id="venmo-button" class="payment-option-button venmo-btn-sm hide">
							<img class="venmo-button-logo" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Venmo">
						</button>
					{{/if}}
				</div>			
			{{else}}
				{{if ~root.$env.enablePaypalFeature}}
					<div class="paypal-button KAS-paypal-checkout paypal-btn-redesign">
						<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
						<div id="paypal-button" class="paypal-button-class-redesign"></div>
					</div>	
				{{/if}}
				<div class="venmo-paylater-container">
					{{if ~root.$env.enableVenmoFeature}}
						<button id="venmo-button" class="payment-option-button venmo-btn-sm hide">
							<img class="venmo-button-logo" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Venmo">
						</button>
					{{/if}}
					{{if ~root.$env.enablePayLaterFeature}}
						<div id="paylater-button"></div>
					{{/if}}
				</div>
			{{/if}}
			</div>
		
		{{else state != "showingSingleCard" && state != "showingAllCards" || !isUserLoggedIn}}
			{{if (state == "showingSingleCard_AddCard" || state == "showingAllCards_AddCard") && isUserLoggedIn}}
				{{for tmpl="tpl-paymentMultiCardRedesign" ~hideEditButton=false ~showMoreVisibleCards=paymentData.showMoreVisibleCards ~env = $env ~cncLabels = cncLabels/}}
			{{/if}}
			<div class="payment-option-buttons">
				<kds-button title="{{:isUserLoggedIn ? cncLabels.kls_paymentForm_add_another_card : cncLabels.kls_paymentForm_use_another_card}}" size="sm" button-type="secondary" class="add-credit-debit-card" aria-label="Credit or debit card" id="{{id:'select-card-method'}}">
					{{if isUserLoggedIn}}
						<div style="display:flex; justify-content:center">
							<img class="saved-card-block-img" style="height:16.5px; width:22px;align-self:center;margin-right:8px;" src="{{_'/cnc/media/images/dgsImages/paymentPanelImages/credit-card@3x.png_}}" alt="" title="">
							<span class="add-card-text-styling-redesign">{{:cncLabels.kls_paymentForm_add_another_card}}</span>
						</div>	
					{{else}}
						<span>{{:cncLabels.kls_paymentForm_use_another_card}}</span>
					{{/if}}										
				</kds-button>
					{{if !~root.$env.enablePayLaterFeature}}
						<div class="paypal-venmo-container">
							{{if ~root.$env.enablePaypalFeature}}
								<div class="paypal-button KAS-paypal-checkout paypal-btn-redesign">	
									<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
									<div id="paypal-button"></div>
								</div>
							{{/if}}
							{{if ~root.$env.enableVenmoFeature}}
								<button id="venmo-button" class="payment-option-button venmo-btn-sm hide">
									<img class="venmo-button-logo" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Venmo">
								</button>
							{{/if}}
						</div>
					{{else}}
						{{if ~root.$env.enablePaypalFeature}}
							<div class="paypal-button KAS-paypal-checkout paypal-btn-redesign">	
								<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
								<div id="paypal-button"></div>
							</div>
						{{/if}}
						<div class="venmo-paylater-container">
							{{if ~root.$env.enableVenmoFeature}}
								<button id="venmo-button" class="payment-option-button venmo-btn-sm hide">
									<img class="venmo-button-logo" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Venmo">
								</button>
							{{/if}}
							{{if ~root.$env.enablePayLaterFeature}}
								<div id="paylater-button"></div>
							{{/if}}
						</div>
					{{/if}}

			</div>
		{{/if}}
	</div>
<!-- END redesign code -->
</script>

<script id="tpl-paymentAddCard" type="text/x-jsrender">
<!-- BEGIN old code -->
	<div class="payment-card-options">
		{{if state == "addCard" || state == "addCardForm" || state == "default"}}
			<div class="payment-common-header default-state-header">{{:cncLabels.kls_paymentForm_payment_header}}</div>
		{{else}}
			<div class="more-payments-header" id="{{id:'toggle-payment-methods'}}">
				<span>{{:cncLabels.kls_paymentForm_more_way_pay}}</span>
				<img class= {{if state == "showingSingleCard" || state == "showingAllCards"}}"more-payments-chevron"{{else}}"more-payments-chevron flip-img-vertical"{{/if}} src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-down.png" alt="" title=""></img>
			</div>
		{{/if}}

		{{if state != "showingSingleCard" && state != "showingAllCards"}}
			<div class="payment-options-thumbnails">
				<button class="payment-kohls-charge payment-option-button" id="{{id:'kc-select'}}" aria-label="Kohl's Card">
					<span class= {{if kohlsChargeSelected}}"payment-kohls-charge-text"{{else}}"credit-debit-card-text"{{/if}}>
						<img class="kohls-charge-thumbnail-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/KohlsCardButton@2x.png" alt="{{:cncLabels.kls_paymentForm_kohls_charge}}" title="{{:cncLabels.kls_paymentForm_kohls_charge}}"></img>
					</span>
				</button>
				<button class="credit-debit-card payment-option-button" id="{{id:'card-select'}}" aria-label="Credit or debit card">
					<span class= {{if cardSelected}}"payment-kohls-charge-text"{{else}}"credit-debit-card-text"{{/if}}>
						<img class="credit-card-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/credit-card@3x.png" alt="{{:cncLabels.kls_paymentForm_credit_debit_card}}" title="{{:cncLabels.kls_paymentForm_credit_debit_card}}"></img>
						<span>{{:cncLabels.kls_paymentForm_credit_debit_card}}</span>
					</span>
				</button>
					{{for tmpl="tpl-thirdPartyButtons" /}}
			</div>
		{{/if}}
	</div>
<!-- END old code -->
</script>

<script id="tpl-paymentAddCardContainer" type="text/x-jsrender">
<!-- BEGIN old code -->
	{{for tmpl="tpl-paymentAddCard" /}}
	{{for tmpl='tpl-moreWaysToPayFooter' /}}
<!-- END old code -->
</script>

<script id="tpl-paymentSingleCardGuestRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
		<div class="payment-card-options payment-card-options-redesign">
			<div class="kohls-charge-block payment-bg no-border card-selected-block">
				<div class="parent-div-radio-btn">
					<span class="kds-radio-btn-positioning-payment-panel">
						<kds-radio checked label=""></kds-radio>
					</span>					
				</div>				
				<img class="saved-card-block-img" src="{{__paymentData.initialVisibleCards[0].cardImage}}" alt="" title="">
				{{if paymentData.initialVisibleCards[0].type == 'paypal'}}
					<div class="paypal-details-container kds">
						<div class="paypal-account-title body-02">{{:cncLabels.paypal_account_title}}</div>
						<div class="saved-paypal-block-text body-02">{{:paymentData.initialVisibleCards[0].cardTitleRedesign}}</div>
					</div>
				{{else}}
					<div class="saved-card-block-text kds"><span class="body-02">{{:paymentData.initialVisibleCards[0].cardTitleRedesign}}</span></div>
				{{/if}}
				{{if paymentData.initialVisibleCards[0].type != 'paypal' && paymentData.initialVisibleCards[0].type != 'venmo'}}
					<div class="edit-saved-card edit-saved-card-redesign"><a href="javascript:void(0)" class="edit-pencil-text-style kds" id="{{id:'edit-saved-card'}}" data-cardType="{{:paymentData.initialVisibleCards[0].type}}" data-paymentId="{{:paymentData.initialVisibleCards[0].paymentId}}">
						<img class="edit-saved-card-img pencil-image-height" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title="">
						<span class="span-edit-button-text supporting-micro-01-link">{{:cncLabels.kls_paymentForm_Edit}}</span></a>
					</div>
				{{/if}}
			</div>
		</div>
<!-- END redesign code -->
</script>

<script id="tpl-paymentSingleCardGuest" type="text/x-jsrender">
<!-- BEGIN old code -->
		<div class="payment-card-options">
			<div class="kohls-charge-block payment-bg no-border">
				<span class="kohls-charge-block-style"><img class="kohls-charge-block-img" src="{{__paymentData.initialVisibleCards[0].cardImage}}" alt="" title=""></span>
				<span class="kohls-charge-block-text">
					<p class="kohls-charge-head-saved-card">{{:paymentData.initialVisibleCards[0].cardTitle}}</p>
					<p class="kohls-charge-text-saved-card">{{:paymentData.initialVisibleCards[0].cardDescription}}</p>
				</span>
				{{if paymentData.initialVisibleCards[0].type != 'paypal' && paymentData.initialVisibleCards[0].type != 'venmo'}}
					<div class="edit-saved-card"><a href="javascript:void(0)"><img  id="{{id:'edit-saved-card'}}"  data-cardType="{{:paymentData.initialVisibleCards[0].type}}" data-paymentId="{{:paymentData.initialVisibleCards[0].paymentId}}" class="edit-saved-card-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
				{{/if}}
			</div>
		</div>
<!-- END old code -->
</script>

<script id="tpl-paymentSingleCardLoggedInRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
	<div id="paymentSingleCardId">
	{{for paymentData.initialVisibleCards ~cncLabels=cncLabels ~env=~env}}
		{{if #data.isSelected}}
			<div class="payment-bg-active-redesign">
				<div class="parent-div-radio-btn parent-div-radio-btn-loggedin-user">
					<span class="kds-radio-btn-positioning-payment-panel">
						<kds-radio checked label=""></kds-radio>
					</span>					
				</div>				
				<img class="card-type-icon" src="{{__data.cardImage}}" alt="{{: #data.type}}" title="">
				{{if #data.type == 'paypal'}}
					<div class="payment-card-saved-block paypal-details-container kds">
						<div class="paypal-account-title body-02">{{:~cncLabels.paypal_account_title}}</div>				
						<div class="ctHidden card-title-redesign body-02" tabindex="0" role="" {{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_paying_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_paying_with_venmo}}"{{/if}}>{{: #data.cardTitleRedesign}}</div>
					</div>
				{{else}}
					<span class="payment-card-saved-block payment-card-saved-block-width-100 kds" tabindex="0" role=""{{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_paying_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_paying_with_venmo}}"{{/if}}>					
						<p class="ctHidden card-title-redesign body-02">{{: #data.cardTitleRedesign}}</p>
						<p {{if #data.type ==='kohlsCharge' || #data.type ==='visa_kohls'}} class="payment-card-bottom-text-kohls-card body-02" {{else}} class="payment-card-sub-txt ctHidden hide-expiration-date-card body-02" {{/if}}>{{: #data.cardDescription}}</p>
					</span>
				{{/if}}
				{{if #data.type != 'paypal' && #data.type != 'venmo'}}
					<div class="edit-saved-card edit-saved-card-redesign">
						<a href="javascript:void(0)" id="{{id:'edit-initial-saved-card'}}" class="edit-pencil-text-style kds" data-cardType="{{:#data.type}}" data-paymentId="{{:#data.paymentId}}">
							<img class="edit-saved-card-img pencil-image-height" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title="">
							<span class="span-edit-button-text edit-button-mr supporting-micro-01-link">{{:~cncLabels.kls_paymentForm_Edit}}</span>
						</a>
					</div>
				{{/if}}
			</div>

			{{if #data.cvvRequired && #data.type !=='venmo' && #data.type !=='paypal' && #data.type !=='kohlscharge'}}
			    <div class="txtbox-form-group-block validateInline cvv-verify-form-block-redesign" id ="cvvVerifyForm">
					<div class="txtbox-form-group cvv-reentry-parent-div">
						<div class="form-group-label cvv-verify-label card-label-text-styling">{{:~cncLabels.kls_paymentForm_CVV}}</div>
						<input type="tel" class="txtbox-form-input txtbox-form-input-small required number-input verify-security-code input-box-border-style remove-margin-top-cvv" name ="cvv" minlength = "3" maxlength="4" id="{{id:'verifyCvvField'}}" data-type="{{:#data.type}}" placeholder="{{:~cncLabels.kls_cvv_input_placeholder}}">						
						<div class="enter-cvv-edit-card" style="visibility:visible" id="cvvErrorMessageTextPaymentPanel">{{:~cncLabels.kls_cvv_required_info}}</div>

						<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-hexagon@3x.png" alt="Kohls" class="alert-circle-icon alert-hexagon"></img>
						<div class="error-message-contact error-msg-cvv-reentry" style="margin-left:19px;">{{:~cncLabels.kls_cvv_verify_error}}</div>
					</div>
				</div>
			{{/if}}

		{{else}}
			<div class="payment-bg-redesign payment-bg-loggedin-user-redesign">
				<span class="payment-bg card-selection-block" id="{{id:'select_card'}}" data-paymentId="{{:#data.paymentId}}" data-cardType="{{:#data.type}}" role="button" tabindex="0" {{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_pay_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_pay_with_venmo}}"{{/if}}>
					<div class="parent-div-radio-btn parent-div-radio-btn-loggedin-user">
					<span class="kds-radio-btn-positioning-payment-panel">
						<kds-radio label=""></kds-radio>
					</span>					
					</div>
					<img class="card-type-icon" src={{_ #data.cardImage}} alt="{{: #data.type}}" title="">
					
					<span class="payment-card-saved-block payment-card-saved-block-span-style kds" style="width:auto;" {{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_pay_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_pay_with_venmo}}"{{/if}}>	
						<p {{if isUserLoggedIn}} class="payment-card-no-redesign ctHidden body-02" {{else}} class="payment-card-no ctHidden" {{/if}}>{{: #data.cardTitleRedesign}}</p>
						<p {{if #data.type ==='kohlsCharge' || #data.type ==='visa_kohls'}} class="payment-card-bottom-text-kohls-card ctHidden body-02" {{else}} class="payment-card-sub-txt ctHidden hide-expiration-date-card body-02" {{/if}}>{{: #data.cardDescription}}</p>
					</span>
				</span>				
				{{if #data.type != 'paypal' && #data.type != 'venmo'}}
					<div class="edit-saved-card">
						<a href="javascript:void(0)" class="edit-pencil-text-style kds" id="{{id:'edit-initial-saved-card'}}" data-cardType="{{:#data.type}}" data-paymentId="{{:#data.paymentId}}">
							<img class="edit-saved-card-img pencil-image-height" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title="">
							<span class="span-edit-button-text edit-button-mr supporting-micro-01-link">{{:~cncLabels.kls_paymentForm_Edit}}</span>
						</a></div>
				{{/if}}
			</div>
		{{/if}}        
				
	{{/for}}
	</div>
	<!-- END redesign code -->
</script>

<script id="tpl-paymentSingleCardLoggedIn" type="text/x-jsrender">
<!-- BEGIN old code -->
	<div id="paymentSingleCardId">
	{{for paymentData.initialVisibleCards ~cncLabels=cncLabels ~env=~env}}
		{{if #data.isSelected}}
			<div class="payment-bg-active">
				<span class="card-select-marker-active"></span>
				<span><img class="kohls-charge-img" src="{{__data.cardImage}}" alt="" title=""></span>
				<span class="payment-card-saved-block" tabindex="0" role=""{{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_paying_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_paying_with_venmo}}"{{/if}}>
					{{if #data.cardHeader}}
					<p  {{if #data.type ==='kohlsCharge'}} class="payment-card-top-text-kohls-card" {{else}} class="payment-card-top-text" {{/if}} >{{:#data.cardHeader}}</p>
					{{/if}}
					<p {{if #data.type ==='kohlsCharge'}} class="payment-card-no-bold-kohls-card ctHidden" {{else}} class="payment-card-no-bold ctHidden" {{/if}} >{{: #data.cardTitle}}</p>
					<p {{if #data.type ==='kohlsCharge'}} class="payment-card-bottom-text-kohls-card" {{else}} class="payment-card-bottom-text ctHidden" {{/if}}>{{: #data.cardDescription}}</p>
				</span>
				{{if #data.type != 'paypal' && #data.type != 'venmo'}}
					<div class="edit-saved-card"><a href="javascript:void(0)"><img id="{{id:'edit-initial-saved-card'}}" data-cardType="{{:#data.type}}" data-paymentId="{{:#data.paymentId}}" class="edit-saved-card-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
				{{/if}}
			</div>

			{{if #data.cvvRequired && #data.type !=='venmo' && #data.type !=='paypal' && #data.type !=='kohlscharge'}}
			    <div class="txtbox-form-group-block validateInline cvv-verify-form-block" id ="cvvVerifyForm">
					<div class="txtbox-form-group">
						<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon">
						<input type="text" class="txtbox-form-input txtbox-form-input-small server-validation-input-border-bottom required number-input verify-security-code" name ="cvv" minlength = "3" maxlength="4" id="{{id:'verifyCvvField'}}" data-type="{{:#data.type}}">
						<div class="form-group-label server-validation-label-color cvv-verify-label">{{:~cncLabels.kls_paymentForm_card_security_code}}</div>
						<div class="error-message-contact" style="visibility:visible">{{:~cncLabels.kls_cvv_verify_error}}</div>
					</div>
				</div>
				<div class="contact-btn-block payment-btn-block">
					<kds-button title="{{:~cncLabels.kls_paymentForm_button_label_done}}" size="sm" class="save-form-payment"
						data-paymentId="{{:#data.paymentId}}" data-paymentType="{{:#data.type}}" id="{{id:'saveCvv'}}">
						{{:~cncLabels.kls_paymentForm_button_label_done}}
					</kds-button>
				</div>
			{{/if}}

		{{else}}
			<div class="payment-bg">
			<span class="card-select-marker"></span>
				<span><img class="kohls-charge-img" src={{_ #data.cardImage}} alt="" title=""></span>
				
				<span class="payment-card-saved-block" id="{{id:'select_card'}}" data-paymentId="{{:#data.paymentId}}" data-cardType="{{:#data.type}}" role="button" tabindex="0" {{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_pay_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_pay_with_venmo}}"{{/if}}>
					{{if #data.type ==='kohlsCharge'}}
						<p class="payment-card-top-text">{{: ~cncLabels.kls_paymentForm_recommended}}</p>
					{{/if}}	
					<p class="payment-card-no ctHidden">{{: #data.cardTitle}}</p>
					<p class="payment-card-bottom-text ctHidden">{{: #data.cardDescription}}</p>
				</span>
				{{if #data.type != 'paypal' && #data.type != 'venmo'}}
					<div class="edit-saved-card"><a href="javascript:void(0)"><img id="{{id:'edit-initial-saved-card'}}" data-cardType="{{:#data.type}}" data-paymentId="{{:#data.paymentId}}" class="edit-saved-card-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
				{{/if}}
			</div>
		{{/if}}        
		<div class="payment-hr"></div>		
	{{/for}}
	</div>
<!-- END old code -->
</script>

<script id="tpl-paymentMultiCardRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
	<div id = "paymentMultiCardId">
		<div id="{{id:'multiCard-EventHandler'}}">
		{{for ~showMoreVisibleCards}}
		<div class="payment-card-options saved-card-bottom-pad other-payment-multi-card-list" style="padding-bottom:unset;">    
			<div class="payment-bg multiCard-selectCard multicard-selection-styling" data-paymentId="{{:#data.paymentId}}" data-cardType="{{:#data.type}}">
				<div class="parent-div-radio-btn align-radio-btn-other-payment align-radio-btn-other-payment">
					<span class="kds-radio-btn-positioning-payment-panel">
						<kds-radio label=""></kds-radio>
					</span>					
				</div>	
				<img class="card-type-icon" src={{__data.cardImage}} alt="{{: #data.type}}" title="">
				<span class="payment-card-saved-block payment-card-saved-block-width-100 kds" tabindex="0" role="button" {{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_pay_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_pay_with_venmo}}"{{/if}}>
					<p class="payment-card-num ctHidden body-02">{{: #data.cardTitleRedesign}}</p>
					<p class="payment-card-sub-txt ctHidden hide-expiration-date-card body-02">{{: #data.cardDescription}}</p>
				</span>
				{{if #data.type != 'paypal' && #data.type != 'venmo'}}
					<div {{if ~hideEditButton}} class="hide-expiration-date-card" {{else}} class="edit-pencil-text-style" {{/if}}>
						<a href="javascript:void(0)" class="edit-pencil-text-style multiCard-edit-card kds" data-paymentId="{{:#data.paymentId}}" data-cardType="{{:#data.type}}" >
							<img class="edit-saved-card-img pencil-image-height" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title="">
							<span class="span-edit-button-text edit-button-mr supporting-micro-01-link">{{:~cncLabels.kls_paymentForm_Edit}}</span>
						</a>
					</div>
				{{/if}}
			</div>	
		</div>
		{{/for}}
		</div>
    </div>
<!-- END redesign code -->
</script>

<script id="tpl-paymentMultiCard" type="text/x-jsrender">

	<div id = "paymentMultiCardId">
	<div id="{{id:'multiCard-EventHandler'}}">
	{{for ~showMoreVisibleCards}}
	<div class="payment-card-options saved-card-bottom-pad">    
		<div class="payment-bg multiCard-selectCard" data-paymentId="{{:#data.paymentId}}" data-cardType="{{:#data.type}}">
			<span class="card-select-marker"></span>
			<span><img class="kohls-charge-img" src={{__data.cardImage}} alt="" title=""></span>
			<span class="payment-card-saved-block" tabindex="0" role="button" {{if #data.type == 'paypal'}}aria-label="{{:~cncLabels.ADA_pay_with_paypal}}"{{/if}}{{if #data.type == 'venmo'}}aria-label="{{:~cncLabels.ADA_pay_with_venmo}}"{{/if}}>
				<p class="payment-card-no ctHidden">{{: #data.cardTitle}}</p>
				<p class="payment-card-bottom-text ctHidden">{{: #data.cardDescription}}</p>
			</span>
			{{if #data.type != 'paypal' && #data.type != 'venmo'}}
				<div class="edit-saved-card"><a href="javascript:void(0)"><img data-paymentId="{{:#data.paymentId}}" data-cardType="{{:#data.type}}" class="edit-saved-card-img multiCard-edit-card" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
			{{/if}}
		</div>	
	</div>
	{{/for}}
	</div>
        </div>
</script>

<script id="tpl-paymentMultiCardContainerRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
    <div class="payment-card-options saved-card-bottom-pad">
   		{{for tmpl='tpl-paymentSingleCardLoggedInRedesign' ~env = ~env/}}
    </div>
	<div class="kohls-charge-block-green-text" id="{{id:'show-fewer-cards-btn'}}">{{: cncLabels.kls_paymentForm_show_few_card}}</div>
<!-- END redesign code -->
</script>


<script id="tpl-paymentMultiCardContainer" type="text/x-jsrender">
<!-- BEGIN old code -->
    <div class="payment-card-options saved-card-bottom-pad">
   		{{for tmpl='tpl-paymentSingleCardLoggedIn' ~env = ~env/}}
    </div>
	<div class="kohls-charge-block-green-text" id="{{id:'show-fewer-cards-btn'}}">{{: cncLabels.kls_paymentForm_show_few_card}}</div>
	{{for tmpl="tpl-paymentMultiCard" ~showMoreVisibleCards=paymentData.showMoreVisibleCards ~env = $env ~cncLabels = cncLabels/}}
<!-- END old code -->
</script>

<script id="tpl-moreWaysToPayFooterRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
	<div class="payment-hr"></div>
        {{if state == "addCardForm" || state == "showingAllCards_AddCard_Form" || state == "showingSingleCard_AddCard_Form"}}
			{{for tmpl='tpl-addPaymentFormRedesign' /}}
		{{/if}}
<!-- END redesign code -->
</script>

<script id="tpl-moreWaysToPayFooter" type="text/x-jsrender">
<!-- BEGIN old code -->
	<div class="payment-hr"></div>
        {{if state == "addCardForm" || state == "showingAllCards_AddCard_Form" || state == "showingSingleCard_AddCard_Form"}}
			{{for tmpl='tpl-addPaymentForm' /}}
		{{/if}}
<!-- END old code -->
</script>


<script id="tpl-thirdPartyButtonsRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
	{{if ~root.$env.enablePaypalFeature || ~root.$env.enableVenmoFeature || ~root.$env.enablePayLaterFeature}}
		<div class="third-party-container">
			<p {{if isUserLoggedIn}} class="linePlusTextInMiddleOtherPaymentMethods kds" {{else}} class="linePlusTextInMiddle kds" {{/if}}><span class="body-01">{{:cncLabels.kls_paymentForm_divider_or}}</span></p>
			
			<div class="other-payment-methods-container kds">
				{{if isUserLoggedIn}}
					<div class="other-payment-methods-title other-payment-methods-no-card body-02">{{:cncLabels.kls_other_payment_methods}}</div>
				{{/if}}
				{{if !~root.$env.enablePayLaterFeature}}
					<div class="paypal-venmo-container">
						<div class="paypal-button KAS-paypal-checkout paypal-btn-redesign">
							<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
							{{if ~root.$env.enablePaypalFeature}}
								<div id="paypal-button" class="paypal-button-class-redesign"></div>
							{{/if}}
						</div>
						{{if ~root.$env.enableVenmoFeature}}
							<button id="venmo-button" class="payment-option-button venmo-btn hide">
								<img class="venmo-button-logo" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Venmo">
							</button>
						{{/if}}
					</div>
				{{else}}
					<div class="paypal-button KAS-paypal-checkout paypal-btn-redesign">
						<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
						{{if ~root.$env.enablePaypalFeature}}
							<div id="paypal-button" class="paypal-button-class-redesign"></div>
						{{/if}}
					</div>
					<div class="venmo-paylater-container">
						{{if ~root.$env.enableVenmoFeature}}
							<button id="venmo-button" class="payment-option-button venmo-btn hide">
								<img class="venmo-button-logo" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Venmo">
							</button>
						{{/if}}
						{{if ~root.$env.enablePayLaterFeature}}
							<div id="paylater-button"></div>
						{{/if}}
					</div>
				{{/if}}
			</div>
		</div>
	{{/if}}
	<!-- END redesign code -->
</script>


<script id="tpl-thirdPartyButtons" type="text/x-jsrender">
<!-- BEGIN old code -->
	{{if ~root.$env.enablePaypalFeature || ~root.$env.enableVenmoFeature || ~root.$env.enablePayLaterFeature}}
		<div class="third-party-container" >
			<div class="paypal-button KAS-paypal-checkout">
				<img alt="Loading..." src="{{v_~root.$resourceRoot('cnc')}}images/ajax-loader.gif" class="loader-img">
				{{if ~root.$env.enablePaypalFeature}}
					<div id="paypal-button"></div>
				{{/if}}
				{{if ~root.$env.enableVenmoFeature}}
					<button id="venmo-button" class="payment-option-button hide">
						<img class="venmo-button-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/venmo.png" alt="Paying with venmo">
					</button>
				{{/if}}
				{{if ~root.$env.enablePayLaterFeature}}
					<div id="paylater-button"></div>
				{{/if}}
			</div>
		</div>
	{{/if}}
	<!-- END old code -->
</script>


<script id="tpl-addPaymentFormRedesign" type="text/x-jsrender">      
<!-- BEGIN redesign code -->
	<div class="card-details-block validateInline" id="addNewCardForm">
			<div class="payment-header-container ">				
				{{if state == "editingCard" || (state == "showingSingleCard_AddCard_Form" && isUserLoggedIn)}}
					{{if state == "showingSingleCard_AddCard_Form" && isUserLoggedIn}}
						<div class="payment-header-title-card">{{:cncLabels.kls_paymentForm_add_credit_or_debit_card}}</div>
						<img id="{{id:'payment-cancel-btn'}}" class="edit-card-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
					{{else}}
						<div class="payment-header-title-card">{{:cncLabels.kls_paymentForm_edit_card_details}}</div>
						<img id="{{id:'payment-cancel-btn'}}" class="edit-card-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
					{{/if}}					
				{{else}}
					<div class="payment-header-title-card">Pay ${{:remainingPayment}} with</div>
				{{/if}}
			</div>
			{{if state == "editingCard"}}
				<div class="edit-payment-card">		
					<img class="saved-card-block-img" src="{{_editCardData____editCardData.cardImage___editCardData.cardImage____cnc_media_images_dgsImages_paymentPanelImages_credit-card@3x.png_}}" alt="" title="">
					<div class="parent-div-card-image kds">
						<span class="card-title-text body-02">{{:editCardData.cardTitleRedesign}}</span>
						<p {{if #data.editCardData.type ==='kohlsCharge' || #data.editCardData.type ==='visa_kohls'}} class="payment-card-bottom-text-edit ctHidden" {{else}} class="payment-card-bottom-text-edit ctHidden hide-expiration-date-card" {{/if}}>{{: #data.cardDescription}}</p>
					</div>					
				</div>
			{{/if}}
			
			{{if state != "editingCard" && state != "showingSingleCard_AddCard_Form" && $env.ksEnablePaymentPanelRedesign}}
				<div class="kds">
					<div class="other-payment-methods-title card-title-text2 body-02">{{:cncLabels.kls_static_payment_title_text}}</div>
				</div>
				<div class="card-title-images"> 
					<img class="card-type-icon1" src="{{v_~root.$env.KDSAssetsPath}}/kohls/payments/kohlsCard.svg" alt="Kohls Card">
					<img class="card-type-icon1" src="{{v_~root.$env.KDSAssetsPath}}/kohls/payments/visa.svg" alt="Visa">
					<img class="card-type-icon1" src="{{v_~root.$env.KDSAssetsPath}}/kohls/payments/masterCard.svg" alt="MasterCard" >
					<img class="card-type-icon1" src="{{v_~root.$env.KDSAssetsPath}}/kohls/payments/discover.svg" alt="Discover" >
					<img class="card-type-icon1" src="{{v_~root.$env.KDSAssetsPath}}/kohls/payments/americanExpress.svg" alt="American Express">
				</div>
			{{/if}}
			<div {{if isUserLoggedIn}} class="add-payment-card-level card-form-group input-parent-card-form-group input-mobile" {{else}} class="add-payment-card-level card-form-group input-parent-card-form-group" {{/if}}>
				<label for="cardholder-name" class="form-group-label card-label-text-styling">{{:cncLabels.kls_paymentForm_name_on_card}}</label>
				<input type="text" class="card-form-input required text-input-alphanumeric-space input-box-border-style" minlength = "2" maxlength="90" name ="nameOnCard" id="cardholder-name" value="{{:editCardData && editCardData.nameOnCard ? editCardData.nameOnCard : '' }}" >				
				<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-hexagon@3x.png" alt="Kohls" class="alert-circle-icon alert-hexagon"></img>
				<div class="error-message-contact ml-24"></div>
			</div>
			<div {{if isUserLoggedIn}} class="add-payment-card-level card-form-group input-parent-card-form-group input-mobile" {{else}} class="add-payment-card-level card-form-group input-parent-card-form-group" {{/if}} {{if state == "editingCard"}} style="visibility:hidden; height:0; padding:0;"{{/if}}>
				<div class="form-group-label card-label-text-styling">{{:cncLabels.kls_paymentForm_card_number}}</div>											
				<input type="tel" class="card-form-input card-form-input-mid required card-number-input card-number-entry input-box-border-style" aria-label="{{:cncLabels.kls_paymentForm_card_number}}" minlength="12" maxlength="16" name="cardNum" id="{{id:'cardNumberInput'}}" value={{:editCardData && editCardData.cardNum ? ~getCreditCard(editCardData.cardNum) : " " }}>		
				<div class="card-type-img-input card-type-image-display"><img id="cardImage" {{if }} class="card-image-input card-popup-mobile-styling" {{else}} class="card-image-input" {{/if}} src="{{_editCardData____editCardData.cardImage___editCardData.cardImage____cnc_media_images_dgsImages_paymentPanelImages_credit-card@3x.png_}}" alt="" title=""></div>
				<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-hexagon@3x.png" alt="Kohls" class="alert-circle-icon alert-hexagon"></img>
				<div class="error-message-contact ml-24"></div>						
			</div>
			
			<div class="two-pair-input-block exp-date-cvv-container input-parent-card-form-group" style= {{if state == "editingCard" && !kohlsChargeSelected}} "display:flex" {{else}} "display:none" {{/if}}>
				<div {{if isUserLoggedIn}} class="add-payment-card-level card-form-group exp-date-container expiration-date-cvv-pad" {{else}} class="add-payment-card-level card-form-group exp-date-container" {{/if}}>				
					<div class="form-group-label card-label-text-styling">{{:cncLabels.kls_paymentForm_card_expiration_date}}</div>
					<input type="text" class="card-form-input card-form-input-small cardExpiry required expiryDate-input input-box-border-style" aria-label="{{:cncLabels.kls_paymentForm_card_expiration_date}}" name ="expMonth" id="{{id:'cardExpiry'}}" minlength="5" maxlength="5" placeholder="MM/YY" value={{:editCardData && editCardData.expMonth && editCardData.expYear ? ~getExpiryDate(editCardData.expMonth,editCardData.expYear) : "" }}>
					<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-hexagon@3x.png" alt="Kohls" class="alert-circle-icon alert-hexagon"></img>
					<div class="error-message-contact ml-24"></div>

				</div>
				<div {{if isUserLoggedIn}} class="add-payment-card-level card-form-group cvv-container expiration-date-cvv-pad" {{else}} class="add-payment-card-level card-form-group cvv-container" {{/if}}>
					<div class="form-group-label card-label-text-styling">{{:cncLabels.kls_paymentForm_CVV}}</div>
					<input type="tel" class="card-form-input card-form-input-small required number-input cardSecurityCode input-box-border-style" aria-label="{{:cncLabels.kls_paymentForm_CVV}}" name="cvv" minlength="3" maxlength="4" id="{{id:'cardSecurityCode'}}" value="" placeholder="{{:cncLabels.kls_cvv_input_placeholder}}">
					{{if state == "editingCard"}}
						<div class="enter-cvv-edit-card">{{:cncLabels.kls_valid_cvv_required_error}}</div>
					{{/if}}
					<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-hexagon@3x.png" alt="Kohls" class="alert-circle-icon alert-hexagon"></img>
					<div class="error-message-contact ml-24 cvv-error-msg"></div>
				</div>
			</div>
		</div>
		{{if isUserLoggedIn }}
			{{if state != "editingCard"}}
			<div {{if state == "showingSingleCard_AddCard_Form" && isUserLoggedIn}} class="add-payment-card-level card-form-group parent-div-checkbox-save-card-later" {{else}} class="add-payment-card-level card-form-group" {{/if}} id="add-payment-checkbox-save">
				<label class="save-card-container save-card-container-redesign">{{:cncLabels.kls_paymentForm_save_card}}
					<input type="checkbox" class="saveCard saveCardInputCheckboxRedesign" id="{{id:'saveCard'}}">
						<span {{if (state == "showingSingleCard_AddCard_Form" || state == "default") && isUserLoggedIn}}  class="save-card-checkbox-redesign" {{else}} class="save-card-checkbox" {{/if}}></span>
				</label>
			</div>
			{{/if}}
		{{/if}}
		{{for tmpl='tpl-paymentFormButtonsTemplateRedesign' ~editCardData=editCardData /}}
		{{if (state == "showingSingleCard_AddCard_Form" && !isUserLoggedIn) || (state == "editingCard" && !isUserLoggedIn)}}
			{{for tmpl="tpl-paymentAddCardRedesign" /}}
		{{/if}}
		<div class="links-to-kohls-text"style="display:none">{{:cncLabels.kls_paymentForm_linked_kohls_pay}}</div>
	</div>
<!-- END redesign code -->
</script>

<script id="tpl-addPaymentForm" type="text/x-jsrender">
<!-- BEGIN old code -->
	{{if editView && editView =='thirdParty'}}
		<div id="edit-user-card">
			<div class="card-details-block validateInline" id="addCardForm">
				<div class="payment-common-header editHeader" style="">{{:cncLabels.kls_paymentForm_edit_paypal_details}}</div>
				{{if isUserLoggedIn}}
					<span class="edit-card-delete-icon" style=""><img id="{{id:'delete-card'}}" data-paymentId ={{:editCardData.paymentId}} class="edit-card-delete-icon-img paymentDeleteButton" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/trash-icon.png" alt="" title=""></span>
				{{/if}}	
				<div class="payment-card-options">
					<div class="kohls-charge-block no-border third-party-edit">
						<span class="kohls-charge-block-style">
							<img class="kohls-charge-block-img" src="{{_editCardData.cardImage}}" alt="" title=""></span>
						<span class="kohls-charge-block-text">
						<p class="kohls-charge-head-saved-card">{{:editCardData.cardNum}}</p>
						</span>
					</div>
					{{for tmpl="tpl-thirdPartyButtons" /}}
					<kds-button title="{{:~cncLabels.kls_contactForm_cancel_button_text}}" size="sm" class="paypal-cancel-btn"
						id="{{id:'payment-cancel-btn'}}">
						{{:~cncLabels.kls_contactForm_cancel_button_text}}
					</kds-button>
				</div>
			</div>
		</div>
    {{else kohlsChargeSelected}}
        
			<div class="card-details-block validateInline" id="addKohlsChargeForm">
				<div class="payment-header-container">
					<div class="payment-common-header">{{:cncLabels.kls_paymentForm_card_details}}</div>
					{{if state == "editingCard"}}
						<img id="{{id:'payment-cancel-btn'}}" class="edit-card-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
					{{/if}}
				</div>
				<div class="add-payment-card-level card-form-group">
					<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
					<input type="text" class="card-form-input required text-input-alphanumeric-space" minlength = "2" maxlength="90" name ="nameOnCard" id="kcCardholderName" value="{{:editCardData && editCardData.nameOnCard ? editCardData.nameOnCard : '' }}" >
					<div class="form-group-label">{{:cncLabels.kls_paymentForm_cardholder_name}}</div>
					<div class="error-message-contact"></div>
				</div>

				<div class="add-payment-card-level card-form-group">
					<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
					<div class="kohls-charge-event-img"><img class="kohls-charge-event-img-style" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/kohls-charge-card-no-padding-flat.png" alt="" title=""></div>
					<input type="text" class="card-form-input card-form-input-mid required number-input" minlength = "12" maxlength = "12" name = "cardNum" id = "kcCardNumber" value={{:editCardData && editCardData.cardNum ? ~getCreditCard(editCardData.cardNum) : " " }}>
					<div class="form-group-label">{{:cncLabels.kls_paymentForm_card_number}}</div>
					<div class="error-message-contact"></div>
				</div>
			</div>
			{{if isUserLoggedIn}}
				{{if state != "editingCard"}}
				<div class="add-payment-card-level card-form-group" style="display:none">
					<label class="save-card-container">{{:cncLabels.kls_paymentForm_save_card}}
						<input type="checkbox" id="kcSaveCard">
							<span class="save-card-checkbox"></span>
					</label>
				</div>
				{{/if}}
			{{/if}}
			{{for tmpl='tpl-paymentFormButtonsTemplate' ~editCardData=editCardData /}}
			<div class="links-to-kohls-text"style="display:none">{{:cncLabels.kls_paymentForm_linked_kohls_pay}}</div>
		</div>
        
    {{else cardSelected}}
                <div id="edit-user-card">
					<div class="card-details-block validateInline" id="addCardForm">
						<div class="payment-header-container">
							<div class="payment-common-header">{{:cncLabels.kls_paymentForm_card_details}}</div>
							{{if state == "editingCard"}}
								<img id="{{id:'payment-cancel-btn'}}" class="edit-card-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
							{{/if}}
						</div>
						<div class="add-payment-card-level card-form-group">
							<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
							<input type="text" class="card-form-input required text-input-alphanumeric-space" minlength = "2" maxlength="90" name ="nameOnCard" id="cardholderName" value="{{:editCardData && editCardData.nameOnCard ? editCardData.nameOnCard : '' }}">
							<div class="form-group-label">{{:cncLabels.kls_paymentForm_cardholder_name}}</div>
                            <div class="error-message-contact"></div>
						</div>
						<div class="add-payment-card-level card-form-group">
						<div class="kohls-charge-event-img"><img id="cardImage" class="kohls-charge-event-img-style" src="{{_editCardData____editCardData.cardImage___editCardData.cardImage____cnc_media_images_dgsImages_paymentPanelImages_credit-card@3x.png_}}" alt="" title=""></div>
						<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
							<input type="text" class="card-form-input card-form-input-mid cardNumber required card-number-input" minlength = "15" maxlength="16" name ="cardNum" id="{{id:'cardNumber'}}" value={{:editCardData && editCardData.cardNum ? ~getCreditCard(editCardData.cardNum) : " " }}>
							<div class="form-group-label">{{:cncLabels.kls_paymentForm_card_number}}</div>
							<div class="error-message-contact"></div>
						</div>
						<div class="two-pair-input-block">
							<div class="add-payment-card-level card-form-group">
								<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
								<input type="text" class="card-form-input card-form-input-small cardExpiry required expiryDate-input" name ="expMonth" id="{{id:'cardExpiry'}}" minlength = "5" maxlength="5" value={{:editCardData && editCardData.expMonth && editCardData.expYear ? ~getExpiryDate(editCardData.expMonth,editCardData.expYear) : "" }}>
								<div class="form-group-label">{{:cncLabels.kls_paymentForm_card_expiration}}</div>
								<div class="error-message-contact"></div>

							</div>
							<div class="add-payment-card-level card-form-group">
							<img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
								<input type="text" class="card-form-input card-form-input-small required number-input cardSecurityCode" name="cvv" minlength="3" maxlength="4" id="{{id:'cardSecurityCode'}}" value="">
								<div class="form-group-label">{{:cncLabels.kls_paymentForm_card_security_code}}</div>
								<div class="error-message-contact"></div>
							</div>
						</div>
						{{if isUserLoggedIn}}
							{{if state != "editingCard"}}
								<div class="add-payment-card-level card-form-group">
									<label class="save-card-container">{{:cncLabels.kls_paymentForm_save_card}}
										<input type="checkbox" class="saveCard" id="{{id:'saveCard'}}">
										<span class="save-card-checkbox"></span>
									</label>
								</div>
							{{/if}}
						{{/if}}
						{{for tmpl='tpl-paymentFormButtonsTemplate' ~editCardData=editCardData /}}
					</div>
                </div>
    {{/if}}
<!-- END old code -->
</script>


<script id="tpl-paymentFormButtonsTemplateRedesign" type="text/x-jsrender">
<!-- BEGIN redesign code -->
	{{if isUserLoggedIn && editCardData}}
		<div class="contact-btn-block save-cancel-button-style">
			<button class="delete-card-btn" id="{{id:'delete-card'}}" data-paymentId ={{:editCardData && editCardData.paymentId ? editCardData.paymentId : " " }}>
				{{:state == "editingCard" && isUserLoggedIn ? cncLabels.kls_paymentForm_button_label_remove_card :cncLabels.kls_paymentForm_button_label_delete}}
			</button>
			<kds-button title="{{:cncLabels.kls_paymentForm_button_label_cancel}}" size="sm" button-type="secondary" class= {{if editCardData}} "cancel-form-payment editing-card" {{else}} "cancel-form-payment" {{/if}}
			id="{{id:'payment-cancel-btn'}}">
				{{:cncLabels.kls_paymentForm_button_label_cancel}}
			</kds-button>
			<kds-button title="{{:cncLabels.kls_paymentForm_button_label_save}}" size="sm" class="save-form-payment" id="{{id:'savePaymentRedesign'}}"
			data-paymentId ={{:editCardData && editCardData.paymentId ? editCardData.paymentId : " " }}>
				{{:cncLabels.kls_paymentForm_button_label_save}}
			</kds-button>
		</div>
		{{else}}
			{{if !isUserLoggedIn && state == "editingCard"}}
				<div class="contact-btn-block save-cancel-button-style">
					<button class= {{if editCardData}} "cancel-edit-card-btn cancel-form-payment editing-card" {{else}} "cancel-edit-card-btn cancel-form-payment" {{/if}}
					id="{{id:'payment-cancel-btn'}}">
						{{:cncLabels.kls_link_label_cancel}}
					</button>
					<kds-button title="{{:cncLabels.kls_paymentForm_button_label_use_this_card}}" size="sm" class="save-form-payment" id="{{id:'savePaymentRedesign'}}"
					data-paymentId ={{:editCardData && editCardData.paymentId ? editCardData.paymentId : " " }}>
						{{:cncLabels.kls_paymentForm_button_label_use_this_card}}
					</kds-button>
				</div>
			{{else}}
				<div {{if isUserLoggedIn}} class="contact-btn-block use-this-card-container contact-btn-block-redesign" {{else}} class="contact-btn-block use-this-card-container" {{/if}}>
					<div {{if state == "showingSingleCard_AddCard_Form" && isUserLoggedIn}} class="use-this-card use-this-card-add-popup-auth" {{else}} class="use-this-card" {{/if}} {{if !(state == "showingSingleCard_AddCard_Form" && isUserLoggedIn)}} style="display:none" {{/if}}>
						{{if state == "showingSingleCard_AddCard_Form" && isUserLoggedIn}}
							<span style="margin-right:5px;">
								<kds-button title="{{:cncLabels.kls_paymentForm_button_label_cancel}}" size="sm" button-type="secondary" class= {{if editCardData}} "cancel-form-payment editing-card" {{else}} "cancel-form-payment" {{/if}}
								id="{{id:'payment-cancel-btn'}}">
									{{:cncLabels.kls_paymentForm_button_label_cancel}}
								</kds-button>
							</span>							
						{{/if}}
						<kds-button title="{{:(state == 'showingSingleCard_AddCard_Form' && isUserLoggedIn) ? cncLabels.kls_paymentForm_add_another_card : cncLabels.kls_paymentForm_button_label_use_this_card}}" size="sm" {{if state == "showingSingleCard_AddCard_Form" && isUserLoggedIn}} class="disable-class-btn-add-card" {{else isUserLoggedIn}} class="save-form-payment save-form-payment-btn-redesign" {{else}} class="save-form-payment" {{/if}} id="{{id:'savePaymentRedesign'}}"
						data-paymentId ={{:editCardData && editCardData.paymentId ? editCardData.paymentId : " " }}>
							{{:(state == 'showingSingleCard_AddCard_Form' && isUserLoggedIn) ? cncLabels.kls_paymentForm_add_another_card : cncLabels.kls_paymentForm_button_label_use_this_card}}
						</kds-button>
					</div>
				</div>
			{{/if}}
		{{/if}}
    </div>
	{{if  state == "addCardForm" || state == "default"}}
		{{for tmpl="tpl-thirdPartyButtonsRedesign" /}}	
	{{/if}}	
<!-- END redesign code -->
</script>

<script id="tpl-paymentFormButtonsTemplate" type="text/x-jsrender">
<!-- BEGIN old code -->
    <div class="contact-btn-block">
		{{if isUserLoggedIn && editCardData}}
		<button class="delete-card-btn" id="{{id:'delete-card'}}" data-paymentId ={{:editCardData && editCardData.paymentId ? editCardData.paymentId : " " }}>
			{{:cncLabels.kls_paymentForm_button_label_delete}}
		</button>
		{{/if}}
		<kds-button title="{{:cncLabels.kls_paymentForm_button_label_cancel}}" size="sm" button-type="secondary" class= {{if editCardData}} "cancel-form-payment editing-card" {{else}} "cancel-form-payment" {{/if}}
		id="{{id:'payment-cancel-btn'}}">
            {{:cncLabels.kls_paymentForm_button_label_cancel}}
        </kds-button>
        <kds-button title="{{:cncLabels.kls_paymentForm_button_label_save}}" size="sm" class="save-form-payment" id="{{id:'savePayment'}}"
		data-paymentId ={{:editCardData && editCardData.paymentId ? editCardData.paymentId : " " }}>
            {{:cncLabels.kls_paymentForm_button_label_save}}
        </kds-button>
    </div>
<!-- END old code -->
</script>
<script id="tpl-addressyTemplate" type="text/x-jsrender">
{{if showShippingModule}}
<div class="address-save-msg sr-only" role="status" aria-live="polite" style="display:none;">Address saved</div>
    <form id="shipping_addressy_form_v3" class="full-screen-width-parent-selector">
        <div class="shipping-common-block new-popup-design-selector" id={{id:'addressyEventHandler'}}>
            <div class="shipping-common-header">{{:cncLabels.kls_shipping_module_label}}</div>
            <div class="cart-thumbs-container">
                {{if shippingDataArray}}
                    <ul class="cart-thumbs-block">
                        {{for shippingDataArray}}
                            {{for imageUrl}}
                                <img class="cart-thumbs-block-img" src={{_ #data}} alt="Ship to home item" title="">
                            {{/for}}
                            <div class="shipping-arriving">{{:formattedDateLabel}}</div>
                        {{/for}}
                    </ul>
                {{/if}}
                <div id="addressyContainer">
                {{if showVerifyAddress}}
                    <div class="shipping-common-verify-header">{{:cncLabels.kls_shipping_verifyaddress_title}}</div>
                    <p class="shipping-common-desc">{{:cncLabels.kls_shipping_verify_address_message}}</p>
                    <p class="shipping-common-desc-nomargin">{{:cncLabels.kls_shipping_entered_address}}</p>
                {{/if}}
                {{if cartJsonData.addressDetails && cartJsonData.addressDetails.shipAddress && cartJsonData.addressDetails.shipAddress.length &&cartJsonData.addressDetails.shipAddress['0'].firstName}}
                    {{if cartJsonData.multipleAddress || isLoggedIn}}
                    {{for cartJsonData.addressDetails.shipAddress}}
                    {{if selected && #data.firstName}}
                    <div class="multiple-shipping-address-block">
                            <div class="multiple-shipping-bg-active">
                                <span class="address-select-marker-active"></span>
                                <span class="address-saved-block">
                                <!-- NOTE: the ctHidden class is used to tag PII for Contentsquare. Do not remove unless an alternative method for PII masking has been implemented. -->
                                    <p class="address-top-text ctHidden">{{: #data.firstName}} {{: #data.lastName}}</p>
                                    <p class="address-no-bold ctHidden">{{: #data.addr1}} {{: #data.addr2}}</p>
                                    <p class="address-no-bold ctHidden">{{: #data.city}} {{: #data.state}} {{: #data.postalCode}}</p>
                                </span>
                                <div class="saved-address-edit editSingleAddress" data-id={{:#data.id}} alt={{:#getIndex()}} ><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
                            </div>
                        </div>
                        {{if ~root.showVerifyAddress}}
                        <!-- 05/09/2022 - DA - Could not find any test case to validate the changes made to the div+button below -->
                            <div class="contact-btn-block reduced-btn-margin">
                                <kds-button class="editSingleAddress" data-id={{: #data.id}} alt={{:#getIndex()}} 
                                    title="{{:cncLabels.kls_shippingForm_button_label_verify}}" size="sm">
                                    {{:~root.cncLabels.kls_shippingForm_button_label_verify}}
                                </kds-button>
                            </div>
                        {{/if}}
                        {{/if}}
                    {{/for}}
                        <div class="common-hr"></div>
                        <div id="militaryAddressTpl">
                            {{for tmpl="tpl-militaryAddressTemplate" #data /}}
                        </div>
                        {{if cartJsonData.multipleAddress && cartJsonData.addressDetails.shipAddress  && cartJsonData.addressDetails.shipAddress[0].firstName}}
                         <div class="common-green-text common-margin-left showMoreAddressForAddressyPanel" id="{{id:'showMoreAddress'}}">{{:cncLabels.kls_show_more_address}}</div>
                          {{else}}
                          <div class="common-green-text common-margin-left" id="{{id:'addAddress'}}">{{:cncLabels.kls_billing_add_new_address}}</div>
                      {{/if}}
                        <!--Start multiple address with show more -->
                        <div id ="{{id:'multipleAddressBlock'}}" class= "showMoreAddressText" style="display:none">
                        {{if cartJsonData.multipleAddress}}
                           {{for showMoreAddressData}}
                             {{if !selected && #data.firstName }}
                            <div class="multiple-shipping-address-block" id ="{{id:'multipleAddressBlock'}}" data-index={{:#getIndex()}} data-id="{{:#data.id}}" >
                                <div class="multiple-shipping-bg">
                                    <span class="address-select-marker"></span>
                                    <span class="address-saved-block">
                                        <p class="address-no-bold ctHidden">{{: #data.firstName}} {{: #data.lastName}}</p>
                                        <p class="address-no-bold ctHidden">{{: #data.addr1}} {{: #data.addr2}}</p>
                                        <p class="address-bottom-text ctHidden">{{: #data.city}} {{: #data.state}} {{: #data.postalCode}}</p>
                                        {{if #data.verificationRequired }}
                                        <p class="verificationNeeded">{{:~root.cncLabels.kls_shipping_verify_address}}</p>
                                        {{/if}}
                                    </span>
                                    <div class="saved-address-edit" id="{{id:'editMultipleAddress'}}" data-id={{: #data.id}} alt={{:#getIndex()}} 
 ><a href="javascript:void(0);" class="verify_address_{{:#getIndex()}}"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
                                </div>
                            </div>
                            
                            <div class="common-hr"></div>
                              {{/if}}
                             {{/for}}
                             {{if addNewAddress}}
                                <div class="common-green-text common-margin-left" id="{{id:'addAddress'}}">{{:~root.cncLabels.kls_billing_add_new_address}}</div>
                             {{/if}}
                           {{/if}}
                        </div>
                        <!--End multiple address after show more-->
                        <!--End multiple address-->
                        {{else}}
                                <div class="shipping-address-top-margin"></div>
                                <div class="saved-address-edit" id="{{id:'editAddress'}}"><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
                                <div class="saved-address">                                
                                    <div class="saved-address-header">{{:cartJsonData.addressDetails.shipAddress[0].firstName}} {{:cartJsonData.addressDetails.shipAddress[0].lastName}}</div>
                                    <div class="saved-address-detail">{{:cartJsonData.addressDetails.shipAddress[0].addr1}} {{:cartJsonData.addressDetails.shipAddress[0].addr2}}</div>
                                    <div class="saved-address-sub-detail">{{:cartJsonData.addressDetails.shipAddress[0].city}}, {{:cartJsonData.addressDetails.shipAddress[0].state}} {{:cartJsonData.addressDetails.shipAddress[0].postalCode}}</div>
                                </div>
                            <div class="common-hr"></div>
                            <div id="militaryAddressTpl">
                            {{for tmpl="tpl-militaryAddressTemplate" #data /}}
                            </div>
                              <div class="common-green-text common-margin-left" id="{{id:'addAddress'}}">{{:cncLabels.kls_billing_add_new_address}}</div>
                        {{/if}}  
                        <div id = "addressPanel" style="display:none">
                            <div class="bill-to-search-input-block">
                                <div><img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/search@3x.png" alt="Kohls" class="searchbox-img"><input type="text" id="tr_add_us_address_suggestion" placeholder="Search for an address" onfocusin="this.placeholder = ''" onfocusout="this.placeholder = 'Search for an address'" class="bill-to-search-input-box shipping-search-input addressyEventHandler searchAddressBox"><a href="javascript:void(0);" id="{{id:'addressyShippingCancelButton'}}" class="shipping-input-cancel">{{:cncLabels.kls_link_label_cancel}}</a></div>
                            </div>
                            <div class="common-green-text center-align"  id="{{id:'enterAddress'}}">{{:cncLabels.kls_shipping_enter_address}}</div>
                            <div class="common-hr"></div>
                            <div class="common-green-text common-margin-left" id="{{id:'addMilitaryAddress'}}">{{:cncLabels.kls_shipping_ship_military_address}}</div>
                            <div id = "suggestedAddressContainer"></div>
                        </div>  
                {{else}}
                    <div class="bill-to-search-input-block">
                        <div><img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/search@3x.png" alt="Kohls" class="searchbox-img"><input type="text" id="tr_add_us_address_suggestion" aria-label="Search for an address" placeholder="Search for an address" onfocusin="this.placeholder = ''" onfocusout="this.placeholder = 'Search for an address'" class="bill-to-search-input-box shipping-search-input addressyEventHandler searchAddressBox"><a href="javascript:void(0);" id="{{id:'addressyShippingCancelButton'}}" class="shipping-input-cancel">{{:cncLabels.kls_link_label_cancel}}</a></div>
                    </div>
                    <div class="common-green-text center-align" id="{{id:'enterAddress'}}">{{:cncLabels.kls_shipping_enter_address}}</div>
                    <div class="common-hr"></div>
                    <div class="common-green-text common-margin-left" id="{{id:'addMilitaryAddress'}}">{{:cncLabels.kls_shipping_ship_military_address}}</div>
                    <div id = "suggestedAddressContainer"></div>
                {{/if}}
                </div>
                <div id="addressForm" class="validateInline" style="display:none">
                    <div class="cart-thumbs-container">
                        <div class="shipping-common-header shipping-margin-mid">
                            {{if !isLoggedIn}} {{:cncLabels.kls_shipping_address_guest}} 
                            {{else}}
                                {{if cartJsonData.isEditAddress}}{{:cncLabels.kls_shipping_edit_address}} 
                                {{else}} {{:cncLabels.kls_shipping_new_address}}{{/if}}
                            {{/if}}
                        </div>
                            <div class="txtbox-form-group">
                                <label class="save-address-container">{{:cncLabels.kls_shipping_billing_address}}
                                    <input type="checkbox" class="topBillingAddr" id="{{id:'sameBillingAddressTop'}}" name="isBillAddressEqualtoShipAddress" {{if !isEditingSecondaryAddr}} checked {{/if}}>
                                    <span class="save-address-checkbox"></span>
                                </label>
                            </div>
                        <div class="txtbox-form-group-block" id="{{id: 'shipping-address-form'}}">
                        <span id ="{{id:'saveAddress'}}" class="edit-card-done-text" style="display:none">{{:cncLabels.kls_paymentForm_button_label_done}}</span>
                            <div class="txtbox-form-group">
                            {{if cartJsonData.userData && cartJsonData.userData.sessionStatus !='ANONYMOUS' && cartJsonData.isEditAddress}}
                              <img class="delete-shipping-address" id="{{id: 'delete-shipping-address'}}" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="Delete address" title="Delete address">
                             {{/if}}   
                                <input type="hidden" name="addressId" id="addressId" value= "{{:shippingAddressDetails.id}}" >
                                <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="firstName" class="txtbox-form-input  required text-input-name" id="firstName" value ="{{:shippingAddressDetails.firstName}}" maxlength="90" minLength="2">
                                <label for="firstName" class="form-group-label">{{:cncLabels.kls_contactForm_firstName_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="lastName" class="txtbox-form-input required text-input-name" id="lastName" value ="{{:shippingAddressDetails.lastName}}" maxlength="90" minLength="2">
                                <label for="lastName" class="form-group-label">{{:cncLabels.kls_contactForm_lastName_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="addr1" class="txtbox-form-input addressyEventHandler required"  id="tr_add_us_address1" value ="{{:shippingAddressDetails.addr1}}" maxlength="40" minLength="2">
                                <label for="tr_add_us_address1" class="form-group-label">{{:cncLabels.kls_shipping_street_address}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                                <input type="text" name="addr2" class="txtbox-form-input txtbox-form-input-small" id="tr_add_us_address2" value ="{{:shippingAddressDetails.addr2}}" minLength="0" maxlength="50" autocomplete="addressline2">
                                <label for="tr_add_us_address2" class="form-group-label">{{:cncLabels.kls_shipping_appartment}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="postalCode" maxLength="10" class="txtbox-form-input zipcode-input txtbox-form-input-small required" id="tr_add_us_zipcode" value ="{{:shippingAddressDetails.postalCode}}" minLength="5">
                                <label for="tr_add_us_zipcode" class="form-group-label">{{:cncLabels.kls_shipping_zip_code}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="two-pair-input-block">
                                <div class="txtbox-form-group">
                                <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                    <input type="text" name="city" class="txtbox-form-input txtbox-form-input-mid-half required text-input-alpha" id="tr_add_us_city" value ="{{:shippingAddressDetails.city}}" maxlength="40" minLength="2">
                                    <label for="tr_add_us_city" class="form-group-label">{{:cncLabels.kls_shipping_city}}</label>
                                    <div class="error-message-contact"></div>
                                </div>
                                <div class="txtbox-form-group">
                                <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                    <input type="text" name="state" class="txtbox-form-input txtbox-form-input-small-min required text-input-alpha" id="tr_add_us_state" value ="{{:shippingAddressDetails.state}}" minLength="2" maxlength="2">
                                    <label for="tr_add_us_state" class="form-group-label">{{:cncLabels.kls_shipping_state}}</label>
                                    <div class="error-message-contact"></div>
                                </div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="phoneNumber" maxLength="14" minLength="14" class="txtbox-form-input required phone-number-input" id="phoneNumber" value ="{{:shippingAddressDetails.phoneNumber}}">
                                <label for="phoneNumber" class="form-group-label">{{:cncLabels.kls_shipping_phone_number}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                                <label class="save-address-container">{{:cncLabels.kls_shipping_billing_address}}
                                    <input type="checkbox" class="bottomBillingAddr" id="{{id:'sameBillingAddressBottom'}}" name="isBillAddressEqualtoShipAddress" {{if !isEditingSecondaryAddr}} checked {{/if}}>
                                    <span class="save-address-checkbox"></span>
                                </label>
                            </div>
                            {{for tmpl='tpl-formButtonsTemplate' ~submitText=cncLabels.kls_shipping_button_label_use_address /}}
                        </div>  
                    </div>
                </div>
                <div id="militaryAddressForm" class="validateInline" style="display:none">
                    <div class="cart-thumbs-container">
                        <div class="shipping-common-header shipping-margin-mid">{{if cartJsonData.isEditAddress}}{{:cncLabels.kls_shipping_edit_military_address}}{{else}}{{:cncLabels.kls_shipping_new_military_address}}{{/if}}
                            <div><span><img class="usa-flag-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/shippingPanelImages/ic-usa-flag@3x.png" alt="" title=""></span><span class="military-service-thanks-msg">{{:cncLabels.kls_shipping_military_message}}</span></div>
                        </div>
                        <div class="txtbox-form-group">
                                <label class="save-address-container">{{:cncLabels.kls_shipping_billing_address}}
                                    <input type="checkbox" class="topBillingAddrMilitary" id="{{id:'sameBillingAddressMilitaryTop'}}" name="isBillAddressEqualtoShipAddress" {{if !isEditingSecondaryAddr}} checked {{/if}}>
                                    <span class="save-address-checkbox"></span>
                                </label>
                            </div>
                        {{if cartJsonData.userData && cartJsonData.userData.sessionStatus !='ANONYMOUS' && cartJsonData.isEditAddress}}
                            <span class="edit-address-delete-icon" id="{{id:'delete-shipping-address'}}">
                                <img class="edit-address-delete-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="" title="">
                            </span>
                        {{/if}}
                        <span class="edit-address-done-text" id ="{{id:'saveAddress'}}" style="display:none">{{:cncLabels.kls_paymentForm_button_label_done}}</span>
                        <div class="txtbox-form-group-block" id="{{id:'shipping-military-form'}}">
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="firstName" maxlength="90" minLength="2" class="txtbox-form-input text-input-name required" value ="{{:shippingAddressDetails.firstName}}" id="militaryFirstName">
                                <label for="militaryFirstName" class="form-group-label">{{:cncLabels.kls_contactForm_firstName_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="lastName" maxlength="90" minLength="2" class="txtbox-form-input text-input-name required" value ="{{:shippingAddressDetails.lastName}}" id="militaryLastName">
                                <label for="militaryLastName" class="form-group-label">{{:cncLabels.kls_contactForm_lastName_text}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group" >
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="addr1" class="txtbox-form-input required" id="militaryAdd1" maxlength="40" value ="{{:shippingAddressDetails.addr1}}">
                                <label for="militaryAdd1" class="form-group-label">{{:cncLabels.kls_shipping_unit_box_number}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group" >
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="postalCode" maxLength="10" minLength="5" class="txtbox-form-input zipcode-input txtbox-form-input-small required" id="zipCodeMilitary" value ="{{:shippingAddressDetails.postalCode}}">
                                <label for="zipCodeMilitary" class="form-group-label">{{:cncLabels.kls_shipping_zip_code}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group military-city-state-block">
                                <div class="form-group-label military-city-state-block-top-pad">{{:cncLabels.kls_shipping_city}}</div>
                                    <div class="military-city-block">
                                    <div class="ship-military-city-box" name="APO" id="{{id:'militaryCity'}}">{{:cncLabels.kls_shipping_apo}}</div>
                                    <div class="military-right-margin"></div>
                                    <div class="ship-military-city-box" name="FPO" id="{{id:'militaryCity'}}">{{:cncLabels.kls_shipping_fpo}}</div>
                                    <div class="military-right-margin"></div>
                                    <div class="ship-military-city-box" name="DPO" id="{{id:'militaryCity'}}">{{:cncLabels.kls_shipping_dpo}}</div>
                                    <div class="military-right-margin"></div>
                                </div>
                                <div class="error-message-contact error-shipping-military-city"></div>
                            </div>
                            <div class="txtbox-form-group military-city-state-block">
                                <div class="form-group-label military-city-state-block-top-pad">{{:cncLabels.kls_shipping_state}}</div>
                                    <div class="military-city-block" >
                                    <div class="ship-military-state-box" name="AA" id="{{id:'militaryState'}}">{{:cncLabels.kls_shipping_aa}}</div>
                                    <div class="military-right-margin"></div>
                                    <div class="ship-military-state-box" name="AE" id="{{id:'militaryState'}}">{{:cncLabels.kls_shipping_ae}}</div>
                                    <div class="military-right-margin"></div>
                                    <div class="ship-military-state-box" name="AP" id="{{id:'militaryState'}}">{{:cncLabels.kls_shipping_ap}}</div>
                                    <div class="military-right-margin"></div>
                                </div>
                                <div class="error-message-contact error-shipping-military-state"></div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="phoneNumber" maxlength="14" minLength="14" class="txtbox-form-input required phone-number-input" id="militaryPhone" value ="{{:shippingAddressDetails.phoneNumber}}">
                                <label for="militaryPhone" class="form-group-label">{{:cncLabels.kls_shipping_phone_number}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                                <label class="save-address-container">{{:cncLabels.kls_shipping_billing_address}}
                                    <input type="checkbox" class="bottomBillingAddrMilitary" id="{{id: 'sameBillingAddressMilitaryBottom'}}" name="isBillAddressEqualtoShipAddress" {{if !isEditingSecondaryAddr}} checked {{/if}}>
                                    <span class="save-address-checkbox"></span>
                                </label>
                            </div>
                            {{for tmpl='tpl-formButtonsTemplate' ~submitText=cncLabels.kls_shipping_button_label_use_address /}}
                        </div>  
                    </div>
                </div>
            </div>
    </div>   
    </div>
    </form>
    {{/if}}
</script>
<script id="tpl-militaryAddressTemplate" type="text/x-jsrender">
    {{if  shipIneligible && shipIneligible.cartItems && shipIneligible.cartItems.length}}
        <div class="shipping-common-header">{{:cncLabels.kls_shipping_error_cant_shipto_military}}</div>
        <div class="delete-product-shipping"><a href="javascript:void(0);"><img class="delete-product-shipping-img" id="{{id:'deleteMilitaryIneligible'}}" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/trash-icon.png" alt="" title=""></a></div>
        <ul class="cart-thumbs-block">
            {{for shipIneligible.cartItems}}
                <li><img class="cart-thumbs-block-img" data-id={{:cartItemId}} src={{_srcUrl}} alt={{:skuId}} title=""></li>
            {{/for}}
        </ul>
        <div class="pickup-options-available-msg">{{:cncLabels.kls_pickup_options_may_be_available}}</div>
    {{/if}}
</script>

<script id="tpl-verifyAddressTemplate" type="text/x-jsrender">
    <div id="addressyContainer">
        <div class="common-hr"></div>
        <div class="shipping-common-header">{{:cncLabels.kls_shipping_verifyaddress_title}}</div>
        <p class="saved-address-sub-detail saved-address">{{:cncLabels.kls_shipping_verifyaddress_notification}}</p>
        <div class="multiple-shipping-address-block" id="enteredAddress">
            <div class="multiple-shipping-bg" id="{{id:'activateVerifyAddress'}}">
                <span class="address-select-marker"></span>
                <span class="address-saved-block">
                    <p class="address-bottom-text ctHidden">{{:cncLabels.kls_shipping_verifyaddress_youentered_label}}</p>
                    <p class="address-top-text ctHidden">{{:addrData.addresses.addr1}} {{:addrData.addresses.addr2}}</p>
                    <p class="address-no-bold ctHidden">{{:addrData.addresses.city}} {{:addrData.addresses.state}} {{:addrData.addresses.postalCode}}</p>
                </span>
                <div class="saved-address-edit" id="{{id:'editAddress'}}" data-id={{:addrData.id}} alt={{:#getIndex()}} ><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
            </div>
        </div>
        <div class="multiple-shipping-address-block" id="verifiedAddress">
            <div class="multiple-shipping-bg" id="{{id:'activateVerifyAddress'}}">
                <span class="address-select-marker"></span>
                <span class="address-saved-block">
                <p class="address-bottom-text ctHidden">{{:cncLabels.kls_shipping_verifyaddress_uspsaddress_label}}</p>
                    <p class="address-top-text ctHidden">{{:addrData.suggestedAddress.addr1}} {{:addrData.suggestedAddress.addr2}}</p>
                    <p class="address-no-bold ctHidden">{{:addrData.suggestedAddress.city}} {{:addrData.suggestedAddress.state}} {{:addrData.suggestedAddress.postalCode}}</p>
                </span>
            </div>
        </div>
        <div class="common-hr"></div>
        <div id="{{id:'verify-address-buttons'}}">
            {{for tmpl='tpl-formButtonsTemplate' ~submitText=cncLabels.kls_paymentForm_button_label_done /}}
        </div>
    </div>
</script>

<script id="tpl-confirmAddressTemplate" type="text/x-jsrender">
    <div id="addressyContainer">
    <div class="common-hr"></div>
        <div class="shipping-common-header">{{:cncLabels.kls_shipping_verifyaddress_title}}</div>
        <p class="saved-address-sub-detail saved-address">{{:cncLabels.kls_shipping_confirmaddress_notification}}</p>
        <div class="multiple-shipping-address-block" id="enteredAddress">
            <div class="multiple-shipping-bg" id="{{id:'activateVerifyAddress'}}">
                <span class="address-select-marker"></span>
                <span class="address-saved-block">
                    <p class="address-bottom-text ctHidden">{{:cncLabels.kls_shipping_verifyaddress_youentered_label}}</p>
                    <p class="address-top-text ctHidden">{{:addrData.addresses.addr1}} {{:addrData.addresses.addr2}}</p>
                    <p class="address-no-bold ctHidden">{{:addrData.addresses.city}} {{:addrData.addresses.state}} {{:addrData.addresses.postalCode}}</p>
                </span>
                <div class="saved-address-edit" id="{{id:'editAddress'}}" data-id={{:addrData.id}} alt={{:#getIndex()}} ><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
            </div>
        </div>        
        <div class="contact-btn-block reduced-btn-margin">
            <kds-button title="{{:cncLabels.kls_shippingForm_button_label_confirm}}" size="sm" id="{{id:'saveConfirmedAddress'}}">
                    {{:cncLabels.kls_shippingForm_button_label_confirm}}
            </kds-button>
        </div>
    </div>
</script>

<script id="tpl-formButtonsTemplate" type="text/x-jsrender">
    <div class="contact-btn-block">
        <kds-button title="{{:cncLabels.kls_paymentForm_button_label_cancel}}" size="sm" button-type="secondary" class="cancel-form-address">
            {{:cncLabels.kls_paymentForm_button_label_cancel}}
        </kds-button>
        <kds-button title="{{:~submitText}}" size="sm" class="save-form-address">
            {{:~submitText}}
        </kds-button>
    </div>
</script><script id="tpl-billingAddressTemplate" type="text/x-jsrender">
<div class="billing-address-save-msg sr-only" role="status" aria-live="polite" style="display:none;">Address saved</div>
<form id="billing_addressy_form_v3" class="full-screen-width-parent-selector">
    <div class="shipping-common-block new-popup-design-selector" id={{id:'billingEventHandler'}}>
        <div class="shipping-common-header" id = "billingAddressLabel">
            {{if cartJsonData.addressDetails && cartJsonData.addressDetails.billingAddress && cartJsonData.addressDetails.billingAddress.length}}
                {{:cncLabels.kls_billing_save_address}}
            {{else}}
                {{:cncLabels.kls_billing_address_new_address}}    
            {{/if}}
        </div>
        <div class="shipping-common-header" id="editBillingLabel" style="display:none">{{:cncLabels.kls_billing_address_edit_address}}</div>
        <div id="billingContainer">
            <div class="cart-thumbs-container">
                    {{if cartJsonData.addressDetails && cartJsonData.addressDetails.billingAddress && cartJsonData.addressDetails.billingAddress.length}}
                                    <div class="shipping-address-top-margin"></div>
                                    <div class="saved-address-edit" id="{{id:'editBillingAddress'}}"><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
                                    <div class="saved-address">
                                        <div class="saved-address-header ctHidden">{{:cartJsonData.addressDetails.billingAddress[0].firstName}} {{:cartJsonData.addressDetails.billingAddress[0].lastName}}</div>
                                        <div class="saved-address-detail ctHidden">{{:cartJsonData.addressDetails.billingAddress[0].addr1}} {{:cartJsonData.addressDetails.billingAddress[0].addr2}}</div>
                                        <div class="saved-address-sub-detail ctHidden">{{:cartJsonData.addressDetails.billingAddress[0].city}}, {{if cartJsonData.addressDetails && cartJsonData.addressDetails.billingAddress && cartJsonData.addressDetails.billingAddress[0].state}}{{:cartJsonData.addressDetails.billingAddress[0].state.toUpperCase()}}{{/if}} {{:cartJsonData.addressDetails.billingAddress[0].postalCode}}</div>
                                    </div>
                    {{else}}
                        <div class="bill-to-search-input-block">
                            <div><img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/search@3x.png" alt="Kohls" class="searchbox-img"><input type="text" id="billingAddr_suggestion" aria-label="Search for an address" placeholder="Search for an address" onfocusin="this.placeholder = ''" onfocusout="this.placeholder = 'Search for an address'" class="bill-to-search-input-box shipping-search-input billingEventHandler searchAddressBox"><a href="javascript:void(0);" id="{{id:'addressyBillingCancelButton'}}" class="shipping-input-cancel">{{:cncLabels.kls_link_label_cancel}}</a></div>
                        </div>
                        <div class="common-green-text center-align" id="{{id:'addBillingAddress'}}">{{:cncLabels.kls_shipping_enter_address}}</div>  
                        <div class="common-hr"></div>
                        <div class="common-green-text common-margin-left" id="{{id:'addBillingMilitaryAddress'}}">{{:cncLabels.kls_shipping_bill_military_address}}</div>                      
                    {{/if}}
                    
                </div>
        </div>
        <div id="billingAddressForm" class="validateInline" style="display:none">
                <div class="common-green-text common-margin-left" id="{{id:'addBillingMilitaryAddress'}}">{{:cncLabels.kls_shipping_bill_military_address}}</div>
                <div class="cart-thumbs-container">
                    <div class="txtbox-form-group-block" id="{{id: 'billing-address-form'}}">
                    <span id ="{{id:'saveBillingAddress'}}" class="edit-card-done-text" style="display:none">{{:cncLabels.kls_paymentForm_button_label_done}}</span>
                    <input type="hidden" name="billingAddressId" id="billingAddressId" value= "{{:billingAddressDetails.id}}" />
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="firstName" class="txtbox-form-input required text-input-name" id="billingFirstName" maxlength="90" minLength="2" value="{{:billingAddressDetails.firstName}}">
                            <label for="billingFirstName" class="form-group-label">{{:cncLabels.kls_contactForm_firstName_text}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="lastName" class="txtbox-form-input required text-input-name" id="billingLastName"  maxlength="90" minLength="2" value="{{:billingAddressDetails.lastName}}">
                            <label for="billingLastName" class="form-group-label">{{:cncLabels.kls_contactForm_lastName_text}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group">    
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="addr1" class="txtbox-form-input text-input-name required txt-input billingEventHandler" id="tr_billing_add_us_address1" maxlength="40" minLength="2" value="{{:billingAddressDetails.addr1}}">
                            <label for="tr_billing_add_us_address1" class="form-group-label">{{:cncLabels.kls_shipping_street_address}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group">
                            <input type="text" name="addr2" class="txtbox-form-input txtbox-form-input-small" id="tr_billing_add_us_address2" value="{{:billingAddressDetails.addr2}}"  minLength="0" maxlength="50" autocomplete="addressline2"> 
                            <label for="tr_billing_add_us_address2" class="form-group-label">{{:cncLabels.kls_shipping_appartment}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="postalCode" maxlength="10" class="txtbox-form-input txtbox-form-input-small zipcode-input required" id="tr_billing_add_us_zipcode" value="{{:billingAddressDetails.postalCode}}" minLength="5">
                            <label for="tr_billing_add_us_zipcode" class="form-group-label">{{:cncLabels.kls_shipping_zip_code}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="two-pair-input-block">
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="city" class="txtbox-form-input text-input-alpha txtbox-form-input-mid-half required" id="tr_billing_add_us_city" value="{{:billingAddressDetails.city}}" maxlength="40" minLength="2">
                                <label for="tr_billing_add_us_city" class="form-group-label">{{:cncLabels.kls_shipping_city}}</label>
                                <div class="error-message-contact"></div>
                            </div>
                            <div class="txtbox-form-group">
                            <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                                <input type="text" name="state" class="txtbox-form-input text-input-alpha txtbox-form-input-small-min required text-input-state" id="tr_billing_add_us_state" value="{{:billingAddressDetails.state}}" minLength="2" maxlength="2">
                                <label for="tr_billing_add_us_state" class="form-group-label">{{:cncLabels.kls_shipping_state}}</label>
                                <div class="error-message-contact"></div>    
                            </div>
                        </div>
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="phoneNumber" maxlength="14" class="txtbox-form-input phone-number-input required" id="billingPhoneNumber" value="{{:billingAddressDetails.phoneNumber}}" minlength="14" >
                            <label for="billingPhoneNumber" class="form-group-label">{{:cncLabels.kls_shipping_phone_number}}</label>
                            <div class="error-message-contact"></div>
                        </div>

                        {{for tmpl='tpl-formButtonsTemplate' ~submitText=cncLabels.kls_shipping_button_label_use_address /}}
                       
                    </div>  
                </div>
            </div>
            <div id="militaryBillingAddressForm" class="validateInline" style="display:none">
                <div class="common-green-text common-margin-left" id="{{id:'addBillingAddress'}}">{{:cncLabels.kls_shipping_bill_to_united_state_address}}</div>
                <div class="cart-thumbs-container">
                    <div class="shipping-common-header shipping-margin-mid">
                        <div><span><img class="usa-flag-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/shippingPanelImages/ic-usa-flag@3x.png" alt="" title=""></span><span class="military-service-thanks-msg">{{:cncLabels.kls_shipping_military_message}}</span></div>
                    </div>
                    <span class="edit-address-done-text" id ="{{id:'saveBillingAddress'}}" style="display:none">{{:cncLabels.kls_paymentForm_button_label_done}}</span>
                    <div class="txtbox-form-group-block" id="{{id:'shipping-military-form'}}">
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="firstName" maxlength="90" minLength="2" class="txtbox-form-input text-input-name required" id="billingMilitaryFirstName" value="{{:billingAddressDetails.firstName}}">
                            <label for="billingMilitaryFirstName" class="form-group-label">{{:cncLabels.kls_contactForm_firstName_text}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="lastName" maxlength="90" minLength="2" class="txtbox-form-input text-input-name required" id="billingMilitaryLastName" value="{{:billingAddressDetails.lastName}}">
                            <label for="billingMilitaryLastName" class="form-group-label">{{:cncLabels.kls_contactForm_lastName_text}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group" >
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="addr1" maxlength="40" class="txtbox-form-input required" id="tr_billing_add_apo_add1" value ="{{:billingAddressDetails.addr1}}">
                            <label for="tr_billing_add_apo_add1" class="form-group-label">{{:cncLabels.kls_shipping_unit_box_number}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group" >
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="postalCode" maxLength="10" minLength="5" class="txtbox-form-input txtbox-form-input-small zipcode-input required" id="zipCodeBillingMilitary" value ="{{:billingAddressDetails.postalCode}}">
                            <label for="zipCodeBillingMilitary" class="form-group-label">{{:cncLabels.kls_shipping_zip_code}}</label>
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group military-city-state-block">
                            <div class="form-group-label military-city-state-block-top-pad">{{:cncLabels.kls_shipping_city}}</div>
                                <div class="military-city-block">
                                <div class="military-city-box" name="billingAPO" id="{{id:'militaryCity'}}">{{:cncLabels.kls_shipping_apo}}</div>
                                <div class="military-right-margin"></div>
                                <div class="military-city-box" name="billingFPO" id="{{id:'militaryCity'}}">{{:cncLabels.kls_shipping_fpo}}</div>
                                <div class="military-right-margin"></div>
                                <div class="military-city-box" name="billingDPO" id="{{id:'militaryCity'}}">{{:cncLabels.kls_shipping_dpo}}</div>
                                <div class="military-right-margin"></div>
                            </div>
                            <div class="error-message-contact error-billing-military-city"></div>
                        </div>
                        <div class="txtbox-form-group military-city-state-block">
                            <div class="form-group-label military-city-state-block-top-pad">{{:cncLabels.kls_shipping_state}}</div>
                                <div class="military-city-block">
                                <div class="military-state-box" name="billingAA" id="{{id:'militaryState'}}">{{:cncLabels.kls_shipping_aa}}</div>
                                <div class="military-right-margin"></div>
                                <div class="military-state-box" name="billingAE" id="{{id:'militaryState'}}">{{:cncLabels.kls_shipping_ae}}</div>
                                <div class="military-right-margin"></div>
                                <div class="military-state-box" name="billingAP" id="{{id:'militaryState'}}">{{:cncLabels.kls_shipping_ap}}</div>
                                <div class="military-right-margin"></div>
                            </div>
                            <div class="error-message-contact error-billing-military-state"></div>
                        </div>
                        <div class="txtbox-form-group">
                        <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                            <input type="text" name="phoneNumber" maxlength="14" minLength="14" class="txtbox-form-input required phone-number-input" id="billingMilitaryPhone" value="{{:billingAddressDetails.phoneNumber}}">
                            <label for="billingMilitaryPhone" class="form-group-label">{{:cncLabels.kls_shipping_phone_number}}</label>
                            <div class="error-message-contact"></div>
                        </div>

                        {{for tmpl='tpl-formButtonsTemplate' ~submitText=cncLabels.kls_shipping_button_label_use_address /}}                
                    </div>                    

                </div>

            </div>
    </form>
</script>


<script id="tpl-formButtonsTemplate" type="text/x-jsrender">
    <div class="contact-btn-block">
        <kds-button title="{{:cncLabels.kls_paymentForm_button_label_cancel}}" size="sm" button-type="secondary" class="cancel-form-address">
            {{:cncLabels.kls_paymentForm_button_label_cancel}}
        </kds-button>
        <kds-button title="{{:~submitText}}" size="sm" class="save-form-address">
            {{:~submitText}}
        </kds-button>
    </div>
</script>
<script id="tpl-viewOrderSummaryTemplate" type="text/x-jsrender">
        <div class="digital-receipt-header wide-screen">
            {{:~root.cncLabels.kls_digital_receipt_order_title}}
        </div>
        <div class="digital-receipt-header narrow-screen">
            <button class="view-order-summary-link viewOrderSummary" id="{{id:'viewOrderSummary'}}" style="display:none;">{{:~root.cncLabels.kls_digital_receipt_view_order_summary}}</button>
            <button class="view-order-summary-link hideOrderSummary" id="{{id:'hideOrderSummary'}}" style="display:none;">{{:~root.cncLabels.kls_digital_receipt_hide_order_summary}}</button>
        </div>
    <div id="viewDigitalReceipt"></div> 
</script>


<script id="tpl-showDigitalReceipt" type="text/x-jsrender">
    {{if cartJsonData.showDigitalReceipt}}
        <div id="orderSummaryDetails">
            <div class="order-summary">
                {{for cartJsonData.cartItems}}
                    <div>                        
                        <div class="order-summary-detail">
                        {{if ~root.$env.ksEnableSephora && itemType && itemType.startsWith('SEPHORA')}}
			                <div class="product-thumb-img">
                                <img class="product-thumb-img-style" src="{{_itemProperties.image.url}}" alt="{{:itemProperties.productTitle}}" title="">
                                <img class="sephora-logo" src="{{v_~root.$env.KDSAssetsPath}}/sephora/tags/small-k.svg" alt="Logo Sephora" />
                            </div>
                        {{else itemType && itemType.startsWith('MARKETPLACE')}}
					        <div class="product-thumb-img marketplace">
                                <img class="product-thumb-img-style" src="{{_itemProperties.image.url}}" alt="{{:itemProperties.productTitle}}" title="">
                                <kds-badge text="marketplace" badge-type="marketplace" class="kds-marketplace-badge"></kds-badge>
                            </div>
                        {{else}}
                            <span class="product-thumb-img"><img class="product-thumb-img-style" src="{{_itemProperties.image.url}}" alt="{{:itemProperties.productTitle}}" title=""></span>
                        {{/if}}
                        {{if itemType != "SEPHORA_GIFT"}}
                            <span class="order-summary-content">
                                    {{if itemPriceInfo && itemPriceInfo.regularPriceLabel}} 
                                        <p class="priceLabel">{{:itemPriceInfo.regularPriceLabel.toLowerCase()}} ({{:quantity}})</p>
                                        {{/if}}
                                        {{if itemPriceInfo.salePriceLabel}}  
                                        <p class="priceLabel">{{:itemPriceInfo.salePriceLabel.toLowerCase()}}</p>
                                        {{/if}}  
                                        {{for finalOffers}}
                                        <p>{{:discountLabel}}</p> 
                                        {{/for}}
                                    </span>
                                    <span class="order-summary-value">
                                    {{if itemPriceInfo && itemPriceInfo.regularPriceLabel  }} 
                                        <p>${{if itemPriceInfo.regularExtendedPrice || itemPriceInfo.regularExtendedPrice===0}}{{:itemPriceInfo.regularExtendedPrice.toFixed(2)}}{{/if}}</p>
                                        {{/if}}
                                        {{if itemPriceInfo.salePriceLabel}}  
                                        <p>${{if itemPriceInfo.saleExtendedPrice || itemPriceInfo.saleExtendedPrice===0}}{{:itemPriceInfo.saleExtendedPrice.toFixed(2)}}{{/if}}</p>
                                        {{/if}} 
                                        {{for finalOffers}}
                                        <p>-${{if discountValue || discountValue===0}}{{:discountValue.toFixed(2)}}{{/if}}</p>
                                        {{/for}}
                                        <p><span class="order-summary-value-bold">${{if itemPriceInfo.grossPrice || itemPriceInfo.grossPrice===0}}{{:itemPriceInfo.grossPrice.toFixed(2)}}{{/if}}</span></p>
                                    </span>
                        {{else}}
                            <span class="order-summary-content">
                            </span>
                            <span class="order-summary-value">
                                <p><span class="order-summary-value-bold">{{:~root.cncLabels.kls_sephora_gift_price}}</span></p>
                            </span>
                        {{/if}}
                        </div>                        
                    </div>
                {{/for}}                                
            </div>
        </div>
    {{/if}}
</script><script id="tpl-orderDisclaimerTemplate" type="text/x-jsrender">
    <!-- This is the same button found in the order summary. We are not using this right now -->
    {{if 1 == 0}}
    <div class="action-panel-block-dactive-chkbtn">
        <a class="checkout-btn{{if ~root.$env.pageName =='newCheckout' && !isCartEligibleForPlaceOrder}} checkout-btn-disable{{/if}}" id="{{id: 'redirectionUrl'}}" href='javascript:void(0);'>{{:windowUrl}}</a>
    </div>
    <div class="place-order-disclaimer">{{:cncLabels.kls_place_order_disclaimer}}</div>
    {{/if}}
</script><script id="tpl-giftCardTemplate" type="text/x-jsrender">
    {{if showGiftCardModule}}
    <div class="giftcard-applied-msg sr-only" role="status" aria-live="polite" style="display:none;">Gift card applied</div>
    <div class="giftcard-unapplied-msg sr-only" role="status" aria-live="polite" style="display:none;">Gift card unapplied</div>
    <div class="giftcard-section">
            <!--Start for more than one gift card available-->
            {{if cartJsonData.paymentTypes && cartJsonData.paymentTypes.giftCards && cartJsonData.paymentTypes.giftCards.length > 1 }}
                {{if state=="collapsed"}}
                <div class="giftcard-common-block giftcard-multi-section">
                    <div class="gift-card-block gift-card-block-border" id="{{id:'handle-collapsed-giftcard'}}">
                        <div class="gift-card-bg">
                                <span class="{{:markerClass}}"></span>
                            <span>
                                <a href="javascript:void(0);" alt={{:colstate}}><img class="giftcard-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftCardImages/{{_imgSrc}}" alt="" title=""></a>
                            </span>
                            <span class="gift-card-saved-block">
                                {{:collapsedLabel}}
                            </span>
                        </div>
                    </div>
                    <div  id="{{id:'expand-gift-card'}}">
                        <div class="cash-stacked-1-bg"></div>
                        <div class="cash-stacked-2-bg"></div>
                    </div>
                </div>
                {{else state=="expanded"}}
        <!--Start expand section--> 
             <div class="giftcard-common-block gift-card-expand">
                <!--Add gift card header-->
                    <div class="giftcard-header-cards-added new-giftcard-block">
                        <div class="giftcard-panel-header">{{:cncLabels.gift_card_header}}</div>
                        {{if cartJsonData.paymentTypes.giftCards.length < $env.CncMaxGiftCardSupported}}
                            <button class="add-gift-card" id="{{id:'add-gift-card'}}">
                                <img class="add-giftcard-icon" src="{{v_~root.$env.KDSAssetsPath}}/icons/cash-office/plus-sign.svg" alt="" title="">
                                <div class="add-giftcard-cta">{{:cncLabels.add_gift_card_label}}</div>
                            </button>
                        {{/if}}
                    </div>
                <!--End gift card header-->
                    {{for cartJsonData.paymentTypes.giftCards}}
                    <div class="gift-card-block giftcard-list">   
                        {{if applied}}  
                        <button class="gift-card-btn single-card" id="{{id:'handle-applied-giftcard'}}" alt="{{:applied}}" title="{{:paymentId}}">                              
                            <div class="gift-card-gc-applied">                                
                                <img class="giftcard-applied" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" alt="" title="">
                                <span class="gift-card-saved-block saved-giftcards">
                                        <p class="giftcard-text-top">${{:appliedAmount.toFixed(2)}} of ${{:remainingBalance.toFixed(2)}} {{:~root.cncLabels.applied_label}}</p>
                                        <p class="giftcard-text-bottom ctHidden">{{:~root.cncLabels.kls_paymentForm_ending_in}} {{:giftCardNum.substr(giftCardNum.length-4)}}</p>                                         
                                </span>   
                            </div>                              
                        </button>  
                        {{else}}
                        <button class="gift-card-btn single-card" id="{{id:'handle-unapplied-giftcard'}}" alt="{{:applied}}" title="{{:giftCardItemKey}}">
                            <div class="gift-card-gc">                                
                                <img class="giftcard-icon" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftCardImages/ic-giftcard@3x.png" alt={{:applied}} title="{{:itemKey}}">
                                <span class="gift-card-saved-block saved-giftcards">
                                    <p class="giftcard-text-top">${{:remainingBalance.toFixed(2)}} {{:~root.cncLabels.giftcard_available_label}}</p>
                                    <p class="giftcard-text-bottom ctHidden">{{:~root.cncLabels.kls_paymentForm_ending_in}} {{:giftCardNum.substr(giftCardNum.length-4)}}</p>                                         
                                </span>   
                                <span class="apply-giftcard-cta">{{:~root.cncLabels.apply_giftcard_cta}}</span>
                                <span class="apply-giftcard-cta-mobile">{{:~root.cncLabels.tap_to_apply}}</span>                              
                            </div> 
                        </button>
                        {{/if}}                          
                    </div>
                    {{/for}}
            {{/if}}
            <!--End expand section-->  
                <!--Start if single gift card available-->  
            {{else cartJsonData.paymentTypes && cartJsonData.paymentTypes.giftCards && cartJsonData.paymentTypes.giftCards.length == 1}}
                <div class="giftcard-common-block">
                <!--Add gift card header-->
                    <div class="giftcard-header-cards-added new-giftcard-block">
                        <div class="giftcard-panel-header">{{:cncLabels.gift_card_header}}</div>
                            <button class="add-gift-card" id="{{id:'add-gift-card'}}">
                                <img class="add-giftcard-icon" src="{{v_~root.$env.KDSAssetsPath}}/icons/cash-office/plus-sign.svg" alt="" title="">
                                <div class="add-giftcard-cta">{{:cncLabels.add_gift_card_label}}</div>
                            </button>
                    </div>
                <!--End gift card header-->
                    <div class="gift-card-block giftcard-list">
                    <div class= {{if cartJsonData.paymentTypes.giftCards[0].applied}} "gift-card-gc-applied single-card" {{else}} "gift-card-gc single-card" {{/if}} id="{{id:'handle-single-giftcard'}}" alt="{{:cartJsonData.paymentTypes.giftCards[0].applied}}" title="{{if cartJsonData.paymentTypes.giftCards[0].applied}}{{:cartJsonData.paymentTypes.giftCards[0].paymentId}}{{else}}{{:cartJsonData.paymentTypes.giftCards[0].giftCardItemKey}}{{/if}}">
                            <span>
                            {{if cartJsonData.paymentTypes.giftCards[0].applied}}
                                <img class="giftcard-applied" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" alt="" title="">
                            {{else}}
                                <img class="giftcard-icon" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftCardImages/ic-giftcard@3x.png" alt="" title="">
                            {{/if}}
                            </span>
                            <span class="gift-card-saved-block saved-giftcards">
                                {{if cartJsonData.paymentTypes.giftCards[0].applied}}
                                    <p class="giftcard-text-top">${{:cartJsonData.paymentTypes.giftCards[0].appliedAmount.toFixed(2)}} {{:cncLabels.applied_label}}</p>
                                {{else}}
                                    <p class="giftcard-text-top">${{:cartJsonData.paymentTypes.giftCards[0].remainingBalance.toFixed(2)}} {{:cncLabels.giftcard_available_label}}</p>
                                {{/if}}
                                <p class="giftcard-text-bottom">{{:cncLabels.kls_paymentForm_ending_in}} {{:cartJsonData.paymentTypes.giftCards[0].giftCardNum.substr(cartJsonData.paymentTypes.giftCards[0].giftCardNum.length-4)}}</p>
                            </span>
                            {{if !cartJsonData.paymentTypes.giftCards[0].applied}}
                            <span class="apply-giftcard-cta">{{:cncLabels.apply_giftcard_cta}}</span>
                            <span class="apply-giftcard-cta-mobile">{{:cncLabels.tap_to_apply}}</span>
                            {{/if}}
                        </div>
                    </div>
                </div>
            {{else cartJsonData.paymentTypes &&  (cartJsonData.paymentTypes.giftCards == null || cartJsonData.paymentTypes.giftCards.length == 0) }}
                    <div class="giftcard-common-block new-giftcard-block">
                    <div class="giftcard-panel-header">{{:cncLabels.gift_card_header}}</div>
                        <button class="add-gift-card" id="{{id:'add-gift-card'}}">
                            <img class="add-giftcard-icon" src="{{v_~root.$env.KDSAssetsPath}}/icons/cash-office/plus-sign.svg" alt="" title="">
                            <div class="add-giftcard-cta">{{:cncLabels.add_gift_card_label}}</div>
                        </button>
                    </div>
            {{/if}}                
        </div>
    {{/if}}
</script>
<script id="tpl-addGiftCardTemplate" type="text/x-jsrender">
    <form id="gift-card-form">
        <div class="giftcard-common-block new-popup-design-selector">
            <div class="giftcard-header-container">
                <div class="giftcard-common-header giftcard-title">{{:cncLabels.gift_card_header}}</div>
                <img class="giftcard-cross" id="{{id:'cancel-gift-card-mobile'}}" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
            </div>
                <div class="gift-card-block gift-card-block-no-margin validateInline" id="giftForm">
                    <div class="txtbox-form-group-block">
                        <div class="txtbox-form-group giftcard-input">
                            <label class="form-group-label giftcard-label">{{:cncLabels.giftcard_gift_card__number_label}}</label>
                            <input type="text" aria-label="Gift card number" id="{{id:'giftCardNum'}}" name="giftCardNum" autocomplete="off" maxlength="{{:$env.CncMaxGiftCardNumber}}" class="txtbox-form-input required number-input giftcard-num">
                            <div class="error-message-contact"></div>
                        </div>
                        <div class="txtbox-form-group giftcard-pin-input">
                            <div class="form-group-label giftcard-pin-label">{{:cncLabels.kohls_cash_or_promo_pnl_offer_pin_code}}</div>
                            <input type="text" aria-label="Gift card PIN" id="{{id:'giftCardPin'}}" maxlength="4" minlength="4" name="pin" autocomplete="off" class="txtbox-form-input txtbox-form-input-small-pin-no required number-input giftcard-pin">
                            <div class="error-message-contact"></div>
                        </div>
                        {{if $env.ksCncApplyGiftCardCaptchaEnabled}}
                            <div class="g-recaptcha invisible" data-sitekey="{{:$env.reCaptchaSecretKey}}" data-size='invisible' id="recaptcha-apply-gift-card"></div>
                        {{/if}}
                    </div>
                    <div class="contact-btn-block giftcard-btns">
                        <kds-button title="{{:cncLabels.kls_contactForm_cancel_button_text}}" size="sm" button-type="secondary" id="{{id:'cancel-gift-card'}}">
                            {{:cncLabels.kls_contactForm_cancel_button_text}}
                        </kds-button>
                        <kds-button title="{{:cncLabels.add_gift_card_btn}}" class="gift-card-submit disable-giftcard-btn" size="sm" id="{{id:'submit-gift-card'}}">
                            {{:cncLabels.add_gift_card_btn}}
                        </kds-button>
                    </div>
                </div>
        </div>
        {{if $env.ksCncApplyGiftCardCaptchaEnabled}}
            {{for tmpl='gReCaptchaTmpl'  ~isInvisible="true" /}}
        {{/if}}
    </form>
</script>
<script id="tpl-pickUpPanel" type="text/x-jsrender">
{{if pickUpDataArr.length > 0}}
    <div class="shipping-common-block new-popup-design-selector" id="pickupPanelContainerDiv">

        {{for pickUpDataArr ~env =$env}}
            <div class="cart-thumbs-container">
                {{for #data.items}}
                    <div class="pickup-common-header">{{:#data.pickUpDescription}}</div>
                    <ul class="cart-thumbs-block">
                        {{for #data.images }}
                            <li><img class="cart-thumbs-block-img" src="{{__data}}" alt="Pickup item" title=""></li>
                        {{/for}}
                    </ul>
                {{/for}}
            
                {{if #data.hasLocation}}
                    {{if ~root.$env.enableRadar}}
                        <div id="radar-map" class="kohls-store-map-radar"></div>
                        <script type="text/javascript">
                            const map = Radar.ui.map({
                                container: 'radar-map',
                                center: [{{:#data.longitude}},{{:#data.latitude}}],
                                zoom: 13,
                                interactive: false,
                            });
                            const marker = Radar.ui.marker({ text: 'Kohls', color: '#BD0F1B'})
                                .setLngLat([{{:#data.longitude}},{{:#data.latitude}}])
                                .addTo(map);                            
                        {{:"<"}}/script>
                    {{else}}
                        <div class="kohls-store-map"><img class="kohls-store-map-img" id = "pickup-kohls-store-map-{{:#data.storeId}}" src="{{__data.googleMapsURL}}"></img></div>
                    {{/if}}                     
                {{/if}}
                <div class="shipping-address-top-margin"></div>
                <div class="pickup-common-sub-header">{{:#data.storeName}}</div>
                <div class="saved-address">

                    <div class="saved-address-detail">{{:#data.address}}</div>
                    <div class="saved-address-sub-detail">
                        {{if #data.distanceLabel}}    
                            <span class="store-drive-time"><img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/ic-car-glyph@3x.png" alt=""
                                    title=""><span class="inside-store-drive-time-{{:#data.storeId}}">{{:#data.distanceLabel}}</span></span>  
                        {{/if}}                      
                        <span class="store-open-time"><img class="store-open-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/clock@3x-dark.png" alt=""
                                title="">{{:#data.openUntilLabel}}</span>
                    </div>
                </div>
            </div>            
            {{if ~root.$env.ksCurbSideFeature}}
                <div class="common-hr"></div>
                <div class="store-pickup-block-container">
                    <div class="store-pickup-block-header">{{:~root.cncLabels.kls_store_pickup_header}}</div>
                    <div class="store-pickup-block {{if !#data.instorePickupEligible}} store-disable {{else #data.inStoreSeleted}}selected-block {{else}} unselected-block {{/if}}" id="{{id:'curbside-instore'}}" data-storeid="{{:#data.storeId}}">                                            
                        <span class="storepickup-select-marker{{if #data.inStoreSeleted}} ks-color-css{{else}} ks-empty-color-css{{/if}} "></span>
                        <span class="storepickup-block">                            
                            <p class="{{if #data.inStoreSeleted}}storepickup-bold{{else}}storepickup-no-bold{{/if}}">{{:~root.cncLabels.kls_driveup_instore_label}}</p>
                            {{if #data.instorePickupEligible}}
                                <p class="storepickup-no-bold">{{:~root.cncLabels.kls_driveup_instore_text}}</p>   
                            {{else}}
                                <p class="storepickup-no-bold">{{:~root.cncLabels.kls_driveup_not_available}}</p> 
                            {{/if}}                         
                        </span>
                        <div class="storepickup-div-img {{if #data.inStoreSeleted === false}} ks-block-css{{/if}}"><img class="storepickup-img" src="{{v_~root.$resourceRoot('cnc')}}images/curbside/instore.png" alt="" title="" style=""></div>
                    </div>                            
                    <div class="store-pickup-block {{if !#data.curbsideEligible}} store-disable {{else #data.driveUpSelected}} selected-block{{else}} unselected-block {{/if}}" id="{{id:'curbside-driveup'}}" data-storeid="{{:#data.storeId}}">                    
                        <span class="storepickup-select-marker{{if #data.driveUpSelected}} ks-color-css{{else}} ks-empty-color-css{{/if}} "></span>
                        <span class="storepickup-block">                            
                            <p class="{{if #data.driveUpSelected}}storepickup-bold{{else}}storepickup-no-bold{{/if}}">{{:~root.cncLabels.kls_driveup_label}}</p>
                            {{if #data.curbsideEligible}}
                                <p class="storepickup-no-bold">{{:~root.cncLabels.kls_driveup_text}}</p>   
                            {{else}}
                                <p class="storepickup-no-bold">{{:~root.cncLabels.kls_driveup_not_available}}</p> 
                            {{/if}}                               
                        </span>                        
                        <div class="driveup-div-img {{if #data.driveUpSelected === false}} ks-block-css{{/if}}"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64">         <defs>          <style>           .a{fill:none;}.b{fill:#fff;opacity:0;}.c{clip-path:url(#a);}.d{fill:#7c3dd7;}          </style>          <clipPath id="a">           <rect class="a" width="64" height="57.143"></rect>          </clipPath>         </defs>         <rect class="b" width="64" height="64"></rect>         <g class="c" transform="translate(0 2.667)">          <path class="d" d="M61.714,23.473a7.473,7.473,0,0,0-.352-2.549L59.78,15.648a12.132,12.132,0,0,0-3.868-5.626H56l.264-.264h.088v-.44h.088V8.7h.088V8.352h0V7.473a4.658,4.658,0,0,0,0-.527V4.571A4.571,4.571,0,0,0,52.571,0H11.429A4.571,4.571,0,0,0,6.857,4.571V6.857a4.835,4.835,0,0,0,0,.527v.44h0v.352h0v.176h.088v.176h0V8.7h.088v.176h.088v.176H7.3a12.132,12.132,0,0,0-3.868,5.626l-.791,6.242a7.385,7.385,0,0,0-.352,2.549A4.571,4.571,0,0,0,0,27.429V41.143a9.055,9.055,0,0,0,2.286,6.066v7.648a2.286,2.286,0,0,0,2.286,2.286h6.857a2.286,2.286,0,0,0,2.286-2.286V50.286H50.286v4.571a2.286,2.286,0,0,0,2.286,2.286h6.857a2.286,2.286,0,0,0,2.286-2.286V47.209A9.055,9.055,0,0,0,64,41.143V27.429a4.571,4.571,0,0,0-2.286-3.956M55.033,10.725l-.791,3.956a6.681,6.681,0,0,1,1.143,2.286l1.582,5.275.176.615H52.571Zm-43.6-6.154H52.571V6.857H11.429ZM28.923,21.8V17.67H35.78V21.8Zm9.143,0V17.67a2.286,2.286,0,0,0-2.286-2.286H28.923a2.286,2.286,0,0,0-2.286,2.286V21.8H23.121V35.516a1.143,1.143,0,0,0,.7,1.055H16V22.418L13.8,11.429H50.2L48,22.418V36.571H40.615a1.143,1.143,0,0,0,.7-1.055V21.8Zm-31.033.44,1.582-5.275a6.681,6.681,0,0,1,1.231-2.2l-.791-3.956,2.374,12.044H6.857l.176-.615m52.4,7.473H57.143A2.286,2.286,0,0,0,54.857,32v4.571a2.286,2.286,0,0,0,2.286,2.286h2.286v2.286a4.571,4.571,0,0,1-4.571,4.571H9.143a4.571,4.571,0,0,1-4.571-4.571V27.429h6.857v9.143A4.571,4.571,0,0,0,16,41.143H48a4.571,4.571,0,0,0,4.571-4.571V27.429h6.857Z" transform="translate(0)"></path>          <path class="d" d="M5.2,41.712v1.231H7.486a2.286,2.286,0,0,0,2.286-2.286V36.086A2.286,2.286,0,0,0,7.486,33.8H5.2v7.824" transform="translate(-0.629 -4.086)"></path>         </g>        </svg></div>                    
                    </div> 
                </div>
                <div class="common-hr"></div>
            {{/if}}
        {{/for}}

        
    {{if isShowAlternatePickupForm}}
      {{for tmpl='tpl-AddPickUpPersonForm' #data /}}
    {{else}}
        {{if alternatePickupDetails.isAlternatePickup}}        
            <div class="saved-address-edit" id= "{{id:'edit-pick-up-person'}}"><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
            <div class="pickup-common-sub-header">{{:alternatePickupDetails.firstName}} {{:alternatePickupDetails.lastName}}</div>
            <div class="shipping-common-txt">{{:cncLabels.kls_can_pickup_this_order}}</div>     

            {{if alternatePickupDetails.phone}}
                <div class="common-hr"></div>
                <div class="saved-address-edit" id= "{{id:'edit-pick-up-person'}}"><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
                <div class="pickup-common-sub-header">{{:alternatePickupDetails.phone}}</div>
                <div class="shipping-common-txt">{{:cncLabels.kls_will_recieve_text_message}}</div>     
            {{/if}}
        {{else}}
            {{if alternatePickupDetails.phone}}                
                <div class="saved-address-edit" id= "{{id:'edit-pick-up-person'}}"><a href="javascript:void(0);"><img class="saved-address-edit-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title=""></a></div>
                <div class="pickup-common-sub-header">{{:alternatePickupDetails.phone}}</div>
                <div class="shipping-common-txt">{{:cncLabels.kls_will_recieve_text_message}}</div>     
            {{else}}
                <div class="common-green-text common-margin-left" id= "{{id:'add-pick-up-person'}}">{{:cncLabels.kls_add_pick_up_person_textnotification}}</div>
            {{/if}}
        {{/if}}
    {{/if}}
    </div>
{{/if}}
</script>

<script id="tpl-AddPickUpPersonForm" type="text/x-jsrender">

    <div class="text-notifications-header pickup-common-header">{{:cncLabels.kls_pick_text_notification}}</div>
    <div id="{{id: 'add-pickup-form'}}">

        <div class="cart-thumbs-container">
            <div class="shipping-address-top-margin"></div>
            <div class="text-notification-icon" id = "{{id:'pickup-textnotification-info'}}"><img class="text-notification-icon-img" src={{if alternatePickupDetails.showTextNotificationInfo}}"{{v:~root.$resourceRoot('cnc')}}images/dgsImages/contactFormImages/cross-circle@3x.png"{{else}}"{{v:~root.$resourceRoot('cnc')}}images/dgsImages/contactFormImages/info@3x.png"{{/if}} alt="" title=""></div>
            <div class="shipping-common-txt">{{:cncLabels.kls_pick_add_mobile_number_text_receive}}</div>
            <div class="txtbox-form-group-block" >
                <div class="txtbox-form-group" id="{{id: 'add-notification-form'}}">
                {{if alternatePickupDetails.phone && alternatePickupDetails.showDelete }}
                   <img class="delete-shipping-address" data-id="deletePhone" id="{{id: 'delete-pickup-data'}}"  src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="Delete pickup" title="Delete pickup"></img>
                {{/if}}
                    <input type="text" name="pickUpFormMobileNumber" class="txtbox-form-input pickup-mobile-number pickUpFormValidate phone-number-input" maxlength="14" value="{{:alternatePickupDetails.phone}}">
                    <label class="form-group-label">{{:cncLabels.kls_pick_mobile_phone}}</label>
                    <div class="error-message-contact" id='pickUpFormMobileNumberError' >{{:cncLabels.kls_billingAddress_phoneNumber_req_error}}</div>
                </div>
            </div>
        </div>
        {{if alternatePickupDetails.showTextNotificationInfo}}
            <div class="shipping-common-txt">{{:cncLabels.kls_pickup_send_text_order_ready}}</div>
            <div class="shipping-address-top-margin"></div>
            <div class="shipping-common-txt">{{:cncLabels.kls_pickup_sendvia_autodiled_text}}
        {{/if}}
        </div>
        <div class="common-hr"></div>
        <div class="pickup-common-header">{{:cncLabels.kls_add_pickup_person}}</div>
        <div class="shipping-common-txt">{{:cncLabels.kls_add_pickup_order_another_order}}</div>
        <div class="txtbox-form-group-block"  id="{{id: 'add-alternate-pickup-form'}}">
            <div class="txtbox-form-group">
            {{if alternatePickupDetails.firstName && alternatePickupDetails.showDelete}}
            <img class="delete-shipping-address" data-id="deletePickUpData" id="{{id: 'delete-pickup-data'}}"  src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="Delete pickup" title="Delete pickup"></img>
            {{/if}}
                <input type="text" name="pickUpFormFirstName" maxlength="40" class="txtbox-form-input pickup-first-name pickUpFormValidate text-input-name" value="{{:alternatePickupDetails.firstName}}">
                <label class="form-group-label">{{:cncLabels.kls_contactForm_firstName_text}}</label>
                <div class="error-message-contact" id='pickUpFormFirstNameError' >{{:cncLabels.kls_shipping_please_enter}}{{:cncLabels.kls_contactForm_firstName_text}}</div>
            </div>
                <div class="txtbox-form-group">
                <input type="text" name="pickUpFormLastName" maxlength="40" class="txtbox-form-input pickup-last-name pickUpFormValidate text-input-name" value="{{:alternatePickupDetails.lastName}}">
                <div class="form-group-label">{{:cncLabels.kls_contactForm_lastName_text}}</div>
                <div class="error-message-contact" id='pickUpFormLastNameError'>{{:cncLabels.kls_shipping_please_enter}}{{:cncLabels.kls_contactForm_lastName_text}}</div>
            </div>
            <div class="txtbox-form-group">
                <input type="text" name="pickUpFormEmail" maxlength="250" class="txtbox-form-input pickup-email pickUpFormValidate" value="{{:alternatePickupDetails.email}}">
                <div class="form-group-label">Email (optional)</div>
                <div class="error-message-contact" id='pickUpFormEmailError'>{{:cncLabels.kls_shipping_please_enter}}{{:cncLabels.kls_contactForm_email_text}}</div>
            </div>
            {{!-- /* hiding the save functionality till it will be ready in account */ --}} 
            {{if isUserLoggedIn && false}}
                <div class="txtbox-form-group">
                    <label class="save-address-container">{{:cncLabels.kls_add_pickup_save_fornext_time}}
                    <input type="checkbox" checked="checked" id="savePickUpPerson" class="pickUpFormChecbox" name="pickUpFormChecbox">
                    <span class="save-address-checkbox"></span>
                    </label>
                </div>
            {{/if}}
            
        </div>
        <div class="contact-btn-block">
            <kds-button title="{{:cncLabels.kls_contactForm_cancel_button_text}}" size="sm" button-type="secondary" 
                id="{{id:'add-pickup-person-cancel-button'}}">
                {{:cncLabels.kls_contactForm_cancel_button_text}}
            </kds-button>
            <kds-button title="{{:cncLabels.kls_contactForm_done_button_text}}" size="sm" id="{{id:'add-pickup-person-done-button'}}">
                {{:cncLabels.kls_contactForm_done_button_text}}
            </kds-button>
        </div>
    </div>

</script><script id="tpl-eGiftTemplate" type="text/x-jsrender">
    {{if eGiftArr.length > 0}}
        <div class="giftcard-common-block">
            <div class="egiftcard-common-header">{{:cncLabels.kls_egift_arrives_today}}</div>
            {{for eGiftArr ~env =$env ~data =#parent.data}}
                {{if ~data.eGiftArr.length > 1}}
                    <div class="gift-card-common-divider"></div>
                {{/if}}
                <div class="gift-card-block">
                    {{for images}}
                        <img class="egiftcard-img" src="{{__data}}" alt="" title="">
                    {{/for}}
                    <div class="mail-to"><img class="mail-to-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/eGiftPanelImages/mail@3x.png" alt="" title=""></div>
                    <p class="gift-card-no-bold gift">{{:recipientName}}</p>
                    <p class="gift-card-bottom-text">{{:recipientEmail}}</p>
                </div>
            {{/for}}
        </div>
    {{/if}}    
</script><script id="tpl-signinPanel-template" type="text/x-jsrender">
    {{if #data.showSignInForm}}
    <div id="signInError"></div>
        <div id="{{id:'cancel-signin-button'}}" class="panel_button_close"></div>
        <div class="contact-common-header">{{:cncLabels.kls_sign_in_message}}</div>
        <div class="sign-in-details-block validateInline" id ="contactSignInForm">
            <div class="sign-in-form-group-block">
                    <div class="sign-in-form-group">
                    <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                        <input type="text" id="signinEmail" maxlength="40" name="email" class="sign-in-form-input required email-input" {{if #data.disableEmailField}}  disabled = "disabled" value = "{{:cartJsonData.userData.profile.emailId}}" {{/if}}>
                        <label for="signinEmail" class="form-group-label">{{:cncLabels.kls_contactForm_email_text}}</label>
                        <div class="error-message-contact"></div>
                    </div>
                    <div class="sign-in-form-group">
                    <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon"></img>
                        <input type="password" id= "{{id:'submitForm'}}" minlength="5" name="pw" class="sign-in-form-input sign-in-form-input-mid required signinPassword" aria-label="{{:cncLabels.kls_sign_in_password_label}}">
                        <div class="form-group-label">{{:cncLabels.kls_sign_in_password_label}}</div>
                        <div class="eye-img" id= "{{id:'showPassword'}}"><img class="eye-img-style" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/contactFormImages/eye@3x.png" alt="" title=""></div>
                        <div class="error-message-contact"></div>
                    </div>
                    <div class="nothankssec rd-CaptchaWrap hide mT20 checkout-captcha">
                        <input type="hidden" id="hdnreCaptchaScriptURL" value="{{:reCaptchaScriptURL}}"/>
                        <input type="hidden" id="hdnCaptachEnabled" value="{{:captchaEnabled}}"/>                                          
                        <div class="captchaErr Err-captcha kas-loginPage-signin-captcha-emptyError" style="display: none;"><a href="javascript:void(0)" alt="please validate captcha">Please validate captcha</a></div>
                        <div class="g-recaptcha" id="recaptcha-checkoutSignin"></div>  
                    <div class="clear"></div>                    
            </div>
            </div>   
            <div class="rd-forgotPass" id="{{id:'reset-password-link'}}"><a href="javascript:void(0)" class=" contact-common-green-text forgot-password-margin-left rd-forgotPass-link fotfamily5r" title="Forgot password?">{{:cncLabels.kls_sign_in_forgot_password_label}}</a></div>
            {{if ~root.$env.keepMeSignedInEnabled}}
                <div class="signintxtbox-form-group">
                    <label class="checkbox-green-container">{{:cncLabels.kls_keep_me_signed_in_label}}   
                        <input type="checkbox" id="keepMeSignedIn" name="keepMeSignedIn">
                        <span class="checkbox-green-checkbox"></span>
                    </label>
                </div>
                <div class="keep_me_signed_in_disclaimer">
                    <label>
                        {{:cncLabels.kls_keep_me_signed_in_disclaimer}}
                    </label>
                </div>
            {{/if}}
            <div class="contact-btn-block">
                <kds-button id="{{id:'continue-as-guest-btn'}}" title="{{:cncLabels.kls_contactForm_guest_button_text}}" size="sm" button-type="secondary">
                    {{:cncLabels.kls_contactForm_guest_button_text}}
                </kds-button>
                <kds-button id="{{id:'done-signin-button'}}" title="{{:cncLabels.kls_contactForm_signin_button_text}}" size="sm">
                    {{:cncLabels.kls_contactForm_signin_button_text}}
                </kds-button>
            </div>
        </div>
        {{for tmpl='gReCaptchaTmpl' /}}
    {{/if}}
</script><script id="gReCaptchaTmpl" type="text/x-jsrender">
    {{if ~isInvisible}}
        <script src='./../../../../../../../www.google.com/recaptcha/api.js_'>  {{:"<"}}/script>
        <script type="text/javascript">

    {{else}}
        <script src='./../../../../../../../www.google.com/recaptcha/api.js_onload=onloadCallback_render=explicit'>  {{:"<"}}/script>
        <script type="text/javascript">
        var recaptchaWidget = {};
        var onloadCallback = function() {
            $('.g-recaptcha').html('');
            $('.g-recaptcha').each(function (i, captcha) {
                recaptchaWidget[captcha.id] = grecaptcha.render(captcha, {
                    'sitekey' : '{{:reCaptchaSecretKey}}'
                });
            });
        };
        var resetReCaptcha = function(widget) {
            grecaptcha.reset(recaptchaWidget[widget]);
        };
    {{/if}}
    {{:"<"}}/script>
</script><script id="tpl-remedyChallengePanel-Template" type="text/x-jsrender"> 
	<div id="remedy_challenge_inner_container">
		<div class="fraud-remedy-modal kas-fraud_remedy_modal"> 
			<div class="modal-background kas-modal_background">
				<div class="modal-box kas-modal_box">				
				<a id= "{{id:'btnRemedyCloseModel'}}"  class="modal-icon-close modal-close link--not-active kas-modal_close_icon" href="javascript:void(0)"><img src="./../../../../../../../www.kohls.com/media/images/fraud-remedy/ic-close.png"></a>
				<div class="modal-box-copy kas-modal_box_copy" id= "{{id:'remedyPanelEvents'}}">			       
				        <h5 class="modal-box-copy-headline kas-modal_box_copy_headline">
				            {{:cncLabels.kls_static_verify_card_number}} 
				        </h5>
			            <p class="modal-box-copy-body kas-modal_box_copy_body">
							{{:~convertHTML(cncLabels.kls_static_payment_verification)}}                      	
			            </p>
						<p class="error-message kas-error_message">
                    		{{:cncLabels.kls_static_remedy_validation_error}}
	                    </p>                  
	                	<h6>{{:cncLabels.kls_static_pay_method_no_change}}</h6>	               
			            <form id="remedyChallengeForm">
				            {{if paymentCards && paymentCards.length > 0}}
								{{for  paymentCards  ~len=paymentCards.length}}
									<div class="radio">
											<input name="optionsRadios" class="optionsRadiosRemedy kas-radio_button" id="optionsRadios{{:#index}}" value="{{:paymentId}}" type="radio" {{if selected}} checked {{/if}}>
											{{if ~len > 1}}                                        
											<label for="optionsRadios{{:#getIndex()}}"><span></span></label>
											{{/if}}								
											<figure {{if  ~len == 1}} class="kas-single_card_img" {{/if}}>
												{{if cardType.toLowerCase() == "kohlscharge"}}
													<img src="./../../../../../../../www.kohls.com/media/images/fraud-remedy/card-kohlscash.png">
												{{else cardType.toLowerCase() == "visa"}}
													<img src="./../../../../../../../www.kohls.com/media/images/fraud-remedy/card-visa.png">
												{{else cardType.toLowerCase() == "mastercard"}}
													<img src="./../../../../../../../www.kohls.com/media/images/fraud-remedy/card-mastercard.png">
												{{else cardType.toLowerCase() == "discover"}}
													<img src="./../../../../../../../www.kohls.com/media/images/fraud-remedy/card-discover.png">
												{{else cardType.toLowerCase() == "amex" || cardType.toLowerCase() == "americanexpress"}}
													<img src="./../../../../../../../www.kohls.com/media/images/americanExpress.png">
												{{/if}}
												<figcaption class="capitalize"><p class="cardType kas-card_type">{{:cardType}}</p>
													{{if cardType.toLowerCase() == "kohlscharge"}}
														<span>x{{:cardNum.slice(-4,-2)}}--</span>
													{{else}}
														<span>x{{:cardNum.slice(-4)}}</span>
													{{/if}}
												</figcaption>
											</figure>
											<input class="form-control cardNumber kas-cardNumber" placeholder="Card Number" type="text" {{if !selected}} disabled {{/if}}/>
	
									</div>
								{{/for}}	
							{{/if}}
							<p class="contact-details kas-contact_details">{{:cncLabels.kls_static_unable_verify_card}}</p>
							<input type="button" id="{{id:'btnRemedy'}}" class="btn btn-continue kas-continue_link" value="Continue" disabled>
							<a href="javascript:void(0)" id="{{id:'btnRemedyAddNewCard'}}" class="add-new-card kas-add_new_card_button">{{:cncLabels.kls_static_add_new_card}}</a>
			            </form>
			        </div>
			    </div>
			</div>
		</div>
	</div>

</script>
<script id="tpl-checkoutOutOfStockPanelTemplate" type="text/x-jsrender">
    {{if cartOOSItems && cartOOSItems.length > 0}}
        <div class="checkout-out-of-stock-block">
            <div class="checkout-outofstock-panel-error">
                <p class="checkout-outofstock-panel-error-msg">{{:cncLabels.kls_checkout_OOS_error_message}}</p>
            </div>

            <div class="checkout-oos-item-container" id="{{id:'checkout-oos-items'}}">
                <ul class="checkout-oos-item-list">
                    {{for cartOOSItems ~len=cartOOSItems.length}}
                        <li class="checkout-oos-item-block">
                            <div class="checkout-oss-item-img-block">
                                <img class="checkout-oos-item-img"  src="{{_itemProperties.image.url}}" alt="" title=""/>
                             </div>
                            <div class= "checkout-oos-item-text-block">
                                {{:#data.itemProperties.productTitle}}
                            </div>
                            <div class="checkout-oos-item-delete-block" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-shipmentType={{:#data.shipmentType}} data-storeName={{:#data.storeId}}>
                                <div>
                                    <a href="javascript:void(0);">
                                        <img class="checkout-oos-item-delete-icon" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="" title=""/>
                                    </a>
                                    <span class="checkout-oos-item-delete-text">{{:~root.cncLabels.kls_remove_text}}</span>
                                </div>
                            </div>
                            {{if ~root.isRegisteredFlow}}
                                <div class="checkout-oos-item-sfl" id="{{id:'checkout-oos-item-sfl'}}" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-shipmentType={{:#data.shipmentType}} data-storeName={{:#data.storeId}}>{{:~root.cncLabels.kls_save_for_later}}</div>
                            {{/if}}
                        </li>
                        {{if #index+1 != ~len}}
                            <div class="checkout-oos-item-hr"></div>
                        {{/if}}
                    {{/for}}
                </ul>
            </div>
        </div>
    {{/if}}
</script><script id="tpl-cartRewardPanel-template" type="text/x-jsrender">
<!-- Kohls reward section started -->

<div class="earning-section {{if ~root.$env.ksEnableSephora && (kccValueLabel.length > 0 || kohlsRewardValueLabel.length > 0 || eligibleForBI) }} earnings-block  {{/if}}">

{{if kccValueLabel.length > 0 || kohlsRewardValueLabel.length > 0 || (~root.$env.ksEnableSephora && eligibleForBI)}}
    {{if !~root.$env.ksEnableSephora}}
        <div class="earning-section-hr"></div>
        <p class="earning-text">{{:cncLabels.earning_your_earnings_label}}</p>
    {{else}}
        <p class="earning-text-redesign">{{:cncLabels.earning_your_earnings_label_reDesign}}</p>
        <div class="earning-section-checkout"></div>
    {{/if}}
{{/if}}

<!-- Kohls Cash section started -->
{{if kccValueLabel.length > 0 }}
        {{if ~root.$env.ksEnableSephora}} <!-- Kohls Cash section REDESIGN -->
            <div class="kohls-cash-container">
                <img class="kohls-cash-image" src="{{v_~root.$resourceRoot('cnc')}}images/kohlcash.png" alt="Kohls Cash" />
                <span class="kohls-cash-amount-redesign">{{: kccValueStr}}</span><br />
                <div class="">
                    <span class="earnings-row-right-top">{{: kccValueLabel}}</span><br />
                    <span class="earnings-row-right-bottom">{{: kccDescLabel}}</span>
                </div>
            </div>
        {{else}}
            <div class="kohls-cash">
                <p><span class="kohls-cash-amount">{{: kccValueLabel}}</span><br />
                <span class="kohls-cash-date">{{: kccDescLabel}}</span></p>
            </div>
        {{/if}}

{{/if}}
<!-- Kohls Cash section Ended -->

<!-- Kohls Reward section started -->


{{if kohlsRewardValueLabel.length > 0}}
   
    {{if ~root.$env.ksEnableSephora}} <!-- Kohls Reward section REDESIGN -->
        <div class="kohls-rewards-container">
            <img class="kohls-rewards-image" src="{{v_~root.$resourceRoot('cnc')}}images/rewards-green-big.png" alt="Kohls Rewards" />
            <span class="kohls-rewards-amount">{{: kohlsRewardAmount}}</span><br />
            <div class="">
                    <span class="earnings-row-right-top">{{: kohlsRewardValueLabel}}</span><br />
                    <span class="earnings-row-right-bottom">{{: kohlsRewardDescLabel}}</span>
            </div>
        </div>
    {{else}}
        <div class="kohls-reward">
        {{if showYes2YouRewardDetailLink}}
        <p><span class="kohls-reward-amount-pink">{{: kohlsRewardValueLabel}}
        {{else}}
        <p><span class="kohls-reward-amount"> {{: kohlsRewardValueLabel}}
        {{/if}}	
        </span><br /><span class="added-balance-text">{{: kohlsRewardDescLabel}}</span></p>
        </div>
    {{/if}}
{{else loyaltyDownMessage}}
    
    {{if ~root.$env.ksEnableSephora}} <!-- Kohls loyalty down REDESIGN -->
        <div class="kohls-rewards-container">
            <img class="kohls-rewards-image-loyaltyDown" src="{{v_~root.$resourceRoot('cnc')}}images/rewards-green-big.png" alt="Kohls Rewards" />
            {{if kccValueLabel.length <= 0 && kohlsRewardValueLabel.length <= 0}}
                    <div class="earning-section-hr"></div>
            {{/if}}
            <div class = "kohls-reward-message-redesign">{{:~convertHTML(cncLabels.kls_loyalty_system_down_label)}}</div>
        </div>
    {{else}}
        {{if kccValueLabel.length <= 0 && kohlsRewardValueLabel.length <= 0}}
            <div class="earning-section-hr"></div>
        {{/if}}
        <div class = "kohls-reward-message">{{:~convertHTML(cncLabels.kls_loyalty_system_down_label)}}</div>
    {{/if}}
{{/if}}

<!-- Old Rewards Detail-->
{{if !~root.$env.ksEnableSephora}}
    {{if showYes2YouRewardDetailLink}}
    <div class="kohls-reward-details">
        <a href= {{: cncLabels.kls_rewards_feature_link}} class="yes2youDetailsLink" target="_blank">  {{: cncLabels.earning_kohls_yes2you_reward_detail_link_label}} </a>
        </div>
    {{/if}}

    {{if showRewardDetailLink}} 
        <div class="kohls-reward-details">
        <button type="button" id="{{id:'reward-detail'}}" data-status="{{if isShowingKohlsRewardDetail}}hidePopUp{{/if}}" >{{if isShowingKohlsRewardDetail}}{{:cncLabels.earning__hide_kohls_reward_detail_label}}{{else}}{{:cncLabels.earning_kohls_reward_detail_label}}{{/if}}</button>
        </div>
    {{/if}}

    {{if isShowingKohlsRewardDetail}}
        {{for tmpl="tpl-cartRewardPanelDetail" /}} 
    {{/if}}
{{/if}}

<!-- Sephora Section Start -->
{{if ~root.$env.ksEnableSephora && eligibleForBI}}

    <div class="earning-sephora-container ">
        <img class="earnings-sephora-image" 
        src="{{v_~root.$imageRoot('cnc')}}kds-web-core/assets/sephora/logos/logo-sephora-beauty-insider-stacked-k.svg"
        alt="Sephora Beauty Insider" />
        <p class="earnings-sephora-message">{{:cncLabels.earning_sephora_message}}</p>
    </div>
{{/if}}


<!-- New Reward Details Button -->
{{if ~root.$env.ksEnableSephora && showRewardDetailLink}}
<div class="earning-section-shoppingBag"></div>
    <div class="kohls-reward-details-new">
        <div><span class="kr_earn_text_bottom">{{: cncLabels.earning_reward_detail_description}}  <a href="/feature/kohls-rewards.jsp" target="_blank"
            class="detailsLink">{{: cncLabels.kls_earning_kohls_reward_link}}</a></span>
        </div>
    </div>
{{/if}}
</div>

<!-- Kohls reward section ended -->

</script>



<script id="tpl-cartRewardPanelDetail" type="text/x-jsrender">
<div class="earning-section-hr"></div>

<!-- kohls reward details block started -->
<div class="earn_details_popup">
    <img class="kohlsRewardsImg_popup" src="{{_$env.resourceRoot}}images/dgsImages/rewards-lockup-d-green@2x.png"
        alt="Kohls Rewards">
    <div class="earn_details_popup_content">{{:rewardDetailMessage}}
            <div class="popup_content_separator">
            </div>
            <div><span class="popup_earn_text_bottom">{{: cncLabels.earning_reward_detail_description}}<br><u><a href="https://www.kohls.com/feature/kohls-rewards.jsp" target="_blank"
                class="detailsLink">{{: cncLabels.earning_kohls_reward_link}}</a></u></span>
            </div>
        </span>
    </div>
    <div id="kohlsRewards_WcsContent" class="popup_WcsContent"> <object type="text/html"
            data="{{:cncLabels.earning_kohls_reward_link_url}}" class="wcsContent_object"></object>
    </div>
</div>
<!-- kohls reward details block ended -->
</script><script id="tpl-orderSummaryNewTemplate" type="text/x-jsrender">
		
        <!-- Header -->
        <div class="order-summary-header">
            <div> {{:~root.cncLabels.kls_digital_receipt_order_summary}} </div>				
        </div>
		<!-- Shipping message -->
        {{if ~root.$env.pageName =='cart'}}
        <div class="free-ship-container">
            <div class="free-ship-msg">{{:freeShippingMsg}}</div>
            {{if freeShippingThreshold > 0}}
            <div class="progress-bar">
                <kds-progress-bar value="{{:freeShippingThresholdValue}}" min="0" max="{{:freeShippingThreshold}}" id="shipping-progress-bar"></kds-progress-bar>
            </div>
            {{/if}}
        </div>
        {{/if}}
		<!-- Items -->
		<div class="cart-block-item">
			<span class="cart-block-item-name">{{:(cncLabels.kls_order_sum_items)}} ({{:quantityCount}})</span>
			<span class="cart-block-item-value">{{:subtotal}}</span>
		</div>

		<!-- KC & Coupons -->
		<div class="cart-block-item">
			<span class="cart-block-item-name">{{:(cncLabels.kls_order_sum_kc)}}</span>
			<span class="cart-block-item-value">-${{:kohlsCashOffers}}</span>
		</div>

		<!-- Surcharges & Fees -->
		{{if surchargesAndFees > 0}}
			<div class="cart-block-item">
				<span class="cart-block-item-name">
					<span> {{:(cncLabels.kls_order_sum_surcharges_and_fees)}} </span>
					<img src="{{v_~root.$env.KDSAssetsPath}}/icons/price-story/info-circle-icon.svg" alt="INFO"
					id="{{id:'taxModalOpen'}}" class="info-circle-icon">
				</span>
				<span class="cart-block-item-value">${{:surchargesAndFees}}</span>
			</div>
		{{/if}}

		<!-- Shipping -->
		<div class="cart-block-item">
			<span class="cart-block-item-name">
				<span> {{:(cncLabels.kls_order_sum_shipping)}} </span>
				<img src="{{v_~root.$env.KDSAssetsPath}}/icons/price-story/info-circle-icon.svg" alt="INFO"
					id="{{id:'shippingModalOpen'}}" class="info-circle-icon">
			</span>
			{{if isEliteFreeShipping}}
				<img alt="MVC" src="{{v_~root.$resourceRoot('cnc')}}images/loyaltyV2/mvc-color.png" class="mvc-image" />
				<span class='free-shipping'>{{:(cncLabels.kls_order_sum_shipping_free)}}</span>
			{{else}}		
				<span class="cart-block-item-value">{{if shippingCharges}}${{:shippingCharges}}{{else}}<span class="free-shipping"> {{:(cncLabels.kls_order_sum_shipping_free)}}</span>{{/if}}</span>
			{{/if}}
		</div>

		<div id="shippingModal"></div>
		
		<!-- Taxes -->
        {{if isCartPage && estimated}}
            <div class="cart-block-item" style="display:none;"></div>
        {{else}}
            <div class="cart-block-item">
                <span class="cart-block-item-name">
                <span>{{if estimated}}{{:(cncLabels.kls_order_sum_estimated_tax)}}{{else}}{{:(cncLabels.kls_order_sum_tax)}}{{/if}}</span>
                <img src="{{v_~root.$env.KDSAssetsPath}}/icons/price-story/info-circle-icon.svg" alt="INFO"
                        id="{{id:'shippingModalOpen'}}" class="info-circle-icon">
                </span>			
                <span class="cart-block-item-value">${{:estimatedTax}}</span>
            </div>
        {{/if}}

		<!-- Order Total -->
		<div class="order-summary-total-hr"></div>
		<div class="cart-block-item cart-block-bold">
			<span class="cart-block-item-name">{{if isCartPage && estimated}}{{:(cncLabels.kls_pre_tax_order_total)}}{{else}}{{:(cncLabels.kls_static_order_total)}}{{/if}}</span>
			<span class="cart-block-item-value">{{if isCartPage && estimated}}${{:preTaxOrderTotal}} {{else}}${{:orderTotal}}{{/if}}</span>
		</div>

		{{if showPaymentBreakup}}
			<!-- Payment Used -->
			<div class= {{if giftcardCount > 0}} "order-summary-payment-section order-summary-multiple-payments" 
				{{else}} "order-summary-payment-section" {{/if}}>
				{{if giftcardCount > 0}}
					<div class="cart-block-item">
						{{if giftcardCount > 1}}
							<span class="cart-block-item-name">{{:(cncLabels.kls_giftcards_ordersummary)}} ({{:giftcardCount}})</span>
						{{else}}
							<span class="cart-block-item-name">{{:(cncLabels.kls_giftcard_ordersummary)}}</span>
						{{/if}}
						<span class="cart-block-item-value"> -${{:giftcardPaymentTotal}} </span>
					</div>
				{{/if}}
				{{if paymentTenderTotal > 0}}
					<div class="cart-block-payment-info">
					<span class="cart-block-payment-nowrap ctHidden">{{:cncLabels.kls_charged_to_ordersummary}}</span>
						<div class="cart-block-payment cart-block-bold">
							<span class="cart-block-payment-nowrap ctHidden">{{:paymentTenderLabel}}</span>
							<span class="cart-block-item-value"> ${{:paymentTenderTotal}} </span>
						</div>
					</div>
				{{/if}}
			</div>
		{{/if}}

		{{if savings}} 
			<!-- Savings -->
			{{if !showPaymentBreakup}}
				<div class="order-summary-total-hr"></div>
			{{/if}}
			<div class="cart-block-item cart-block-bold cart-block-savings">
				{{:(cncLabels.kls_order_sum_total_savings)}} ${{:savings}}
			</div>
		{{/if}}
        {{if ~root.$env.pageName =='cart'}}
            <kds-button title="Proceed to Checkout" class="checkout-btn" id="{{id: 'redirectUrl'}}" aria-label="Proceed to checkout" size="md" tabindex="0">{{:cncLabels.proceed_to_checkout}}</kds-button>
        {{else}}
            <kds-button title="Place Order" class= {{if isCartEligibleForPlaceOrder}} "checkout-btn" {{else}} "checkout-btn disabled-checkout-btn" {{/if}} id="{{id: 'redirectUrl'}}" aria-label="Place order" size="md" tabindex="0">{{:cncLabels.place_order_text}}</kds-button>
            <div class={{if !isCartEligibleForPlaceOrder}} "place-order-disclaimer-msg place-order-disclaimer-disable" {{else}} "place-order-disclaimer-msg" {{/if}}>{{:cncLabels.kls_place_order_disclaimer}}</div>
        {{/if}}
        {{if ~root.$env.pageName =='cart' && ~root.$env.ksPayLaterMessageInCart}}
            <div data-pp-message data-pp-placement="cart" data-pp-amount="{{v:preTaxOrderTotal}}" data-pp-style-logo-type="alternative" data-pp-style-layout="text" data-pp-style-text-align="center"></div>
        {{/if}}
        <div id="{{id: 'cartRewardPanel'}}">
		</div>
</script>

<script id="tpl_shippingModal" type="text/x-jsrender">
<div class="shippingTaxOuterWrap" id="shippingTaxTooltip">
        <div id="csimodal-container" class="kds">
            <div class="shipping-tax-container">
                {{if cartJsonData.orderSummary}}
                    <h2 class="title_background">
                        <div class="heading-06 shipping-fees-title">{{:cncLabels.kls_static_tr_checkout_ship_tax}}</div>
                        <button id="close_model_shippingTax"><img class="button_close_shippingTax" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross@3x.png" alt="Close Shipping and Tax pop-up" title=""></img></button>
                    </h2>
                    <div class="shipping-tax-hr"></div>
                {{/if}}
                <div class="csi-content shippingTax_wrap">
                    <div class="shippingTax_LHS_tr">
                        {{if #data.cartJsonData.shipmentInfo && #data.cartJsonData.shipmentInfo.length > 0}}                            
                            {{for #data.cartJsonData.shipmentInfo ~cncLabels = #data.cncLabels}}
                            {{if fulfillmentType && fulfillmentType.toLowerCase() != 'email'}}
                                <div class="shippingTax">
                                    <!--Start: Code changes for Marketplace defect MPP-550 -->
                                    {{if shippingMethod !='BOPUS' && shippingMethod !='BOSS'}}
                                        <!-- START Ship to Home section -->
                                       <div class="fleft">
                                            <div class="shippingDetailModal supporting-micro-01-medium">{{:~getShippingDetailLabelByMethod(trCheckoutCartErrors,shippingMethod,shippingMethodDescription)}}&nbsp;{{:~cncLabels.kls_ship_to_home}}</div>
                                            <div class="item-count supporting-micro-01">{{:shipmentItemsCount}}&nbsp;{{:~cncLabels.kls_static_tr_checkout_gift_items_comma_wo_kohls}}</div>
                                            {{if ~root.cartJsonData.addressDetails && ~root.cartJsonData.addressDetails.shipAddress}}
                                                {{if ~root.cartJsonData.addressDetails && ~root.cartJsonData.addressDetails.shipAddress.length > 0}}
                                                    {{if !~root.cartJsonData.isVGCOnly && !~root.cartJsonData.isOnlyVGCAddress}}
                                                            {{for ~root.cartJsonData.addressDetails.shipAddress}}
                                                                {{if selected}}
                                                                    <div class="fleft supporting-micro-01">{{:city}}</div>
                                                                {{/if}}
                                                            {{/for}}
                                                    {{/if}}
                                                {{/if}}
                                            {{/if}}
                                        </div>
                                        <!-- END Ship to Home section -->
                                    {{else}}
                                        {{if shippingMethod ==='BOPUS' || shippingMethod ==='BOSS' }}
                                        <div class="fleft">
                                            <div class="shippingDetailModal supporting-micro-01-medium">{{:~getShippingDetailLabelByMethod(trCheckoutCartErrors,shippingMethod,shippingMethodDescription)}}</div>
                                            <div class="item-count supporting-micro-01">{{:shipmentItemsCount}}&nbsp;{{:~cncLabels.kls_static_tr_checkout_gift_items_comma_wo_kohls}}</div>
                                            <div class="supporting-micro-01">{{:~getBopusStoreName(~getStoreIdOrName(shipmentId,true))}}</div>
                                        </div>
                                        {{/if}}
                                    {{/if}}
                                    <!--End: Code changes for Marketplace defect MPP-550 -->
                                    <!-- START : BOPUSV-134 -->
                                    {{if shippingMethod !='BOPUS' && shippingMethod !='BOSS' && shipmentPriceInfo}}
                                        <span class="fright supporting-micro-01">{{:~addCurrency(shipmentPriceInfo.originalShippingCharges)}}</span>
                                    {{else}}
                                     {{if shippingMethod ==='BOPUS' || shippingMethod ==='BOSS' }}
                                        <span class="fright supporting-micro-01-medium">{{:~cncLabels.kls_static_tr2_pb_free_shipping_label}}</span>
                                     {{/if}}
                                    {{/if}}
                                    <!-- END : BOPUSV-134 -->
                                    <!-- START: Code Changes for MarketPlace Defect MPP-794/ATG-6541 -->
                                        {{if shipmentPriceInfo && shipmentPriceInfo.freeShipQualificationDelta != "" && shipmentPriceInfo.freeShipQualificationDelta}}
                                            <span class="fleft supporting-micro-01-medium away-from-free-ship">{{:~addCurrency(shipmentPriceInfo.freeShipQualificationDelta)}} {{:~cncLabels.kls_static_tr2_free_ship_suggest_message_modal_window}}</span>
                                        {{/if}}
                                    <!-- END: Code Changes for MarketPlace Defect MPP-794/ATG-6541 -->
                                    {{if shippingOffers}}
                                        {{if shippingOffers.length > 0}}
                                            {{for shippingOffers}}
                                                {{if confirmationMessage}}
                                                    <div class="clear"></div>
                                                    <span class="shipping-text supporting-micro-01-medium fleft"> {{:confirmationMessage}}</span>
                                                    <span class="supporting-micro-01-medium fright">{{:~cncLabels.kls_static_checkout_minus_sign}}{{:~addCurrency(discountAmount)}}</span>
                                                {{else eliteFreeShipping && offerName}}
                                                    <div class="clear"></div>
                                                    <span class="fleft supporting-micro-01"> {{:offerName}}</span>
                                                    <span class="supporting-micro-01-medium">{{:~cncLabels.kls_static_checkout_minus_sign}}{{:~addCurrency(discountAmount)}}</span>
                                                {{/if}}
                                            {{/for}}
                                        {{/if}}
                                    {{/if}}
                                </div>
                            {{/if}}
                            {{/for}}
							{{if #data.cartJsonData.shipmentInfo && ~getAllVgcShipmentItemsCountOrTotal(shipmentInfo,'vgcItem')}}
                            <div class="shippingTax">
                                <span class="fleft shippingDetailModal supporting-micro-01">
                                    <div>{{:~root.cncLabels.kls_static_tr2_pb_vgc_email_label_shipping_tax_model}}</div>
                                    <div>{{:~getAllVgcShipmentItemsCountOrTotal(shipmentInfo,'itemsCount')}} {{:~root.cncLabels.kls_static_tr_checkout_gift_items_comma_wo_kohls}}</div>
                                    <div>{{:~getAllVgcShipmentItemsCountOrTotal(shipmentInfo,'itemsTotal')}}</div>
                                </span>
                                <span class="fright supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr2_pb_free_shipping_label}}</span>
                            </div>
							{{/if}}
                        {{/if}}                     
                        <div class="clear"></div>                            
						{{if !cartJsonData.orderSummary.taxFeeEnabled  }}
   							{{if cartJsonData.orderSummary && cartJsonData.orderSummary.totalSurcharges}}
                                <div class="clear"></div>
                                    <div class="shippingTax shippingTaxpad">
                                        <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr_checkout_gift_total_surcharges}}</span>
                                        <span class="fright supporting-micro-01-medium">{{if shipmentPriceInfo}}{{:~addCurrency(cartJsonData.orderSummary.totalSurcharges)}}{{/if}}</span>
                                    </div>
                                <div class="clear"></div>
							{{/if}}
						{{/if}}
                        {{if #data.cartJsonData.orderSummary && #data.cartJsonData.orderSummary.taxFeeEnabled }}
                            <div class="taxfeecontainer padTB0">                                        
                                {{if cartJsonData.orderSummary.totalSurcharges}}
                                    {{if cartJsonData.cartItems && cartJsonData.cartItems.length > 0}}
                                        {{for cartJsonData.cartItems}}
                                            {{if surchargePriceInfo}}
                                                {{if surchargePriceInfo.surchargeExtendedPriceAfterDiscounts}}
                                                    <div class="shippingTax marTB0 ship-surcharge">
                                                        <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr_checkout_gift_shipping_surcharges}}</span>
                                                        <span class="fright supporting-micro-01">{{:~addCurrency(surchargePriceInfo.surchargeExtendedPriceAfterDiscounts)}}</span>
                                                    </div>
                                                    <div class="clear"></div>
                                                {{/if}}
                                            {{/if}}
                                        {{/for}}
                                    {{/if}}
                                {{/if}}
                                {{if cartJsonData.cartItems && cartJsonData.cartItems.length > 0}}
                                    {{for cartJsonData.cartItems}}
                                        {{if taxFee}}
                                            {{if taxFee.length > 0}}
                                                {{for taxFee}}
                                                    {{if feeDescription}}
                                                        <div class="shippingTaxpad mtop5">
                                                            <span class="fleft supporting-micro-01-medium">{{:feeDescription}}</span>
                                                            <span class="fright supporting-micro-01">{{:~addCurrency(feeAmount)}}</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    {{/if}}
                                                {{/for}}
                                            {{/if}}
                                        {{/if}}
                                    {{/for}}
                                {{/if}}                                 
                            </div>
                            <div class="clear"></div>
                            {{if cartJsonData.orderSummary}}
                                <div class="taxfeecontainer">
                                    <div class="shippingTax">
                                        <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr_checkout_sales_tax}}</span>                                        
                                        <span class="fright supporting-micro-01">{{:~addCurrency(cartJsonData.orderSummary.tax.tax)}}</span>
                                    </div>
                                </div>
                            {{/if}}
                            <div class="shippingTax shippingTaxpad marTB0">                             
                                <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.Kls_static_tr_cart_shipping_total}}</span>
                                <span class="fright supporting-micro-01-medium">{{:~addCurrency(cartJsonData.orderSummary.totalShippingAndTax)}}</span>
                            </div>
                        {{else}}
                           {{if cartJsonData.orderSummary}}
                                <div class="shippingTax shippingTaxpad">
                                    <p class="fleft supporting-micro-01-medium">{{if cartJsonData.orderSummary.tax.estimated}}{{:~root.cncLabels.kls_static_tr_checkout_gift_tax_est}}{{else}}{{:(cncLabels.kls_static_tr_checkout_gift_tax)}}{{/if}}
                                    {{if cartJsonData.orderSummary.tax.estimated}}
                                        <span>{{:cartJsonData.orderSummary.tax.taxRate}}%</span></p>
                                    {{/if}}
                                    <p class="fright supporting-micro-01-medium">{{:~addCurrency(cartJsonData.orderSummary.tax.tax)}}</p>
                                </div>
                            {{/if}}
                        {{/if}}
                    </div>

                     <div class="fleft shippingTax_RHS_tr">
                        <div class="shipping-tax-hr"></div>
                        <div class="shippingTax">
                            {{if cartJsonData.orderSummary}}
                                {{if cartJsonData.orderSummary.taxFeeEnabled}}
                                    <p class="fleft supporting-micro-01-medium mtop10 mbot_10">{{:~convertHTML(cncLabels.kls_static_tr_checkout_how_ship_rates_calculated)}}</p>
                                {{/if}}
                            {{/if}}
                            <p class="fleft supporting-micro-01">{{:(cncLabels.kls_static_tr_checkout_shipping_ratesV2)}}</p>                          
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</script>


<script id="tpl-actionPanel-template" type="text/x-jsrender">
<!-- This file historically held coupons panel and proceed to checkout + place order functionality for desktop - now it is home for the CNC header and the primary CTA button for mobile screens -->
    <div id="remedy_challenge_container" class="display-none"></div> 
    <div class="checkout-logo-block checkout-logo-block-new" id="checkout-logo-block">
        {{if isCheckoutPage}} 
            <span class="checkout-logo-block-items checkout-logo-block-items-new">
                <div class="header-kohls-logo" id="header-kohls-logo"><a href="/"><img class="header-kohls-logo-img" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohl's" /></a></div>
                <div class="header-text-checkout" id="header-checkout-text">{{:cncLabels.checkout_text_on_shopping_cart_page}}</div>
            </span>
    </div>
    <div class="return-to-cart-button" id="header-shopping-cart"><a href="/checkout/shopping_cart.jsp"><span><img class="return-to-cart-arrow" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title=""><span class="return-to-cart-text">{{:cncLabels.return_to_cart}}</span></span></a></div>
        {{else}}
            <!-- Do not remove the div with id="header-container", as it is necessary for the search API to display suggestions in the ta-suggestion-box div  -->
            <div id="header-container">
                <div class="cart-logo-block-items cart-logo-block">
                    <span class="header-shopping-cart-title" id="header-shopping-cart">
                        <span class="header-kohls-logo" id="header-kohls-logo">
                            <a href="/"><img class="header-kohls-logo-img" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohl's" title=""></a>
                        </span>
                        <div class="header-text-checkout">{{:cncLabels.my_cart_text}}</div>
                    </span>
                    <span class="header-search-bar-small">
                        {{if ~root.$env.ksEnableSearchInShoppingBag}}
                            <form action="/search.jsp" method="get" id="site-search-bar">
                                <fieldset>
                                    <label class="visually-hidden" for="search">Search by
                                        Keyword or Web ID </label> <span class="blank_tt">&nbsp;</span>
                                    <input type="submit" value="web-regular" class="button-submit button-search" name="submit-search" >
                                    <input type="text" placeholder="What else are you looking for today?" class="input-text" id="search" name="search" autocomplete="off">
                                </fieldset>
                            </form>
                        {{/if}}
                    </span>
                    <span class="testAndTargetSlot" id="testAndTargetSlot"></span>
                </div>
            </div>   
    </div>
        {{/if}}

    <div class="action-panel-container" id="left-nav-sticky">
        <div class= {{if isCheckoutPage}} "header-margin-checkout" {{else}} "header-margin-cart" {{/if}} id="header-margin"></div>
        <div class="action-panel-placeorder-error placeorder-error-header">
                <p class="action-panel-placeorder-error-msg"></p>
        </div>
        <div class="action-panel-mobile" id="action-panel-expand">
            {{if isCheckoutPage}}
                <div class="order-disclaimer-panel order-disclaimer-panel-hide-drawer">
                    <kds-button title="Place Order" class= {{if isCartEligibleForPlaceOrder}}"checkout-button" {{else}} "checkout-button disabled-checkout-btn" {{/if}} id="{{id: 'redirectionUrl'}}" aria-label="Place order" size="md" tabindex="0" >{{:cncLabels.place_order_text}}</kds-button>
                    <div class={{if !isCartEligibleForPlaceOrder}} "place-order-disclaimer-new place-order-disclaimer-disable" {{else}}"place-order-disclaimer-new" {{/if}}>{{:cncLabels.kls_place_order_disclaimer}}</div>
                </div>
            {{else}}
                <div class="action-panel-block-dactive-chkbtn action-panel-chkbtn-hide-drawer">
                    <kds-button title="Proceed to Checkout" class="checkout-button" id="{{id: 'redirectionUrl'}}" aria-label="Proceed to checkout" size="md" tabindex="0">{{:cncLabels.proceed_to_checkout}}</kds-button>
                </div>
            {{/if}}
        </div>
    </div>
</script>


<script id="tpl-actionPanel-peek-offer" type="text/x-jsrender">

                <span class="peek-icon"><img class="peek-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/user-profile.png" alt="" title="" /></span>
                <span class="action-panel-offer-unlocked">Offer unlocked! <br/> <span class="action-panel-open-for-details"> Open for details</span></span>
                              
   
</script>


<script id="tpl-openOffersPanel-template" type="text/x-jsrender">
        <div class="open-offers-container new-popup-design-selector" id="openOffersContainerDiv">
            <button class="open-offers-btn" id="{{id:'offersPanelToggle'}}" aria-label="Apply Coupons & Kohl's Cash">
                <div class="open-offers-block">
                    <span class="open-offers-img-container">
                        <img class="open-offers-icon-img" src="./../../../../../../../www.kohls.com/cnc/media/images/dgsImages/ic-percent-dotwack@3x.png"></img>
                        <img class="open-offers-icon-img" src="./../../../../../../../www.kohls.com/cnc/media/images/dgsImages/ic-dollarsign-circle@3x.png"></img>
                    </span>
                    <span class="open-offers-text">{{:cncLabels.apply_coupons_and_kc}}</span>
                    {{if isCollapsed}}
                        <img class= "open-offers-chevron" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-right.png" alt="" title=""></img>
                    {{else}}
                        <img class="open-offers-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
                        <img class= "open-offers-chevron-up" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-up.png" alt="" title=""></img>
                    {{/if}}
                </div>
            </button>
            {{if !isCollapsed}}
                {{for tmpl='tpl-openOffersPanelExpand' ~env = $env /}}
            {{/if}}
        </div>
</script> 

<script id="tpl-openOffersPanelExpand" type="text/x-jsrender">
    <div class="open-offers-block-expand">
        <div class="open-offers-expand-header">
            <div class="savingPanel">
                <span class="savingText">{{:cncLabels.ur_saving_text}} </span>
                <span class="savingAmount">{{:savings.currency}}</span>
            </div>
        </div> 
        <div class="open-offers-list">
        {{if isGuestOrSoftLoggedIn}}
            <div class="sign-in-kohls-cash">
                <div class="signInPanel" id="{{id: 'signInPanel'}}">
                    <a href="javascript:void(0);">
                        <div class="sign-in-block-container">
                            <span class="sign-in-block">
                                    <span class="user-profile-span"><img class="userProfile" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-user-profile@3x.png" /></span>
                                    <span class="sign-in-text-inline">{{:cncLabels.sign_in_to_see_coupons}}</span>
                            </span>
                            <kds-button title="Sign In" class="sign-in-inline-panel-btn" aria-label="Sign in button" size="sm" tabindex="0">{{:cncLabels.sign_in_coupons_btn}}</kds-button>
                        </div>
                    </a>
                </div>
            </div>
        {{/if}}
            <!-- Container for coupons, KC, and manual entry panel + pop-up messages -->
            <div class="inline-offers-container">
                <div class="offers-block" id="offersBlock">
                {{if isGuestUser}}
                    <div class="offers-sign-in-divider"></div>
                {{/if}}
                    <div id="{{id:'inlineOfferContainer'}}" class="inlineOfferContainer"></div>
                </div>
            </div>
        </div>
        <div class="return-to-cart-btn-container">
            <kds-button title="Return to Cart" button-type="secondary" class="return-to-cart-btn" id="{{id:'returnToCartBtn'}}" aria-label="Return to cart button" size="md" tabindex="0">{{:cncLabels.return_to_cart_coupons_panel}}</kds-button>
        </div>
    </div>
</script><script id="tpl-inlinePanelOffer" type="text/x-jsrender">
<!-- Data is coming from the open offers panel -->
{{if #data.state == "show" || #data.state == "enter"}}
    {{for tmpl="tpl-kohlsCashInlinePanelOffer" ~data = #data /}}
    {{for tmpl="tpl-enterkohlsCashInline"/}}
{{else #data.state =="loading"}}
    {{for tmpl="tpl-inlinePanelLoading"/}}
{{/if}}
</script>

<script id="tpl-inlinePanelLoading" type="text/x-jsrender">
    <div class="kohls-cash-panel">
         <div>Loading...</div>
    </div>
</script>

<script id="tpl-inlineFailurePanel" type="text/x-jsrender">
    <div class="kohls-cash-msg message-block">  
        <div class="manual-entry-block">
            <div class="manual-entry-failure-img"><img class="manual-entry-failure-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-alert-triangle.png" /></div>
                <div class="manual-entry-failure-msg" role="status" aria-live="polite">{{:message}}
            </div>
        </div>
        <!--<div class="close-kc-msg closeDiv" id="{{id:'closeApplyKc'}}"><img class="close-kc-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/cross-circle@3x.png" /></div>-->
    </div>
</script>

<script id="tpl-kohlsCashSuccessInlinePanel" type="text/x-jsrender">
    <div class="kohls-cash-msg message-block">   
        <div class="manual-entry-block">
            <div class="manual-entry-success-img"><img class="manual-entry-success-img-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" /></div>
            <div class="manual-entry-success-msg" role="status" aria-live="polite">{{:cncLabels.wallet_success_message_head}}</div>
        </div>
        <!--<div class="close-kc-msg closeDiv" id="{{id:'closeApplyKc'}}"><img class="close-kc-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/cross-circle@3x.png" /></div>-->
    </div>
</script>
 
<script id="tpl-enterkohlsCashInline" type="text/x-jsrender">
    <div class="kohls-cash-panel validateInline" id="actionPanelForm">
        {{if #data.applyState =="failure"}}
            {{for tmpl="tpl-inlineFailurePanel" /}}
        {{else #data.applyState =="success"}}
            {{for tmpl="tpl-kohlsCashSuccessInlinePanel"/}}
        {{/if}}
        <div class="kohls-cash-panel-header">{{:cncLabels.have_any_kohls_cash_or_coupons}}</div>
        <div class="apply-kc-coupon-container">
            <div class="input-container">
                <div class="promo-code-panel">
                    <div class="kohls-cash-promo-code-error-message"></div>
                    <div class="promo-code-txt">{{:cncLabels.enter_code}}</div>
                    <input type="text" name="promoCode" aria-label="Add Kohl's Cash or Coupons" placeholder="" class="kcCodeTxt required action-panel-kc-number" id="{{id:'kcCodeTxt'}}" maxlength="15"></input>
                    <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon-offer-panel promo-alert-img"></img>
                    <div class="error-message-contact offers-panel-error-margin promo-code-error"></div>
                    <div class="panel-hr"></div>
                </div>
                <div class="pin-panel">
                    <div class="pin-txt">{{:cncLabels.enter_pin}}</div>
                    <input type="text" name="pin" aria-label="Enter Kohl's Cash PIN" maxlength="4" placeholder="" class="input-text number-input required action-panel-pin kcPinTxt" id="{{id:'kcPinTxt'}}"></input>
                    <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon-offer-panel pin-alert-img"></img>
                    <div class="error-message-contact offers-panel-error-margin"></div>
                    <div class="panel-hr"></div>
                </div>
            </div>
            <div class="apply-btn-container">
                <kds-button title="Add" class="apply-btn" id="{{id:'applyBtn'}}" aria-label="Add Kohl's Cash or Coupon button" size="sm" button-type="secondary" tabindex="0">{{:cncLabels.kls_addCouponForm_label_add}}</kds-button>
            </div>
        </div>
    </div>
</script>

<script id="tpl-inlineOfferDetails" type="text/x-jsrender">
    <div>
        <div class="legal-popup" tabindex="0">
            <button id="closeCouponInfoModal" class="close-msg" aria-label="Close"><img class="coupon-info-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" /></button>
            <div class="legal-popup-header">{{if offerDetails.offerUsage == 'SUPC'}}{{:offerDetails.code}}{{else}}{{:offerDetails.name}}{{/if}}<img class="store-icon-img" {{if offerDetails.offerImage}} src={{_offerDetails.offerImage}} {{else}} style="display:none" {{/if}} alt="" title="" /></div>
            <div class="legal-popup-discount-eligible">{{:offerDetails.description}}</div>                
            <div class="legal-popup-discount-expire">{{:offerDetails.additionalInfo}}</div>   
            <div class="legal-popup-content">                                      
                {{if offerDetails.code}}     
                    <p class="legal-popup-promo">{{:cncLabels.kls_with_promo_code_label}} <span class="legal-popup-promo-bold">{{:offerDetails.code}}</span></p>             
                {{/if}}                    
                <p>{{:offerDetails.disclaimer}}</p>        
            </div>
        </div>
    
</script>

<script id="tpl-inlineOfferError" type="text/x-jsrender">
        <div class="legal-popup">
            <img class="closeDiv" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/cross-circle@3x.png" />
            <div class="legal-popup-content">                                                                             
                <p>{{:offerError}}</p>        
            </div>
        </div>
</script>

<script id="tpl-kohlsCashInlinePanelOffer" type="text/x-jsrender">
<div class="kohls-cash-offers scrollbar-width-thin" id="{{id:'offerIdInline'}}">
        <div id="message" style="display:none"></div>
        {{if kcItems.length > 1 && maxCoupons && maxCoupons.cartEligibleForKC}}
            <div class="offer-panel-stacked" >
                <div class={{if multipleKcData.KcUsed}} "offer-panel offer-panel-stack-applied" {{else}} "offer-panel offer-panel-stack-unapplied" {{/if}}>
                <button class="apply-offer-container" id="{{id:'KCOfferIdStacked'}}">
                    <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
                    <span class="offer-text-block-hide-drawer">
                        <p class="offer-head-hide-drawer">{{:multipleKcData.amountMessage}}</p>
                        <p class="offer-expire-hide-drawer">{{:multipleKcData.expireMessage}}</p>
                    </span>
                {{if multipleKcData.KcUsed}}
                    <span class="offer-applied-flag-stacked-hide-drawer">
                    <img class="applied-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" />
                    </span>
                    <span class="offer-applied-amount-stacked-hide-drawer">-${{:multipleKcData.totalAmount.toFixed(2)}}</span>
                {{/if}}
                </button> 
                <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
            </div>              
                <div class="cash-stacked-1-bg"></div>
                <button class="cash-stacked-2-bg" id="{{id:'offer-panel'}}" aria-label="Expand stacked Kohl's Cash"></button>
            </div> 
        {{else kcItems.length > 1}}
            <div class="offer-panel-stacked">
                <div class="offer-panel">
                {{if state !="APPLIED" &&  !maxCoupons.cartEligibleForKC}}
                    <span class="side-bar-disable">
                        <img class="side-bar-disable-dollar-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/store@3x.png" />
                    </span>
                {{/if}}
                    <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
                    <span class="offer-text-block-hide-drawer" id="{{id:'KCOfferIdStacked'}}">
                        <p class="offer-head-grey-hide-drawer">{{:multipleKcData.amountMessage}}</p>
                        <p class="offer-expire-grey-hide-drawer">{{:multipleKcData.expireMessage}}</p>
                    </span>
                    <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
                </div>    
                    <div class="cash-stacked-1-bg"></div>
                    <div class="cash-stacked-2-bg" id="{{id:'offer-panel'}}"></div>
                </div> 
        {{/if}}
        {{for tmpl="tpl-multiplekohlsCashInlinePanel" ~data =#parent.data /}}
        {{if offerItems && offerItems.length > 0}}
            {{for offerItems}}  
                {{if state !='IRRELEVANT' || ~root.$env.ksActionPanelAllOffer}}  
                    {{if !systemInitiated && (state =='APPLIED' || isBestOffer || ~root.$env.ksActionPanelBestOffer) }}  
                        <div class= {{if state =="APPLIED"}} "offer-panel offer-panel-applied" {{else}} "offer-panel" {{/if}} data-name={{:name}} data-id = {{:code}} >
                        <button class="apply-offer-container" id="{{id:'panelOfferId'}}">
                        <span class="offer-icon-hide-drawer" id="{{:name}}"><img class="offer-icon-dollar-img-hide-drawer" src="{{_offerImage}}" /></span>
                                {{if state=="LOCKED"}}
                                    <span class="offer-text-block-hide-drawer">                                   
                                        <p class="offer-head-grey-hide-drawer">{{:description}}</p>
                                        <p class="offer-expire-grey-hide-drawer">{{:additionalInfo}}</p>
                                    </span>
                                {{else state =='IRRELEVANT'}}    
                                    <span class="offer-text-block-hide-drawer">                                   
                                        <p class="offer-head-grey-hide-drawer">{{:description}}</p>
                                        <p class="offer-expire-grey-hide-drawer">{{:additionalInfo}}</p>
                                    </span>
                                {{else}}
                                    {{if state=="APPLIED"}}
                                        <span class="offer-applied-flag-hide-drawer">
                                            <img class="applied-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" />
                                        </span>
                                        {{if amount > 0}}
                                            <span class="offer-applied-amount-hide-drawer">-${{:amount.toFixed(2)}}</span>
                                        {{/if}}
                                    {{else state =="QUALIFIED"}}
                                        <span class="apply-coupon-cta">{{:~root.cncLabels.apply_coupon_cta}}</span>
                                        <span class="apply-coupon-cta-mobile">{{:~root.cncLabels.tap_to_apply}}</span>   
                                    {{/if}}
                                    <span class="offer-text-block-hide-drawer">    
                                        <p class="offer-head-hide-drawer">{{:description}}</p>                               
                                        <p class="offer-expire-hide-drawer">{{:additionalInfo}}</p>
                                    </span>
                                {{/if}}
                            </button>
                            <button class="offer-info-hide-drawer" aria-label="Coupon info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
                        </div>
                    {{/if}}
                {{/if}}  
            {{/for}} 
        {{/if}}    
    </div>
</script>

<script id="tpl-multiplekohlsCashInlinePanel" type="text/x-jsrender">
    {{if kcItems.length>1}}
        <div class="kohls-cash-stack-up" style="display:none"><img class="kohls-cash-stack-up-img" id="{{id:'offer-panel'}}" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-up-circle@3x.png" /></div>
    {{/if}}
    {{for kcItems ~env =$env ~kcLength = kcItems.length}}
    {{if ~kcLength}} 
        {{if ~kcLength > 1}}
            <!-- +++++++++++Hide single kohls cash item initially when item is more than one +++++ -->
            <div class={{if state =="APPLIED"}} "offer-panel offer-panel-green-bar offer-panel-applied" {{else}} "offer-panel offer-panel-green-bar" {{/if}} KC-id = {{:itemKey}} style="display:none">
        {{else}}
            <!-- +++++++++++++++for single kohls cash item +++++++++ -->
            <div class={{if state =="APPLIED"}} "offer-panel offer-panel-green-bar offer-panel-applied" {{else}} "offer-panel offer-panel-green-bar" {{/if}} KC-id = {{:itemKey}}>        
        {{/if}} 
    {{/if}}
    {{if state !="APPLIED" && (~data && ~data.maxCoupons && !~data.maxCoupons.cartEligibleForKC)}}    
        <span class="offer-text-block-hide-drawer" id="{{id:'KCOfferId'}}">
            <p class="offer-head-grey-hide-drawer">{{:message}}</p>
            <p class="offer-expire-grey-hide-drawer">{{:expDate}}</p>
        </span>
        <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
        <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
    {{else}}
        <button class="apply-offer-container" id="{{id:'KCOfferId'}}">
            {{if ~data && ~data.maxCoupons}}
                <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
            {{/if}}
            {{if state=="APPLIED"}}
                <span class="side-bar-applied"></span>
                <span class="offer-applied-flag-hide-drawer">
                    <img class="applied-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" />
                </span>
                <span class="offer-applied-amount-hide-drawer">-${{:amount.toFixed(2)}}</span>
            {{else state=="QUALIFIED"}}
                <span class="apply-coupon-cta">{{:~root.cncLabels.apply_coupon_cta}}</span>
                <span class="apply-coupon-cta-mobile">{{:~root.cncLabels.tap_to_apply}}</span>  
            {{else}}
                <span class="side-bar"></span>
            {{/if}} 
            {{if ~kcLength > 0}}
                <span class="offer-text-block-hide-drawer">
                        <p class="offer-head-hide-drawer">{{:message}}</p>
                        <p class="offer-expire-hide-drawer">{{:expDate}}</p>
                </span>
            {{/if}}
        </button>
        {{if ~data && ~data.maxCoupons}}
            <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
        {{/if}} 
    {{/if}}
        </div>
    {{/for}}
</script>
<script id="tpl-messageSlotPanel-template" type="text/x-jsrender">
    {{if $env.pageName === "cart" && cncLabels.kls_static_global_cart_text != ""}}
        <div class="message-slot-block">
        <div class="message-slot-text">
        {{:cncLabels.kls_static_global_cart_text}}
        </div>
        </div>
    {{/if}}
    {{if $env.pageName === "newCheckout" && cncLabels.kls_static_global_checkout_text != ""}}
        <div class="message-slot-block">
        <div class="message-slot-text">
        {{:cncLabels.kls_static_global_checkout_text}}
        </div>
        </div>
    {{/if}}
</script>
