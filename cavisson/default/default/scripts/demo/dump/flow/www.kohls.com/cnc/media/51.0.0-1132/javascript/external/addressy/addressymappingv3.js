$module('adressyInitialization', function (log, env, module, Kjs) {

    var formAddressyObj,
        billingFormAddressyObj,
        selectedAddr,
        addressyFields = {
            'US': [
                {
                    element:
                    "tr_add_us_address_suggestion", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_add_us_address1", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_add_us_address1", field:
                    "Field1", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_us_address2", field:
                    "SubBuilding", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_us_city", field:
                    "City", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_us_state", field:
                    "ProvinceCode", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_us_zipcode", field:
                    "PostalCode", mode:
                    pca.fieldMode.POPULATE
                }
            ],
            'APO': [
                {
                    element:
                    "tr_add_us_address_suggestion", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_add_apo_add1", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_add_apo_add1", field:
                    "Field1", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_apo_city", field:
                    "City", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_apo_state", field:
                    "ProvinceCode", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_add_us_zipcode", field:
                    "PostalCode", mode:
                    pca.fieldMode.POPULATE
                }
            ]

        },
        billingAddressFields = {
            'US': [
                {
                    element:
                    "billingAddr_suggestion", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_billing_add_us_address1", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_billing_add_us_address1", field:
                    "Field1", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_us_address2", field:
                    "SubBuilding", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_us_city", field:
                    "City", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_us_state", field:
                    "ProvinceCode", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_us_zipcode", field:
                    "PostalCode", mode:
                    pca.fieldMode.POPULATE
                }
            ],
            'APO': [
                {
                    element:
                    "billingAddr_suggestion", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_billing_add_apo_add1", field:
                    "{StreetAddress}"
                },

                {
                    element:
                    "tr_billing_add_apo_add1", field:
                    "Field1", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_apo_city", field:
                    "City", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_apo_state", field:
                    "ProvinceCode", mode:
                    pca.fieldMode.POPULATE
                },

                {
                    element:
                    "tr_billing_add_us_zipcode", field:
                    "PostalCode", mode:
                    pca.fieldMode.POPULATE
                }
            ]

        },
        addressyOptions = {
            key: $env.addressyKey,
            search: {
                limit: $env.addressyDefaultSuggestionCount,
                countries: "USA",
                setCountryByIP: true
            },
            minSearch: $env.addressyDefaultKeyStroke
        },
        clearAddressySuggestionEmpty = function (e) {
            if ($(this).val().length < 1) {
                setTimeout(function () {
                    $(".pca > .pcaautocomplete").hide();
                }, 100);
            }
        },
        _activateShipping = function (form) {
            if (typeof tntAddressyValidation != "undefined" && tntAddressyValidation == false) {
                log.info('addressy turned off from T&T');
            } else {
                try {
                    //$form = form; // assigned to global variable to use on event callbacks
                    //const selectedCountry = $form.find('.add_option_radio:checked').val();

                    formAddressyObj = new pca.Address(addressyFields['US'], addressyOptions);
                     selectedAddr = ""; // make it clear the previous form data
                    formAddressyObj.listen("populate", function( detail, items, key){
                        // save the fileds state on selectedAddr variable
                        //capture omniture for Single Page Checkout - Address Selected from Search
                        Kjs.cncOmniture.captureLinkEvent143('Single Page Checkout - Address Selected from Search','address selected from search');
                        $("#addressyContainer").html('');
                        $("#addressForm").show();
                        // if(!cartJsonData.addressDetails.billingAddress || cartJsonData.addressDetails.billingAddress && cartJsonData.addressDetails.billingAddress.length==0){
                        //     $('#sameBillingAddress').closest('.save-address-container').hide();
                        // }
                        selectedAddr = detail;         
                    });

                    $("body").off("keyup", "#tr_add_us_address1, #tr_add_us_address_suggestion", clearAddressySuggestionEmpty); // unbind the previous event
                    $("body").on("keyup", "#tr_add_us_address1, #tr_add_us_address_suggestion", clearAddressySuggestionEmpty); // bind the event
                    return formAddressyObj;
                } catch (e) {
                    log.error("exception in typeahead:" + e.message)
                }
            }
        },
        _activateBilling = function (form) {
            if (typeof tntAddressyValidation != "undefined" && tntAddressyValidation == false) {
                log.info('addressy turned off from T&T');
            } else {
                try {
                    
                    billingFormAddressyObj = new pca.Address(billingAddressFields['US'], addressyOptions);
                    selectedAddr = ""; // make it clear the previous form data

                    billingFormAddressyObj.listen("populate", function( detail, items, key){
                        // save the fileds state on selectedAddr variable
                        $("#billingContainer").html('');
                        $("#billingAddressForm").show();
                        selectedAddr = detail;
                    });
                    $("body").off("keyup", "#tr_billing_add_us_address1, #billingAddr_suggestion", clearAddressySuggestionEmpty); // unbind the previous event
                    $("body").on("keyup", "#tr_billing_add_us_address1, #billingAddr_suggestion", clearAddressySuggestionEmpty); // bind the event

                    return billingFormAddressyObj;
                } catch (e) {
                    log.error("exception in typeahead:" + e.message)
                }
            }
        },
        _validateAddressFieldsChanged = function ($from) {
            // whether address fields values got changed or not
            var isAddrChanged = false,
                fields = {
                    "US": {
                        'tr_add_us_address1': 'Field1',
                        'tr_add_us_city': 'City',
                        'tr_add_us_state': 'Province',
                        'tr_add_us_zipcode': 'PostalCode'
                    },
                    "APO": {
                        'tr_add_us_address1': 'Field1',
                        'tr_add_apo_city': 'City',
                        'tr_add_apo_state': 'Province',
                        'tr_add_us_zipcode': 'PostalCode'
                    }

                };

            const selectedCountry = $from.find('.add_option_radio:checked').val() || 'US';
            if (!selectedAddr) {
                return true; // make it changed if the no previo\us selection from auto complete
            }

            const selectedFields = fields[selectedCountry];
            for (field in selectedFields) {
                if ($.trim($from.find("#" + field).val()).toLowerCase() !== selectedAddr[selectedFields[field]].toLowerCase()) {
                    isAddrChanged = true;
                    break;
                }
            }

            return isAddrChanged;
        };

    return {
        activateShipping: _activateShipping,
        activateBilling:_activateBilling,
        validateAddressFieldsChanged: _validateAddressFieldsChanged
    }
});