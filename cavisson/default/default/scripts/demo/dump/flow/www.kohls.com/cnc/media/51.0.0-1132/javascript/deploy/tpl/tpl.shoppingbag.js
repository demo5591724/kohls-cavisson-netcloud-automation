<script id="tpl-shoppingBagPanel" type="text/x-jsrender">
	<a href="#main-content-cart" class="skip-to-main-content">Skip to main content</a>
	<div id="{{id: 'actionPanel'}}"></div>
	
	<div class= "smart-cart-screen">
		<div class="smart-cart-screen-block" id="main-content-cart">
			<div class="left-column-block">
				<div id="{{id: 'smartCartPanel'}}" class="smartCard-panel"></div>
				<div id="{{id: 'outOfStockPanel'}}"></div>
				<div id="{{id: 'openOffersPanel'}}" class="open-offers-panel full-screen-width-parent-selector"></div>
				<div id="{{id: 'messageSlotPanel'}}" class="message-slot-panel"></div>
				<div id="{{id: 'cartItemPanel'}}" class="cartItemPanel-hook-position"></div>
				<div id="{{id: 'biBirthdayGift'}}"></div>
				<div id="{{id: 'giftOptions'}}" class="gift-options-panel"></div>
			</div>
			<div class="screen-gutter"></div>
			<div class="right-column-block">
				<div class="cart-block"><div id="{{id: 'orderSummary'}}"></div></div>
				<div id="{{id: 'cartRewardPanel'}}" class="cart-earnings-panel"></div>
				<div id="{{id: 'spendAndEarnTrackerPanel'}}"></div>
				<div id="{{id: 'prequalPanel'}}"></div>
				<div id="{{id: 'holidayAssortmentList'}}" class="holiday-assortment-list-panel"></div>
				<div id="{{id: 'bigDataRecommendationPanel'}}"></div>
				<div id="{{id: 'saveForLater'}}" class="saveForLater-nonEmpty"></div>
			</div>
		</div>	
	</div>
</script><script id="tpl-smartCartItemTemplate" type="text/x-jsrender">
<div class="smartCardErrMsg" style="display:none;">
            <p class="smartCardErrMsgTxt"></p>
        </div>
    {{if smartCartData.showBanner }}
        {{if !smartCartData.toggleEnabled }}
            {{for tmpl = "tpl-pickUpPerkBlock" /}}
         {{else}}   
            {{if smartCartData.showError }}
                {{for tmpl = "tpl-pickUpPerkErrorBlock" /}}
            {{else}}
                {{for tmpl = "tpl-rewardBlock" /}}    
            {{/if}}
        {{/if}}    
    {{/if}}
</script>
<script id="tpl-pickUpPerkBlock" type="text/x-jsrender">
    <div class="pickup-perk-block">
        <span class="pickup-perk-header">
            <img class="saving-dollar-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/savings-launcher-line-grey@3x.png" alt="" title="" />
                Pickup Perk
        </span>
        <span class="pickup-perk-on">
            <label class="pickup-perk-switch">
                <input type="checkbox" class="pickUpCheckBox" id="{{id:'pickUpCheckBox'}}">
                    <span class="pickup-perk-slider round"></span>
            </label>
        </span>
        <p class="fulfillment-option">{{:smartCartData.storePickupLabel}}</p>
        <span class="pickup-perk-store">{{:smartCartData.storeName}}</span>
        {{if smartCartData.storeDistance}}
        <span class="pickup-perk-drive-time"><img class="pickup-perk-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/ic-car-glyph@3x.png" alt="" title="">{{:smartCartData.storeDistance}}</span>
        {{/if}}     
            <div class="pickup-hr"></div>
                <div class="cart-thumbs-container">
                    <ul class="cart-thumbs-block">
                        {{for smartCartData.toggleItemImages }}
                        <li><img class="cart-thumbs-block-img" src={{_ #data}} alt="" title=""></li>
                        {{/for}}
                    </ul>
                </div>
            </div>
    </div>        
</script>

<script id="tpl-pickUpPerkErrorBlock" type="text/x-jsrender">
    <div class="pickup-perk-block">
        <span class="pickup-perk-header">
            <img class="saving-dollar-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/savings-launcher-line-grey@3x.png" alt="" title="">
            Pickup Perk
        </span>
        <span class="pickup-perk-store">{{:smartCartData.errorLabel}}
        </span>
    </div>
</script>

<script id="tpl-rewardBlock" type="text/x-jsrender">
    <div class="pickup-perk-block pickup-perk-block-default">
        <span class="pickup-perk-header pickup-perk-header-top-padding">
            <img class="saving-dollar-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/savings-launcher-line-grey@3x.png" alt="" title="">
            Pickup Perk
        </span>
        <span class="pickup-perk-on">
            <label class="pickup-perk-switch">
                <input type="checkbox" class="pickUpCheckBox" id="{{id:'pickUpCheckBox'}}" checked>
                <span class="pickup-perk-slider round"></span>
            </label>
        </span>
    </div>
    <div class="pickup-perk-block kohls-cash-pickup-perk-block">
        <div>
            <span class="pickup-perk-text pickup-perk-header-top-padding">{{:smartCartData.emailConfirmationLabel}}
            </span>
            <p><a href={{: cncLabels.kls_smartCart_kcash_link}} class="kohls-cash-green-link" target="_blank">{{:cncLabels.kls_smartCart_more_details_text}}</a></p>
        </div>
        <span class="pickup-perk-on">
            <img class="pickup-perk-on-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash@3x.png" alt="" title="">
        </span>

    </div>
</script><script id="tpl-cartItemTemplate" type="text/x-jsrender">
<div class="tpl-showMoreOptionsList-loader loader-section" style="display: none;">
        <div class="tpl-showMoreOptionsList-loader-box">
                <img class="tpl-show-more-loader-icon" src="./../../../../../../../www.kohls.com/cnc/media/images/dgsImages/moreOptionsImages/loading@3x.png">
                <p class="tpl-show-more-loader-text">{{:cncLabels.kls_moreoption_reorganazing_label}}</p>
        </div>
    </div>

<div class="saveForLaterErrMsg">
            <p class="saveForLaterErrMsgTxt"></p>
            </div>
			
<!-- new cart item code started -->
		{{for packageTplDataArray ~cartTplData=#data}}
		{{if #data.partialInventory && #data.partialInventory.partialAvailableStatus && #data.partialInventory.cartItemId}}
		<div class="partialInventoryErrMsg">
            <p class="partialInventoryErrMsgTxt">{{:~root.cncLabels.kls_error_partialinventory_item_cart_changed}}</p>
        </div>
		{{/if}}
		{{if #data.billingAddressLtlObj && #data.billingAddressLtlObj.errorStatus}}
		<div class="ineligibleForShipErrorMsg">
            <p class="ineligibleForShipErrorMsgTxt">{{:~root.cncLabels.kls_shipping_error_cant_shipto_military}}</p>
        </div>
		{{/if}}
			<div class="cart-item-panel cart-item-panel-redesign" id="{{id:'cartItems'}}">

			{{if #data.shippingInfo.shipmentType == "EMAIL"}}				
				<div class="gift-card-block locator-email store-detail-header-card-redesign">
					<div class="div-icon-fullfillment">
						<img class="delivery-icon-img-fullfillment" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftcard-icon@3x.png" alt="email icon" title="">
					</div>
					<div class="delivery-block-text">
						<div class="pick-up-at-location-redesign">
							{{:~convertHTML(~root.cncLabels.kls_recipient_label)}} {{:#data.shippingInfo.recipientName}}, {{:#data.shippingInfo.recipientEmail}}
						</div>
						<div class="head-redesign">
							{{:~convertHTML(~root.cncLabels.kls_delivery_label)}} 
							<span class="head-detail-redesign">
								{{:~convertHTML(~root.cncLabels.kls_today_label)}}
							</span>
						</div>
					</div>						
				</div>
				<div class="cart-item-panel-hr-redesign"></div>				
			{{else}}
				<!-- Store Detail Start   -->
					{{if #data.shippingInfo.shipmentType == 'PICKUP' || #data.shippingInfo.shipmentType == 'BOSS'}}
					<div class="module-426-store-detail store-detail-header-card-redesign {{if #data.shippingInfo.shipmentType == 'PICKUP'}}locator-bopus{{else}}locator-boss{{/if}}">
						<button class="bopus-boss-block" id="{{id: 'chygi-bopus-boss'}}">
							<div class="div-icon-fullfillment">
								<img class="delivery-icon-img-fullfillment" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohlsstore-icon@3x.png" alt="store pickup" title=""></img>
							</div>						
							<div class="bopus-boss-text">
								<div class="pick-up-at pick-up-at-redesign">
									{{:~convertHTML(~root.cncLabels.kls_pick_up_upper_case_Label)}}
									<span class="pick-up-at-word-redesign">
										{{:~convertHTML(~root.cncLabels.kls_at_Label)}}
									</span>
									<span class="store-pickup-bold berry-color-font-link">
										{{if #data.shippingInfo && #data.shippingInfo.selectedBopusStore && #data.shippingInfo.selectedBopusStore.storeName !=undefined}}{{:#data.shippingInfo.selectedBopusStore.storeName}}{{/if}}
										{{:~convertHTML(~root.cncLabels.kls_store_Label)}}
									</span>									
								</div>
								<div class="head-redesign">{{if #data.shippingInfo.shipmentType == 'PICKUP'}} {{:~convertHTML(~root.cncLabels.kls_date_label)}} {{else #data.shippingInfo.shipmentType == 'BOSS'}} {{:~convertHTML(~root.cncLabels.kls_date_label)}} {{/if}} <span class="head-detail-redesign">{{:#data.shippingInfo.formattedDate}}</span> </div>
								<div class="pick-up-at-location height-unset-redesign">									
									{{if #data.shippingInfo.storeAwayTime}}
										<span class="cart-item-store-away-time">
											<img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/smartCartImages/ic-car-glyph@3x.png" alt="" title="">
											{{:#data.shippingInfo.storeAwayTime}}
										</span>
									{{/if}}
								</div>
							</div>							
						</button>
					</div>
					<div class="cart-item-panel-hr-redesign"></div>
					{{else}}
					<div class="module-426-store-detail locator-ship store-detail-header-card-redesign">
						<button class="delivery-block" id="{{id: 'chygi-ship'}}">
							<div class="div-icon-fullfillment">
								<img class="delivery-icon-img-fullfillment" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/truck-blk.svg" alt="ship to home" title=""></img>
							</div>
							<div class="delivery-block-text">								
								{{if #data.shippingInfo.zipCode}}
									<div class="pick-up-at-location-redesign">{{:~convertHTML(~root.cncLabels.kls_shipping_label)}} <span class="pick-up-at-word-redesign">{{:~convertHTML(~root.cncLabels.kls_to_label)}} </span> <span class="berry-color-font-link">{{:#data.shippingInfo.zipCode}}</span></div>
								{{/if}}
								<div class="head-redesign">{{:~convertHTML(~root.cncLabels.kls_estimated_delivery)}} <span class="head-detail-redesign">{{:#data.shippingInfo.formattedDate}}</span></div>
							</div>							
						</button>
					</div>
					<div class="cart-item-panel-hr-redesign"></div>
					{{/if}}
					{{if #data.partialInventory && #data.partialInventory.partialAvailableStatus && #data.partialInventory.cartItemId}}
					<div class="module-426-store-detail locator-ship">
						<div  class="pick-up-at-partialinventory">{{:#data.partialInventory.limitedStockStatus}}</div>
					</div>
					<div class="module-426-store-detail locator-ship">
						<div  class="partial-inventory" data-index="{{:#data.partialInventory.cartItemId}}"><a href="javascript:void(0);">{{:#data.partialInventory.changeHowToGetStatus}}</a></div>
					</div>
					{{/if}}
				<!-- Store Detail End   -->
			{{/if}}

				{{for #data.items ~packagedata=shippingInfo ~showingGWPGetItemsForId=~cartTplData.showingGWPGetItemsForId ~gwpGetItems=~cartTplData.gwpGetItems}}
                    {{if ~packagedata.shipmentType == "EMAIL" }}
                        {{for tmpl = "tpl-eGiftBlock" /}}
						<!-- Closing div below needed. It is for the div with id cartItems. -->
						</div>
                    {{else}}
                        {{if ~cartTplData.editingGiftCartItemId == #data.cartItemId}}
                            <div class="cart-item-panel-item">
                                {{for tmpl='tpl-selectFreeGift' /}}
                            </div>
                        {{else}}
                            {{for tmpl = "tpl-cartBlock" /}}
                            <div class="cart-item-panel-hr"></div>
                        {{/if}}
                    {{/if}}		
			    {{/for}}
		
			</div>
{{/for}}
<!-- Status messages for screen readers -->
<div class="quantity-status-msg sr-only" role="status" aria-live="polite" style="display:none;">Quantity updated</div>
<div class="delete-status-msg sr-only" role="status" aria-live="polite" style="display:none;">Item deleted</div>
<!-- new cart item code ended -->
</script>
<script id="tpl-eGiftBlock" type="text/x-jsrender">
	<div class="cart-item-panel-item">
		<div class="gift-card-block e-gift-card-block">
			<div class="e-gift-card-description">
				<span class="e-gift-card-img-block">
					<div>
						<a href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}"><img class="e-giftcard-img" src="{{__data.imageUrl}}" alt="{{:#data.productTitle}}"/></a>
					</div>
					<div class="gift-card-top-text-no-bold right-align">{{: #data.priceValue}}</div>
				</span>
				<span class="e-gift-card-text-block">
					<div class="e-gift-card-header">
						<a href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}">{{:~convertHTML(#data.productTitle)}}</a>
					</div>
					{{if #data && #data.vgcInfo && #data.vgcInfo.giftMessage}}
						<div class="e-gift-card-text">{{:#data.vgcInfo.giftMessage}}</div>
					{{/if}}
				</span>
			</div>
		{{if ~root.$env.ksCnCSaveForLater && #data.itemType !== 'VGC'}}
            <span class="e-gift-card-save-for-later-block">
				<div class="saveForLater" id="{{id:'saveForLater'}}" data-quantityId="{{: #data.quantity != 0  ? #data.quantity : ''}}" data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName={{:#data.storeId}}>{{:~root.cncLabels.kls_save_for_later}}</div>
			</span>
		{{/if}}
			<button class="e-gift-card-delete-block" aria-label="delete cart item" data-quantityId="{{: #data.quantity != 0  ? #data.quantity : ''}}" data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName={{:#data.storeId}}>
			<img class="delete-giftcard-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="delete icon" title="" />
			</button>
			</div>
</script>

<script id="tpl-cartBlock" type="text/x-jsrender">
	<div class="cart-item-panel-item">
		<div class="product-details-block">
			<div class="product-left-block">
				<div class="product-left-block__top">
			        {{if #data &&  (#data.isGWPGetItem || #data.itemType == "SEPHORA_GIFT")}}
                        <a class="product-img-tappable-off" href="javascript:void(0);"> <img class="product-img-style" src="{{__data.imageUrl}}" alt="{{:#data.productTitle}}"/></a>
                    {{else}}
                    <a href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}"> <img class="product-img-style" src="{{__data.imageUrl}}" alt="{{:#data.productTitle}}"/></a>
                    {{/if}}
					{{if #data.giftInfo && #data.giftInfo.isGiftSelected}}
						<img class="product-gift-icon" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/gift-box@3x.png"></img>
					{{/if}}
				</div>					
				<div class="product-footer-block-price-list">
					<p class="product-footer-block-price-list-sale-txt {{:#data.priceColorClass}}-color">{{if #data.itemType == "SEPHORA_GIFT"}}{{:~root.cncLabels.sephora_text}}{{else}}{{: #data.priceLabel}}{{/if}}</p>
					<div>      
						<span class="product-footer-block-price-list-strikethrough" style="{{if #data.StrikethroughPrice.toString().length>6 && #data.priceValue.toString().length>6}} top: 40px; text-align: right; width: 104px;{{/if}}" >{{: #data.StrikethroughPrice}}</span>
						<span class="product-footer-block-price-list-sale-price {{:#data.priceColorClass}}-color">{{if #data.itemType == "SEPHORA_GIFT"}}{{:~root.cncLabels.sephora_gift_item}}{{else}}{{: #data.priceValue}}{{/if}}</span>
					</div>
				</div>
			</div>
			<div class="product-right-block">
				{{if ~root.$env.ksEnableSephora && #data.itemType && #data.itemType.startsWith('SEPHORA')}}
					<div class="sephora-logo">
						<img src="{{v_~root.$imageRoot('cnc')}}kds-web-core/assets/sephora/tags/small-k.svg" alt="Logo Sephora" />
					</div>
				{{else #data.itemType && #data.itemType.startsWith('MARKETPLACE')}}
					<kds-badge text="marketplace" badge-type="marketplace" class="kds-marketplace-badge"></kds-badge>						
				{{/if}}
				<div class="product-name">
					<a {{if #data.itemType == 'VGC'}}"vgc_shoppingbag_title"{{/if}} href="{{if #data.productSeoURL && #data.itemType != "SEPHORA_GIFT"}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}">{{:~convertHTML(#data.productTitle)}}</a>
				</div>
				{{if ~root.$env.ksEnableSephora && #data.itemType == "SEPHORA_GIFT"}}
					<div class="bi-free-with-purchase-text">
							<div class="module-426-more-options-contextual-msg">
								Free with Purchase
							</div>
					</div>
				{{else}}
					<div class="product-context-msg">
						{{for #data.contextualMessage}}
							<div class="module-426-more-options-contextual-msg">
								{{if #data.isShipIcon}}
									<img class="module-426-more-options-contextual-msg-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/delivery-truck@3x.png" alt="" title="">
								{{/if}}
								{{: #data.message}}
							</div>
						{{/for}}
					</div>
				{{/if}}
				<div class="product-name-space15"></div>
			</div>
			<div class="product-props">
				{{if #data.size && #data.size !== 'NO SIZE'}}
					<div class="product-size-name">{{:~root.cncLabels.kls_product_size_text}} <span class="product-size-value">{{: #data.size}}</span></div>
				{{/if}}
				{{if #data.color }}
					{{if !~root.$env.ksEnableSephora }}
					<div class="product-color-name">{{:~root.cncLabels.kls_product_color_text}} <span class="product-color-value">{{: #data.color}}</span></div>
					{{else}}
						{{if #data.itemType != "SEPHORA_GIFT"}}
							<div class="product-color-name">{{:~root.cncLabels.kls_product_color_text}} <span class="product-color-value">{{: #data.color}}</span></div>
						{{/if}}
					{{/if}}
				{{/if}}	
				{{if #data.showScarcityBadge}}
					<div class="product-scarcity-div">
						<span class="product-scarcity-text">{{:~root.cncLabels.kls_static_product_scarcity_text.replace('{0}', #data.invOnHandQty)}}</span>
					</div>
				{{/if}}			
			</div>
				
		</div>


	<div class="product-footer-block">
		<div class="product-footer-gutter"></div>
		{{if #data.isGWPGetItem}}
			<div class="product-edit-icon" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName={{:#data.storeId}}><a href="javascript:void(0);"><img class="out-of-stock-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="Edit icon" title=""></a></div>
		{{else}}
			{{if ~root.$env.ksCnCSaveForLater && #data.itemType != "SEPHORA_GIFT"}}
				<button class="saveForLater" id="{{id:'saveForLater'}}" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName={{:#data.storeId}}>{{:~root.cncLabels.kls_save_for_later}}</button>
			{{/if}}			
			<button class="product-delete-icon" aria-label="delete cart item" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName={{:#data.storeId}}><img class="out-of-stock-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="Delete icon" title=""></button>
			{{if #data.itemType != "SEPHORA_GIFT"}}
				<div class="product-change-quantity">	
					{{if #data.quantity==1}}
						<button class="minus-product-disabled change-qty-btn" disabled>&ndash;</button>
					{{else}}
						<button class="minus-product change-qty-btn" aria-label="Decrease quantity" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName={{:#data.storeId}} id="{{id:'quantityDec'}}">&ndash;</button>
					{{/if}}
						<div class="no-of-product"><label  data-quantity={{: #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}}>{{:#data.quantity}}</label></div>
					{{if #data.quantityIncreaseAllowed && ~root.maxAllowedQuantity< ~root.$env.cartItemMaxQuantity}}
						<button class="add-product change-qty-btn" aria-label="Increase quantity" data-quantityId={{:quantity != 0  ? #data.quantity : #data.quantity}} data-cartItemId={{:#data.cartItemId}} data-skuId={{:#data.skuId}}  data-prdId={{:#data.productId}} data-shippingMethod={{:#data.fulfillmentType}} data-storeName="{{:#data.storeId}}">+</button>
					{{else}}
						<button class="add-product-disabled change-qty-btn" disabled>+</button>
					{{/if}}
				</div>
			{{/if}}
		{{/if}}
	</div>
	{{if #data.isGWPBuyItem  && !#data.getItemAdded}}
		<div class="smart-cart-hr"></div>
		{{if #data.cartItemId == ~showingGWPGetItemsForId}}

			{{for tmpl='tpl-selectFreeGift' /}}

		{{else}}	

			<div class="select-free-gift-block">
				<div class="free-gift-txt">Event Message: gift is available</div>
				<div class="common-green-text" id="{{id:'selectAGift'}}" data-buyItemId="{{:#data.cartItemId}}">Select a gift</div>
			</div>
		{{/if}}
	{{/if}}
    </div>
</script>

<script id="tpl-emptyCartPanel" type="text/x-jsrender">
	{{if ~root.$env.ksEnableNewEmptyCart}}
		<div class="checkout-logo-block checkout-logo-block-empty-cart" id="checkout-logo-block">
			<!-- Do not remove the div with id="header-container", as it is necessary for the search API to display suggestions in the ta-suggestion-box div  -->
			<div id="header-container">
				<div class="cart-logo-block-items cart-logo-block-empty-cart">
					<span class="header-empty-cart-title" id="header-shopping-cart">
						<span class="header-kohls-logo" id="header-kohls-logo">
							<a href="/"><img class="header-kohls-logo-img" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohl's" title=""></a>
						</span>
						<div class="header-text-checkout">{{:cncLabels.my_cart_text}}</div>
					</span>
					<span class="header-search-bar-empty-cart">
						{{if ~root.$env.ksEnableSearchInShoppingBag}}
							<form action="/search.jsp" method="get" id="site-search-bar">
								<fieldset>
									<label class="visually-hidden" for="search">Search by
										Keyword or Web ID </label> <span class="blank_tt">&nbsp;</span>
									<input type="submit" value="web-regular" class="button-submit button-search search-icon-styling-redesign" name="submit-search" >
									<input type="text" placeholder="What else are you looking for today?" class="input-text" id="search" name="search" autocomplete="off">
								</fieldset>
							</form>
						{{/if}}
					</span>
					<span class="testAndTargetSlot" id="testAndTargetSlot"></span>
				</div>
			</div>   
		</div>
		<div class="header-margin-cart header-margin-empty-cart" id="header-margin"></div>
		<div class="smart-cart-screen">
			<div class="smart-cart-screen-block">
				<div class="left-column-block">		
				<div id="outOfStockPanel">{{for tmpl='tpl-outOfStockPanelTemplate' /}} </div>	
					<div class="empty-cart-left">
						<div class="empty-cart-cta kds">
							<div><img class="emtpy-cart-img" src="{{v_~root.$env.KDSAssetsPath}}/icons/header/cart.svg" alt="Cart empty" /></div>
							<div class="empty-cart-msg body-02" role="status" aria-live="polite">{{:cncLabels.empty_cart_msg}}</div>  
							<div>
								<kds-button size="md" id="{{id:'empty-cart-continue-shop'}}" class="empty-cart-continue-shop-btn">{{:cncLabels.continue_shopping_caps}}</kds-button>
							</div>							
						</div>
					</div>
				</div>
				<div class="screen-gutter"></div>
				<div class="right-column-block">
						<div id="{{id: 'bigDataRecommendationPanel'}}" class="empty_cart_suggestion"></div>
						<div class="saveForLater-nonEmpty" id="{{id: 'saveForLater'}}"></div>
				</div>
			</div>
		</div>  
	{{else}}
		<div class="kohls-checkout-header" id = "kohls-shoppingbag-header">
			<div class="kohls-logo">
				<a href="/"><img class="kohls-logo-style" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohls" title="" /></a>
			</div>
		</div>
		<div class="smart-cart-screen">
			<div class="smart-cart-screen-block">
				<div class="left-column-block">			
					<div class="empty-cart-left-col">
						<div class="empty-cart-text" role="status" aria-live="polite">{{:cncLabels.emptyCart_Message}}</div>  
					</div>
				</div>
				<div class="screen-gutter"></div>
				<div class="right-column-block">
				
					<div class="empty-cart-right-col">
							<div class="start-shopping-btn-block"><a class="start-shopping-btn" href="/home.jsp">{{:cncLabels.emptyCart_btn_txt}}</a></div>
					</div>
				</div>
			</div>
			<div id="outOfStockPanel">
				{{for tmpl='tpl-outOfStockPanelTemplate' /}} 

			</div>
			<div class="saveForLaterEmpty" id="{{id: 'saveForLater'}}"></div>

		</div>  
	{{/if}}
</script>


<script id="tpl-selectFreeGift" type="text/x-jsrender">
	<div class="giftcard-common-block free-gift-common-block gwp-gift-common-block">
    <div class="free-gift-block-header">{{:~root.cncLabels.kls_gwp_select_gift_header}}</div>
    <div class="free-gift-block">
        {{for ~gwpGetItems }}
            <div class="gift-card-bg  {{if #data.isSelected}}free-gift-bg-active{{else}}free-gift-bg{{/if}}" {{if !#data.isSelected}}id="{{id:'gwpGetItems'}}"{{/if}} data-productId="{{:#data.productId}}" data-skuId="{{:#data.skuId}}">
            
            <span class="{{if #data.isSelected}}free-gift-select-marker-active{{else}}free-gift-select-marker{{/if}}"></span>
            {{if #data.imageURL}}
			<span>
                <a  class="product-img-tappable-off" href="javascript:void(0);"><img class="giftcard-img" src="{{__data.imageURL}}" alt="" title=""></a>
            </span>
			{{/if}}
			{{if #data.productId != -1 && #data.size && #data.size !== 'NO SIZE'}}
				<div class="product-img-size-detail">{{:#data.size}}</div>
			{{/if}}
            <span class="gift-card-saved-block">
                    <p class="gift-card-no-bold">{{:#data.displayName}}</p>
            </span>
            </div>
			{{if #data.productId != -1 && #data.size && #data.size !== 'NO SIZE'}}
			      <div class="cart-item-panel-hr"></div>
			{{/if}}
        
        {{/for}}
        </div>
    </div>
</script>
<script id="tpl-getCartFailure" type="text/x-jsrender">
<div class="kohls-checkout-header" id = "kohls-shoppingbag-header">
		<div class="kohls-logo">
			<a href="/"><img class="kohls-logo-style" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohls" title="" /></a>
		</div>
	</div>
	<div class="smart-cart-screen">
		<div class="smart-cart-failure-block">
        	<div class="left-column-block">			
				<div class="empty-cart-left-col">
					<div class="empty-cart-text">{{:cncLabels.cart_failure_Message}}</div>  
				</div>
            </div>
        </div>
    </div>  
</script><script id="tpl-orderSummaryNewTemplate" type="text/x-jsrender">
		
        <!-- Header -->
        <div class="order-summary-header">
            <div> {{:~root.cncLabels.kls_digital_receipt_order_summary}} </div>				
        </div>
		<!-- Shipping message -->
        {{if ~root.$env.pageName =='cart'}}
        <div class="free-ship-container">
            <div class="free-ship-msg">{{:freeShippingMsg}}</div>
            {{if freeShippingThreshold > 0}}
            <div class="progress-bar">
                <kds-progress-bar value="{{:freeShippingThresholdValue}}" min="0" max="{{:freeShippingThreshold}}" id="shipping-progress-bar"></kds-progress-bar>
            </div>
            {{/if}}
        </div>
        {{/if}}
		<!-- Items -->
		<div class="cart-block-item">
			<span class="cart-block-item-name">{{:(cncLabels.kls_order_sum_items)}} ({{:quantityCount}})</span>
			<span class="cart-block-item-value">{{:subtotal}}</span>
		</div>

		<!-- KC & Coupons -->
		<div class="cart-block-item">
			<span class="cart-block-item-name">{{:(cncLabels.kls_order_sum_kc)}}</span>
			<span class="cart-block-item-value">-${{:kohlsCashOffers}}</span>
		</div>

		<!-- Surcharges & Fees -->
		{{if surchargesAndFees > 0}}
			<div class="cart-block-item">
				<span class="cart-block-item-name">
					<span> {{:(cncLabels.kls_order_sum_surcharges_and_fees)}} </span>
					<img src="{{v_~root.$env.KDSAssetsPath}}/icons/price-story/info-circle-icon.svg" alt="INFO"
					id="{{id:'taxModalOpen'}}" class="info-circle-icon">
				</span>
				<span class="cart-block-item-value">${{:surchargesAndFees}}</span>
			</div>
		{{/if}}

		<!-- Shipping -->
		<div class="cart-block-item">
			<span class="cart-block-item-name">
				<span> {{:(cncLabels.kls_order_sum_shipping)}} </span>
				<img src="{{v_~root.$env.KDSAssetsPath}}/icons/price-story/info-circle-icon.svg" alt="INFO"
					id="{{id:'shippingModalOpen'}}" class="info-circle-icon">
			</span>
			{{if isEliteFreeShipping}}
				<img alt="MVC" src="{{v_~root.$resourceRoot('cnc')}}images/loyaltyV2/mvc-color.png" class="mvc-image" />
				<span class='free-shipping'>{{:(cncLabels.kls_order_sum_shipping_free)}}</span>
			{{else}}		
				<span class="cart-block-item-value">{{if shippingCharges}}${{:shippingCharges}}{{else}}<span class="free-shipping"> {{:(cncLabels.kls_order_sum_shipping_free)}}</span>{{/if}}</span>
			{{/if}}
		</div>

		<div id="shippingModal"></div>
		
		<!-- Taxes -->
        {{if isCartPage && estimated}}
            <div class="cart-block-item" style="display:none;"></div>
        {{else}}
            <div class="cart-block-item">
                <span class="cart-block-item-name">
                <span>{{if estimated}}{{:(cncLabels.kls_order_sum_estimated_tax)}}{{else}}{{:(cncLabels.kls_order_sum_tax)}}{{/if}}</span>
                <img src="{{v_~root.$env.KDSAssetsPath}}/icons/price-story/info-circle-icon.svg" alt="INFO"
                        id="{{id:'shippingModalOpen'}}" class="info-circle-icon">
                </span>			
                <span class="cart-block-item-value">${{:estimatedTax}}</span>
            </div>
        {{/if}}

		<!-- Order Total -->
		<div class="order-summary-total-hr"></div>
		<div class="cart-block-item cart-block-bold">
			<span class="cart-block-item-name">{{if isCartPage && estimated}}{{:(cncLabels.kls_pre_tax_order_total)}}{{else}}{{:(cncLabels.kls_static_order_total)}}{{/if}}</span>
			<span class="cart-block-item-value">{{if isCartPage && estimated}}${{:preTaxOrderTotal}} {{else}}${{:orderTotal}}{{/if}}</span>
		</div>

		{{if showPaymentBreakup}}
			<!-- Payment Used -->
			<div class= {{if giftcardCount > 0}} "order-summary-payment-section order-summary-multiple-payments" 
				{{else}} "order-summary-payment-section" {{/if}}>
				{{if giftcardCount > 0}}
					<div class="cart-block-item">
						{{if giftcardCount > 1}}
							<span class="cart-block-item-name">{{:(cncLabels.kls_giftcards_ordersummary)}} ({{:giftcardCount}})</span>
						{{else}}
							<span class="cart-block-item-name">{{:(cncLabels.kls_giftcard_ordersummary)}}</span>
						{{/if}}
						<span class="cart-block-item-value"> -${{:giftcardPaymentTotal}} </span>
					</div>
				{{/if}}
				{{if paymentTenderTotal > 0}}
					<div class="cart-block-payment-info">
					<span class="cart-block-payment-nowrap ctHidden">{{:cncLabels.kls_charged_to_ordersummary}}</span>
						<div class="cart-block-payment cart-block-bold">
							<span class="cart-block-payment-nowrap ctHidden">{{:paymentTenderLabel}}</span>
							<span class="cart-block-item-value"> ${{:paymentTenderTotal}} </span>
						</div>
					</div>
				{{/if}}
			</div>
		{{/if}}

		{{if savings}} 
			<!-- Savings -->
			{{if !showPaymentBreakup}}
				<div class="order-summary-total-hr"></div>
			{{/if}}
			<div class="cart-block-item cart-block-bold cart-block-savings">
				{{:(cncLabels.kls_order_sum_total_savings)}} ${{:savings}}
			</div>
		{{/if}}
        {{if ~root.$env.pageName =='cart'}}
            <kds-button title="Proceed to Checkout" class="checkout-btn" id="{{id: 'redirectUrl'}}" aria-label="Proceed to checkout" size="md" tabindex="0">{{:cncLabels.proceed_to_checkout}}</kds-button>
        {{else}}
            <kds-button title="Place Order" class= {{if isCartEligibleForPlaceOrder}} "checkout-btn" {{else}} "checkout-btn disabled-checkout-btn" {{/if}} id="{{id: 'redirectUrl'}}" aria-label="Place order" size="md" tabindex="0">{{:cncLabels.place_order_text}}</kds-button>
            <div class={{if !isCartEligibleForPlaceOrder}} "place-order-disclaimer-msg place-order-disclaimer-disable" {{else}} "place-order-disclaimer-msg" {{/if}}>{{:cncLabels.kls_place_order_disclaimer}}</div>
        {{/if}}
        {{if ~root.$env.pageName =='cart' && ~root.$env.ksPayLaterMessageInCart}}
            <div data-pp-message data-pp-placement="cart" data-pp-amount="{{v:preTaxOrderTotal}}" data-pp-style-logo-type="alternative" data-pp-style-layout="text" data-pp-style-text-align="center"></div>
        {{/if}}
        <div id="{{id: 'cartRewardPanel'}}">
		</div>
</script>

<script id="tpl_shippingModal" type="text/x-jsrender">
<div class="shippingTaxOuterWrap" id="shippingTaxTooltip">
        <div id="csimodal-container" class="kds">
            <div class="shipping-tax-container">
                {{if cartJsonData.orderSummary}}
                    <h2 class="title_background">
                        <div class="heading-06 shipping-fees-title">{{:cncLabels.kls_static_tr_checkout_ship_tax}}</div>
                        <button id="close_model_shippingTax"><img class="button_close_shippingTax" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross@3x.png" alt="Close Shipping and Tax pop-up" title=""></img></button>
                    </h2>
                    <div class="shipping-tax-hr"></div>
                {{/if}}
                <div class="csi-content shippingTax_wrap">
                    <div class="shippingTax_LHS_tr">
                        {{if #data.cartJsonData.shipmentInfo && #data.cartJsonData.shipmentInfo.length > 0}}                            
                            {{for #data.cartJsonData.shipmentInfo ~cncLabels = #data.cncLabels}}
                            {{if fulfillmentType && fulfillmentType.toLowerCase() != 'email'}}
                                <div class="shippingTax">
                                    <!--Start: Code changes for Marketplace defect MPP-550 -->
                                    {{if shippingMethod !='BOPUS' && shippingMethod !='BOSS'}}
                                        <!-- START Ship to Home section -->
                                       <div class="fleft">
                                            <div class="shippingDetailModal supporting-micro-01-medium">{{:~getShippingDetailLabelByMethod(trCheckoutCartErrors,shippingMethod,shippingMethodDescription)}}&nbsp;{{:~cncLabels.kls_ship_to_home}}</div>
                                            <div class="item-count supporting-micro-01">{{:shipmentItemsCount}}&nbsp;{{:~cncLabels.kls_static_tr_checkout_gift_items_comma_wo_kohls}}</div>
                                            {{if ~root.cartJsonData.addressDetails && ~root.cartJsonData.addressDetails.shipAddress}}
                                                {{if ~root.cartJsonData.addressDetails && ~root.cartJsonData.addressDetails.shipAddress.length > 0}}
                                                    {{if !~root.cartJsonData.isVGCOnly && !~root.cartJsonData.isOnlyVGCAddress}}
                                                            {{for ~root.cartJsonData.addressDetails.shipAddress}}
                                                                {{if selected}}
                                                                    <div class="fleft supporting-micro-01">{{:city}}</div>
                                                                {{/if}}
                                                            {{/for}}
                                                    {{/if}}
                                                {{/if}}
                                            {{/if}}
                                        </div>
                                        <!-- END Ship to Home section -->
                                    {{else}}
                                        {{if shippingMethod ==='BOPUS' || shippingMethod ==='BOSS' }}
                                        <div class="fleft">
                                            <div class="shippingDetailModal supporting-micro-01-medium">{{:~getShippingDetailLabelByMethod(trCheckoutCartErrors,shippingMethod,shippingMethodDescription)}}</div>
                                            <div class="item-count supporting-micro-01">{{:shipmentItemsCount}}&nbsp;{{:~cncLabels.kls_static_tr_checkout_gift_items_comma_wo_kohls}}</div>
                                            <div class="supporting-micro-01">{{:~getBopusStoreName(~getStoreIdOrName(shipmentId,true))}}</div>
                                        </div>
                                        {{/if}}
                                    {{/if}}
                                    <!--End: Code changes for Marketplace defect MPP-550 -->
                                    <!-- START : BOPUSV-134 -->
                                    {{if shippingMethod !='BOPUS' && shippingMethod !='BOSS' && shipmentPriceInfo}}
                                        <span class="fright supporting-micro-01">{{:~addCurrency(shipmentPriceInfo.originalShippingCharges)}}</span>
                                    {{else}}
                                     {{if shippingMethod ==='BOPUS' || shippingMethod ==='BOSS' }}
                                        <span class="fright supporting-micro-01-medium">{{:~cncLabels.kls_static_tr2_pb_free_shipping_label}}</span>
                                     {{/if}}
                                    {{/if}}
                                    <!-- END : BOPUSV-134 -->
                                    <!-- START: Code Changes for MarketPlace Defect MPP-794/ATG-6541 -->
                                        {{if shipmentPriceInfo && shipmentPriceInfo.freeShipQualificationDelta != "" && shipmentPriceInfo.freeShipQualificationDelta}}
                                            <span class="fleft supporting-micro-01-medium away-from-free-ship">{{:~addCurrency(shipmentPriceInfo.freeShipQualificationDelta)}} {{:~cncLabels.kls_static_tr2_free_ship_suggest_message_modal_window}}</span>
                                        {{/if}}
                                    <!-- END: Code Changes for MarketPlace Defect MPP-794/ATG-6541 -->
                                    {{if shippingOffers}}
                                        {{if shippingOffers.length > 0}}
                                            {{for shippingOffers}}
                                                {{if confirmationMessage}}
                                                    <div class="clear"></div>
                                                    <span class="shipping-text supporting-micro-01-medium fleft"> {{:confirmationMessage}}</span>
                                                    <span class="supporting-micro-01-medium fright">{{:~cncLabels.kls_static_checkout_minus_sign}}{{:~addCurrency(discountAmount)}}</span>
                                                {{else eliteFreeShipping && offerName}}
                                                    <div class="clear"></div>
                                                    <span class="fleft supporting-micro-01"> {{:offerName}}</span>
                                                    <span class="supporting-micro-01-medium">{{:~cncLabels.kls_static_checkout_minus_sign}}{{:~addCurrency(discountAmount)}}</span>
                                                {{/if}}
                                            {{/for}}
                                        {{/if}}
                                    {{/if}}
                                </div>
                            {{/if}}
                            {{/for}}
							{{if #data.cartJsonData.shipmentInfo && ~getAllVgcShipmentItemsCountOrTotal(shipmentInfo,'vgcItem')}}
                            <div class="shippingTax">
                                <span class="fleft shippingDetailModal supporting-micro-01">
                                    <div>{{:~root.cncLabels.kls_static_tr2_pb_vgc_email_label_shipping_tax_model}}</div>
                                    <div>{{:~getAllVgcShipmentItemsCountOrTotal(shipmentInfo,'itemsCount')}} {{:~root.cncLabels.kls_static_tr_checkout_gift_items_comma_wo_kohls}}</div>
                                    <div>{{:~getAllVgcShipmentItemsCountOrTotal(shipmentInfo,'itemsTotal')}}</div>
                                </span>
                                <span class="fright supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr2_pb_free_shipping_label}}</span>
                            </div>
							{{/if}}
                        {{/if}}                     
                        <div class="clear"></div>                            
						{{if !cartJsonData.orderSummary.taxFeeEnabled  }}
   							{{if cartJsonData.orderSummary && cartJsonData.orderSummary.totalSurcharges}}
                                <div class="clear"></div>
                                    <div class="shippingTax shippingTaxpad">
                                        <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr_checkout_gift_total_surcharges}}</span>
                                        <span class="fright supporting-micro-01-medium">{{if shipmentPriceInfo}}{{:~addCurrency(cartJsonData.orderSummary.totalSurcharges)}}{{/if}}</span>
                                    </div>
                                <div class="clear"></div>
							{{/if}}
						{{/if}}
                        {{if #data.cartJsonData.orderSummary && #data.cartJsonData.orderSummary.taxFeeEnabled }}
                            <div class="taxfeecontainer padTB0">                                        
                                {{if cartJsonData.orderSummary.totalSurcharges}}
                                    {{if cartJsonData.cartItems && cartJsonData.cartItems.length > 0}}
                                        {{for cartJsonData.cartItems}}
                                            {{if surchargePriceInfo}}
                                                {{if surchargePriceInfo.surchargeExtendedPriceAfterDiscounts}}
                                                    <div class="shippingTax marTB0 ship-surcharge">
                                                        <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr_checkout_gift_shipping_surcharges}}</span>
                                                        <span class="fright supporting-micro-01">{{:~addCurrency(surchargePriceInfo.surchargeExtendedPriceAfterDiscounts)}}</span>
                                                    </div>
                                                    <div class="clear"></div>
                                                {{/if}}
                                            {{/if}}
                                        {{/for}}
                                    {{/if}}
                                {{/if}}
                                {{if cartJsonData.cartItems && cartJsonData.cartItems.length > 0}}
                                    {{for cartJsonData.cartItems}}
                                        {{if taxFee}}
                                            {{if taxFee.length > 0}}
                                                {{for taxFee}}
                                                    {{if feeDescription}}
                                                        <div class="shippingTaxpad mtop5">
                                                            <span class="fleft supporting-micro-01-medium">{{:feeDescription}}</span>
                                                            <span class="fright supporting-micro-01">{{:~addCurrency(feeAmount)}}</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    {{/if}}
                                                {{/for}}
                                            {{/if}}
                                        {{/if}}
                                    {{/for}}
                                {{/if}}                                 
                            </div>
                            <div class="clear"></div>
                            {{if cartJsonData.orderSummary}}
                                <div class="taxfeecontainer">
                                    <div class="shippingTax">
                                        <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.kls_static_tr_checkout_sales_tax}}</span>                                        
                                        <span class="fright supporting-micro-01">{{:~addCurrency(cartJsonData.orderSummary.tax.tax)}}</span>
                                    </div>
                                </div>
                            {{/if}}
                            <div class="shippingTax shippingTaxpad marTB0">                             
                                <span class="fleft supporting-micro-01-medium">{{:~root.cncLabels.Kls_static_tr_cart_shipping_total}}</span>
                                <span class="fright supporting-micro-01-medium">{{:~addCurrency(cartJsonData.orderSummary.totalShippingAndTax)}}</span>
                            </div>
                        {{else}}
                           {{if cartJsonData.orderSummary}}
                                <div class="shippingTax shippingTaxpad">
                                    <p class="fleft supporting-micro-01-medium">{{if cartJsonData.orderSummary.tax.estimated}}{{:~root.cncLabels.kls_static_tr_checkout_gift_tax_est}}{{else}}{{:(cncLabels.kls_static_tr_checkout_gift_tax)}}{{/if}}
                                    {{if cartJsonData.orderSummary.tax.estimated}}
                                        <span>{{:cartJsonData.orderSummary.tax.taxRate}}%</span></p>
                                    {{/if}}
                                    <p class="fright supporting-micro-01-medium">{{:~addCurrency(cartJsonData.orderSummary.tax.tax)}}</p>
                                </div>
                            {{/if}}
                        {{/if}}
                    </div>

                     <div class="fleft shippingTax_RHS_tr">
                        <div class="shipping-tax-hr"></div>
                        <div class="shippingTax">
                            {{if cartJsonData.orderSummary}}
                                {{if cartJsonData.orderSummary.taxFeeEnabled}}
                                    <p class="fleft supporting-micro-01-medium mtop10 mbot_10">{{:~convertHTML(cncLabels.kls_static_tr_checkout_how_ship_rates_calculated)}}</p>
                                {{/if}}
                            {{/if}}
                            <p class="fleft supporting-micro-01">{{:(cncLabels.kls_static_tr_checkout_shipping_ratesV2)}}</p>                          
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</script>


<script id="tpl-actionPanel-template" type="text/x-jsrender">
<!-- This file historically held coupons panel and proceed to checkout + place order functionality for desktop - now it is home for the CNC header and the primary CTA button for mobile screens -->
    <div id="remedy_challenge_container" class="display-none"></div> 
    <div class="checkout-logo-block checkout-logo-block-new" id="checkout-logo-block">
        {{if isCheckoutPage}} 
            <span class="checkout-logo-block-items checkout-logo-block-items-new">
                <div class="header-kohls-logo" id="header-kohls-logo"><a href="/"><img class="header-kohls-logo-img" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohl's" /></a></div>
                <div class="header-text-checkout" id="header-checkout-text">{{:cncLabels.checkout_text_on_shopping_cart_page}}</div>
            </span>
    </div>
    <div class="return-to-cart-button" id="header-shopping-cart"><a href="/checkout/shopping_cart.jsp"><span><img class="return-to-cart-arrow" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title=""><span class="return-to-cart-text">{{:cncLabels.return_to_cart}}</span></span></a></div>
        {{else}}
            <!-- Do not remove the div with id="header-container", as it is necessary for the search API to display suggestions in the ta-suggestion-box div  -->
            <div id="header-container">
                <div class="cart-logo-block-items cart-logo-block">
                    <span class="header-shopping-cart-title" id="header-shopping-cart">
                        <span class="header-kohls-logo" id="header-kohls-logo">
                            <a href="/"><img class="header-kohls-logo-img" src="{{v_~root.$env.KDSAssetsPath}}/kohls/brushstroke-logos/kohls-logo-berry-brush.svg" alt="Kohl's" title=""></a>
                        </span>
                        <div class="header-text-checkout">{{:cncLabels.my_cart_text}}</div>
                    </span>
                    <span class="header-search-bar-small">
                        {{if ~root.$env.ksEnableSearchInShoppingBag}}
                            <form action="/search.jsp" method="get" id="site-search-bar">
                                <fieldset>
                                    <label class="visually-hidden" for="search">Search by
                                        Keyword or Web ID </label> <span class="blank_tt">&nbsp;</span>
                                    <input type="submit" value="web-regular" class="button-submit button-search" name="submit-search" >
                                    <input type="text" placeholder="What else are you looking for today?" class="input-text" id="search" name="search" autocomplete="off">
                                </fieldset>
                            </form>
                        {{/if}}
                    </span>
                    <span class="testAndTargetSlot" id="testAndTargetSlot"></span>
                </div>
            </div>   
    </div>
        {{/if}}

    <div class="action-panel-container" id="left-nav-sticky">
        <div class= {{if isCheckoutPage}} "header-margin-checkout" {{else}} "header-margin-cart" {{/if}} id="header-margin"></div>
        <div class="action-panel-placeorder-error placeorder-error-header">
                <p class="action-panel-placeorder-error-msg"></p>
        </div>
        <div class="action-panel-mobile" id="action-panel-expand">
            {{if isCheckoutPage}}
                <div class="order-disclaimer-panel order-disclaimer-panel-hide-drawer">
                    <kds-button title="Place Order" class= {{if isCartEligibleForPlaceOrder}}"checkout-button" {{else}} "checkout-button disabled-checkout-btn" {{/if}} id="{{id: 'redirectionUrl'}}" aria-label="Place order" size="md" tabindex="0" >{{:cncLabels.place_order_text}}</kds-button>
                    <div class={{if !isCartEligibleForPlaceOrder}} "place-order-disclaimer-new place-order-disclaimer-disable" {{else}}"place-order-disclaimer-new" {{/if}}>{{:cncLabels.kls_place_order_disclaimer}}</div>
                </div>
            {{else}}
                <div class="action-panel-block-dactive-chkbtn action-panel-chkbtn-hide-drawer">
                    <kds-button title="Proceed to Checkout" class="checkout-button" id="{{id: 'redirectionUrl'}}" aria-label="Proceed to checkout" size="md" tabindex="0">{{:cncLabels.proceed_to_checkout}}</kds-button>
                </div>
            {{/if}}
        </div>
    </div>
</script>


<script id="tpl-actionPanel-peek-offer" type="text/x-jsrender">

                <span class="peek-icon"><img class="peek-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/user-profile.png" alt="" title="" /></span>
                <span class="action-panel-offer-unlocked">Offer unlocked! <br/> <span class="action-panel-open-for-details"> Open for details</span></span>
                              
   
</script>


<script id="tpl-giftOptionsTemplate" type="text/x-jsrender">
{{if #data.showGiftOptionsPanel}}
    {{if #data.state =="showGiftModule"}}
        {{for tmpl="tpl-showGiftModule"/}}
    {{else #data.state =="showCartLevelMsgOptions"}}
        {{for tmpl="tpl-giftMessage"/}}
    {{else #data.state =="moreOptions"}}
        {{for tmpl="tpl-giftItemLevelOptions"/}}
    {{else #data.state =="showGiftMessage"}}
        {{for tmpl="tpl-showCartGiftMessage" ~itemLevel=false/}}
    {{else #data.state =="multipleGifts"}}
        {{for tmpl="tpl-showMultiGiftMsg"/}}
    {{else #data.state == 'edit'}}
        {{for tmpl="tpl-giftingPanelEdit" ~itemLevel=false  /}}
    {{else #data.state == 'custom'}}
        {{for tmpl="tpl-giftingPanelEdit" ~itemLevel=false  /}}
    {{/if}}
{{/if}}
</script>

<script id="tpl-showGiftModule" type="text/x-jsrender">
    <div class="kohls-gifting-panel">
        <div class="gift-block">
            <span class="gift-block-style">
                <img class="gift-block-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/gifting@3x.png" alt="Gift these items" title="">
            </span>
                <button class="gift-block-text" id="{{id:'text-block'}}">
                    <p class="gift-head">{{:cncLabels.giftOption_cartLevel_heading}}</p>
                    <p class="gift-text">{{:cncLabels.giftOption_cartLevel_subheading}}</p>
                </button>       
            </div>
        </div>
    </div>
</script>

<script id="tpl-giftMessage" type="text/x-jsrender">
    <div class="kohls-gifting-panel">
            <div class="gifting-options-block">
                <div class="gifting-options-right-block gifting-no-border">
                    {{if valueList.error}}
                        <div class="gifting-error">
                            <div class="gifting-error-message">
                                {{:valueList.error}}
                            </div>
                        </div>
                    {{/if}}
                    <div class="gift-header">{{:cncLabels.giftOption_cartLevel_heading}}</div>
                    <div class="gift-msg-optional">{{:cncLabels.giftOption_giftMessage_label}}</div>
                    <div class="gift-options" id="{{id:'gift-options'}}">
                        {{for valueList}}
                            <span class="option-box" >{{:#data}}</span>
                        {{/for}}
                        <span class="option-box-custom">{{:cncLabels.Kls_gift_message_custom}}</span>
                    </div>
                    {{if $env.ksCncItemLevelGiftingEnabled}}
                        <div class="gifting-options-hr-full"></div>
                        <p class="more-gift-options"><a href="javascript:void(0)" id="{{id:'more-gift-options'}}" class="gift-options-green-link">{{:cncLabels.giftOption_moreGiftOption_label}}</a></p>
                    {{/if}}
                </div>
             </div>
    </div>
</script>

<script id="tpl-giftingPanelEdit" type="text/x-jsrender">
    <div class="gifting-options-block ">
        <div class="gifting-options-right-block gifting-no-border validateInline" id="GiftOptionForm">
            <div class="gift-header">{{:cncLabels.giftOption_cartLevel_heading}}</div>
                <div id="gift-option-error"></div>
            <button class="gift-delete-icon" id="{{id:'delete-edit-message'}}" aria-label="Delete gift message">
                <img class="gift-delete-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/trash-icon.png" alt="Delete" title="" />
            </button>
            <label for="gift-message" class="gift-msg-optional gift-option-message-cart-level">{{:cncLabels.giftOption_editForm_message_label}}</label>
            <div class="gift-option-padding-cart-level gifting-form-group"  id="{{id:'message-char-count'}}">
                <input type="text" name="message" id="gift-message" minlength="2" class="gifting-form-input required text-input-name text-keyup"  maxlength="100" value="{{:giftInfo.giftMessage}}">
                <label id="messageCharCount" for="message" class="gifting-form-label float-right">{{:giftInfo && giftInfo.giftMessage && giftInfo.giftMessage.length ? giftInfo.giftMessage.length: 0 }}/100</label>    
                <div class="error-message-contact"></div>
            </div>
            <span class="gifting-msg-from">{{:cncLabels.giftOption_editForm_from_label}}</span>
            <div class="gift-option-padding-cart-level gifting-form-group">
                <input id="{{id:'giftFromCharCount'}}" type="text" name="giftFrom" class="gifting-form-input" aria-label="From" maxlength="60" value="{{:giftInfo.giftFrom}}">
                <div class="error-message-contact"></div>
            </div>
            <div class="gifting-btn-block">
                <span data-cartitemid="{{:cartItemId}}"><a class="gifting-done-btn" id="{{id:'gifting-done-btn'}}" href="javascript:void(0)">{{:cncLabels.giftOption_editForm_doneBtn_text}}</a><a id="{{id:'gifting-cancel-btn'}}" class="gifting-cancel-btn" href="javascript:void(0)">{{:cncLabels.giftOption_editForm_cancelBtn_text}}</a></span>
            </div>
            </div>
        </div>
    </div>
</script>
<script id="tpl-giftItemLevelOptions" type="text/x-jsrender">
    <div class="gifting-options-block" id="{{id:'item-gift-options'}}"> 
    <div class="validateInline" id="GiftOptionForm"> 
    <div class="gift-options-header">
        <span class="gift-options-header-icon" id="{{id:'more-options-back-btn'}}"><a href="javascript:void(0)"><img class="gift-options-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/left@3x.png" alt="" title=""></a></span>
        <span class="gift-options-header-back"><a href="javascript:void(0)" id="{{id:'more-options-back-btn'}}">{{:cncLabels.giftOption_back_label}}</a></span>
        <span class="gift-options-header-findstore">{{:cncLabels.giftOption_moreGiftOption_label}}</span>
    </div>
    <br />
    <div class="gift-options-hr"></div>
    {{for giftCartItems ~cncLabels=cncLabels ~itemid=giftCartItems.editItemId}}              
            <div class="gifting-options-product-block" id={{:cartItemId}}>
                <div class="gifting-options-left-block">
                    <div class="gifting-options-product-img"><img class="gifting-options-product-img-style" src={{_itemProperties.image.url}} alt="" title=""></div>
                </div>
                <div class="gifting-options-right-block itemLevel-gift-options"  id="{{:cartItemId}}_gift">
                    {{if ~itemid && cartItemId == ~itemid}}
                        <div class="gift-header">{{:~root.cncLabels.giftOption_cartLevel_heading}}</div>
                        <div id="gift-option-error"></div>
                        <span class="gift-delete-icon" id="{{id:'delete-edit-message'}}">
                        <a href="javascript:void(0);"><img class="gift-delete-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/trash-icon.png" alt="" title="" /></a>
                        </span>
                        <div class="gift-msg-optional gift-option-message-cart-level">{{:~root.cncLabels.giftOption_editForm_message_label}}</div>
                        <div class="gift-option-padding-cart-level gifting-form-group width-auto" id="{{id:'message-char-count'}}">
                            <input  type="text" minlength="2" name="message" class="gifting-form-input itemLevel-input-field required text-input-name text-keyup"  maxlength="100" value="{{:giftInfo.giftMessage}}">
                            <label id="messageCharCount" for="message" class="gifting-form-label float-right" style="margin-left: 0px;">{{:giftInfo && giftInfo.giftMessage && giftInfo.giftMessage.length ? giftInfo.giftMessage.length: 0 }}/100</label>
                            <div class="error-message-contact" id="error-message_from_{{:cartItemId}}"></div>
                        </div>
                        <span class="gifting-msg-from">{{:~root.cncLabels.giftOption_editForm_from_label}}</span>
                        <div class="gift-option-padding-cart-level gifting-form-group">
                            <input id="{{id:'giftFromCharCount'}}" type="text" name="giftFrom" class="gifting-form-input itemLevel-input-field"  maxlength="60" value="{{:giftInfo.giftFrom}}">
                            <label style="display:none;" for="message" class="gifting-form-label"></label>
                            <div class="error-message-contact" id="error-message_from_{{:cartItemId}}"></div>
                        </div>
                        <div class="gifting-btn-block">
                            <span data-cartitemid="{{:cartItemId}}"><a class="gifting-done-btn" id="{{id:'gifting-done-btn'}}" href="javascript:void(0)">{{:~root.cncLabels.giftOption_editForm_doneBtn_text}}</a><a id="{{id:'gifting-cancel-btn'}}" class="gifting-cancel-btn" href="javascript:void(0)">{{:~root.cncLabels.giftOption_editForm_cancelBtn_text}}</a></span>
                        </div>
                        </div>
                    
                    {{else}}
                        <div class="gift-header">{{:~cncLabels.giftOption_itemLevel_message_label}}</div>
                        {{if giftInfo && giftInfo.giftMessage && giftInfo.giftMessage.length}}
                        <div class="gifting-message-edit-block"  id="{{id:'edit-cart-msg'}}" data-cartItemId="{{:cartItemId}}">
                            <div class="edit-gift-msg">
                            <a href="javascript:void(0);"><img class="edit-gift-msg-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="" title="" ></a> </div>
                            <div class="gifting-msg">{{:giftInfo.giftMessage}}<br>
                            {{if giftInfo.giftFrom && giftInfo.giftFrom.length}}{{:~cncLabels.kls_static_gift_from_text}} {{:giftInfo.giftFrom}}{{/if}}</div>
                        </div>
                        {{else}}
                            <div class="gift-options itemLevel-gift-options">
                                {{for ~root.valueList}}
                                    <span class="option-box gift-unselected-msg">{{:#data}}</span>
                                {{/for}}
                                <span class="option-box-custom">{{:~cncLabels.Kls_gift_message_custom}}</span>
                            </div>
                        {{/if}}
                        </div>
                    {{/if}}

                </div>
        {{/for}}
    </div>
    </div>
</script>
<script id="tpl-showCartGiftMessage" type="text/x-jsrender">
<div class="kohls-gifting-panel">
    <div class="gifting-options-block">
        <div class="gifting-options-right-block gifting-no-border">
            <div class="gift-header">{{:cncLabels.giftOption_cartLevel_heading}}</div>
            <div class="edit-gift-msg">
                <button id="{{id:'edit-cart-msg'}}" aria-label="Edit gift message"><img class="edit-gift-msg-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/pencil@3x.png" alt="Edit" title="" {{if ~itemLevel}} data-cartItemId="{{:cartItemId}}"{{/if}}></button></div>
                <div class="gifting-msg gifting-msg-cursor" id="{{id:'edit-cart-msg'}}">
                    {{if giftInfo}}
                        {{:giftInfo.giftMessage}}
                        <br />
                        {{if giftInfo.giftFrom && giftInfo.giftFrom.length}}{{:cncLabels.kls_static_gift_from_text}} {{:giftInfo.giftFrom}}{{/if}}
                    {{/if}}
                </div>
                {{if $env.ksCncItemLevelGiftingEnabled}}
                    <div class="gifting-options-hr-full"></div>
                    <p class="more-gift-options"><a href="javascript:void(0)" id="{{id:'more-gift-options'}}" class="gift-options-green-link">{{:cncLabels.giftOption_moreGiftOption_label}}</a></p>
                {{/if}}
        </div>
    </div> 
</div>
</script>

<script id="tpl-showMultiGiftMsg" type="text/x-jsrender">
<div class="gifting-options-block">       
    <div class="gifting-options-right-block gifting-no-border">
            <div class="gift-header multiMsg-head">{{:giftInfo.multiGiftMsg}}</div>
            <div class="gifting-options-hr-full"></div> 
            <p class="more-gift-options multiMsg-option"><a href="javascript:void(0)" id="{{id:'more-gift-options'}}" class="gift-options-green-link">{{:cncLabels.giftOption_moreGiftOption_label}}</a></p>        
    </div>
</div>
</script><script id="tpl-prequalTemplate" type="text/x-jsrender">
    {{if $env.ksCnCPrequalInCart && $env.displayPreQualContent && ($env.hasKCC !== 'true' || $env.hasKCC !== true) }}
        <a href={{:cncLabels.prequal_inquiry_link}} style="text-decoration: none;">
            <div class="kohls-charge-block">
                <span class="kohls-charge-block-style"><img class="kohls-charge-block-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/kohls-charge-card-no-padding-flat.png" alt="" title=""></span>
                <span class="kohls-charge-block-text">
                    <p class="kohls-charge-head">{{:cncLabels.prequal_save_more_text}}</p>
                    <p class="kohls-charge-text1">{{:cncLabels.prequal_prequalify_text1}}</p>
                    <p class="kohls-charge-text2">{{:cncLabels.prequal_prequalify_text2}}</p>
                </span>
            </div>
        </a>
    {{/if}}
</script><script id="tpl-outOfStockPanelTemplate" type="text/x-jsrender">
    {{if cartOOSItems && cartOOSItems.length > 0}}
        <div class="out-of-stock-block">
            <div class="out-of-stock-header">
                <span class="out-of-stock-header-text">{{:cncLabels.kls_out_of_stock_heading}}</span>
                <span class="out-of-stock-header-icon out-of-stock-delete-icon" id="{{id: 'out-of-stock-delete-icon'}}"><a href="javascript:void(0);"><img class="out-of-stock-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="Delete out of stock items" title=""></a></span>
            </div>
            <div class="oos-cart-thumbs-container">
                <ul class="oos-cart-thumbs-block">
                    {{for cartOOSItems}}
                        <li>
                        <a href="{{:itemProperties.productSeoURL}}" id="{{id: 'out-of-stock-product-url'}}" data-prdId="{{:#data.productId}}" data-skuId="{{:#data.skuId}}"><img class="oos-cart-thumbs-block-img"  src="{{_itemProperties.image.url}}" alt="" title=""></a></li>
                    {{/for}}
                </ul>
            </div>
            <!--  <div><a class="oos-more-color" href="javascript:void(0);">More Colors</a></div> -->
        </div>
    {{/if}}
</script><script id="tpl-spendAndEarnTrackerPanelTemplate" type="text/x-jsrender">
    {{if spendtracker || kohlsRewardTracker }}
        <div class="kohls-rewards-block">
            {{if spendtracker}}
                {{if spendtracker.showKohlsCashtracker}}
                    <div class="spendaway_tracker_progbar_container">
                        <div class="Meter-Background">
                            <div class="Meter-Fill" style = "{{:spendtracker.widthforProgressBar}}"></div>
                            <div class="pointerright"></div>
                        </div>
                        <div class="spendawayTracker_progbar_text" style="margin-bottom:0px!important">{{:spendtracker.loyaltyMessage}}</div>
                    </div>                    
                {{else}}
                    <div class="spendaway_tracker_container">
                        <div class="spendawayTracker_text_genericVersionDiv">
                            <span class="spendawayTracker_text_genericVersion">{{:spendtracker.loyaltyMessage}}</span>
                            <span>
                                <img class="spendTracker_RewardsImg spendTrackerRewardsImg" src="{{v_~root.$resourceRoot('cnc')}}images/kohlcash.png" alt="Kohls Rewards" />
                            </span>
                        </div>
                    </div>
                {{/if}}
            {{/if}}
            {{if kohlsRewardTracker}}
                {{if kohlsRewardTracker.showProgressBar}}                    
                    {{!--/** Tracker in case of kohl cash */ --}}
                    <div class="spendaway_tracker_progbar_container">
                        <div class="Meter-Background">
                            <div class="Meter-Fill" style = "{{kohlsRewardTracker.widthforProgressBar}}" ></div>
                            <div class="pointerright"></div>
                        </div> 
                        <div class="spendawayTracker_progbar_text" style="margin-bottom:0px!important">
                            {{:kohlsRewardTracker.loyaltyMessage}}
                        </div>                        
                    </div>
                {{else}}
                    <div class="spendaway_tracker_container">
                        <span class="spendawayTracker_text">{{:kohlsRewardTracker.loyaltyMessage}}</span>
                        <span>
                            <img class="spendTracker_RewardsImg" src="{{v_~root.$resourceRoot('cnc')}}images/kohlcash.png" alt="Kohls Rewards" />
                        </span>
                    </div>
                {{/if}}
            {{/if}}
        </div>
    {{/if}}
</script><script id="tpl-cartRewardPanel-template" type="text/x-jsrender">
<!-- Kohls reward section started -->

<div class="earning-section {{if ~root.$env.ksEnableSephora && (kccValueLabel.length > 0 || kohlsRewardValueLabel.length > 0 || eligibleForBI) }} earnings-block  {{/if}}">

{{if kccValueLabel.length > 0 || kohlsRewardValueLabel.length > 0 || (~root.$env.ksEnableSephora && eligibleForBI)}}
    {{if !~root.$env.ksEnableSephora}}
        <div class="earning-section-hr"></div>
        <p class="earning-text">{{:cncLabels.earning_your_earnings_label}}</p>
    {{else}}
        <p class="earning-text-redesign">{{:cncLabels.earning_your_earnings_label_reDesign}}</p>
        <div class="earning-section-checkout"></div>
    {{/if}}
{{/if}}

<!-- Kohls Cash section started -->
{{if kccValueLabel.length > 0 }}
        {{if ~root.$env.ksEnableSephora}} <!-- Kohls Cash section REDESIGN -->
            <div class="kohls-cash-container">
                <img class="kohls-cash-image" src="{{v_~root.$resourceRoot('cnc')}}images/kohlcash.png" alt="Kohls Cash" />
                <span class="kohls-cash-amount-redesign">{{: kccValueStr}}</span><br />
                <div class="">
                    <span class="earnings-row-right-top">{{: kccValueLabel}}</span><br />
                    <span class="earnings-row-right-bottom">{{: kccDescLabel}}</span>
                </div>
            </div>
        {{else}}
            <div class="kohls-cash">
                <p><span class="kohls-cash-amount">{{: kccValueLabel}}</span><br />
                <span class="kohls-cash-date">{{: kccDescLabel}}</span></p>
            </div>
        {{/if}}

{{/if}}
<!-- Kohls Cash section Ended -->

<!-- Kohls Reward section started -->


{{if kohlsRewardValueLabel.length > 0}}
   
    {{if ~root.$env.ksEnableSephora}} <!-- Kohls Reward section REDESIGN -->
        <div class="kohls-rewards-container">
            <img class="kohls-rewards-image" src="{{v_~root.$resourceRoot('cnc')}}images/rewards-green-big.png" alt="Kohls Rewards" />
            <span class="kohls-rewards-amount">{{: kohlsRewardAmount}}</span><br />
            <div class="">
                    <span class="earnings-row-right-top">{{: kohlsRewardValueLabel}}</span><br />
                    <span class="earnings-row-right-bottom">{{: kohlsRewardDescLabel}}</span>
            </div>
        </div>
    {{else}}
        <div class="kohls-reward">
        {{if showYes2YouRewardDetailLink}}
        <p><span class="kohls-reward-amount-pink">{{: kohlsRewardValueLabel}}
        {{else}}
        <p><span class="kohls-reward-amount"> {{: kohlsRewardValueLabel}}
        {{/if}}	
        </span><br /><span class="added-balance-text">{{: kohlsRewardDescLabel}}</span></p>
        </div>
    {{/if}}
{{else loyaltyDownMessage}}
    
    {{if ~root.$env.ksEnableSephora}} <!-- Kohls loyalty down REDESIGN -->
        <div class="kohls-rewards-container">
            <img class="kohls-rewards-image-loyaltyDown" src="{{v_~root.$resourceRoot('cnc')}}images/rewards-green-big.png" alt="Kohls Rewards" />
            {{if kccValueLabel.length <= 0 && kohlsRewardValueLabel.length <= 0}}
                    <div class="earning-section-hr"></div>
            {{/if}}
            <div class = "kohls-reward-message-redesign">{{:~convertHTML(cncLabels.kls_loyalty_system_down_label)}}</div>
        </div>
    {{else}}
        {{if kccValueLabel.length <= 0 && kohlsRewardValueLabel.length <= 0}}
            <div class="earning-section-hr"></div>
        {{/if}}
        <div class = "kohls-reward-message">{{:~convertHTML(cncLabels.kls_loyalty_system_down_label)}}</div>
    {{/if}}
{{/if}}

<!-- Old Rewards Detail-->
{{if !~root.$env.ksEnableSephora}}
    {{if showYes2YouRewardDetailLink}}
    <div class="kohls-reward-details">
        <a href= {{: cncLabels.kls_rewards_feature_link}} class="yes2youDetailsLink" target="_blank">  {{: cncLabels.earning_kohls_yes2you_reward_detail_link_label}} </a>
        </div>
    {{/if}}

    {{if showRewardDetailLink}} 
        <div class="kohls-reward-details">
        <button type="button" id="{{id:'reward-detail'}}" data-status="{{if isShowingKohlsRewardDetail}}hidePopUp{{/if}}" >{{if isShowingKohlsRewardDetail}}{{:cncLabels.earning__hide_kohls_reward_detail_label}}{{else}}{{:cncLabels.earning_kohls_reward_detail_label}}{{/if}}</button>
        </div>
    {{/if}}

    {{if isShowingKohlsRewardDetail}}
        {{for tmpl="tpl-cartRewardPanelDetail" /}} 
    {{/if}}
{{/if}}

<!-- Sephora Section Start -->
{{if ~root.$env.ksEnableSephora && eligibleForBI}}

    <div class="earning-sephora-container ">
        <img class="earnings-sephora-image" 
        src="{{v_~root.$imageRoot('cnc')}}kds-web-core/assets/sephora/logos/logo-sephora-beauty-insider-stacked-k.svg"
        alt="Sephora Beauty Insider" />
        <p class="earnings-sephora-message">{{:cncLabels.earning_sephora_message}}</p>
    </div>
{{/if}}


<!-- New Reward Details Button -->
{{if ~root.$env.ksEnableSephora && showRewardDetailLink}}
<div class="earning-section-shoppingBag"></div>
    <div class="kohls-reward-details-new">
        <div><span class="kr_earn_text_bottom">{{: cncLabels.earning_reward_detail_description}}  <a href="/feature/kohls-rewards.jsp" target="_blank"
            class="detailsLink">{{: cncLabels.kls_earning_kohls_reward_link}}</a></span>
        </div>
    </div>
{{/if}}
</div>

<!-- Kohls reward section ended -->

</script>



<script id="tpl-cartRewardPanelDetail" type="text/x-jsrender">
<div class="earning-section-hr"></div>

<!-- kohls reward details block started -->
<div class="earn_details_popup">
    <img class="kohlsRewardsImg_popup" src="{{_$env.resourceRoot}}images/dgsImages/rewards-lockup-d-green@2x.png"
        alt="Kohls Rewards">
    <div class="earn_details_popup_content">{{:rewardDetailMessage}}
            <div class="popup_content_separator">
            </div>
            <div><span class="popup_earn_text_bottom">{{: cncLabels.earning_reward_detail_description}}<br><u><a href="https://www.kohls.com/feature/kohls-rewards.jsp" target="_blank"
                class="detailsLink">{{: cncLabels.earning_kohls_reward_link}}</a></u></span>
            </div>
        </span>
    </div>
    <div id="kohlsRewards_WcsContent" class="popup_WcsContent"> <object type="text/html"
            data="{{:cncLabels.earning_kohls_reward_link_url}}" class="wcsContent_object"></object>
    </div>
</div>
<!-- kohls reward details block ended -->
</script><script id="tpl-showMoreOptionsListModule" type="text/x-jsrender">
   
    {{if shipItFasterStatus.showMoreOptionList}}
    <div id="chygi-modal" class="chygi-modal">
        <div class="module-426-more-options-block" id="{{id:'update-shipment-Id'}}">
        
            <div class="module-426-more-options-header font-normal-black">
                <button id="{{id:'back-more-option-panel'}}" aria-label="Return to Cart">
                    <div class="module-426-more-options-return" id="more-options-return-div-id">
                        <span class="module-426-more-options-header-icon">
                            <img class="module-426-more-options-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title="">
                        </span>
                        <span class="return-text font-gotham-4r font-15"> Return to Cart </span>
                    </div>
                    <img class="chygi-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
                </button>
                <div class="module-426-more-options-hr header-bottom-hr"></div>
                <div class="module-426-more-options-header-findstore pickup-shipping-options font-gotham-5r">
                    <span>{{:cncLabels.kls_pickup_and_shipping_options}}</span>
            </div>
            </div>
            {{if ~root.cartJsonData.newStore}}<div class="module-426-more-options-name">{{:cncLabels.kls_moreoption_based_on}} <span class="module-426-more-options-name-green" id="{{id:'check-other-store'}}">{{:~root.cartJsonData.newStore.defaultStoreName}}, {{:~root.cartJsonData.newStore.storeState}}</span></div>
            {{else ~root.cartJsonData.defaultStoreName}}<div class="module-426-more-options-name">{{:cncLabels.kls_moreoption_based_on}} <span class="module-426-more-options-name-green" id="{{id:'check-other-store'}}">{{:~root.cartJsonData.defaultStoreName}}, {{:~root.cartJsonData.storeState}}</span></div>{{/if}}
            {{if cartJsonData.moreOptionData && cartJsonData.moreOptionData.itemList && cartJsonData.moreOptionData.itemList.length}}
            {{for cartJsonData.moreOptionData.itemList ~cncLabels=cncLabels}}
            <div class="module-426-more-options-product-block">
                    <div class="module-426-more-options-left-block">
                        <div class="module-426-more-options-product-img">
                            <span><a href="{{if productSeoURL}}{{:productSeoURL}}{{else}}javascript:void(0);{{/if}}"><img class="module-426-more-options-product-img-style" src="{{_productImage}}" alt="" title=""></a></span>
                            <span class="module-426-product-count">{{:~cncLabels.Kls_moreoptions_quantity_text}}{{:productQuantity}}</span>                                            
                        </div>
                        {{for contextualMessage}}
                        <div class="module-426-more-options-contextual-msg">
                        {{if isShipIcon}}
                            <img class="module-426-more-options-contextual-msg-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/delivery-truck@3x.png" alt="" title="">
                        {{/if}}
                            {{:message}}
                        </div>
                        {{/for}}
                    </div>
                    <div class="module-426-store-detail-block" > 
                    {{if ~root.cartJsonData.noLongerAvailablePickUp}}
                    {{if methodSearchData && methodSearchData.dateText }}
                            <div {{if methodSearchData.storeDetails && methodSearchData.storeDetails.storeName !=undefined}} data-storeId={{:methodSearchData.storeDetails.storeId}} {{/if}}  {{if methodSearchData.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail update-shipment" data-skuId="{{:skuId}}" data-cartItemId="{{:cartItemId}}" data-productId="{{:productId}}" data-quantity="{{:productQuantity}}" data-methodCode="{{:methodSearchData.methodCode}}" {{/if}}>
                                <div {{if methodSearchData.isSelected}} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker"  {{/if}}></div>
                                <div {{if methodSearchData.isSelected}} class="module-426-store-detail-wrap" {{else}} class="module-426-store-detail-wrap module-426-store-cursor" {{/if}}>
                                        <div class="store-icon">
                                        {{if methodSearchData.isSelected}} 
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-store@3x.png" alt="" title="">
                                            {{else}}
                                            <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store@3x.png" alt="" title="">
                                            {{/if}} 
                                            </div>
                                        <div class="head">{{:methodSearchData.dateText}}</div>
                                        {{if methodSearchData.storeDetails && methodSearchData.storeDetails.storeName !=undefined}}
                                        <span class="store-pickup">{{:~cncLabels.kls_pick_up_Label}} </span><span class="store-pickup-bold">{{:methodSearchData.storeDetails.storeName}}</span>
                                        {{/if}}
                                        <div>
                                        {{if methodSearchData.storeDetails && methodSearchData.storeDetails.storeName !=undefined}}
                                                <span class="store-name">{{:methodSearchData.storeDetails.storeName}}, {{:methodSearchData.storeDetails.state}}</span>
                                        {{/if}}		
                                        {{if methodPickup.awayTime}}
                                                <span class="store-drive-time"><img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-car-glyph@3x.png" alt="" title="">{{:methodPickup.awayTime}}</span>
                                        {{/if}}        
                                        </div>
                                </div>  
                            </div>
                        {{else}}    
                        {{!--/* <div class="module-426-store-detail">
                                <div class="module-426-select-store-marker-disable"></div>
                                    <div class="module-426-store-detail-wrap">
                                        <div class="store-not-available no-longer-available"><p>{{:~root.cncLabels.kls_moreoption_no_longer_available_topick_up}}</p></div>
                                </div>                      
                            </div> */--}}
                            <div class="module-426-store-detail">
                                <div class="module-426-select-store-marker-disable"></div>
                                    <div class="module-426-store-detail-wrap">
                                        <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store-icon.png" alt="" title=""></div>
                                        <div class="store-not-available">{{:~root.cncLabels.kls_moreoption_not_available_to_pickup}}</div>
                                        {{if ~root.cartJsonData.newStore.defaultStoreName}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_moreoption_near}}</span><span class="store-pickup-bold">{{:~root.cartJsonData.newStore.defaultStoreName}}</span>
                                        {{else}}
                                        {{if ~root.cartJsonData.defaultFavStoreZip}} 
                                        <span class="store-pickup">{{:~root.cncLabels.kls_moreoption_near}}</span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultFavStoreZip}}</span>
                                        {{/if}}
                                        {{/if}}
                                </div>                      
                            </div>
                        {{/if}}   
                        {{/if}}
                        {{if methodPickup && methodPickup.dateText }}
                            <div {{if methodPickup.storeDetails && methodPickup.storeDetails.storeName !=undefined}} data-storeId={{:methodPickup.storeDetails.id}} {{/if}}  {{if methodPickup.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail update-shipment" data-skuId="{{:skuId}}" data-cartItemId="{{:cartItemId}}" data-productId="{{:productId}}" data-quantity="{{:productQuantity}}" data-methodCode="{{:methodPickup.methodCode}}" {{/if}}>
                                <div {{if methodPickup.isSelected}} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker"  {{/if}}></div>
                                <div {{if methodPickup.isSelected}} class="module-426-store-detail-wrap" {{else}} class="module-426-store-detail-wrap module-426-store-cursor" {{/if}} >
                                        <div class="store-icon">
                                        {{if methodPickup.isSelected}} 
                                            <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-store@3x.png" alt="" title="">
                                            {{else}}
                                            <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store@3x.png" alt="" title=""></img>
                                            {{/if}}
                                            </div>
                                        <div class="head">{{:methodPickup.dateText}}</div>
                                        {{if methodPickup.storeDetails && methodPickup.storeDetails.storeName !=undefined}}
                                        <span class="store-pickup">{{:~cncLabels.kls_pick_up_Label}} </span><span class="store-pickup-bold">{{:methodPickup.storeDetails.storeName}}</span>
                                        {{/if}}
                                        <div>
                                        {{if methodPickup.storeDetails && methodPickup.storeDetails.storeName !=undefined}}
                                                <span class="store-name">{{:methodPickup.storeDetails.storeName}}, {{:methodPickup.storeDetails.state}}</span>
                                        {{/if}}		
                                        {{if methodPickup.awayTime}}
                                                <span class="store-drive-time"><img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-car-glyph@3x.png" alt="" title="">{{:methodPickup.awayTime}}</span>
                                        {{/if}}
                                        </div>
                                </div>  
                            </div>
                        {{else}}
                        <div class="module-426-store-detail">
                                <div class="module-426-select-store-marker-disable"></div>
                                    <div class="module-426-store-detail-wrap">
                                        <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store-icon.png" alt="" title=""></div>
                                        <div class="store-not-available">{{:~root.cncLabels.kls_moreoption_not_available_to_pickup}}</div>
                                        {{if ~root.cartJsonData.defaultStoreName}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_moreoption_near}}</span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultStoreName}}</span>
                                        {{else}}
                                        {{if ~root.cartJsonData.defaultFavStoreZip}} 
                                        <span class="store-pickup">{{:~root.cncLabels.kls_moreoption_near}}</span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultFavStoreZip}}</span>
                                        {{/if}}
                                        {{/if}}
                                </div>                      
                            </div>
                        {{/if}}
                        {{if methodShip && methodShip.dateText}}
                            <div {{if methodShip.storeDetails && methodShip.storeDetails.storeName !=undefined}} data-storeId="{{:methodShip.storeDetails.id}} "{{/if}}  {{if methodShip.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail update-shipment" data-skuId="{{:skuId}}" data-cartItemId="{{:cartItemId}}" data-productId="{{:productId}}" data-quantity="{{:productQuantity}}" data-methodCode="{{:methodShip.methodCode}}" {{/if}}>
                                <div {{if methodShip.isSelected}} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker" {{/if}}></div>
                                <div {{if methodShip.isSelected}} class="module-426-store-detail-wrap-delivery-home" {{else}} class="module-426-store-detail-wrap-delivery-home module-426-store-cursor" {{/if}}>
                                        <div class="store-icon-today-home">
                                        {{if methodShip.isSelected}}   
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-truck@3x-green.png" alt="" title="">
                                        {{else}}
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/truck@3x.png" alt="" title=""></img>
                                        {{/if}}
                                        </div>
                                        <div class="head">{{:methodShip.dateText}}</div>
                                        {{if methodShip.isSelected}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_estimated_arrival}} </span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultShipPostalCode}}</span>	
                                        {{else}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_estimated_arrival}} </span><span class="store-pickup-bold">{{if ~root.cartJsonData.defaultShipPostalCode}}{{:~root.cartJsonData.defaultShipPostalCode}}{{else}} {{:~root.cartJsonData.defaultFavStoreZip}} {{/if}}</span>	
                                        {{/if}}
                                </div>	
                            </div>
                        {{else}}
                            <div class="module-426-store-detail">
                            <div class="module-426-select-store-marker-disable"></div>
                            <div class="module-426-store-detail-wrap">
                                    <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/delivery-truck@3x.png" alt="" title=""></div>
                                    <div class="ship-not-available">{{:~cncLabels.kls_moreoption_not_available_to_ship}}</div>        
                            </div>
                            </div>
                        {{/if}}
                        {{if expeditedShipData && expeditedShipData.dateText}}
                            <div {{if expeditedShipData.storeDetails && expeditedShipData.storeDetails.storeName !=undefined}} data-storeId={{:expeditedShipData.storeDetails.id}} {{/if}}  {{if expeditedShipData.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail update-shipment" data-skuId="{{:skuId}}" data-cartItemId="{{:cartItemId}}" data-productId="{{:productId}}" data-quantity="{{:productQuantity}}" data-methodCode="{{:methodShip.expeditedShipData}}" {{/if}}>
                                <div {{if expeditedShipData.isSelected}} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker" {{/if}}></div>
                                <div {{if expeditedShipData.isSelected}} class="module-426-store-detail-wrap-delivery-home" {{else}} class="module-426-store-detail-wrap-delivery-home module-426-store-cursor" {{/if}}>
                                        <div class="store-icon-today-home">
                                        {{if expeditedShipData.isSelected}}   
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-truck@3x-green.png" alt="" title="">
                                        {{else}}
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/truck@3x.png" alt="" title=""></img>
                                        {{/if}}
                                        </div>
                                        <div class="head">{{:expeditedShipData.dateText}}</div>
                                        {{if expeditedShipData.isSelected}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_estimated_arrival}} </span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultShipPostalCode}}</span>	
                                        {{else}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_estimated_arrival}} </span><span class="store-pickup-bold">{{if ~root.cartJsonData.defaultShipPostalCode}}{ {:~root.cartJsonData.defaultShipPostalCode}}{{else}} {{:~root.cartJsonData.defaultFavStoreZip}} {{/if}}</span>	
                                        {{/if}}
                                </div>	
                            </div>
                        {{/if}}
                            <div class="module-426-more-options-product-hr"></div> 
                    </div>
                </div>
                {{/for}}
            {{/if}}
                <div class="ship-it-faster-block">
                    <span class="ship-it-faster-head">{{:cncLabels.kls_shipit_faster_label}} </span><span><img class="ship-it-faster-clock" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/clock@3x.png" alt="" title=""></span>
                    <div class="smart-cart-hr"></div>
                    {{if cartJsonData.moreOptionData && cartJsonData.moreOptionData.shipItFaster}}
                    {{if cartJsonData.moreOptionData.shipItFaster.SDD}}
                    <div class="ship-it-faster-product">
                        <div class="ship-it-faster-product-images">
                            {{for cartJsonData.moreOptionData.shipItFaster.SDD.productImages}}
                            <span class="ship-it-faster-img"><img class="ship-in-faster-product-img-style" src="{{_url}}" alt="" title=""></span>
                            {{/for}}
                        </div>
                        <div class="ship-it-faster-left-block">
                            <div class="ship-it-faster-day">{{:cartJsonData.moreOptionData.shipItFaster.SDD.shipMethodDateTxt}}</div>
                            <div class="ship-it-faster-arrival">{{:cncLabels.kls_estimated_arrival}} <strong>{{if cartJsonData.defaultShipPostalCode}}{{:cartJsonData.defaultShipPostalCode}}{{else}}{{:cartJsonData.defaultFavStoreZip}}{{/if}}</strong></div>
                        </div>
                        <div class="ship-it-faster-price"><a  id="{{id:'ship-faster-price'}}" data-ship-method="SDD"  class="gifting-cancel-btn" style="padding:7px 11px" href="javascript:void(0);">{{:$env.cncShippingPricesSDD}}</a></div>
                    </div>
                    {{/if}}
                    {{if cartJsonData.moreOptionData.shipItFaster.ODD}}
                    <div class="ship-it-faster-product">
                        <div class="ship-it-faster-product-images">
                        {{for cartJsonData.moreOptionData.shipItFaster.ODD.productImages}}
                            <span class="ship-it-faster-img"><img class="ship-in-faster-product-img-style" src="{{_url}}" alt="" title=""></span>
                            {{/for}}
                        </div>
                        <div class="ship-it-faster-left-block">
                            <div class="ship-it-faster-day">{{:cartJsonData.moreOptionData.shipItFaster.ODD.shipMethodDateTxt}}</div>
                            <div class="ship-it-faster-arrival">{{:cncLabels.kls_estimated_arrival}} <strong>{{if cartJsonData.defaultShipPostalCode}}{{:cartJsonData.defaultShipPostalCode}}{{else}}{{:cartJsonData.defaultFavStoreZip}}{{/if}}</strong></div>
                        </div>
                        <div class="ship-it-faster-price"><a  id="{{id:'ship-faster-price'}}" data-ship-method="ODD"  class="gifting-cancel-btn" style="padding:7px 11px" href="javascript:void(0);">{{:$env.cncShippingPricesODD}}</a></div>
                    </div>
                    {{/if}}
                    {{if cartJsonData.moreOptionData.shipItFaster.TDD}}
                    <div class="ship-it-faster-product">
                        <div class="ship-it-faster-product-images">
                            {{for cartJsonData.moreOptionData.shipItFaster.TDD.productImages}}
                            <span class="ship-it-faster-img"><img class="ship-in-faster-product-img-style" src="{{_url}}" alt="" title=""></span>
                            {{/for}}
                        </div>
                        <div class="ship-it-faster-left-block">
                            <div class="ship-it-faster-day">{{:cartJsonData.moreOptionData.shipItFaster.TDD.shipMethodDateTxt}}</div>
                            <div class="ship-it-faster-arrival">{{:cncLabels.kls_estimated_arrival}} <strong>{{if cartJsonData.defaultShipPostalCode}}{{:cartJsonData.defaultShipPostalCode}}{{else}}{{:cartJsonData.defaultFavStoreZip}}{{/if}}</strong></div>
                        </div>
                        <div class="ship-it-faster-price"><a  id="{{id:'ship-faster-price'}}" data-ship-method="TDD"  class="gifting-cancel-btn" style="padding:7px 11px" href="javascript:void(0);">{{:$env.cncShippingPricesTDD}}</a></div>
                    </div>
                    {{/if}}
                    {{if cartJsonData.moreOptionData.shipItFaster.SDD == undefined && cartJsonData.moreOptionData.shipItFaster.ODD == undefined && cartJsonData.moreOptionData.shipItFaster.TDD == undefined}}
                        <div class="expedited-shipping-unavailable">{{:cncLabels.expedited_shipping_unavailable}}</div>
                    {{/if}}
                {{/if}}
                
                </div>
        </div>
    </div>
    {{/if}}
</script>
<script id="tpl-checkOtherStoreModule" type="text/x-jsrender">
    <div id="change-store-modal" class="module-426-more-options-block">
            <div class="module-426-change-store-header font-normal-black">
                <button class="samestore-click" id="{{id:'back-moreOptionList-panel'}}">
                    <div class="module-426-more-options-back">
                        <span class="module-426-more-options-header-icon">
                            <img class="module-426-more-options-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title="">
                        </span>
                        <span class="return-text font-gotham-4r font-15"> {{:cncLabels.kls_back_pickup_shipping}} </span>
                    </div>
                </button>
                <div class="module-426-change-store-hr header-bottom-hr"></div>
                <div class="module-426-more-options-header-findstore change-store-block font-gotham-5r font-17">
                    <span>
                        <img class="change-how-icon" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-store-kohls@3x.png" alt="" title="">
                    </span>
                    <span>{{:cncLabels.kls_moreoption_change_store_label}}</span>
                </div>
            </div>
            <div class="partialInventoryErrMsg" style="display:none;">
                        <p class="partialInventoryErrMsgTxt"></p>
            </div>
            <div class="module-426-more-store-locator-pin">
                <div class="locator-zip-input-block">
                <div class="txtbox-form-group-block">
                        <div class="txtbox-form-group check-other-store-input" id="{{id:'check-other-onsubmit'}}">
                            <label for="store-zipcode" class="moreoptions-city-state">{{:cncLabels.kls_moreoptions_city_zipcode_state}}</label>
                            <div class="store-search-bar">                                
                                {{if ~root.$env.enableRadar}}
                                    <div id="store-zipcode" class="store-autocomplete"></div>  
                                {{else}}
                                    <div class="search-store-list"><img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/search@3x.png" alt="" class="zip-search-icon"></div>
                                    <input type="text" id="store-zipcode" class="store-zipcode">
                                {{/if}}
                                <kds-button class="get-store-list" id="{{id:'get-store-list'}}" title="Search" text="Search" size="sm" button-type="secondary" aria-label="Search" tabindex="0">{{:cncLabels.kls_moreoptions_search}}</kds-button>
                            </div>
                            <div class="error-message-contact zipCodeError">{{:cncLabels.kls_moreoptions_store_zipcode_lerror}}</div>
                        </div>
                        
                    </div>
                </div>
            </div>
            {{if storelist  && storelist.length=='0'}}
            <div class="module-426-more-options-name">
                <span class="store-found">{{:cncLabels.kls_moreoptions_store_avilable_location}}</span>
            </div>
            {{/if}}
            <div class="module-426-more-options-product-block  module-426-store-detail-block-right-align">
                <div class="module-426-store-detail-block">
                        <!-- Store Detail Start   -->
                     {{if storelist && storelist.length}} 
                     <div class="module-426-more-options-product-hr module-426-more-options-product-hr-no-margin"></div> 
                        {{for storelist ~cncLabels=cncLabels}}
                          {{if shipNode }}
                        <div class="module-426-store-detail module-426-store-detail-no-margin module-426-store-cursor">
                            <div class="module-426-store-detail-wrap" id="{{id:'apply-other-store'}}" data-id="{{:shipNode}}">
                                    <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/map-pin-white@3x.png" alt="" title=""></div>
                                    <div class="head">{{if organizationName}}{{:organizationName}}{{/if}}</div>
                                    <div><span class="store-name store-name-zip-search">{{if organizationName}}{{:organizationName}}{{/if}}, {{:state}} {{:zipCode}}</span>
                                    {{if #data.awayTime}}
                                        <span class="store-drive-time"><img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-car-glyph@3x.png" alt="" title="">{{:#data.awayTime}}</span>
                                    {{/if}}
                                    </div>
                            </div>
                        </div>
                      <div class="module-426-more-options-product-hr module-426-more-options-product-hr-no-margin"></div>
                        {{/if}}
                     {{/for}}
                     {{/if}}
                </div>
            </div>
        </div>
</script>
<script id="tpl-partialInventoryModule" type="text/x-jsrender">
    {{if cartJsonData.partialInventoryStatus}}
    <div class="partial-inventory-modal" id="partial-inventory-modal">
        <div class="module-426-more-options-block">
            <div class="module-426-more-options-header partial-inventory-header">
                    <button id="{{id:'back-shopping-cart-panel'}}" class="partial-inventory-back" aria-label="Back">
                        <img class="module-426-more-options-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title="">
                        <span class="module-426-more-options-header-partial-inventory">{{:cncLabels.kls_moreoption_back_label}}</span>
                    </button> 
            </div>
            <div class="module-426-partial-inv-hr"></div>
            {{if ~root.cartJsonData.defaultStoreName}}<div class="module-426-more-options-name">{{:cncLabels.kls_moreoption_based_on}} <span class="module-426-more-options-name-green" id="{{id:'check-other-store'}}">{{:~root.cartJsonData.defaultStoreName}}, {{:~root.cartJsonData.storeState}}</span></div>{{/if}}
            {{if cartJsonData.partialInventoryData && cartJsonData.partialInventoryData.itemList && cartJsonData.partialInventoryData.itemList.length}}
            {{for cartJsonData.partialInventoryData.itemList ~cncLabels=cncLabels}}
            <div class="module-426-more-options-product-block">
                    <div class="module-426-more-options-left-block">
                        <div class="module-426-more-options-product-img">
                            <span><a href="{{if productSeoURL}}{{:productSeoURL}}{{else}}javascript:void(0);{{/if}}"><img class="module-426-more-options-product-img-style" src="{{_productImage}}" alt="" title=""></a></span>
                            <span class="module-426-product-count">{{:~cncLabels.Kls_moreoptions_quantity_text}}{{:productQuantity}}</span>                                            
                        </div>
                        {{for contextualMessage}}
                        <div class="module-426-more-options-contextual-msg">
                        {{if isShipIcon}}
                            <img class="module-426-more-options-contextual-msg-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/delivery-truck@3x.png" alt="" title="">
                        {{/if}}
                            {{:message}}
                        </div>
                        {{/for}}
                    </div>
                    <div class="module-426-store-detail-block"> 
                    {{if ~root.cartJsonData.noLongerAvailablePickUp}}
                    {{if methodSearchData && methodSearchData.length >0 }}
                            {{for methodSearchData }}
                            {{if #data.storeName}}
                            <div {{if #data.storeName !=undefined}} data-storeId={{:#data.storeID}} {{/if}}  {{if #data.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail" id="{{id:'update-shipment-data'}}" data-skuId={{:skuId}} data-cartItemId={{:cartItemId}} data-productId={{:productId}} data-quantity={{:productQuantity}} data-methodCode={{:#data.methodCode}} {{/if}}>
                                <div {{if #data.isSelected }} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker"  {{/if}}></div>
                                <div {{if #data.isSelected }} class="module-426-store-detail-wrap" {{else}} class="module-426-store-detail-wrap module-426-store-cursor" {{/if}}>
                                        <div class="store-icon">
                                        {{if #data.isSelected}} 
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-store@3x.png" alt="" title="">
                                        {{else}}
                                            <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store@3x.png" alt="" title="">
                                        {{/if}} 
                                        </div>
                                        <div class="head">{{:#data.dateText}}</div>
                                        {{if #data.storeName  !=undefined}}
                                        <span class="store-pickup">{{:~cncLabels.kls_pick_up_Label}} </span><span class="store-pickup-bold">{{:#data.storeName }}</span>
                                        {{/if}}
                                        <div>
                                        {{if #data.storeName !=undefined}}
                                                <span class="store-name">{{:#data.storeName }}, {{:#data.storeState }}</span>
                                        {{/if}}
                                        {{if #data.awayTime}}		
                                                <span class="store-drive-time"><img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-car-glyph@3x.png" alt="" title="">{{:#data.awayTime}}</span>
                                        {{/if}}        
                                        <span class="store-name">{{:#data.pickupStoreAlertLevel }}</span>
                                        </div>
                                </div>  
                            </div>
                            {{else}}
                        <div class="module-426-store-detail">
                                <div class="module-426-select-store-marker-disable"></div>
                                    <div class="module-426-store-detail-wrap">
                                        <div class="store-not-available no-longer-available"><p>{{:~root.cncLabels.kls_moreoption_no_longer_available_topick_up}}</p></div>
                                </div>                      
                            </div>
                            {{/if}}
                        {{/for}}
                        {{/if}}  
                        {{/if}}  
                        {{if methodPickup && methodPickup.dateText }}
                                <div {{if methodPickup.storeDetails && methodPickup.storeDetails.storeName !=undefined}} data-storeId={{:methodPickup.storeDetails.id}} {{/if}}  {{if methodPickup.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail" id="{{id:'update-shipment-data'}}" data-skuId={{:skuId}} data-cartItemId={{:cartItemId}} data-productId={{:productId}} data-quantity={{:productQuantity}} data-methodCode={{:methodPickup.methodCode}} {{/if}}>
                                    <div {{if methodPickup.isSelected}} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker"  {{/if}}></div>
                                    <div {{if methodPickup.isSelected}} class="module-426-store-detail-wrap" {{else}} class="module-426-store-detail-wrap module-426-store-cursor" {{/if}} >    
                                            <div class="store-icon">
                                            {{if methodPickup.isSelected}} 
                                            <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-store@3x.png" alt="" title="">
                                            {{else}}
                                            <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store@3x.png" alt="" title=""></img>
                                            {{/if}}
                                            </div>
                                            <div class="head">{{:methodPickup.dateText}}</div>
                                            {{if methodPickup.storeDetails && methodPickup.storeDetails.storeName !=undefined}}
                                            <span class="store-pickup">{{:~cncLabels.kls_pick_up_Label}} </span><span class="store-pickup-bold">{{:methodPickup.storeDetails.storeName}}</span>
                                            {{/if}}
                                            <div>
                                            {{if methodPickup.storeDetails && methodPickup.storeDetails.storeName !=undefined}}
                                                    <span class="store-name">{{:methodPickup.storeDetails.storeName}}, {{:methodPickup.storeDetails.state}}</span>
                                            {{/if}}		
                                            {{if methodPickup.awayTime}}
                                                    <span class="store-drive-time"><img class="store-drive-time-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-car-glyph@3x.png" alt="" title="">{{:methodPickup.awayTime}}</span>
                                            {{/if}}        
                                            </div>
                                    </div>  
                                </div>
                            {{else}}
                            <div class="module-426-store-detail">
                                    <div class="module-426-select-store-marker-disable"></div>
                                        <div class="module-426-store-detail-wrap">
                                            <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/store-icon.png" alt="" title=""></div>
                                            <div class="store-not-available">{{:~root.cncLabels.kls_moreoption_not_available_to_pickup}}</div>
                                            {{if ~root.cartJsonData.defaultStoreName}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_moreoption_near}}</span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultStoreName}}</span>
                                        {{else}}
                                        {{if ~root.cartJsonData.defaultShipPostalCode}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_moreoption_near}}</span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultShipPostalCode}}</span>
                                        {{/if}}
                                        {{/if}}
                                    </div>                      
                                </div>
                            {{/if}}
                        {{if methodShip && methodShip.dateText}}
                            <div {{if methodShip.storeDetails && methodShip.storeDetails.storeName !=undefined}} data-storeId={{:methodShip.storeDetails.id}} {{/if}}  {{if methodShip.isSelected}} class="module-426-store-detail module-426-store-detail-active" {{else}} class="module-426-store-detail" id="{{id:'update-shipment-data'}}" data-skuId={{:skuId}} data-cartItemId={{:cartItemId}} data-productId={{:productId}} data-quantity={{:productQuantity}} data-requestedquantity={{:requestedQuantity}} data-methodCode={{:methodShip.methodCode}} {{/if}}>
                                <div {{if methodShip.isSelected}} class="module-426-select-store-marker-active" {{else}} class="module-426-select-store-marker" {{/if}}></div>
                                <div {{if methodShip.isSelected}} class="module-426-store-detail-wrap-delivery-home" {{else}} class="module-426-store-detail-wrap-delivery-home module-426-store-cursor" {{/if}}>
                                        <div class="store-icon-today-home">
                                        {{if methodShip.isSelected}}   
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/ic-truck@3x-green.png" alt="" title="">
                                        {{else}}
                                        <img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/truck@3x.png" alt="" title=""></img>
                                        {{/if}}
                                            </div>
                                        <div class="head">{{:methodShip.dateText}}</div>
                                        {{if methodShip.isSelected}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_estimated_arrival}} </span><span class="store-pickup-bold">{{:~root.cartJsonData.defaultShipPostalCode}}</span>	
                                        {{else}}
                                        <span class="store-pickup">{{:~root.cncLabels.kls_estimated_arrival}} </span><span class="store-pickup-bold">{{if ~root.cartJsonData.defaultFavStoreZip}}{{:~root.cartJsonData.defaultFavStoreZip}}{{else}} {{:~root.cartJsonData.defaultShipPostalCode}} {{/if}}</span>	
                                        {{/if}}
                                </div>	
                            </div>
                        {{else}}
                            <div class="module-426-store-detail">
                            <div class="module-426-select-store-marker-disable"></div>
                            <div class="module-426-store-detail-wrap">
                                    <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/delivery-truck@3x.png" alt="" title=""></div>
                                    <div class="ship-not-available">{{:~cncLabels.kls_moreoption_not_available_to_ship}}</div>        
                            </div>
                            </div>
                        {{/if}}
                            <div class="module-426-more-options-product-hr"></div> 
                    </div>
                </div>
                {{/for}}
            {{/if}}    
        </div>
    </div>
    {{/if}}
</script>
<script id="tpl-checkOtherPartialStoreModule" type="text/x-jsrender">
    <div id="check-other-store-modal" class="module-426-more-options-block">
            <div class="partialInventoryErrMsg" style="display:none;">
                    <p class="partialInventoryErrMsgTxt"></p>
            </div>
            <div class="module-426-more-options-header partial-inventory-header">
                <button id="{{id:'back-partial-inventory-panel'}}" class="partial-inventory-back" aria-label="Back">
                    <span class="module-426-more-options-header-icon"><img class="module-426-more-options-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title=""></span>
                    <span class="module-426-more-options-header-partial-inventory">{{:cncLabels.kls_moreoption_back_label}}</span>
                </button>
            </div>
            <div class="module-426-more-options-hr"></div>
            <div class="module-426-more-store-locator-pin">
                <div class="locator-zip-input-block">
                <div class="txtbox-form-group-block">
                        <div class="txtbox-form-group check-other-store-input" id="{{id:'check-other-onsubmit'}}">
                        <label for="store-zipcode" class="moreoptions-city-state">{{:cncLabels.kls_moreoptions_city_zipcode_state}}</label>
                        <div class="store-search-bar">
                            <div class="search-store-list"><img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/search@3x.png" alt="" class="zip-search-icon"/></div>
                            <input type="text" id="store-zipcode" class="store-zipcode">
                            <kds-button class="get-store-list" id="{{id:'get-store-list'}}" title="Search" text="Search" size="sm" button-type="secondary" aria-label="Search" tabindex="0">{{:cncLabels.kls_moreoptions_search}}</kds-button>
                        </div>
                            <div class="error-message-contact zipCodeError">{{:cncLabels.kls_moreoptions_store_zipcode_lerror}}</div>
                        </div>
                        
                    </div>
                </div>
            </div>
            {{if storelist  && storelist.length=='0'}}
            <div class="module-426-more-options-name">
                <span class="store-found">{{:cncLabels.kls_moreoptions_store_avilable_location}}</span>
            </div>
            {{/if}}
            <div class="module-426-more-options-product-block  module-426-store-detail-block-right-align">
                <div class="module-426-store-detail-block">
                    <div class="module-426-more-options-product-hr module-426-more-options-product-hr-no-margin"></div> 
                        <!-- Store Detail Start   -->
                     {{if storelist && storelist.length}} 
                        {{for storelist ~cncLabels=cncLabels}}
                          {{if shipNode }}
                        <div class="module-426-store-detail module-426-store-detail-no-margin module-426-store-cursor">
                            <div class="module-426-store-detail-wrap" id="{{id:'apply-get-other-data'}}"  data-storeName="{{:organizationName}}" data-storeState="{{:state}}"  data-zipcode="{{:zipCode}}" data-id="{{:shipNode}}">
                                    <div class="store-icon"><img class="store-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/map-pin-white@3x.png" alt="" title=""></div>
                                    <div class="head">{{if organizationName}}{{:organizationName}}{{/if}}</div>
                                    <div><span class="store-name store-name-zip-search">{{if organizationName}}{{:organizationName}}{{/if}}, {{:state}} {{:zipCode}}</span>
                                    </div>
                            </div>
                        </div>
                   <div class="module-426-more-options-product-hr module-426-more-options-product-hr-no-margin"></div>
                        {{/if}}
                     {{/for}}
                     {{/if}}
                </div>
            </div>
        </div>
</script>
<script id="tpl-saveForLaterTemplate" type="text/x-jsrender">
    <div class="saveForLaterDiv">
		<div class="saveForLaterPanelErrMsg">
			<p class="saveForLaterPanelErrMsgTxt"></p>
		</div>
		{{if packageTplDataArray && packageTplDataArray.length}}
			<div class="saveForLaterItemCountDiv">
			<p class="saveForLaterItemCount">{{: cncLabels.kls_save_for_later_text}} ({{:totalListCount}})</p>
			</div>
		{{/if}}
	<!-- new cart item code started -->
		{{for packageTplDataArray ~cncLabels=cncLabels}}
			<div class="cart-item-panel saveForLater-cart-item-panel" id="{{id:'cartItems'}}">
					{{for #data.items tmpl = "tpl-cartBlockSaveitLater" /}}
				<div class="cart-item-panel-hr"></div>
			</div>
		{{/for}}
	</div>
	<!-- new cart item code ended -->
{{if showMoreItem}}
<div id="{{id:'saveForLaterMoreItemLink'}}" class="saveForLaterMoreItemLink-attr"><p class="saveForLaterMoreItemText">{{: cncLabels.kls_more_items_text}}</p></div>
{{/if}}
</div>
</script>
<script id="tpl-cartBlockSaveitLater" type="text/x-jsrender">
	<div class="cart-item-panel-item">
		<div class="product-details-block">
			<div class="product-left-block">
				<div class="product-left-block__top">
					<a href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}"> <img class="product-img-style" src="{{__data.imageUrl}}" alt="{{:#data.productTitle}}"/></a>
					{{if #data.giftInfo && #data.giftInfo.isGiftSelected}}
						<img class="product-gift-icon" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/giftOptionsImages/gift-box@3x.png"></img>
					{{/if}}	
				</div>				
				<div class="product-footer-block-price-list"> 
					<p class="product-footer-block-price-list-sale-txt {{:#data.priceColorClass}}-color">{{: #data.priceLabel}}</p>
					<div>      
						<span class="product-footer-block-price-list-strikethrough">{{: #data.StrikethroughPrice}}</span>
						<span class="product-footer-block-price-list-sale-price {{:#data.priceColorClass}}-color">{{: #data.priceValue}}</span>
					</div>
				</div>
			</div>
			<div class="product-right-block">
				{{if #data.itemType && #data.itemType.startsWith('SEPHORA')}}
					<div class="sephora-logo">
						<img src="{{v_~root.$imageRoot('cnc')}}kds-web-core/assets/sephora/tags/small-k.svg" alt="Logo Sephora" />
					</div>
				{{else #data.itemType == 'MARKETPLACE'}}
					<kds-badge text="marketplace" badge-type="marketplace" class="kds-marketplace-badge"></kds-badge>
				{{/if}}
			
				<div class="product-name">
					<a {{if #data.itemType == 'VGC'}}"vgc_shoppingbag_title"{{/if}} href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}">{{:#data.productTitle}}</a>
				</div>
				<div class="product-context-msg">
					{{for #data.contextualMessage}}
						<div class="module-426-more-options-contextual-msg">
							{{if #data.isShipIcon}}
								<img class="module-426-more-options-contextual-msg-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/delivery-truck@3x.png" alt="" title="">
							{{/if}}
							{{: #data.message}}
						</div>
					{{/for}}
				</div>
			</div>
			<div class="product-props">
				{{if #data.size && #data.size !== 'NO SIZE'}}
					<div class="product-size-name">{{:~root.cncLabels.kls_product_size_text}} <span class="product-size-value">{{: #data.size}}</span></div>
				{{/if}}
				{{if #data.color}}
				<div class="product-color-name">{{:~root.cncLabels.kls_product_color_text}} <span class="product-color-value">{{: #data.color}}</span></div>
				{{/if}}
			</div>
		</div>

		<div class="product-footer-block" id="{{id:'saveForLaterMoveToCart'}}">
			<div class="product-footer-gutter"></div>
			{{if #data.outOfStock==false}}
			<button class="saveForLaterMovetoCart"  data-quantityId="{{: #data.quantity != 0  ? #data.quantity : ''}}" data-skuId="{{:#data.skuId}}"  data-prdId="{{:#data.productId}}" data-shippingMethod="{{:#data.fulfillmentType}}" data-storeName="{{:#data.storeId}}" data-itemType="{{:#data.itemType}}" data-cartItemId="{{:#data.cartItemId}}">{{: ~root.cncLabels.kls_move_to_cart_text}}</button>
			{{/if}}
			<button class="product-delete-icon deleteSaveForLater" aria-label="delete item from save for later" id="{{id:'deleteSaveForLater'}}"  data-quantityId="{{: #data.quantity != 0  ? #data.quantity : ''}}" data-cartItemId="{{:#data.cartItemId}}" data-skuId="{{:#data.skuId}}"  data-prdId="{{:#data.productId}}" data-itemType="{{:#data.itemType}}" data-shippingMethod="{{:#data.fulfillmentType}}" data-cartItemId="{{:#data.cartItemId}}" ><img class="out-of-stock-header-icon-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/trash-icon.png" alt="" title=""></button>
		</div>
	</div>
</script><script id="tpl-bigDataRecommendationPanelTemplate" type="text/x-jsrender">
    {{if recoSystemParameters.isRecoEnabled === "true"}}
		  <div id="bd_rec_{{:recoSystemParameters.recoBDPlacementName}}" data-template-type="{{:recoSystemParameters.recoBDTemplateName}}" class="big-data-base"></div>		
	  {{/if}}
</script><script id="tpl-biBirthdayGiftModule" type="text/x-jsrender">
    {{if showBdayPanel}}
    <div class="biBirthdayPanel"> 
            <div class="bi-birthday-block" id="{{id:'birthday-gift-panel'}}">
                <span class="bi-birthday-gift-icon"><img class="bi-birthday-block-img1" src="{{v_~root.$resourceRoot('cnc')}}kds-web-core/assets/sephora/icons/main/birthday_gift.svg" alt="BI Gift Icon" title=""></span>
                <span class="bi-birthday-block-text">
                        <p class="bi-birthday-block-top">{{:bday_msg}}</p>
                        <p class="bi-birthday-middle">{{:cncLabels.bi_redeem_gift_label}}</p>
                        <p class="bi-birthday-bottom">{{:cncLabels.bi_free_with_purchase_label}}</p>
                </span>
                <span class="bi-birthday-right" ><a href="javascript:void(0);" ><img class="bi-birthday-block-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-right.png" alt="" title=""></a></span>
            </div>
        </div>
    {{/if}}
</script>


<script id="tpl-biBirthdayGiftPanel" type="text/x-jsrender">
    {{if biGifts.show}}
    <div class="bi-birthday-gift-block">
        <div class="birthday-gift-header">
        
            <span class="birthday-gift-header-icon">
                <a href="javascript:void(0);" id="{{id:'back-bi-birthday-block'}}" >
                <img class="birthday-gift-header-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/moreOptionsImages/left@3x.png" alt="" title=""> </a>
            </span>
            <span class="bi-redeem-text">Redeem your Sephora gift</span>
        </div>
        <div class="bi-gift-item-hr"></div>
        {{for giftItems}}
            {{if #data.inStock}}   
                <div class="bi-gift-item" id="{{id: 'birthday-gift-item'}}"
                     data-productId="{{:#data.productId}}"
                     data-skuId="{{:#data.skuId}}"
                >
                    <div class="bi-gift-item-block">
                        <div class="bi-gift-left"></div>
                        <img class="bi-gift-img" src={{__data.imageUrl}} alt="{{:#data.displayName}}"/>
                        <p class="bi-gift-title">{{:#data.displayName}}</p>
                    </div>    
                    <div class="bi-gift-error">{{:~root.cncLabels.kls_bi_add_to_cart_error}}</div>
                </div>
            {{/if}}
        {{/for}}
    </div>
    {{/if}}

</script>
<script id="tpl-openOffersPanel-template" type="text/x-jsrender">
        <div class="open-offers-container new-popup-design-selector" id="openOffersContainerDiv">
            <button class="open-offers-btn" id="{{id:'offersPanelToggle'}}" aria-label="Apply Coupons & Kohl's Cash">
                <div class="open-offers-block">
                    <span class="open-offers-img-container">
                        <img class="open-offers-icon-img" src="./../../../../../../../www.kohls.com/cnc/media/images/dgsImages/ic-percent-dotwack@3x.png"></img>
                        <img class="open-offers-icon-img" src="./../../../../../../../www.kohls.com/cnc/media/images/dgsImages/ic-dollarsign-circle@3x.png"></img>
                    </span>
                    <span class="open-offers-text">{{:cncLabels.apply_coupons_and_kc}}</span>
                    {{if isCollapsed}}
                        <img class= "open-offers-chevron" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-right.png" alt="" title=""></img>
                    {{else}}
                        <img class="open-offers-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" title=""></img>
                        <img class= "open-offers-chevron-up" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-up.png" alt="" title=""></img>
                    {{/if}}
                </div>
            </button>
            {{if !isCollapsed}}
                {{for tmpl='tpl-openOffersPanelExpand' ~env = $env /}}
            {{/if}}
        </div>
</script> 

<script id="tpl-openOffersPanelExpand" type="text/x-jsrender">
    <div class="open-offers-block-expand">
        <div class="open-offers-expand-header">
            <div class="savingPanel">
                <span class="savingText">{{:cncLabels.ur_saving_text}} </span>
                <span class="savingAmount">{{:savings.currency}}</span>
            </div>
        </div> 
        <div class="open-offers-list">
        {{if isGuestOrSoftLoggedIn}}
            <div class="sign-in-kohls-cash">
                <div class="signInPanel" id="{{id: 'signInPanel'}}">
                    <a href="javascript:void(0);">
                        <div class="sign-in-block-container">
                            <span class="sign-in-block">
                                    <span class="user-profile-span"><img class="userProfile" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-user-profile@3x.png" /></span>
                                    <span class="sign-in-text-inline">{{:cncLabels.sign_in_to_see_coupons}}</span>
                            </span>
                            <kds-button title="Sign In" class="sign-in-inline-panel-btn" aria-label="Sign in button" size="sm" tabindex="0">{{:cncLabels.sign_in_coupons_btn}}</kds-button>
                        </div>
                    </a>
                </div>
            </div>
        {{/if}}
            <!-- Container for coupons, KC, and manual entry panel + pop-up messages -->
            <div class="inline-offers-container">
                <div class="offers-block" id="offersBlock">
                {{if isGuestUser}}
                    <div class="offers-sign-in-divider"></div>
                {{/if}}
                    <div id="{{id:'inlineOfferContainer'}}" class="inlineOfferContainer"></div>
                </div>
            </div>
        </div>
        <div class="return-to-cart-btn-container">
            <kds-button title="Return to Cart" button-type="secondary" class="return-to-cart-btn" id="{{id:'returnToCartBtn'}}" aria-label="Return to cart button" size="md" tabindex="0">{{:cncLabels.return_to_cart_coupons_panel}}</kds-button>
        </div>
    </div>
</script><script id="tpl-inlinePanelOffer" type="text/x-jsrender">
<!-- Data is coming from the open offers panel -->
{{if #data.state == "show" || #data.state == "enter"}}
    {{for tmpl="tpl-kohlsCashInlinePanelOffer" ~data = #data /}}
    {{for tmpl="tpl-enterkohlsCashInline"/}}
{{else #data.state =="loading"}}
    {{for tmpl="tpl-inlinePanelLoading"/}}
{{/if}}
</script>

<script id="tpl-inlinePanelLoading" type="text/x-jsrender">
    <div class="kohls-cash-panel">
         <div>Loading...</div>
    </div>
</script>

<script id="tpl-inlineFailurePanel" type="text/x-jsrender">
    <div class="kohls-cash-msg message-block">  
        <div class="manual-entry-block">
            <div class="manual-entry-failure-img"><img class="manual-entry-failure-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-alert-triangle.png" /></div>
                <div class="manual-entry-failure-msg" role="status" aria-live="polite">{{:message}}
            </div>
        </div>
        <!--<div class="close-kc-msg closeDiv" id="{{id:'closeApplyKc'}}"><img class="close-kc-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/cross-circle@3x.png" /></div>-->
    </div>
</script>

<script id="tpl-kohlsCashSuccessInlinePanel" type="text/x-jsrender">
    <div class="kohls-cash-msg message-block">   
        <div class="manual-entry-block">
            <div class="manual-entry-success-img"><img class="manual-entry-success-img-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" /></div>
            <div class="manual-entry-success-msg" role="status" aria-live="polite">{{:cncLabels.wallet_success_message_head}}</div>
        </div>
        <!--<div class="close-kc-msg closeDiv" id="{{id:'closeApplyKc'}}"><img class="close-kc-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/cross-circle@3x.png" /></div>-->
    </div>
</script>
 
<script id="tpl-enterkohlsCashInline" type="text/x-jsrender">
    <div class="kohls-cash-panel validateInline" id="actionPanelForm">
        {{if #data.applyState =="failure"}}
            {{for tmpl="tpl-inlineFailurePanel" /}}
        {{else #data.applyState =="success"}}
            {{for tmpl="tpl-kohlsCashSuccessInlinePanel"/}}
        {{/if}}
        <div class="kohls-cash-panel-header">{{:cncLabels.have_any_kohls_cash_or_coupons}}</div>
        <div class="apply-kc-coupon-container">
            <div class="input-container">
                <div class="promo-code-panel">
                    <div class="kohls-cash-promo-code-error-message"></div>
                    <div class="promo-code-txt">{{:cncLabels.enter_code}}</div>
                    <input type="text" name="promoCode" aria-label="Add Kohl's Cash or Coupons" placeholder="" class="kcCodeTxt required action-panel-kc-number" id="{{id:'kcCodeTxt'}}" maxlength="15"></input>
                    <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon-offer-panel promo-alert-img"></img>
                    <div class="error-message-contact offers-panel-error-margin promo-code-error"></div>
                    <div class="panel-hr"></div>
                </div>
                <div class="pin-panel">
                    <div class="pin-txt">{{:cncLabels.enter_pin}}</div>
                    <input type="text" name="pin" aria-label="Enter Kohl's Cash PIN" maxlength="4" placeholder="" class="input-text number-input required action-panel-pin kcPinTxt" id="{{id:'kcPinTxt'}}"></input>
                    <img src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/paymentPanelImages/alert-circle@3x.png" alt="Kohls" class="alert-circle-icon-offer-panel pin-alert-img"></img>
                    <div class="error-message-contact offers-panel-error-margin"></div>
                    <div class="panel-hr"></div>
                </div>
            </div>
            <div class="apply-btn-container">
                <kds-button title="Add" class="apply-btn" id="{{id:'applyBtn'}}" aria-label="Add Kohl's Cash or Coupon button" size="sm" button-type="secondary" tabindex="0">{{:cncLabels.kls_addCouponForm_label_add}}</kds-button>
            </div>
        </div>
    </div>
</script>

<script id="tpl-inlineOfferDetails" type="text/x-jsrender">
    <div>
        <div class="legal-popup" tabindex="0">
            <button id="closeCouponInfoModal" class="close-msg" aria-label="Close"><img class="coupon-info-cross" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-cross-bold.png" alt="" /></button>
            <div class="legal-popup-header">{{if offerDetails.offerUsage == 'SUPC'}}{{:offerDetails.code}}{{else}}{{:offerDetails.name}}{{/if}}<img class="store-icon-img" {{if offerDetails.offerImage}} src={{_offerDetails.offerImage}} {{else}} style="display:none" {{/if}} alt="" title="" /></div>
            <div class="legal-popup-discount-eligible">{{:offerDetails.description}}</div>                
            <div class="legal-popup-discount-expire">{{:offerDetails.additionalInfo}}</div>   
            <div class="legal-popup-content">                                      
                {{if offerDetails.code}}     
                    <p class="legal-popup-promo">{{:cncLabels.kls_with_promo_code_label}} <span class="legal-popup-promo-bold">{{:offerDetails.code}}</span></p>             
                {{/if}}                    
                <p>{{:offerDetails.disclaimer}}</p>        
            </div>
        </div>
    
</script>

<script id="tpl-inlineOfferError" type="text/x-jsrender">
        <div class="legal-popup">
            <img class="closeDiv" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/cross-circle@3x.png" />
            <div class="legal-popup-content">                                                                             
                <p>{{:offerError}}</p>        
            </div>
        </div>
</script>

<script id="tpl-kohlsCashInlinePanelOffer" type="text/x-jsrender">
<div class="kohls-cash-offers scrollbar-width-thin" id="{{id:'offerIdInline'}}">
        <div id="message" style="display:none"></div>
        {{if kcItems.length > 1 && maxCoupons && maxCoupons.cartEligibleForKC}}
            <div class="offer-panel-stacked" >
                <div class={{if multipleKcData.KcUsed}} "offer-panel offer-panel-stack-applied" {{else}} "offer-panel offer-panel-stack-unapplied" {{/if}}>
                <button class="apply-offer-container" id="{{id:'KCOfferIdStacked'}}">
                    <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
                    <span class="offer-text-block-hide-drawer">
                        <p class="offer-head-hide-drawer">{{:multipleKcData.amountMessage}}</p>
                        <p class="offer-expire-hide-drawer">{{:multipleKcData.expireMessage}}</p>
                    </span>
                {{if multipleKcData.KcUsed}}
                    <span class="offer-applied-flag-stacked-hide-drawer">
                    <img class="applied-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" />
                    </span>
                    <span class="offer-applied-amount-stacked-hide-drawer">-${{:multipleKcData.totalAmount.toFixed(2)}}</span>
                {{/if}}
                </button> 
                <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
            </div>              
                <div class="cash-stacked-1-bg"></div>
                <button class="cash-stacked-2-bg" id="{{id:'offer-panel'}}" aria-label="Expand stacked Kohl's Cash"></button>
            </div> 
        {{else kcItems.length > 1}}
            <div class="offer-panel-stacked">
                <div class="offer-panel">
                {{if state !="APPLIED" &&  !maxCoupons.cartEligibleForKC}}
                    <span class="side-bar-disable">
                        <img class="side-bar-disable-dollar-img" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/store@3x.png" />
                    </span>
                {{/if}}
                    <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
                    <span class="offer-text-block-hide-drawer" id="{{id:'KCOfferIdStacked'}}">
                        <p class="offer-head-grey-hide-drawer">{{:multipleKcData.amountMessage}}</p>
                        <p class="offer-expire-grey-hide-drawer">{{:multipleKcData.expireMessage}}</p>
                    </span>
                    <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
                </div>    
                    <div class="cash-stacked-1-bg"></div>
                    <div class="cash-stacked-2-bg" id="{{id:'offer-panel'}}"></div>
                </div> 
        {{/if}}
        {{for tmpl="tpl-multiplekohlsCashInlinePanel" ~data =#parent.data /}}
        {{if offerItems && offerItems.length > 0}}
            {{for offerItems}}  
                {{if state !='IRRELEVANT' || ~root.$env.ksActionPanelAllOffer}}  
                    {{if !systemInitiated && (state =='APPLIED' || isBestOffer || ~root.$env.ksActionPanelBestOffer) }}  
                        <div class= {{if state =="APPLIED"}} "offer-panel offer-panel-applied" {{else}} "offer-panel" {{/if}} data-name={{:name}} data-id = {{:code}} >
                        <button class="apply-offer-container" id="{{id:'panelOfferId'}}">
                        <span class="offer-icon-hide-drawer" id="{{:name}}"><img class="offer-icon-dollar-img-hide-drawer" src="{{_offerImage}}" /></span>
                                {{if state=="LOCKED"}}
                                    <span class="offer-text-block-hide-drawer">                                   
                                        <p class="offer-head-grey-hide-drawer">{{:description}}</p>
                                        <p class="offer-expire-grey-hide-drawer">{{:additionalInfo}}</p>
                                    </span>
                                {{else state =='IRRELEVANT'}}    
                                    <span class="offer-text-block-hide-drawer">                                   
                                        <p class="offer-head-grey-hide-drawer">{{:description}}</p>
                                        <p class="offer-expire-grey-hide-drawer">{{:additionalInfo}}</p>
                                    </span>
                                {{else}}
                                    {{if state=="APPLIED"}}
                                        <span class="offer-applied-flag-hide-drawer">
                                            <img class="applied-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" />
                                        </span>
                                        {{if amount > 0}}
                                            <span class="offer-applied-amount-hide-drawer">-${{:amount.toFixed(2)}}</span>
                                        {{/if}}
                                    {{else state =="QUALIFIED"}}
                                        <span class="apply-coupon-cta">{{:~root.cncLabels.apply_coupon_cta}}</span>
                                        <span class="apply-coupon-cta-mobile">{{:~root.cncLabels.tap_to_apply}}</span>   
                                    {{/if}}
                                    <span class="offer-text-block-hide-drawer">    
                                        <p class="offer-head-hide-drawer">{{:description}}</p>                               
                                        <p class="offer-expire-hide-drawer">{{:additionalInfo}}</p>
                                    </span>
                                {{/if}}
                            </button>
                            <button class="offer-info-hide-drawer" aria-label="Coupon info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
                        </div>
                    {{/if}}
                {{/if}}  
            {{/for}} 
        {{/if}}    
    </div>
</script>

<script id="tpl-multiplekohlsCashInlinePanel" type="text/x-jsrender">
    {{if kcItems.length>1}}
        <div class="kohls-cash-stack-up" style="display:none"><img class="kohls-cash-stack-up-img" id="{{id:'offer-panel'}}" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-chevron-up-circle@3x.png" /></div>
    {{/if}}
    {{for kcItems ~env =$env ~kcLength = kcItems.length}}
    {{if ~kcLength}} 
        {{if ~kcLength > 1}}
            <!-- +++++++++++Hide single kohls cash item initially when item is more than one +++++ -->
            <div class={{if state =="APPLIED"}} "offer-panel offer-panel-green-bar offer-panel-applied" {{else}} "offer-panel offer-panel-green-bar" {{/if}} KC-id = {{:itemKey}} style="display:none">
        {{else}}
            <!-- +++++++++++++++for single kohls cash item +++++++++ -->
            <div class={{if state =="APPLIED"}} "offer-panel offer-panel-green-bar offer-panel-applied" {{else}} "offer-panel offer-panel-green-bar" {{/if}} KC-id = {{:itemKey}}>        
        {{/if}} 
    {{/if}}
    {{if state !="APPLIED" && (~data && ~data.maxCoupons && !~data.maxCoupons.cartEligibleForKC)}}    
        <span class="offer-text-block-hide-drawer" id="{{id:'KCOfferId'}}">
            <p class="offer-head-grey-hide-drawer">{{:message}}</p>
            <p class="offer-expire-grey-hide-drawer">{{:expDate}}</p>
        </span>
        <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
        <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
    {{else}}
        <button class="apply-offer-container" id="{{id:'KCOfferId'}}">
            {{if ~data && ~data.maxCoupons}}
                <span class="offer-icon-hide-drawer"><img class="offer-icon-cash-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/kohls-cash.png" /></span>
            {{/if}}
            {{if state=="APPLIED"}}
                <span class="side-bar-applied"></span>
                <span class="offer-applied-flag-hide-drawer">
                    <img class="applied-checkmark" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-checkmark-black@3x.png" />
                </span>
                <span class="offer-applied-amount-hide-drawer">-${{:amount.toFixed(2)}}</span>
            {{else state=="QUALIFIED"}}
                <span class="apply-coupon-cta">{{:~root.cncLabels.apply_coupon_cta}}</span>
                <span class="apply-coupon-cta-mobile">{{:~root.cncLabels.tap_to_apply}}</span>  
            {{else}}
                <span class="side-bar"></span>
            {{/if}} 
            {{if ~kcLength > 0}}
                <span class="offer-text-block-hide-drawer">
                        <p class="offer-head-hide-drawer">{{:message}}</p>
                        <p class="offer-expire-hide-drawer">{{:expDate}}</p>
                </span>
            {{/if}}
        </button>
        {{if ~data && ~data.maxCoupons}}
            <button class="offer-info-hide-drawer" aria-label="Kohl's Cash info"><img class="offer-info-img-hide-drawer" src="{{v_~root.$resourceRoot('cnc')}}images/dgsImages/ic-info-circle@3x.png" /></button>
        {{/if}} 
    {{/if}}
        </div>
    {{/for}}
</script>
<script id="tpl-messageSlotPanel-template" type="text/x-jsrender">
    {{if $env.pageName === "cart" && cncLabels.kls_static_global_cart_text != ""}}
        <div class="message-slot-block">
        <div class="message-slot-text">
        {{:cncLabels.kls_static_global_cart_text}}
        </div>
        </div>
    {{/if}}
    {{if $env.pageName === "newCheckout" && cncLabels.kls_static_global_checkout_text != ""}}
        <div class="message-slot-block">
        <div class="message-slot-text">
        {{:cncLabels.kls_static_global_checkout_text}}
        </div>
        </div>
    {{/if}}
</script><script type="text/x-jsrender" id="tpl-holidayAssortmentList">
    <div class="holiday-assortment-block">
        <div class="holiday-assortment-title">{{:cncLabels.holiday_assortment_title}}</div>
        <div class="holiday-assortment-items">
            {{for holidayAssortmentList ~env =$env}}
                <div class="holiday-item">
                    <div class="holiday-item-container">
                        <div class="holiday-card-front">                            
                            <img src="{{__data.imageUrl}}" class="holiday-item-image" alt="{{:#data.displayName}}"/>
                            
                            <div class="holiday-price-story">
                                <div class="sale-price-container">   
                                    {{if #data.priceInfo.salePriceStatus}}                                 
                                        <div class="sale-container">
                                            <span class="sale-price-label">{{:#data.priceInfo.salePriceStatus}}</span>&nbsp;<span class="sale-price">{{:#data.priceInfo.salePrice}}</span>
                                        </div>
                                        <div class="regular-strikethrough-container">                                        
                                            <span class="regular-price">{{:#data.priceInfo.regularPrice}}</span>
                                        </div>
                                    {{else}}
                                        <div class="regular-container">                                        
                                        <span class="regular-price-label">{{:#data.priceInfo.regularPriceType}}</span>&nbsp;<span class="regular-price">{{:#data.priceInfo.regularPrice}}</span>
                                        </div>
                                    {{/if}}
                                </div>
                            </div>

                            <div class="holiday-item-title">
                                <span class="holiday-item-title-text">{{:#data.displayName}}</span>
                            </div>
                            <kds-button title="Add" class="add-holiday-item-cart" id="{{id:'addHolidayItemToCart'}}" aria-label="Add item to cart" size="sm" button-type="secondary" tabindex="0" data-productId="{{:#data.productId}}" data-skuId="{{:#data.skuId}}">
                                <img src="{{v_~root.$env.KDSAssetsPath}}/icons/accordion/plus-icon.svg" alt="" />
                                &nbsp;&nbsp;{{:~root.cncLabels.kls_holidaylist_label_add}}
                            </kds-button>                            
                        </div>
                        <div class="kds holiday-card-back">
                            <img src="{{v_~root.$env.KDSAssetsPath}}/icons/stepper/checkmark-circle-icon.svg" class="holiday-item-checkmark" alt="Checkmark icon" />
                            <div class="holiday-item-added-msg supporting-micro-01">{{:~root.cncLabels.item_added_to_cart}}</div>
                        </div>
                    </div>
                </div>
            {{/for}}
        </div>
    </div>
</script>

