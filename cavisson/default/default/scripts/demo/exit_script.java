/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 11/21/2023 07:10:29
    Flow details:
    Build details: 4.11.0 (build# 71)
    Modification History:
-----------------------------------------------------------------------------*/

package demo;
import pacJnvmApi.NSApi;
import pacJnvmApi.NsFlow;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
